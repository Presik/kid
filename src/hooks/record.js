import { useQuery } from "@tanstack/react-query";
import proxy from "api/proxy-old";

export function useRecord(model, record_id, fields) {
  let enabled = false;
  const record_ids = [record_id];
  if (model && record_id && record_id > 0) {
    enabled = true;
  }
  return useQuery(["record", model, record_ids, fields], proxy.browseQuery, {
    enabled: enabled,
    refetchOnWindowFocus: false,
    // refetchInterval: 120000,
  });
}
