import { useMutation } from "@tanstack/react-query";

export default function useMutateForm() {
  return useMutation({
    mutationFn: ({ proxyFn, args }) => {
      return proxyFn(args);
    },
    onSuccess: function (data) {
      console.log("Run on Success !!!", data);
    },
    onError: (errorMsg) => {
      console.log("Ohhh you have a problem !!!", errorMsg);
      return { msg: errorMsg };
    },
  });
}
