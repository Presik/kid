import React from "react";
import date from "date-and-time";

import { classNames } from "tools/ui";
import helper from "./helper";

const fmtDate = "YYYY-MM-DD";

function CalendarHeader({ dates }) {
  // Render table head of calendar
  let cellsHeader = dates.map((rawDate, idx) => {
    const today = date.format(new Date(), fmtDate);
    const fdate = date.format(rawDate, fmtDate);
    const isToday = today === fdate ? "-today" : "";
    let style = "";
    let bgColor = "";
    if (isToday) {
      bgColor = "bg-sky-900 text-white";
    }
    return (
      <th className={classNames(style, bgColor)} key={idx}>
        <div className="text-sm">{rawDate.getDate()}</div>
        <div className="text-sm">{helper.getShortMonthName(rawDate)}</div>
      </th>
    );
  });

  return (
    <tr className="h-12">
      <th
        key="rooms"
        className="!bg-zinc-800 text-white !font-bold text-xl min-w-[8rem]"
      >
        ROOMS
      </th>
      {cellsHeader}
    </tr>
  );
}

export default CalendarHeader;
