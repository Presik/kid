import React from "react";
import { FormattedMessage as FM } from "react-intl";

import { classNames } from "tools/ui";
import { fillColor, textColor } from "./constants";

const base =
  "mr-4 h-9 inline-flex items-center rounded-xl px-2.5 py-0.5 text-md ";

function GroupTags() {
  const values = Object.entries(fillColor);
  return (
    <div className="my-auto flex">
      {values.map(([name, color]) => {
        const _textColor = textColor[name];
        return (
          <span key={name} className={classNames(base, color, _textColor)}>
            <FM id={`room.${name}`} />
          </span>
        );
      })}
    </div>
  );
}

export default GroupTags;
