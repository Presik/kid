import React from "react";

function CalendarHeaderRow({ title, data, attr }) {
  return (
    <tr className="text-zinc-500 h-8 bg-sky-100">
      <th className="!bg-zinc-700 text-white">{title}</th>
      {data.map((val, idx) => {
        return (
          <th className={"text-sm"} key={idx}>
            {val[attr]}%
          </th>
        );
      })}
    </tr>
  );
}

export default CalendarHeaderRow;
