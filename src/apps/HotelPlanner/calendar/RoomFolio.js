import React, { useContext } from "react";
import { useDrop } from "react-dnd";

import { ItemTypes } from "./constants";
import CalendarContext from "./CalendarContext";

function RoomFolio(props) {
  // load default context
  const context = useContext(CalendarContext);
  const { day, room, cellWidth, children } = props;

  // enable drop
  const [{ isOver }, drop] = useDrop({
    accept: ItemTypes.BOOKING,
    drop: (singleBookingDraggableItem) => {
      context.actionMoveBooking(
        singleBookingDraggableItem.singleBooking,
        room.id,
        day,
      );
    },

    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
  });

  const clickHandler = () => {
    context.actionOpenPopup({
      room_id: room.id,
      from_date: day,
      to_date: day,
    });
  };

  return (
    <td
      ref={drop}
      key={day.getTime()}
      style={{ width: cellWidth + "px" }}
      onClick={clickHandler}
    >
      {children}
    </td>
  );
}

export default RoomFolio;
