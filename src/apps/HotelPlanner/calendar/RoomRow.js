import React from "react";
import date from "date-and-time";

import Booking from "./Booking";
import RoomFolio from "./RoomFolio";
import helper from "./helper";

const fmtDate = "YYYY-MM-DD";

function RoomRow(props) {
  const { dates, bookings, room, cellWidth, initialDate } = props;
  let daysTd = dates.map((day, index) => {
    const currentDay = day.toDateString();
    const bookingsActive = bookings.filter((booking) => {
      let from_date;
      let from_date_map;
      if (initialDate > booking.from_date) {
        from_date = date.parse(initialDate, fmtDate);
        from_date_map = initialDate;
      } else {
        from_date = new Date(booking.from_date);
        from_date = date.addDays(from_date, 1);
        from_date_map = booking.from_date;
      }

      // calculate number of days for booking
      booking.duration = helper.durationOfDays(booking.to_date, from_date_map);
      return from_date.toDateString() === currentDay ? true : false;
    });

    // get all booking jsx code for current day
    const bookingsTarget = bookingsActive.map((operation) => {
      return <Booking operation={operation} key={operation.id} />;
    });

    return (
      <RoomFolio key={index} day={day} room={room} cellWidth={cellWidth}>
        {bookingsTarget}
      </RoomFolio>
    );
  });

  return (
    <tr key={room.id} className="">
      <td>
        <span className="pl-4 text-base text-slate-600 font-bold">
          {room.title}
        </span>
      </td>
      {daysTd}
    </tr>
  );
}

export default RoomRow;
