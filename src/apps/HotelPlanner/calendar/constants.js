import despegarImg from "assets/apps/despegar.png";
import bookingImg from "assets/apps/booking.png";
import expediaImg from "assets/apps/expedia.png";
import houseImg from "assets/apps/house.png";

export const channelsImg = {
  booking: bookingImg,
  despegar: despegarImg,
  expedia: expediaImg,
  house: houseImg,
};

export const ItemTypes = {
  BOOKING: "booking",
};

export const fillColor = {
  none: "bg-zinc-300",
  check_in: "bg-lime-300",
  check_out: "bg-sky-300",
  pending: "bg-amber-200",
  maintenance: "bg-rose-200",
};

export const textColor = {
  none: "text-zinc-900",
  check_in: "text-lime-800",
  check_out: "text-sky-700",
  pending: "text-amber-600",
  maintenance: "text-rose-800",
};

export const borderColor = {
  none: "border-zinc-600",
  check_in: "border-lime-500",
  check_out: "border-sky-400",
  pending: "border-amber-300",
  maintenance: "border-rose-500",
};
