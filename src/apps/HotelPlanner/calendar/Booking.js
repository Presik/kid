import React, { useContext, useState } from "react";
// import { useDrag } from "react-dnd";

import Popup from "./Popup";
import PopupHeader from "./PopupHeader";
import PopupContent from "./PopupContent";
import CalendarContext from "./CalendarContext";
import { fillColor, borderColor, textColor, channelsImg } from "./constants";
import { classNames } from "tools/ui";

const base = "flex rounded-full leading-8 border-[1px] pl-1 my-1 mx-1 border";

function Folio(props) {
  // load default context
  const { operation } = props;
  const context = useContext(CalendarContext);
  const [openPopup, setOpenPopup] = useState(false);

  // enable dragging of component
  // const [{ isDragging }, dragRef] = useDrag({
  //   item: { singleBooking: operation },
  //   type: ItemTypes.BOOKING,
  //   collect: (monitor) => ({
  //     isDragging: !!monitor.isDragging(),
  //   }),
  // });

  const getContent = (duration) => {
    // get inner content of the booking
    let label;
    if (operation.kind === "accommodation") {
      // Fixme why this values are ""
      if (operation.guest_name && operation.guest_name !== "") {
        label = operation.guest_name;
      } else if (operation.party !== "" && operation.party.name) {
        label = operation.party.name;
      } else {
        label = operation.contact;
      }
      if (label && label !== "") {
        if (duration > 1) {
          label = label.slice(0, 6 * duration);
        } else {
          label = label.slice(0, 2);
        }
      }
    } else {
      label = operation.room;
    }
    const style = "pl-2 text-md my-auto h-full !font-mono";
    const colorText = textColor[operation.status];
    return (
      <p className={classNames(style, colorText)} key={operation.id}>
        {label}
      </p>
    );
  };

  const handleMouse = (value) => {
    setOpenPopup(value);
  };

  const onClick = (event, operation) => {
    context.actionOpenPopup(operation.id);
    event.stopPropagation();
    event.preventDefault();
  };

  if (operation.duration > 0) {
    if (operation.channel_code === "") {
      operation.channel_code = "house";
    }
    let duration = operation.duration * 70 - 2;
    const _color = fillColor[operation.status];
    const _border = borderColor[operation.status];
    return (
      <div
        onClick={(e) => onClick(e, operation)}
        className={classNames(base, _color, _border)}
        style={{ width: duration + "px" }}
        onMouseEnter={() => handleMouse(true)}
        onMouseLeave={() => handleMouse(false)}
      >
        <div className="rounded-full w-7 h-7 my-1">
          <img src={channelsImg[operation.channel_code]} />
        </div>
        {getContent(operation.duration)}
        <Popup
          open={openPopup}
          header={<PopupHeader record={operation} />}
          handleMouseOut={handleMouse}
        >
          <PopupContent operation={operation} />
        </Popup>
      </div>
    );
  } else {
    return null;
  }
}

export default Folio;
