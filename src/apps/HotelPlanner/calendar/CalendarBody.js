import React from "react";
import date from "date-and-time";

import RoomRow from "./RoomRow";

const fmtDate = "YYYY-MM-DD";

function CalendarBody({ rooms, rack, dates }) {
  const initialDate = date.format(dates[0], fmtDate);
  let body = rooms.map((room) => {
    let bookingsByRoom = rack.get(room.id);
    if (!bookingsByRoom) {
      bookingsByRoom = [];
    }
    return (
      <RoomRow
        key={room.id}
        room={room}
        bookings={bookingsByRoom}
        dates={dates}
        cellWith={50}
        initialDate={initialDate}
      />
    );
  });

  return <tbody>{body}</tbody>;
}

export default CalendarBody;
