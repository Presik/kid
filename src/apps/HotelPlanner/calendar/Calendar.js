import React, { useState } from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import ScrollContainer from "react-indiana-drag-scroll";
import date from "date-and-time";

import { useMethod } from "hooks/records";
import CalendarContext from "./CalendarContext";
import CalendarHeader from "./CalendarHeader";
import CalendarHeaderRow from "./CalendarHeaderRow";
import CalendarBody from "./CalendarBody";
import GroupTags from "./GroupTags";
// import ModalStdForm from "components/ModalStdForm";
import ModalForm from "components/Modals/ModalForm";
import DateField from "components/DateField";
import ModelFolio from "apps/HotelPlanner/ModelFolio";
import helper from "./helper";
import proxy from "api/proxy";
import funcs from "tools/functions";
import { useFormStore } from "store/formStore";
import "./style.scss";

const fmtDate = "YYYY-MM-DD";

function Calendar(props) {
  const { rooms } = props;
  let [selectedDate, setSelectedDate] = useState(new Date());
  const { activeRecord, upActiveRecord, setActiveRecordFromId } =
    useFormStore();
  const ctxView = ModelFolio.ctxView();

  const dates = fillUpDates();
  let [openFolio, setOpenFolio] = useState(false);

  let selectedDateFmt = date.format(selectedDate, fmtDate);
  const ctxArgs = {
    model: "hotel.folio",
    method: "get_folios",
    args: [{ start_date: selectedDateFmt }],
  };

  let { data, error, isLoading } = useMethod(ctxArgs);
  console.log("data...", data);
  let rack = new Map();
  if (data) {
    data.rooms.map((room) => {
      rack.set(room["id"], []);
    });
    data.folios.map((folio) => {
      if (!folio["room."]) {
        return;
      }
      const room_id = folio["room."].id;
      let roomFo = rack.get(room_id);
      roomFo.push(helper.convertFolio(folio));
    });
    data.mnts.map((mnt) => {
      const room_id = mnt["room."].id;
      let roomMnt = rack.get(room_id);
      roomMnt.push(helper.convertOpMant(mnt));
    });
  }

  function onChangeDate(field, value) {
    let value_ = date.parse(value, fmtDate);
    setSelectedDate(value_);
  }

  function fillUpDates() {
    // Fill up dates in component state
    let dates = [];
    const day = date.addDays(selectedDate, -4);
    for (let x = 0; x < 34; x++) {
      dates.push(date.addDays(day, x));
    }
    return dates;
  }

  function actionMoveBooking(singleBooking, room, newStartDate, newEndDate) {
    let allBookings = helper.moveBooking(
      singleBooking,
      room,
      newStartDate,
      newEndDate,
    );
    if (allBookings === false) {
      console.log("Unable to move to target date");
    }
  }

  function actionCreateBooking(singleBooking) {
    if (
      helper.canExistBooking(
        singleBooking,
        singleBooking.room_id,
        singleBooking.from_date,
        singleBooking.to_date,
      )
    ) {
      console.log("setBookingsActive...fake");
    } else {
      console.log("Cannot create booking");
    }
  }

  function actionCanExistBooking(
    singleBooking,
    room,
    newStartDate,
    newEndDate,
  ) {
    return helper.canExistBooking(
      singleBooking,
      room,
      newStartDate,
      newEndDate,
    );
  }

  async function actionOpenPopup(folioId) {
    // const domain = [["id", "=", folioId]];
    console.log("hola....");
    const model = "hotel.folio";
    // const fields_names = funcs.getViewFields(ctxView, "form");
    // const { data, error } = await proxy.search(model, domain, fields_names, 1);
    // upActiveRecord(data[0]);
    // activeRecord
    setActiveRecordFromId(folioId, model, ctxView);
    setOpenFolio(true);
  }

  function actionClosePopup() {
    setOpenFolio(false);
  }

  // create context, to make data available to other child components
  const contextValue = {
    data: {}, // ????
    actionMoveBooking: actionMoveBooking,
    actionCanExistBooking: actionCanExistBooking,
    actionOpenPopup: actionOpenPopup,
    actionClosePopup: actionClosePopup,
    actionCreateBooking: actionCreateBooking,
  };

  const fieldOptions = {
    name: "target_date",
    onChange: onChangeDate,
  };

  const field = {
    name: "planner_date",
    labeled: false,
    onChange: onChangeDate,
  };

  return (
    <CalendarContext.Provider value={contextValue}>
      <div className="flex h-20 space-x-12">
        <p className="my-auto text-4xl font-bold ml-6 text-sky-800">PLANNER</p>
        <div className="my-auto">
          <DateField value={selectedDate} {...field} />
        </div>
        <GroupTags />
      </div>

      <div className="r-calendar">
        <DndProvider backend={HTML5Backend}>
          <ScrollContainer className="scroll-container" ignoreElements="td">
            <table
              id="hotel-table-calendar"
              className="mb-12 border-solid border border-gray-400 r-calendar-main-table"
            >
              <thead>
                {data && (
                  <CalendarHeaderRow
                    title={"OCC. RATE"}
                    attr={"occ_rate"}
                    data={Object.values(data.occ_rate)}
                  />
                )}
                <CalendarHeader dates={dates} />
              </thead>
              <CalendarBody rack={rack} dates={dates} rooms={rooms} />
            </table>
          </ScrollContainer>
        </DndProvider>
        {openFolio && (
          <ModalForm
            open={openFolio}
            ctxView={ctxView}
            level="main"
            onClose={() => setOpenFolio(false)}
          />
        )}
      </div>
    </CalendarContext.Provider>
  );
}

export default Calendar;
