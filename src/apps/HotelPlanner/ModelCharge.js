// Folio Charge model

const getView = () => {
  let DictCtxView = {
    model: "hotel.folio.charge",
    form_action: [], // options: ['save', 'delete']
    table_action: [], // options: ['open', 'delete', 'edit', 'add']
    domain: [], // Options: null or valid domain
    orderBy: [["date_service", "ASC"]],
    limit: 100,
    selectable: null, // Options for table rows: null, multi, one
    webfields: {
      date_service: { type: "date", readOnly: true },
      quantity: {
        type: "integer",
        readOnly: true,
      },
      amount: { type: "number", readOnly: true },
      status: { type: "char", readOnly: true },
      product: {
        type: "many2one",
        model: "product.product",
        readOnly: true,
      },
      unit_price_w_tax: {
        type: "number",
        readOnly: true,
      },
      // cleaning_type: {
      //   type: "many2one",
      //   model: "hotel.room.cleaning_type",
      //   readOnly: true,
      // },
      // last_check_in: {
      //   type: "datetime",
      //   readOnly: true,
      // },
      // last_check_out: {
      //   type: "datetime",
      //   readOnly: true,
      // },
      // clean: {
      //   type: "button",
      //   button_method: "clean",
      //   visible: ["dirty"],
      //   onSuccessMsg: "AHORA LA HABITACION ESTA LIMPIA!",
      //   color: "blue",
      // },
      // not_authorized: {
      //   type: "button",
      //   button_method: "not_authorized",
      //   visible: ["dirty"],
      //   onSuccessMsg: "SE MARCO QUE LA HAB. NO AUTORIZO LIMPIEZA!",
      //   color: "green",
      // },
    },
    webtree: [
      { name: "date_service", width: "10%" },
      { name: "product", width: "25%" },
      { name: "quantity", width: "20%" },
      { name: "unit_price_w_tax", width: "20%" },
      { name: "amount", width: "25%" },
      { name: "status", width: "25%" },
    ],
    webform: [
      { name: "product" },
      { name: "date_service" },
      { name: "quantity" },
      { name: "unit_price_w_tax" },
      { name: "amount" },
      { name: "status" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
