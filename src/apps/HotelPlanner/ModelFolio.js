// Folio model
import tools from "tools/common";
import modelGuest from "./ModelGuest";
import modelCharge from "./ModelCharge";

let today = tools.fmtDate2Tryton(new Date());

const searchProduct = () => {
  return `[
      ('template.type', '=', 'service'),
      ('template.kind', '=', 'accommodation'),
    ]`;
};

// const getAddress = (record) => {
//   if (record && record.party) {
//     const party_id = record.party.id;
//     return `[('party', '=', ${party_id})]`;
//   }
// };

// const onChangeLines = (activeRecord, fieldValue) => {
//   if (activeRecord.lines) {
//     let total_amount = 0;
//     for (let l of fieldValue.values()) {
//       total_amount += l.amount;
//     }
//     activeRecord["total_amount"] = total_amount;
//   }
//   return activeRecord;
// };

const getName = (record) => {
  return record.booking;
};

const getFilters = (session) => {
  return {
    today: `[
      ('sale_date', '=', '${today}'),
      ('create_uid', '=', ${session.user}),
    ]`,
  };
};

const getView = () => {
  let DictCtxView = {
    model: "hotel.folio",
    row_selectable: false,
    form_action: ["edit"], // options: ['edit', 'delete']
    table_action: [], // options: ['open', 'delete', 'edit', 'add']
    activeSearch: true,
    form_rec_name: getName,
    filters: getFilters,
    title: { field: "booking", component: "title" },
    webfields: {
      registration_card: { type: "char", readOnly: true },
      main_guest: {
        type: "many2one",
        model: "party.party",
        readOnly: true,
      },
      booking: {
        type: "many2one",
        model: "hotel.booking",
        readOnly: true,
      },
      arrival_date: {
        type: "date",
        required: true,
        readOnly: {
          registration_state: ["check_in", "check_out"],
        },
      },
      departure_date: {
        type: "date",
        readOnly: {
          registration_state: ["check_out"],
        },
        required: true,
      },
      unit_price: {
        type: "number",
        required: true,
        readOnly: {
          registration_state: ["check_out", "check_in"],
        },
      },
      pending_total: { type: "number", readOnly: true },
      nights_quantity: {
        type: "number",
        readOnly: true,
      },
      room: {
        type: "many2one",
        model: "hotel.room",
        required: true,
        readOnly: {
          registration_state: ["check_out", "check_in"],
        },
      },
      product: {
        type: "many2one",
        model: "product.product",
        recSearch: searchProduct,
        readOnly: true,
      },
      channel: {
        type: "many2one",
        model: "hotel.channel",
        readOnly: true,
        images: { targetField: "code", source: {} },
      },
      registration_state: {
        type: "char",
        readOnly: true,
        translate: true,
      },
      plan: {
        type: "char",
        readOnly: true,
        translate: true,
      },
      unit_price_w_tax: {
        type: "number",
        readOnly: true,
      },
      guests: {
        type: "one2many",
        model: "hotel.folio.guest",
        ctxView: modelGuest.ctxView(),
        readOnly: true,
      },
      charges: {
        type: "one2many",
        model: "hotel.folio.charge",
        ctxView: modelCharge.ctxView(),
        readOnly: true,
      },
      total_amount: { type: "number", readOnly: true },
      notes: {
        type: "text-area",
        readOnly: false,
      },
      vehicle_plate: {
        type: "char",
        readOnly: ["check_out"],
      },
      group: {
        type: "boolean",
        readOnly: {
          registration_state: ["check_out", "check_in"],
        },
        translate: true,
      },
      payment_status: {
        type: "char",
        readOnly: true,
        translate: true,
      },
      check_in: {
        type: "button",
        button_method: "check_in",
        visible: { registration_state: ["pending"] },
        onSuccessMsg: "Check In exitoso!",
        color: "blue",
      },
      check_out: {
        type: "button",
        button_method: "check_out",
        visible: [{ registration_state: ["check_in"] }],
        onSuccessMsg: "Check Out exitoso!",
        color: "blue",
      },
    },
    webtree: [
      { name: "registration_card", width: "15%" },
      { name: "room", width: "15%" },
      { name: "main_guest", width: "15%" },
      { name: "product", width: "15%" },
      { name: "arrival_date", width: "10%" },
      { name: "departure_date", width: "10%" },
      { name: "nights_quantity", width: "10%" },
      { name: "registration_state", width: "10%" },
    ],
    webform: [
      { name: "product" },
      {
        id: "booking",
        grid: [{ name: "booking" }, { name: "registration_card" }],
        size: [1, 2],
      },

      { name: "room" },
      { name: "main_guest" },
      {
        id: "dates",
        grid: [
          { name: "arrival_date" },
          { name: "departure_date" },
          { name: "nights_quantity" },
        ],
        size: [1, 3],
        // border: "visible",
        span: "md:col-span-2",
      },
      {
        id: "amounts",
        grid: [
          { name: "unit_price" },
          { name: "unit_price_w_tax" },
          { name: "total_amount" },
        ],
        span: "md:col-span-2",
        size: [1, 3],
      },
      {
        id: "sales",
        grid: [{ name: "channel" }, { name: "plan" }, { name: "group" }],
        size: [1, 3],
        span: "md:col-span-2",
      },
      { name: "pending_total" },
      { name: "registration_state" },
      { name: "payment_status" },
      { name: "notes" },
      {
        name: "more_info",
        widget: "group",
        children: [
          {
            name: "guests",
            widget: "button_modal",
            color: "sky",
            icon: "fi fi-rr-users",
          },
          {
            name: "charges",
            widget: "button_modal",
            color: "amber",
            icon: "fi fi-rr-file-invoice",
          },
        ],
      },
      { name: "check_out" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
