// Guest model

const getView = () => {
  let DictCtxView = {
    model: "hotel.folio.guest",
    form_action: [], // options: ['save', 'delete']
    table_action: [], // options: ['open', 'delete', 'edit', 'add']
    domain: [], // Options: null or valid domain
    orderBy: [["name", "ASC"]],
    limit: 10,
    selectable: null, // Options for table rows: null, multi, one
    webfields: {
      name: { type: "char", readOnly: true },
      type_guest: { type: "char", readOnly: true },
      mobile: { type: "char", readOnly: true },
      main_guest: {
        type: "boolean",
        readOnly: true,
        translate: true,
      },
      // classification: {
      //   type: "many2one",
      //   model: "hotel.room.classification",
      //   readOnly: true,
      // },
      // cleaning_type: {
      //   type: "many2one",
      //   model: "hotel.room.cleaning_type",
      //   readOnly: true,
      // },
      // last_check_in: {
      //   type: "datetime",
      //   readOnly: true,
      // },
      // last_check_out: {
      //   type: "datetime",
      //   readOnly: true,
      // },
      // last_clean: {
      //   type: "datetime",
      //   readOnly: true,
      // },
      // clean: {
      //   type: "button",
      //   button_method: "clean",
      //   visible: ["dirty"],
      //   onSuccessMsg: "AHORA LA HABITACION ESTA LIMPIA!",
      //   color: "blue",
      // },
      // not_authorized: {
      //   type: "button",
      //   button_method: "not_authorized",
      //   visible: ["dirty"],
      //   onSuccessMsg: "SE MARCO QUE LA HAB. NO AUTORIZO LIMPIEZA!",
      //   color: "green",
      // },
    },
    webtree: [
      { name: "name", width: "25%" },
      { name: "type_guest", width: "20%" },
      { name: "mobile", width: "25%" },
      // { name: "main_guest", width: "10%" },
    ],
    webform: [
      { name: "name" },
      { name: "main_guest" },
      { name: "type_guest" },
      { name: "mobile" },
      // { name: "last_check_out" },
      // { name: "last_clean" },
      // { name: "cleaning_type" },
      // { name: "notes" },
      // { name: "state" },
      // { name: "clean" },
      // { name: "not_authorized" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
