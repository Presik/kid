import React, { useState, useEffect } from "react";

import HotelCalendar from "./calendar/Calendar";
import proxy from "api/proxy";

function Planner() {
  const [rooms, setRooms] = useState([]);

  async function getRooms() {
    const fields_names = ["name", "classification.name"];
    const order = [["name", "ASC"]];
    const { data } = await proxy.search(
      "hotel.room",
      "[]",
      fields_names,
      500,
      order,
    );
    const rooms_ = data.map((room) => {
      return {
        id: room.id,
        room: room.name,
        classification: room["classification."]["name"],
        title: room.name,
        status: "clean",
        roomCount: 1,
      };
    });
    setRooms(rooms_);
  }

  useEffect(() => {
    getRooms();
  }, []);

  return (
    <div className="w-full">
      {rooms.length > 0 && <HotelCalendar rooms={rooms} />}
    </div>
  );
}

export default Planner;
