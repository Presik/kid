// FIX Add required fields

function searchParty(data, record, parentRec) {
  let targetIds;
  if (parentRec) {
    targetIds = [];
    if (parentRec.party) {
      targetIds.push(parentRec.party.id);
    }
    for (const folio of parentRec.lines) {
      if (folio.main_guest) {
        targetIds.push(folio.main_guest.id);
      }
    }
  }
  const partiesDom = targetIds ? [["id", "in", targetIds]] : [];
  return partiesDom;
}

const OPTIONS = [
  { id: "all", name: "all" },
  { id: "only_accommodation", name: "only_accommodation" },
  { id: "only_products", name: "only_products" },
];

const getView = () => {
  let DictCtxView = {
    model: "hotel.booking.bill",
    row_selectable: false,
    form_action: [],
    table_action: [],
    webfields: {
      party: {
        type: "many2one",
        model: "party.party",
        required: true,
        recSearch: searchParty,
      },
      kind: {
        type: "selection",
        required: true,
        options: OPTIONS,
        translate: true,
      },
      // voucher: {
      //   type: "char",
      //   readOnly: false,
      //   // required: true,
      // },
      bill: {
        type: "button",
        // method: addPayment, This method is added in custom model
        color: "green",
        iconRight: "fi fi-rr-add",
        visible: true,
      },
    },
    webform: [{ name: "party" }, { name: "kind" }, { name: "bill" }],
    webtree: [],
  };

  return DictCtxView;
};

export default { ctxView: getView };
