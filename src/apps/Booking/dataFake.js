const dataListCustomers = {
  data: [
    {
      id: 1,
      name: "Victor alfonso",
      lastName: "Ruiz Reyes",
      image:
        "https://avatar.canva.com/avatars/users/06aeaa13-75b8-473c-88d6-399c25e4f8cc/50.jpg",
      room: [
        {
          name: "102",
          dateBooking: "12 diciembre 92",
        },
        {
          name: "103",
          dateBooking: "12 diciembre 92",
        },
        {
          name: "104",
          dateBooking: "12 diciembre 92",
        },
      ],
      idChanel: "despegar",
    },
    {
      id: 2,
      name: "Carlos Andres",
      lastName: "Rodriguez",
      image: undefined,
      room: [
        {
          name: "102",
          dateBooking: "12 diciembre 92",
        },
        {
          name: "103",
          dateBooking: "12 diciembre 92",
        },
      ],
      idChanel: "booking",
    },
    {
      id: 3,
      name: "Daniela ",
      lastName: "Rangel",
      image: undefined,
      room: [
        {
          name: "102",
          dateBooking: "12 diciembre 92",
        },
        {
          name: "103",
          dateBooking: "12 diciembre 92",
        },
        {
          name: "104",
          dateBooking: "12 diciembre 92",
        },
      ],
      idChanel: "house",
    },
    {
      id: 4,
      name: "Hipolito",
      lastName: "Patiño",
      image: undefined,
      room: [],
      idChanel: "expedia",
    },

    {
      id: 5,
      name: "Oscar",
      lastName: "Lopez",
      image: undefined,
      room: [],
      idChanel: "booking",
    },
  ],

  status: [
    {
      name: "active",
      id: 2,
    },
    {
      name: "cancele",
      id: 3,
    },
  ],
};

const dataChartChanels = {
  dataChart: [1, 2, 3, 4, 5, 6, 7],
};

export { dataListCustomers, dataChartChanels };
