import React, { useState } from "react";

import FormWizard from "components/Form/FormWizard";
import ModelWizardBill from "./ModelWizardBill";
import { useWizardStore } from "store/wizardStore";
import { useFormStore } from "store/formStore";
import proxy from "api/proxy";

const WizardBill = ({ record, onClose }) => {
  const { storeWizard, resetWizard } = useWizardStore();
  const [error, setError] = useState(null);
  const { activeRecord } = useFormStore();

  async function doInvoice() {
    let foliosIds = [];
    for (const folioId of activeRecord.lines.keys()) {
      foliosIds.push(folioId);
    }

    const args = [
      activeRecord.id,
      foliosIds,
      storeWizard.party.id,
      storeWizard.kind.id,
    ];
    const model = "hotel.booking";
    const method = "create_invoice";
    const { data, error } = await proxy.methodCall({ model, method, args });
    onClose();
    resetWizard();
  }

  let ctxView = ModelWizardBill.ctxView();
  return (
    <FormWizard
      key="hotel-booking-wizard-bill"
      ctxView={ctxView}
      parentRec={activeRecord}
      handleButton={doInvoice}
    />
  );
};

export default WizardBill;
