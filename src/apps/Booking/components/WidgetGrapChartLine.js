import React from "react";
import { Line } from "react-chartjs-2";
// import { BUTTON_COLORS } from "components/Constants/constants";

const WidgetGrapChartsLine = (props) => {
  const { title, num, size = "w-full" } = props;

  // Datos de ejemplo para las ventas de la semana
  const data = {
    options: {
      scales: {
        r: {
          ticks: {
            backdropPadding: {
              x: 10,
              y: 20,
            },
          },
        },
      },
    },
    labels: [
      "Lunes",
      "Martes",
      "Miércoles",
      "Jueves",
      "Viernes",
      "Sábado",
      "Domingo",
    ],
    datasets: [
      {
        label: "Ventas",
        data: [12, 8, 15, 10, 20, 14, 18], // Ventas diarias
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "#1a1f3d",
        borderWidth: 1,
      },
    ],
  };

  const options = {
    plugins: {
      legend: {
        display: false, // Desactivar la visualización de la leyenda (labels)
      },
    },
  };

  return (
    <div
      className={`bg-white border-gray-100 shadow-sm p-3 flex overflow-hidden space-x-1 rounded-lg ${size}`}
    >
      <div className="flex flex-col space-y-2">
        <h3 className="truncate text-xl font-bold text-sky-800">{title}</h3>
        <hr />
        <div style={{ height: "10vw", width: "17vw", maxWidth: "100%;" }}>
          <Line data={data} options={options} /> {/* Gráfico de línea */}
        </div>
        <span className="text-xl text-bluePresik font-medium">{num}</span>
      </div>
    </div>
  );
};

export default WidgetGrapChartsLine;
