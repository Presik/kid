import React from "react";
// import { IconButton } from "../../../components/Constants/constants";

const WidgetCheckInSide = ({ rooms, day }) => {
  let checkIn = rooms.filter((room) => {
    return (
      room["arrival_date"] == day && room["registration_state"] == "check_in"
    );
  });
  let checkOut = rooms.filter((room) => {
    return (
      room["arrival_date"] == day && room["registration_state"] === "check_out"
    );
  });
  let pending = rooms.filter((room) => {
    return (
      room["arrival_date"] == day && room["registration_state"] === "pending"
    );
  });

  const dataCheckIn = {
    checkIn: {
      num: checkIn.length,
      name: "Check In",
      color: "text-lime-700",
      bgColor: "bg-lime-200",
    },
    checkOut: {
      num: checkOut.length,
      name: "Check Out",
      color: "text-bluePresik",
      bgColor: "bg-yellow-300",
    },
    cancelled: {
      num: pending.length,
      name: "Pending",
      color: "text-rose-700",
      bgColor: "bg-rose-200",
    },
  };

  const getItemStatus = (data) => {
    return (
      <div
        className={`
          group shadow-sm rounded-md px-2 cursor-pointer   py-3 flex flex-col justify-center  items-center -space-y-2 
           text-white w-full  ${data.bgColor}`}
      >
        <div className="flex flex-col ">
          <span
            className={`text-[32px] font-semibold text-center ${data.color}`}
          >
            {data.num}
          </span>
          <span
            className={`text-[14px] font-semibold uppercase  ${data.color}`}
          >
            {data.name}
          </span>
        </div>
      </div>
    );
  };

  return (
    <div className="bg-white border-gray-100 shadow-sm p-3 rounded-lg overflow-hidden">
      {/* <h3 class="truncate text-xl font-bold text-sky-800 mb-2">{title}</h3> */}
      <div className="grid grid-cols-3 justify-between gap-3">
        {getItemStatus(dataCheckIn.checkIn)}
        {getItemStatus(dataCheckIn.checkOut)}
        {getItemStatus(dataCheckIn.cancelled)}
      </div>
    </div>
  );
};

export default WidgetCheckInSide;
