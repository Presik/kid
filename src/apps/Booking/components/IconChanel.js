import React from "react";
import { channelsImg } from "apps/HotelPlanner/calendar/constants";

const IconChanel = ({ data }) => {
  let image;
  if (data === 1) {
    image = channelsImg.booking;
  } else if (data === 2) {
    image = channelsImg.despegar;
  } else if (data === 3) {
    image = channelsImg.expedia;
  } else {
    image = channelsImg.house;
  }

  return <img src={image} className="w-6 h-6 rounded-full cursor-pointer" />;
};

export default IconChanel;
