import React from "react";
import date from "date-and-time";
import { ArrowDownIcon } from "@heroicons/react/20/solid";
import tools from "tools/common";
import { FormattedMessage as FM } from "react-intl";

// import { IconButton } from "./Constants/constants";

const WidgetSideStatusHotel = ({ rooms, day }) => {
  let nextDay = new Date(day);
  let nextDayFormatted = date.format(nextDay, "YYYY-MM-DD");
  nextDay.setDate(nextDay.getDate() + 2);

  let valueToday = 0;
  let valueTomorrow = 0;

  if (rooms.length !== 0) {
    const roomsToday = rooms.filter((room) => {
      return room["arrival_date"] === day;
    });
    const roomsTomorrow = rooms.filter((room) => {
      return room["arrival_date"] === nextDayFormatted;
    });

    valueToday = roomsToday.reduce((accumulator, room) => {
      return accumulator + room.unit_price_w_tax;
    }, 0);

    valueTomorrow = roomsTomorrow.reduce((accumulator, room) => {
      return accumulator + room.unit_price_w_tax;
    }, 0);
    valueToday = tools.formatNumber(valueToday);
    valueTomorrow = tools.formatNumber(valueTomorrow);
    // console.log("mañana", roomsTomorrow);
  }

  return (
    <div className=" space-y-3 rounded-lg">
      <div className="flex flex-col space-y-2 ">
        <div className="space-x-3 flex justify-around text-center">
          <span className="text-1xl font-semibold">
            <FM id="booking.widget_side_status.revenue_today" />
          </span>{" "}
          <span className="text-1xl font-semibold text-gray-300 flex">
            <FM id="booking.widget_side_status.revenue_tomorrow" />
          </span>
        </div>
      </div>
      <div className="flex flex-col space-y-2">
        <div className="space-x-3 flex justify-around pb-2">
          <span className="text-2xl font-semibold">${valueToday}</span>{" "}
          <span className="text-2xl font-semibold text-gray-300 flex">
            ${valueTomorrow} <ArrowDownIcon className="w-4" />{" "}
          </span>
        </div>
        <hr className="mt-5" />
      </div>
    </div>
  );
};

export default WidgetSideStatusHotel;
