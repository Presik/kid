// Model Add Room Wizard

function getOptionsRooms(record) {
  let res = new Map();
  if (record) {
    res = record.rooms;
  }
  return res;
}

const getView = () => {
  let DictCtxView = {
    model: "hotel.booking.add_rooms.selector",
    form_action: [],
    table_action: [],
    webfields: {
      main_guest: {
        type: "many2one",
        model: "party.party",
        readOnly: {
          registration_state: ["check_in", "check_out"],
        },
      },
      unit_price: {
        type: "number",
        required: true,
        editable: true,
      },
      discount: {
        type: "number",
        // required: true,
      },
      room: {
        type: "many2one",
        model: "hotel.room",
        // required: true,
        editable: true,
        options: getOptionsRooms,
      },
      product: {
        type: "many2one",
        model: "product.product",
        recSearch: () => [
          ["template.type", "=", "service"],
          ["template.kind", "=", "accommodation"],
        ],
      },
      sale_price_taxed: {
        type: "number",
        editable: true,
      },
      available: {
        type: "integer",
        readOnly: true,
      },
      adults: {
        type: "number",
        editable: true,
      },
      children: {
        type: "number",
        editable: true,
      },
      add: {
        type: "button",
        color: "blue",
        iconRight: "fi fi-rr-add",
        // method: addRoom, Thism method is added in custom model
        // visible: true,
      },
    },
    webtree: [
      { name: "product", width: "20%" },
      { name: "available", width: "5%" },
      { name: "sale_price_taxed", width: "10%" },
      { name: "discount", width: "10%" },
      { name: "adults", width: "5%" },
      { name: "children", width: "5%" },
      { name: "room", width: "20%" },
      { name: "add", width: "10%" },
    ],
    webform: [],
  };

  return DictCtxView;
};

export default { ctxView: getView };
