// Folio Charge model
import dates from "tools/dates";

const getUnitPrice = (record) => {
  if (record.product) {
    let res = record.product.sale_price_taxed;
    return res;
  }
};

const onSearchProduct = (record) => {
  let res = [
    ["account_category", "!=", null],
    ["salable", "=", true],
  ];
  return res;
};

const onChangeProduct = (record) => {
  if (record.product) {
    const product = record.product;
    // let unit_price = product.sale_price_taxed;
    let unit_price = product.list_price;
    let sale_price_taxed = record.product.sale_price_taxed;
    const fmt_unit_price = unit_price.toFixed(2).toString();
    console.log(unit_price, typeof unit_price, fmt_unit_price);
    // unit_price = parseFloat(unit_price.toString());
    const updateToStore = {
      unit_price: fmt_unit_price,
    };
    const updateToActive = {
      unit_price: unit_price,
      unit_price_w_tax: record.product.sale_price_taxed,
      amount: unit_price * sale_price_taxed,
    };
    return [updateToStore, updateToActive];
  } else {
    return [{}, {}];
  }
};

const getAmount = (record) => {
  if (record.unit_price_w_tax && record.quantity) {
    console.log("price_w_tax...", record.unit_price_w_tax);
    const amount = (record.unit_price_w_tax * record.quantity).toFixed(2);
    return amount;
  }
};

const getView = () => {
  let DictCtxView = {
    model: "hotel.folio.charge",
    form_action: ["add", "edit", "delete"], // options: ['edit', 'delete']
    table_action: ["add", "edit"], // options: ['open', 'delete', 'edit', 'add']
    domain: [], // Options: null or valid domain
    orderBy: [["date_service", "ASC"]],
    limit: 100,
    selectable: null, // Options for table rows: null, multi, one,
    target: "folio",
    webfields: {
      date_service: {
        type: "date",
        readOnly: false,
        default: dates.dateToday(),
        required: true,
      },
      kind: {
        type: "selection",
        readOnly: true,
        required: true,
        default: { id: "product", name: "product" },
        translate: true,
        options: [
          { id: "product", name: "product" },
          { id: "accommodation", name: "accommodation" },
        ],
      },
      amount: {
        type: "number",
        readOnly: true,
        function: getAmount,
        decimalPlaces: 2,
      },
      status: { type: "char", readOnly: true, translate: true },
      folio: {
        type: "many2one",
        model: "hotel.folio",
        readOnly: true,
      },
      invoice_line: {
        type: "many2one",
        model: "account.invoice.line",
        readOnly: true,
      },
      product: {
        type: "many2one",
        model: "product.product",
        required: true,
        recSearch: onSearchProduct,
        withChange: onChangeProduct,
        attrs: [
          "id",
          "list_price",
          "name",
          "sale_price_taxed",
          "sale_uom.rec_name",
        ],
      },
      quantity: {
        type: "number",
        default: 1,
        required: true,
      },
      order: {
        type: "char",
        readOnly: true,
        // default: 1,
      },
      unit_price_w_tax: {
        type: "number",
        function: getUnitPrice,
        // required: true,
        decimalPlaces: 2,
        readOnly: true,
      },
    },
    webtree: [
      { name: "date_service", width: "10%" },
      { name: "product", width: "25%" },
      { name: "quantity", width: "20%" },
      { name: "unit_price_w_tax", width: "20%" },
      { name: "amount", width: "25%" },
      { name: "status", width: "25%" },
    ],
    webform: [
      { name: "product" },
      {
        id: "info_main",
        grid: [{ name: "date_service" }, { name: "folio" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "info_qty",
        grid: [{ name: "quantity" }, { name: "unit_price_w_tax" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "info_amounts",
        grid: [{ name: "amount" }, { name: "kind" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "order" },
      {
        id: "info_status",
        grid: [{ name: "status" }, { name: "invoice_line" }],
        size: [1, 2],
        span: "col-span-1",
      },
      // { name: "add_product" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
