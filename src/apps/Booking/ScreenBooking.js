import React, { useState, useEffect } from "react";
import { FormattedMessage as FM } from "react-intl";
import date from "date-and-time";

import Screen from "components/Screen";
import Board from "components/Board";
import Booking from "./Booking";
import DateRangeFilter from "components/DateRangeFilter";
import WidgetSide from "./components/WidgetSide";
import WidgetSideStatusHotel from "./components/WidgetSideStatusHotel";
import SectionRigth from "components/SectionRigth";
import ListPeopleCheckInWidget from "./components/ListPeopleCheckInWidget";
import WidgetSideCustomers from "./components/WIdgetSideCustomers";
import WidgetCheckInSide from "./components/WidgetCheckInSide";
import { BUTTON_COLORS } from "components/Constants/constants";
import { dataListCustomers } from "apps/Booking/dataFake";
import proxy from "api/proxy";

const fmt = "YYYY-MM-DD";

const fields = [
  "company.rec_name",
  "party.name",
  "party.email",
  "party.phone",
  "number",
  "adult_num",
  "contact",
  "channel.name",
  "booking_date",
  "state",
  "total_amount",
  "party.name",
  "lines.room.name",
  "lines.room.cleaning_type",
  "lines.room.state",
  "lines.room.last_check_in",
  "lines.room.last_check_out",
  "lines.product.template.name",
  "lines.room.classification.name",
  "lines.arrival_date",
  "lines.departure_date",
  "lines.nights_quantity",
  "lines.guests",
  "lines.registration_state",
  "lines.unit_price_w_tax",
];

const ScreenBooking = (props) => {
  const ctxView = Booking.ctxView(props.config);
  const [statusSection, setStatusSection] = useState(true);
  let [newCtxView, setNewCtxView] = useState(ctxView);
  const [filterDay, setFilterDay] = useState(date.format(new Date(), fmt));
  let [booking, setBooking] = useState([]);
  let [rooms, setRooms] = useState([]);

  // const style = `${BUTTON_COLORS.rose[0]} ${BUTTON_COLORS.rose[1]}`;
  // const styleOpen = "bg-white text-bluePresik";

  const handleChangeDate = (value) => {
    let daySelected = date.format(new Date(value), "YYYY-MM-DD");
    setFilterDay(daySelected);
  };

  async function getBooking() {
    let nextDay = new Date(filterDay);
    nextDay.setDate(nextDay.getDate() + 2);
    let nextDayFormatted = date.format(nextDay, "YYYY-MM-DD");
    let dom = [
      ["lines.arrival_date", ">=", filterDay],
      ["lines.arrival_date", "<=", nextDayFormatted],
    ];

    const { data } = await proxy.search("hotel.booking", dom, fields);

    if (data) {
      setBooking(data);
      const linesBooking = data.flatMap((item) => item["lines."]);
      setRooms(linesBooking);
      const newDomain = [["lines.arrival_date", "=", filterDay]];
      newCtxView.domain = newDomain;
      setNewCtxView(newCtxView);
    }
  }

  useEffect(() => {
    getBooking();
  }, [filterDay]);

  let statusStyle = "md:w-12/12 relative pr-20 custom-transition duration-500";
  if (statusSection) {
    statusStyle = "md:w-[65vw] xl:w-[70vw] custom-transition duration-500";
  }

  return (
    <Screen>
      <DateRangeFilter action={handleChangeDate} />
      <div
        className={`flex ${
          statusSection ? "flex-row justify-between " : "flex-col"
        }`}
      >
        <div className={statusStyle}>
          <Board ctxView={newCtxView} />
        </div>

        <SectionRigth
          position="right"
          widgets={["status", "numPeople"]}
          title="360° SMART HOTEL"
          bgColor={statusSection ? "bg-gray-100" : "bg-blue-presik"}
          status={statusSection}
          handleChangeStatus={setStatusSection}
        >
          <WidgetSideStatusHotel
            title={<FM id="booking.widget.screen.huesped_house" />}
            background={"bg-gray-100"}
            name="statusHotel"
            rooms={rooms}
            day={filterDay}
          />
          <WidgetCheckInSide
            title="Estado"
            background={"bg-gray-100"}
            rooms={rooms}
            day={filterDay}
          />
          <WidgetSide
            title={<FM id="booking.widget.screen.huesped_house" />}
            num={50}
            background={"bg-gray-100"}
            name="statusHotel"
            rooms={rooms}
            day={filterDay}
          />
          <WidgetSide
            title={<FM id="booking.widget.screen.huesped_house" />}
            num={3}
            background={"bg-gray-100"}
            name="statusPeopleHotel"
            rooms={rooms}
            day={filterDay}
          />
          <WidgetSideCustomers
            dataCustomers={dataListCustomers}
            title={<FM id="booking.widget.screen.last_bookings" />}
            data={booking}
            // day={filterDay}
          />
          <ListPeopleCheckInWidget
            title={<FM id="booking.widget.screen.more_options" />}
          />
        </SectionRigth>
      </div>
    </Screen>
  );
};

export default ScreenBooking;
