import { useFormStore } from "store/formStore";
import { useWizardStore } from "store/wizardStore";

function defaultAmount() {
  const { activeRecord } = useFormStore.getState();
  const { updateWizard } = useWizardStore.getState();
  const res = { amount: activeRecord.pending_to_pay };
  updateWizard(res);
}

const getView = () => {
  let DictCtxView = {
    model: "hotel.booking.add_payment",
    form_action: [],
    table_action: [],
    defaults: defaultAmount,
    webfields: {
      statement: {
        type: "many2one",
        model: "account.statement",
        readOnly: false,
        required: true,
        recSearch: () => [["state", "=", "draft"]],
      },
      amount: {
        type: "number",
        readOnly: false,
        required: true,
      },
      voucher: {
        type: "char",
        readOnly: false,
        // required: true,
      },
      pay: {
        type: "button",
        // method: addPayment, This method is added in custom model
        color: "green",
        iconRight: "fi fi-rr-add",
        visible: true,
      },
    },
    webform: [
      { name: "statement" },
      { name: "amount" },
      { name: "voucher" },
      { name: "pay" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
