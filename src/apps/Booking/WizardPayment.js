import React, { useEffect } from "react";

import FormWizard from "components/Form/FormWizard";
import ModelWizardPayment from "./ModelWizardPayment";
import { useWizardStore } from "store/wizardStore";
import { useFormStore } from "store/formStore";
import proxy from "api/proxy";
import dates from "tools/dates";

const ctxView = ModelWizardPayment.ctxView();

const WizardAddPayment = ({ onClose, openModal }) => {
  const { storeWizard, upStoreWizard, resetWizard } = useWizardStore();
  const { activeRecord } = useFormStore();

  async function addPayment() {
    if (!activeRecord.party) {
      onClose(false, "missing_party");
      return;
    }
    let toStore = {
      booking: activeRecord.id,
      statement: storeWizard.statement.id,
      amount_to_pay: storeWizard.amount,
      number: storeWizard.voucher,
      party: activeRecord.party.id,
    };
    const values = {
      model: "hotel.booking",
      method: "add_payment",
      args: [toStore],
    };
    const { data, error } = await proxy.methodCall(values);
    onClose(true, error);
    await resetWizard();
  }

  // function defaultStore() {
  //   if (activeRecord) {
  //     console.log("activeRecord >>> ", activeRecord);
  //     upStoreWizard({
  //       amount: activeRecord.pending_to_pay,
  //     });
  //   }
  // }

  return (
    <FormWizard
      key="hotel-booking-wizard-payment"
      ctxView={ctxView}
      parentRec={activeRecord}
      handleButton={addPayment}
    />
  );
};

export default WizardAddPayment;
