import date from "date-and-time";

import dates from "tools/dates";
import modelGuest from "./Guest";
import modelCharge from "./Charge";

const getName = (record) => {
  return record.booking;
};

function visibleCheckIn(fieldName, record) {
  if (record && record.registration_state === "pending") {
    return true;
  }
  return false;
}

function getNights(arrival, departure) {
  const arrival_date = dates.getTrytonDate2Js(arrival);
  const departure_date = dates.getTrytonDate2Js(departure);
  return date.subtract(departure_date, arrival_date).toDays();
}

const withChangeProduct = (record) => {
  if (record.product) {
    const product = record.product;
    let uom = product["sale_uom."];
    let unit_price = product.sale_price_taxed;
    unit_price = parseFloat(unit_price.toString());
    const updateToStore = {
      unit_price: unit_price,
      uom: uom.id,
    };
    const updateToActive = {
      unit_price: unit_price,
      unit_price_w_tax: record.product.sale_price_taxed,
      // amount: unit_price * record.quantity,
      uom: uom,
    };
    return [updateToStore, updateToActive];
  } else {
    return [{}, {}];
  }
};

const withChangeDate = (record) => {
  const { arrival_date, departure_date, unit_price_w_tax } = record;
  if (arrival_date && departure_date) {
    const nights = getNights(arrival_date, departure_date);
    let updateToStore = {
      nights_quantity: nights,
    };
    let updateToActive = {
      nights_quantity: nights,
    };

    if (unit_price_w_tax) {
      const total_amount = (nights * unit_price_w_tax).toFixed(2);
      updateToActive.total_amount = total_amount;
    }
    return [updateToStore, updateToActive];
  } else {
    return [{}, {}];
  }
};

const getView = () => {
  let DictCtxView = {
    model: "hotel.folio",
    row_selectable: false,
    form_action_add: "modal",
    form_action: ["add", "edit"], // options: ['save', 'delete']
    table_action: ["add", "edit"], // options: ['open', 'delete', 'edit', 'add']
    activeSearch: true,
    form_rec_name: getName,
    target: "booking",
    title: { field: "booking", component: "title" },
    webfields: {
      registration_card: { type: "char", readOnly: true },
      main_guest: {
        type: "many2one",
        model: "party.party",
        readOnly: true,
      },
      booking: {
        type: "many2one",
        model: "hotel.booking",
        readOnly: true,
      },
      arrival_date: {
        type: "date",
        required: true,
        withChange: withChangeDate,
        readOnly: {
          registration_state: ["check_in", "check_out"],
        },
      },
      departure_date: {
        type: "date",
        readOnly: {
          registration_state: ["check_in", "check_out"],
        },
        required: true,
        withChange: withChangeDate,
      },
      unit_price: {
        type: "number",
        required: true,
        readOnly: {
          registration_state: ["check_out", "check_in"],
        },
      },
      pending_total: { type: "number", readOnly: true },
      nights_quantity: { type: "number", readOnly: true },
      room: {
        type: "many2one",
        model: "hotel.room",
        required: true,
        recSearch: () => [],
        readOnly: {
          registration_state: ["check_out", "check_in"],
        },
      },
      product: {
        type: "many2one",
        model: "product.product",
        recSearch: () => [["kind", "=", "accommodation"]],
        readOnly: {
          registration_state: ["check_in", "check_out"],
        },
        withChange: withChangeProduct,
        attrs: [
          "id",
          "list_price",
          "name",
          "sale_price_taxed",
          "sale_uom.rec_name",
        ],
      },
      channel: {
        type: "many2one",
        model: "hotel.channel",
        readOnly: true,
        images: { targetField: "code", source: {} },
      },
      registration_state: {
        type: "char",
        readOnly: true,
        translate: true,
        default: "pending",
      },
      plan: {
        type: "char",
        readOnly: true,
        translate: true,
      },
      unit_price_w_tax: {
        type: "number",
        readOnly: true,
        // function: true,
      },
      guests: {
        type: "one2many",
        model: "hotel.folio.guest",
        ctxView: modelGuest.ctxView(),
        readOnly: {
          registration_state: ["check_in", "check_out"],
        },
      },
      charges: {
        type: "one2many",
        model: "hotel.folio.charge",
        ctxView: modelCharge.ctxView(),
        readOnly: {
          registration_state: ["check_in", "check_out"],
        },
      },
      total_amount: { type: "number", readOnly: true, function: true },
      notes: {
        type: "text-area",
        readOnly: false,
      },
      vehicle_plate: {
        type: "char",
        readOnly: ["check_out"],
      },
      group: {
        type: "boolean",
        readOnly: true,
        translate: true,
      },
      payment_status: {
        type: "char",
        readOnly: true,
        translate: true,
        depends: ["charges"],
      },
      check_in: {
        type: "button",
        button_method: "check_in",
        visible: visibleCheckIn,
        onSuccessMsg: "Check In exitoso!",
        color: "blue",
      },
      check_out: {
        type: "button",
        button_method: "check_out",
        visible: [{ registration_state: ["check_in"] }],
        onSuccessMsg: "Check Out exitoso!",
        color: "blue",
      },
    },
    webtree: [
      { name: "room", width: "15%" },
      { name: "product", width: "15%" },
      { name: "arrival_date", width: "10%" },
      { name: "departure_date", width: "10%" },
      { name: "unit_price", width: "10%" },
      { name: "unit_price_w_tax", width: "10%" },
      { name: "nights_quantity", width: "10%" },
      { name: "registration_state", width: "10%" },
    ],
    webform: [
      { name: "product" },
      {
        id: "booking",
        grid: [{ name: "booking" }, { name: "registration_card" }],
        size: [1, 2],
      },

      { name: "room" },
      { name: "main_guest" },
      {
        id: "dates",
        grid: [
          { name: "arrival_date" },
          { name: "departure_date" },
          { name: "nights_quantity" },
        ],
        size: [1, 3],
        // border: "visible",
        span: "md:col-span-2",
      },
      {
        id: "amounts",
        grid: [
          { name: "unit_price" },
          { name: "unit_price_w_tax" },
          { name: "total_amount" },
        ],
        span: "md:col-span-2",
        size: [1, 3],
      },
      {
        id: "sales",
        grid: [{ name: "channel" }, { name: "plan" }, { name: "group" }],
        size: [1, 3],
        span: "md:col-span-2",
      },
      { name: "pending_total" },
      { name: "registration_state" },
      { name: "payment_status" },
      { name: "notes" },
      {
        name: "more_info",
        widget: "group",
        children: [
          {
            name: "guests",
            widget: "button_modal",
            color: "sky",
            icon: "fi fi-rr-users",
          },
          {
            name: "charges",
            widget: "button_modal",
            color: "amber",
            icon: "fi fi-rr-file-invoice",
          },
        ],
      },
      {
        id: "buttons",
        grid: [{ name: "check_in" }, { name: "check_out" }],
        size: [1, 2],
      },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
