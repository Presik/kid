// Guest model
import { useFormStore } from "store/formStore";
import { useFormSubChildStore } from "store/formSubChildStore";
import proxy from "api/proxy";

const GUEST = [
  { id: "adult", name: "adult" },
  { id: "child", name: "child" },
];

const SEX = [
  { id: "female", name: "female" },
  { id: "male", name: "male" },
];

const TYPES = [
  { id: "11", name: "registro_civil" },
  { id: "12", name: "tarjeta_identidad" },
  { id: "13", name: "cedula_ciudadania" },
  { id: "21", name: "tarjeta_extranjeria" },
  { id: "22", name: "cedula_extranjeria" },
  { id: "31", name: "nit" },
  { id: "41", name: "pasaporte" },
  { id: "42", name: "tipo_documento_extranjero" },
  { id: "47", name: "pep" },
  { id: "50", name: "nit_otro_pais" },
  { id: "91", name: "nuip" },
];

const model = "party.party";
const fields = [
  "name",
  "id_number",
  "type_document",
  "sex",
  "mobile",
  "email",
  "address",
  "country",
];

function getRequiredByMainGuest(record) {
  const res = record ? record.main_guest : false;
  return res;
}

function getCities(data, record, value) {
  let dom = null;
  if (record && record.subdivision) {
    dom = [["department", "=", record.subdivision.id]];
  }
  return dom;
}

function reqByCountry(record) {
  console.log("reqByCountry...", record);
  let res = false;
  if (record.country && record.country.rec_name === "COLOMBIA") {
    res = true;
  }
  return res;
}

async function onAutoCreateGuest(value) {
  const { activeRecord, upStoreRecord } = useFormStore.getState();
  const { activeSubChild, storeSubChild, upActiveSubChild, upStoreSubChild } =
    useFormSubChildStore.getState();

  if (activeRecord.party) {
    const dom = [["id", "=", activeRecord.party.id]];
    const { data, error } = await proxy.search(model, dom, fields);
    if (data) {
      let _data = { ...data[0] };
      _data.main_guest = true;
      let activeGuest = { ..._data };
      let storeGuest = { ..._data };
      if (storeGuest.type_document) {
        for (const _type of TYPES) {
          if (_type.id === storeGuest.type_document) {
            activeGuest.type_document = _type;
          }
        }
      }
      if (storeGuest.sex) {
        for (const _type of SEX) {
          if (_type.id === storeGuest.sex) {
            activeGuest.type_document = _type;
          }
        }
      }
      const activeSub = { ...activeSubChild, ...activeGuest };
      const storeSub = { ...storeSubChild, ...storeGuest };
      upActiveSubChild(activeSub);
      upStoreSubChild(storeSub);
    }
  }
}

function visibleCreateGuest() {
  return true;
}

const getView = () => {
  let DictCtxView = {
    model: "hotel.folio.guest",
    form_action: ["add", "delete", "edit"],
    table_action: ["add", "edit"],
    orderBy: [["name", "ASC"]],
    selectable: null, // Options for table rows: null, multi, one
    target: "folio",
    // domain: [], // Options: null or valid domain
    // limit: 10,
    webfields: {
      name: { type: "char", required: true },
      type_guest: {
        type: "selection",
        options: GUEST,
        required: true,
        translate: true,
        default: { id: "adult", name: "adult" },
      },
      type_document: {
        type: "selection",
        options: TYPES,
        required: true,
        translate: true,
      },
      sex: {
        type: "selection",
        options: SEX,
        required: true,
        translate: true,
        default: { id: "male", name: "male" },
      },
      id_number: { type: "char", required: true },
      mobile: { type: "char", required: true },
      email: { type: "char", required: true },
      address: { type: "char", required: getRequiredByMainGuest },
      birthday: { type: "date" },
      visa_date: { type: "date" },
      visa_number: { type: "char" },
      profession: { type: "char" },
      notes: { type: "text-area" },
      main_guest: {
        type: "boolean",
        translate: true,
      },
      country: {
        type: "many2one",
        model: "party.country_code",
      },
      subdivision: {
        type: "many2one",
        model: "party.department_code",
        required: reqByCountry,
        visible: reqByCountry,
      },
      city: {
        type: "many2one",
        model: "party.city_code",
        required: reqByCountry,
        visible: reqByCountry,
        recSearch: getCities,
      },
      origin_country: {
        type: "many2one",
        model: "country.country",
      },
      target_country: {
        type: "many2one",
        model: "country.country",
      },
      nationality: {
        type: "many2one",
        model: "country.country",
        required: true,
      },
      create_guest: {
        type: "button",
        visible: visibleCreateGuest,
        color: "amber",
        method: onAutoCreateGuest,
        iconLeft: "fi fi-rr-users",
      },
    },
    webtree: [
      { name: "name", width: "25%" },
      { name: "type_guest", width: "15%" },
      { name: "type_document", width: "10%" },
      { name: "id_number", width: "10%" },
      { name: "mobile", width: "10%" },
      { name: "main_guest", width: "5%" },
    ],
    webform: [
      { name: "name" },
      {
        id: "info1",
        grid: [{ name: "type_guest" }, { name: "main_guest" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "type_document" },
      { name: "id_number" },
      {
        id: "info2",
        grid: [{ name: "mobile" }, { name: "sex" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "email" },
      { name: "country" },
      { name: "address" },
      { name: "subdivision" },
      { name: "city" },
      { name: "nationality" },
      {
        id: "info_country",
        grid: [{ name: "origin_country" }, { name: "target_country" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "birthday" },
      { name: "profession" },
      {
        id: "info4",
        grid: [{ name: "visa_date" }, { name: "visa_number" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "notes" },
      { name: "create_guest" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
