import React from "react";

import ModelFolios from "./BookingLine";
import WizardAddRoom from "./WizardAddRoom";
import WizardPayment from "./WizardPayment";
import modelWizardPayment from "./ModelWizardPayment";
import WizardBill from "./WizardBill";
import party from "./Party";
import imgDespegar from "assets/apps/despegar.png";
import imgBooking from "assets/apps/booking.png";
import imgExpedia from "assets/apps/expedia.png";
import imgHouse from "assets/apps/house.png";
import dates from "tools/dates";

const stateColors = {
  offer: "amber",
  confirmed: "lime",
  cancelled: "rose",
  not_show: "gray",
  finished: "sky",
};

const MEDIA = [
  { id: "phone", name: "phone" },
  { id: "fax", name: "fax" },
  { id: "email", name: "email" },
  { id: "chat", name: "chat" },
  { id: "walking", name: "walking" },
  { id: "web", name: "web" },
  { id: "channel_manager", name: "channel_manager" },
  { id: "ota", name: "ota" },
];

const PLAN = [
  { id: "no_breakfast", name: "no_breakfast" },
  { id: "all_inclusive", name: "all_inclusive" },
  { id: "bed_breakfast", name: "bed_breakfast" },
  { id: "full_american", name: "full_american" },
  { id: "half_american", name: "half_american" },
];

const RESPONSIBLE = [
  { id: "holder", name: "holder" },
  { id: "holder_guest", name: "holder_guest" },
  { id: "guest", name: "guest" },
];

const CHANNEL_PAYMENT_METHOD = [
  { id: "at_destination", name: "at_destination" },
  { id: "ota_collect", name: "ota_collect" },
];

const iconOTA = {
  house: imgHouse,
  booking: imgBooking,
  despegar: imgDespegar,
  expedia: imgExpedia,
};

function getImageChannel(record) {
  let image = imgHouse;
  if (record && record.code && iconOTA[record.code]) {
    image = iconOTA[record.code];
  }
  return <img src={image} width={30} alt="channel" />;
}

const visibleConfirm = (name, record) => {
  let res = false;
  if (
    record &&
    record.state === "offer" &&
    record.lines &&
    record.lines.size > 0
  ) {
    res = true;
  }
  return res;
};

const visibleBill = (name, record) => {
  let res = false;
  if (
    record &&
    record.state === "confirmed" &&
    record.lines &&
    record.lines.size > 0
  ) {
    res = true;
  }
  return res;
};

function visibleAddPayment(name, record) {
  return record.number ? true : false;
}

const getFilters = () => {
  return {
    offer: [
      ["state", "=", "offer"],
      // ["booking_date", ">=", yesterday],
      // ["shop", "=", session.shop],
    ],
  };
};

const getView = () => {
  let DictCtxView = {
    model: "hotel.booking",
    form_action: ["edit", "add"], // options: ['save', 'delete']
    table_action: ["edit", "add"], // options: ['open', 'delete', 'edit', 'add', "info"]
    activeSearch: true,
    filters: getFilters,
    orderBy: [["number", "DESC"]],
    limit: 30,
    selectable: null, // Options for table rows: null, multi, one
    // domain: [["lines.arrival_date", "=", "2023-06-27"]], // Options: null or valid domain
    // domain: [],
    // pagination: [],
    reports: [
      {
        name: "hotel.booking",
        description: "Detalle de Reserva confirmada",
        send_print: true,
        send_email: "send_email",
      },
      {
        name: "hotel.booking_statement",
        description: "Estado de cuenta de cliente",
        send_print: true,
        send_email: "send_email",
      },
    ],
    tags: {
      state: stateColors,
    },
    sources: {
      image: iconOTA,
    },
    webfields: {
      number: {
        type: "char",
        readOnly: true,
        searchable: true,
      },
      contact: {
        type: "char",
        searchable: true,
        required: true,
      },
      channel: {
        type: "many2one",
        model: "hotel.channel",
        searchable: true,
        getIcon: getImageChannel,
        attrs: ["code"],
      },
      responsible_payment: {
        type: "selection",
        options: RESPONSIBLE,
        default: { id: "holder", name: "holder" },
        translate: true,
        required: true,
      },
      party: {
        type: "many2one",
        model: "party.party",
        searchable: true,
        ctxView: party.ctxView(),
        attrs: ["account_receivable"],
      },
      lines: {
        type: "one2many",
        model: "hotel.folio",
        ctxView: ModelFolios.ctxView(),
      },
      space: {
        type: "many2one",
        model: "analytic_account.space",
      },
      company: {
        type: "many2one",
        model: "company.rec_name",
      },
      write_uid: {
        type: "many2one",
        model: "res.use",
        readOnly: true,
      },
      booking_date: {
        type: "datetime",
        readOnly: true,
        default: () => dates.getNow(),
      },
      total_amount: {
        type: "number",
        readOnly: true,
      },
      tax_amount: {
        type: "number",
        readOnly: true,
      },
      total_advances: {
        type: "number",
        readOnly: true,
      },
      pending_to_pay: {
        type: "number",
        readOnly: true,
      },
      media: {
        type: "selection",
        options: MEDIA,
        translate: true,
      },
      group: {
        type: "boolean",
        translate: true,
      },
      breakfast_included: {
        type: "boolean",
        translate: true,
      },
      corporative: {
        type: "boolean",
        translate: true,
      },
      plan: {
        type: "selection",
        options: PLAN,
        default: { id: "bed_breakfast", name: "bed_breakfast" },
        translate: true,
      },
      state: {
        type: "char",
        translate: true,
        default: "offer",
        readOnly: true,
        tags: stateColors,
      },
      complementary: {
        type: "boolean",
        translate: true,
        readOnly: true,
      },
      channel_payment_method: {
        type: "selection",
        translate: true,
        options: CHANNEL_PAYMENT_METHOD,
      },
      payment_term: {
        type: "many2one",
        model: "account.invoice.payment_term",
        required: true,
        default: 1,
      },
      price_list: {
        type: "many2one",
        recSearch: () => [],
        model: "product.price_list",
      },
      rate_plan: {
        type: "many2one",
        model: "hotel.rate_plan",
        // required: true,
      },
      lead_origin: {
        type: "many2one",
        recSearch: () => [],
        model: "crm.lead_origin",
      },
      confirm: {
        type: "button",
        button_method: "confirm",
        visible: visibleConfirm,
        onSuccessMsg: "Confirmacion exitosa!",
        color: "blue",
      },
      add_rooms: {
        type: "button-wizard",
        Component: WizardAddRoom,
        color: "blue",
        icon: "fi fi-rr-add",
      },
      add_payment: {
        type: "button-wizard",
        Component: WizardPayment,
        ctxView: modelWizardPayment.ctxView(),
        color: "lime",
        icon: "fi fi-rr-add",
        visible: visibleAddPayment,
      },
      bill: {
        type: "button-wizard",
        Component: WizardBill,
        visible: visibleBill,
        color: "amber",
        icon: "fi fi-rs-receipt",
      },
    },
    webtree: [
      { name: "number", width: "8%" },
      {
        name: "channel",
        width: "8%",
        widget: "image",
      },
      { name: "party", width: "12%" },
      { name: "contact", width: "8%" },
      { name: "pending_to_pay", width: "8%" },
      { name: "total_amount", width: "8%" },
      { name: "booking_date", width: "8%", formatString: "YYYY-MM-DD" },
      { name: "group", width: "8%" },
      {
        name: "state",
        width: "10%",
        widget: "badge",
      },
    ],
    webform: [
      { name: "party", widget: "search-add" },
      {
        id: "infoDate",
        grid: [{ name: "number" }, { name: "booking_date" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "contact",
        grid: [{ name: "contact" }, { name: "rate_plan" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "payments",
        grid: [{ name: "responsible_payment" }, { name: "payment_term" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "info_commercial",
        grid: [{ name: "channel" }, { name: "plan" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "info_kind",
        grid: [
          { name: "group" },
          { name: "corporative" },
          { name: "add_rooms" },
          { name: "add_payment" },
        ],
        size: [1, 4],
        span: "col-span-1",
      },
      { name: "lines", widget: "cards" },
      {
        id: "payment",
        grid: [
          { name: "total_advances" },
          { name: "pending_to_pay" },
          { name: "total_amount" },
        ],
        size: [1, 3],
        span: "col-span-2",
      },
      {
        id: "amount",
        grid: [{ name: "lead_origin" }, { name: "media" }, { name: "state" }],
        size: [1, 3],
        span: "col-span-2",
        collapse: true,
      },
      { name: "confirm" },
      { name: "bill" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
