import React, { useState } from "react";
import date from "date-and-time";

import GridCards from "components/GridCards";
import FullTable from "components/FullTable";
import DateField from "components/DateField";
import IntegerField from "components/IntegerField";
import DropdownField from "components/DropdownField";
import StdButton from "components/StdButton";
import ModelWizardRooms from "./ModelWizardRooms";
import ModelFolioWizard from "./ModelFolioWizard";
import proxy from "api/proxy";
import dates from "tools/dates";
import funcs from "tools/functions";
import { useFormStore } from "store/formStore";

function getRandomRoom(rooms) {
  const roomRand = Math.floor(Math.random() * rooms.length);
  const foundRooms = rooms.splice(roomRand, 1);
  if (foundRooms.length > 0) {
    return foundRooms[0];
  }
}

const AddRoomWizard = ({ onClose }) => {
  let [records, setRecords] = useState(null);
  let [roomsSelected, setRoomsSelected] = useState(new Map());
  let [wizStore, setWizStore] = useState({});
  let [nights, setNights] = useState(0);
  let [ratePlan, setRatePlan] = useState(null);
  const {
    storeRecord,
    activeRecord,
    upFieldActive,
    upFieldStore,
    saveRecord,
    reloadRecord,
  } = useFormStore();
  let ctxViewRooms = ModelWizardRooms.ctxView();

  function handleDelete(record) {
    let _roomsSelected = new Map(roomsSelected);
    _roomsSelected.delete(record.room.id);
    setRoomsSelected(_roomsSelected);
    let _records = new Map(records);
    const category = _records.get(record.product.id);
    category.rooms.push(record.room);
    category.available = category.rooms.length;
  }

  function updateRecords(field, value, recId) {
    let _records = new Map(records);
    let category = _records.get(recId);
    category[field] = value;
    if (field === "sale_price_taxed") {
      const discount = (value / category.sale_price_taxed_default - 1) * 100;
      category.discount = discount.toFixed(1);
      setRecords(_records);
    }
  }

  async function handleAccept() {
    let toStore = funcs.cloneMap(roomsSelected);
    for (let value of toStore.values()) {
      delete value.unit_price_w_tax;
      delete value.nights;
      delete value.total_amount;
    }

    let linesActive = { ...activeRecord }.lines;
    if (activeRecord.id > 0) {
      let toCreate = new Map();
      toCreate.set("create", toStore);
      upFieldStore("lines", toCreate);
      if (!linesActive) {
        upFieldActive("lines", linesActive);
      } else {
        const _lines = new Map([...linesActive, ...roomsSelected]);
        upFieldActive("lines", _lines);
      }
      await saveRecord();
      await reloadRecord();
    } else {
      let linesStore = { ...storeRecord }.lines;
      if (!linesActive) {
        upFieldActive("lines", linesActive);
      } else {
        const _lines = new Map([...linesActive, ...roomsSelected]);
        upFieldActive("lines", _lines);
      }
      if (!linesStore) {
        linesStore = new Map();
        linesStore.set("create", toStore);
      } else {
        let meStore = linesStore.get("create");
        if (!meStore) {
          linesStore.set("create", new Map());
        } else {
          toStore = new Map([...meStore, ...toStore]);
        }
        linesStore.set("create", toStore);
        upFieldStore("lines", linesStore);
      }
    }
    onClose();
  }

  function addRoom(record) {
    let _roomsSelected = new Map(roomsSelected);
    let room;
    if (record.room.id) {
      room = record.room;
    } else {
      room = getRandomRoom(record.rooms);
    }
    record.available = record.rooms.length;
    if (room) {
      const newRoom = {
        room: room,
        product: record.product,
        arrival_date: wizStore.arrival_date,
        departure_date: wizStore.departure_date,
        unit_price: record.sale_price_taxed,
        unit_price_w_tax: record.sale_price_taxed,
        nights_quantity: nights,
        total_amount: (nights * record.sale_price_taxed).toFixed(2),
      };
      _roomsSelected.set(room.id, newRoom);
      setRoomsSelected(_roomsSelected);
    }
  }

  ctxViewRooms.webfields.add.method = addRoom;

  function getNights(wizStore) {
    const arrival_date = dates.getTrytonDate2Js(wizStore.arrival_date);
    const departure_date = dates.getTrytonDate2Js(wizStore.departure_date);
    const res = date.subtract(departure_date, arrival_date).toDays();
    return res;
  }

  const onChange = (name, value) => {
    let _wizStore = { ...wizStore };
    _wizStore[name] = value;
    setWizStore({ ..._wizStore });
    let _nights;
    if (_wizStore.arrival_date && _wizStore.departure_date) {
      _nights = getNights(_wizStore);
      fillCategories(_wizStore);
    } else {
      _nights = 0;
    }
    setNights(_nights);
  };

  const fillCategories = async (store) => {
    let rate_plan = activeRecord.rate_plan ? activeRecord.rate_plan.id : null;
    const { data } = await proxy.methodCall({
      model: "hotel.room",
      method: "available_by_classification",
      args: [store.arrival_date, store.departure_date, rate_plan],
      kwargs: {},
    });
    console.log("data....", data);
    const mapRecs = new Map();
    data.forEach((cat) => {
      cat.id = cat.product.id;
      cat.adults = 2;
      cat.children = 0;
      cat.sale_price_taxed_default = cat.sale_price_taxed;
      cat.room = {};
      // cat.room.options = cat.rooms;
      // cat.rooms = cat.rooms;
      mapRecs.set(cat.product.id, cat);
    });
    setRecords(mapRecs);
    if (data.length > 0) {
      const cat = data[0];
      setRatePlan(cat.rate_plan);
    }
  };

  const selectedRooms = new Map(roomsSelected);
  return (
    <div id="hotel-booking-wizard-rooms" className="block">
      <div className="grid grid-cols-4 gap-x-2 pb-3">
        <DateField
          onChange={onChange}
          name="arrival_date"
          label="hotel.booking.add_room.arrival_date"
        />
        <DateField
          onChange={onChange}
          name="departure_date"
          label="hotel.booking.add_room.departure_date"
        />
        <IntegerField
          onChange={onChange}
          name="nights_quantity"
          label="hotel.booking.add_rooms.nights_quantity"
          readOnly={true}
          value={nights}
        />
        <DropdownField
          name="price_list"
          label="hotel.booking.price_list"
          key="price_list"
          model="product.price_list"
          readOnly={true}
          value={ratePlan}
          recSearch={() => []}
          onChange={(name, value) => onChange(name, value)}
        />
      </div>
      {records && records.size > 0 && (
        <FullTable
          records={records}
          isLoading={false}
          limit={30}
          updateRecords={updateRecords}
          ctxView={ctxViewRooms}
        />
      )}
      <GridCards
        id="add-room-grid-cards"
        records={selectedRooms}
        handleDelete={handleDelete}
        ctxView={ModelFolioWizard.ctxView()}
      />
      <div className="float-right">
        {selectedRooms.size > 0 && (
          <StdButton
            style="font-semibold"
            color="amber"
            onClick={handleAccept}
            content="hotel.booking.add_rooms.accept"
          />
        )}
      </div>
    </div>
  );
};

export default AddRoomWizard;
