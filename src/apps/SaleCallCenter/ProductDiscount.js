import React from "react";
import { useQuery } from "@tanstack/react-query";
import { useFormStore } from "store/formStore";
import { useFormChildStore } from "store/formChildStore";

import proxy from "api/proxy";
import { Fragment } from "react";

function ProductDiscount({ record, onClose }) {
  // const [selected, setSelected] = useState([]);
  const { storeRecord, activeRecord, upFieldActive, upFieldStore } =
    useFormStore();
  const { upActiveChild, upStoreChild } = useFormChildStore();
  const shopId = activeRecord?.shop?.id;
  const queryProductDiscount = useQuery(
    ["productDiscount", shopId],
    async () => {
      let discounts = [];
      if (shopId) {
        const { data } = await proxy.search(
          "sale.shop",
          [["id", "=", shopId]],
          ["discounts.name", "discounts.type_discount", "discounts.discount"],
        );
        discounts = data;
      }

      return discounts;
    },
  );

  const handleSelect = async (discount) => {
    const list_price = record.product.list_price;
    const sale_price_taxed = record.amount;
    let _activeRecord = { ...activeRecord };
    let lines = _activeRecord.lines;
    let record_ = lines.get(record.id);

    let _storeRecord = { ...storeRecord };
    let to_create = _storeRecord.lines.get("create").get(record.id);
    if (discount.type_discount === "percentage") {
      const dsc = discount.discount / 100;
      record_["unit_price"] = Number(list_price - dsc * list_price).toFixed(2);
      record_["amount"] = Number(
        sale_price_taxed - dsc * sale_price_taxed,
      ).toFixed(2);
      record_["total_amount"] = Number(
        (sale_price_taxed - dsc * sale_price_taxed) * record.quantity,
      ).toFixed(2);

      to_create.unit_price = record_.unit_price;
    } else {
      const price_taxed = sale_price_taxed - discount.discount;
      const args = {
        product: record.product.id,
        sale_price_taxed: price_taxed,
      };
      const { data: result } = await proxy.method({
        model: "sale.sale",
        method: "dash_get_reverse_sale_price_taxed",
        args: [args],
      });
      record_["unit_price"] = Number(result["unit_price"]).toFixed(2);
      record_["amount"] = Number(price_taxed).toFixed(2);
      record_["total_amount"] = Number(price_taxed * record.quantity).toFixed(
        2,
      );
      to_create.unit_price = record_.unit_price;
    }
    upActiveChild(record_);
    upStoreChild(to_create);
    upFieldActive("lines", lines);
    upFieldStore("lines", _storeRecord.lines);
    onClose();
  };

  return (
    <Fragment>
      <div className="px-4 sm:px-6 lg:px-8 py-2 md:col-span-2">
        <div className="grid grid-cols-2 md:grid-cols-3 gap-4 pt-2">
          {queryProductDiscount.data?.[0]["discounts."].map(
            (discount, index) => {
              return (
                <div
                  className={
                    "rounded-md hover:opacity-50 text-center py-8 bg-orange-100"
                  }
                  key={discount.id}
                  onClick={() => {
                    handleSelect(discount);
                  }}
                  name={index}
                >
                  <p
                    key={discount.id}
                    className="pointer-events-none flex-wrap p-2"
                  >
                    {discount.name}
                  </p>
                </div>
              );
            },
          )}
        </div>
      </div>
    </Fragment>
  );
}

export default ProductDiscount;
