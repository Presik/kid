import React, { useState } from "react";

import { useFormStore } from "store/formStore";
import QuickForm from "components/QuickForm";
import StdButton from "components/StdButton";
import Kind from "./Kind";
import Shop from "./Shop";
import Source from "./Source";
import ScrollToTop from "./ScrollToTop";
import Consumer from "./Consumer";

import functions from "tools/functions";
import dates from "tools/dates";
import Timer from "./Timer";

var nextId = -1;
const STEPS = {
  source: Source,
  kind: Kind,
  shop: Shop,
};

function CreateOrder({ ctxView }) {
  const { storeRecord, activeRecord, upStoreRecord, upActiveRecord } =
    useFormStore();
  const [openConsumer, setOpenConsumer] = useState(false);
  const [startTimer, setStartTimer] = useState(null);
  // const [openMessage, setOpenMessage] = useState(false);
  const closeConsumer = (consumer) => {
    if (consumer) {
      updateStore("consumer", consumer);
    }
    setOpenConsumer(false);
  };

  async function updateStore(field, value) {
    if (value === undefined) return;
    let toStore = { ...storeRecord };
    let toActive = { ...activeRecord };

    if (!toActive.id) {
      [toStore, toActive] = await functions.getDefaults(nextId, ctxView);
    }
    if (!startTimer) {
      toStore["created"] = dates.getNow();
      setStartTimer(toStore["created"]);
    }
    toActive[field] = value;
    toStore[field] = value;
    if (field === "shop" && !toActive.party) {
      toActive["party"] = value["party."];
      toStore["party"] = value["party."].id;
    }

    upStoreRecord(toStore);
    upActiveRecord(toActive);
  }

  function getSteps() {
    const steps = Object.keys(STEPS).map((step_) => {
      const Component = STEPS[step_];
      return (
        <Component
          key={step_}
          onClick={updateStore}
          record={activeRecord[step_]}
          section={step_}
        />
      );
    });
    return steps;
  }

  return (
    <div className="mt-3">
      {getSteps()}
      <div className="flex justify-center pb-2">
        <StdButton
          color={"blue"}
          size={"w-3/4 h-16"}
          content={"sale.sale.add_consumer"}
          onClick={() => {
            setOpenConsumer(true);
          }}
        />
      </div>
      <hr className="h-px m-2 bg-gray-200 border-1 dark:bg-gray-700" />
      <QuickForm ctxView={ctxView} level="main" />
      {openConsumer && (
        <Consumer
          open={openConsumer}
          consumerId={activeRecord?.consumer?.id}
          closeConsumer={closeConsumer}
        />
      )}
      <Timer start={startTimer} />
      <ScrollToTop />
    </div>
  );
}

export default CreateOrder;
