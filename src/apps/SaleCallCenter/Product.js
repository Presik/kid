import React, { Fragment, useState, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import TwComboBox from "components/TwComboBox";
import StdButton from "components/StdButton";
import ProductCombine from "./ProductCombine";
import ProductCombo from "./ProductCombo";

function Product(props) {
  const LINE = { quantity: 1, amount: 0, total_amount: 0, notes: null };
  const { line } = props;
  const [line_, setLine] = useState(line ?? LINE);
  const [openCombine, setOpenCombine] = useState(false);
  const [openCombo, setOpenCombo] = useState(false);

  const closeProduct = (event) => {
    event.preventDefault();
    handleAddLine(event);
    props.onClose();
  };

  const handleField = (field, value) => {
    let _line = { ...line_ };
    if (field === "product") {
      _line["amount"] = value["template."]["sale_price_w_tax"];
      _line["unit_price"] = value["template."]["list_price"];
    } else if (field === "quatity") {
      value = Number(value);
    }
    _line[field] = value;
    setLine(_line);
  };

  const handleCombine = () => {
    if (line_.product.categories.length > 0) setOpenCombine(true);
  };

  const handleCombo = () => {
    if (line_.product.products_mix.length > 0) setOpenCombo(true);
  };

  const handleCloseCombine = (product) => {
    let line_prev = { ...line_ };
    const newPrice = product["template."].sale_price_w_tax;
    const unitPrice = product["template."].list_price;
    let newLine = {
      product: product,
      quantity: line_prev.quantity,
      amount: line_prev.amount,
      unit_price: line_prev.unit_price,
    };
    if (line_prev.amount < newPrice) {
      newLine["unit_price"] = unitPrice;
      newLine["amount"] = newPrice;
      line_prev["amount"] = newPrice;
      line_prev["unit_price"] = unitPrice;
      props.onChange(line_prev);
      setLine(line_prev);
      props.onChange(newLine);
    }
    setOpenCombine(false);
  };

  const handleCloseCombo = (choises) => {
    let newLines = [];
    for (const p of choises) {
      newLines.push({
        product: p,
        quantity: line_.quantity,
        amount: 0,
        unit_price: 0,
      });
    }
    props.onChange(newLines);
    setOpenCombo(false);
  };

  const handleAddLine = (event) => {
    event.preventDefault();
    event.target.form.reset();
    if (line_?.product && line_?.quantity) {
      props.onChange(line_);
    }
    setLine(LINE);
  };

  useEffect(() => {}, [line_]);

  return (
    <>
      <Transition appear show={true} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeProduct}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-70" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <form className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-4xl transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h6"
                    className="text-lg font-medium leading-6 text-gray-900 text-center"
                  >
                    AGREGAR PRODUCTO
                  </Dialog.Title>
                  <div className="px-4 sm:px-6 lg:px-8 py-2 md:col-span-2">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-4 pt-2">
                      <div className="basis-4/12">
                        <TwComboBox
                          name={"product"}
                          label={"PRODUCTO"}
                          onChange={handleField}
                          selected={line_?.product}
                        />
                      </div>
                      <div className="basis-1/12">
                        <label
                          htmlFor="quantity"
                          className="block text-gray-700 font-bold mb-2"
                        >
                          CANTIDAD
                        </label>
                        <input
                          className="shadow border appearance-none py-2 px-3 w-full focus:outline-none focus:border focus:border-cyan-500"
                          value={line_?.quantity ?? 1}
                          onChange={(e) => {
                            handleField(e.target.name, e.target.value);
                          }}
                          type={"number"}
                          name="quantity"
                        />
                      </div>
                      <div className="basis-2/12">
                        <label
                          htmlFor="amount"
                          className="block text-gray-700 font-bold mb-2"
                        >
                          PRECIO
                        </label>
                        <input
                          className="shadow border appearance-none py-2 px-3 w-full focus:outline-none focus:border focus:border-cyan-500"
                          type={"number"}
                          name="amount"
                          value={line_?.amount}
                          readOnly={true}
                        />
                      </div>
                      <div className="basis-3/12">
                        <label
                          htmlFor="total_amount"
                          className="block text-gray-700 font-bold mb-2"
                        >
                          TOTAL
                        </label>
                        <input
                          className="shadow border appearance-none py-2 px-3 w-full focus:outline-none focus:border focus:border-cyan-500"
                          type={"number"}
                          name="total_amount"
                          value={
                            line_?.amount && line_.quantity
                              ? line_.amount * line_.quantity
                              : 0
                          }
                          readOnly={true}
                        />
                      </div>
                      <div className=" md:col-span-2">
                        <label
                          htmlFor="notes"
                          className="block text-gray-700 font-bold mb-2"
                        >
                          NOTAS
                        </label>
                        <textarea
                          className="shadow border appearance-none py-2 px-3 w-full focus:outline-none focus:border focus:border-cyan-500"
                          name="notes"
                          value={line_?.notes}
                          onChange={(e) => {
                            handleField(e.target.name, e.target.value);
                          }}
                        />
                      </div>
                    </div>
                    <div className="grid grid-cols-2 md:grid-cols-4 gap-4 space-x-3">
                      <StdButton
                        style={"font-semibold"}
                        color={"amber"}
                        handleClick={handleCombo}
                        name={"ELECCIÓN"}
                      />
                      <StdButton
                        style={"font-semibold"}
                        color={"indigo"}
                        handleClick={handleCombine}
                        name={"COMBINAR"}
                      />
                      <StdButton
                        style={"font-semibold"}
                        color={"rose"}
                        handleClick={closeProduct}
                        name={"FINALIZAR"}
                      />
                      <StdButton
                        style={"font-semibold"}
                        color={"blue"}
                        handleClick={handleAddLine}
                        name={"AGREGAR"}
                      />
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </form>
          </div>
        </Dialog>
      </Transition>
      {openCombine && (
        <ProductCombine
          categories={line_.product.categories}
          onClose={handleCloseCombine}
        />
      )}
      {openCombo && (
        <ProductCombo
          products={line_.product.products_mix}
          onClose={handleCloseCombo}
        />
      )}
    </>
  );
}

export default Product;
