import React, { useState } from "react";
import { useQuery } from "@tanstack/react-query";
import { CheckIcon } from "@heroicons/react/20/solid";

import proxy from "api/proxy";
import BasicModal from "components/BasicModal";
import Button from "components/StdButton";

function ModalConsumer({ consumerId, closeConsumer }) {
  const [idConsumer, setIdConsumer] = useState(consumerId ?? -1);
  const [checkVisible, setCheckVisible] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const [storeRec, setStoreRec] = useState({});

  const queryConsumer = useQuery(["consumer", idConsumer], async () => {
    if (idConsumer) {
      const { data } = await proxy.search(
        "party.consumer",
        [["id", "=", idConsumer]],
        ["phone", "address", "id_number", "name", "notes", "delivery"],
      );
      return data[0] ?? null;
    } else {
      return null;
    }
  });

  const handlePhone = async (event) => {
    const phone = event.target.value;
    setIsChecked(false);

    if (phone) {
      const { data: consumer_ } = await proxy.search(
        "party.consumer",
        [["phone", "=", phone]],
        [],
      );
      setCheckVisible(false);

      if (consumer_ && consumer_.length > 0) {
        event.target.form.reset();
        setIdConsumer(consumer_[0].id);
        setCheckVisible(true);
        setStoreRec({});
      } else {
        setIdConsumer(null);
        setCheckVisible(false);
        setStoreRec({ ...storeRec, ...{ phone: phone } });
      }
    }
  };

  const handleField = (event) => {
    const target = event.target;
    let storeRec_ = { ...storeRec };
    if (target.name === "newPhone") {
      storeRec_["phone"] = target.value;
    } else {
      storeRec_[target.name] = target.value;
    }
    setStoreRec(storeRec_);
  };

  async function acceptConsumer(event) {
    event.stopPropagation();
    event.preventDefault();
    let consumer_ = { ...storeRec, ...{ id: idConsumer } };
    const { data: res } = await proxy.saveQuery({
      model: "party.consumer",
      storeRec: consumer_,
    });
    if (idConsumer <= 0 && res) {
      consumer_["id"] = res[0];
    }
    const { data: consumer } = await proxy.browse(
      "party.consumer",
      [consumer_.id],
      ["rec_name"],
    );
    queryConsumer.refetch();
    closeConsumer(consumer[0]);
  }

  const closeConsumer_ = () => {
    closeConsumer(null);
  };

  return (
    <BasicModal
      title={"sale.consumer.title"}
      open={true}
      width={"sm:w-2/6"}
      onClose={closeConsumer_}
    >
      <form className={"pb-4 space-y-4"}>
        <div>
          <label htmlFor="phone" className="block text-gray-700 font-bold mb-2">
            TELEFONO
          </label>
          <div className="relative">
            <input
              className="shadow border appearance-none py-2 px-3 w-full"
              type={"text"}
              name="phone"
              defaultValue={queryConsumer.data?.phone}
              onBlur={handlePhone}
            />
            {checkVisible && (
              <CheckIcon
                className="h-[30px] w-[30px] py-1 px-1 absolute right-0 top-1"
                color="#4cc9f0"
              />
            )}
          </div>
        </div>
        {idConsumer && idConsumer !== -1 && (
          <div>
            <label
              htmlFor="name"
              className="block text-gray-700 font-bold mb-2"
            >
              ACTUALIZAR A NUEVO TELEFONO
              <input
                type="checkbox"
                className="ml-3"
                onChange={() => {
                  setIsChecked(!isChecked);
                }}
              />
            </label>
          </div>
        )}
        {isChecked && (
          <div>
            <label
              htmlFor="name"
              className="block text-gray-700 font-bold mb-2"
            >
              NUEVO TELEFONO
            </label>
            <input
              className="shadow border appearance-none py-2 px-3 w-full"
              type={"text"}
              name="newPhone"
              defaultValue={queryConsumer.data?.newPhone}
              onChange={handleField}
            />
          </div>
        )}
        <div>
          <label htmlFor="name" className="block text-gray-700 font-bold mb-2">
            NOMBRE
          </label>
          <input
            className="shadow border appearance-none py-2 px-3 w-full"
            type={"text"}
            name="name"
            defaultValue={queryConsumer.data?.name}
            onChange={handleField}
          />
        </div>
        <div>
          <label
            htmlFor="address"
            className="block text-gray-700 font-bold mb-2"
          >
            DIRECCION
          </label>
          <input
            className="shadow border appearance-none py-2 px-3 w-full"
            type={"text"}
            name="address"
            defaultValue={queryConsumer.data?.address}
            onChange={handleField}
          />
        </div>
        <div>
          <label
            htmlFor="id_number"
            className="block text-gray-700 font-bold mb-2"
          >
            NUMERO DOCUMENTO
          </label>
          <input
            className="shadow border appearance-none py-2 px-3 w-full"
            type={"text"}
            name="id_number"
            defaultValue={queryConsumer.data?.id_number}
            onChange={handleField}
          />
        </div>
        <div>
          <label
            htmlFor="delivery"
            className="block text-gray-700 font-bold mb-2"
          >
            DOMICILIO
          </label>
          <input
            className="shadow border appearance-none py-2 px-3 w-full"
            type={"number"}
            name="delivery"
            defaultValue={queryConsumer.data?.delivery}
            onChange={handleField}
          />
        </div>
        <div>
          <label htmlFor="note" className="block text-gray-700 font-bold mb-2">
            NOTAS
          </label>
          <textarea
            className="shadow border appearance-none py-2 px-3 w-full"
            name="notes"
            defaultValue={queryConsumer.data?.notes}
            onChange={handleField}
          />
        </div>
        <div className="mt-4 grid grid-cols-2 gap-4 sticky bg-gray-50 bottom-0 w-[106%] p-3 -ml-[16px]">
          <Button
            onClick={closeConsumer_}
            key="cancel"
            color="rose"
            size="w-full"
            content="basic_modal.button_cancel"
          />
          <button
            className="flex my-auto h-12 from-blue-400 to-cyan-500 text-white text-base font-bold hover:shadow-md bg-gradient-to-r rounded-lg py-2 px-3 items-center text-center hover:opacity-90 disabled:opacity-50 justify-center gap-2"
            onClick={acceptConsumer}
            key="ok"
          >
            OK
          </button>
          {/* <button
            className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-8 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
            // type="submit"
            // onClick={closeConsumer}
          >
            Aceptar
          </button> */}
        </div>
      </form>
    </BasicModal>
  );
}

export default ModalConsumer;
