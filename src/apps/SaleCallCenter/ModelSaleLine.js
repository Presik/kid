// Sale model line

import ProductCombine from "./ProductCombine";
import ProductCombo from "./ProductCombo";
import ProductDiscount from "./ProductDiscount";
import proxy from "api/proxy";

const getProduct = (value, record, parentRecord) => {
  if (!value || value === "") return;
  let category_ids = [];
  if (parentRecord?.shop?.product_categories.length) {
    category_ids = parentRecord.shop.product_categories;
  }
  if (parentRecord?.shop && parentRecord?.shop["product_categories."]) {
    for (const item of parentRecord.shop["product_categories."]) {
      category_ids = [...category_ids, ...item.childs];
    }
  }
  let domain = [
    ["template.active", "=", true],
    ["active", "=", true],
    ["template.salable", "=", true],
  ];
  if (category_ids) {
    domain.push(["template.categories", "in", category_ids]);
  }

  const target_words = value.split(" ");
  target_words.forEach((tw) => {
    if (tw.length <= 1) {
      return;
    }
    const clause = [
      "OR",
      ["template.name", "ilike", `%${tw}%`],
      ["description", "ilike", `%${tw}%`],
      ["code", "ilike", `%${tw}%`],
      ["barcode", "=", `%${tw}%`],
    ];
    domain.push(clause);
  });
  console.log(domain, "tshis is dom");
  return domain;
};

const getSalePrices = async (activeRecord, parentRecord) => {
  const { data: unit_price } = await proxy.methodInstance({
    model: "product.product",
    method: "_get_sale_unit_price",
    instance: activeRecord?.product?.id ?? activeRecord.product,
    args: [activeRecord.quantity],
    ctx: {
      price_list: parentRecord?.price_list?.id,
      uom: activeRecord.unit ?? null,
      customer: parentRecord?.party?.id,
    },
  });
  if (!activeRecord.product) {
    return {};
  }

  const { data: price_w_tax } = await proxy.methodCall({
    model: "sale.sale",
    method: "dash_get_amount_w_tax",
    args: [
      {
        product: activeRecord.product["id"],
        list_price: unit_price,
      },
    ],
  });

  // const factor = activeRecord.unit.factor; // ???? bug
  return {
    unit_price: Number(unit_price).toFixed(4),
    price_w_tax: Number(price_w_tax).toFixed(2),
  };
};

const onChangeProduct = async (activeRecord, parentRecord) => {
  // const priceList = parentRecord?.price_list;
  const product = activeRecord?.product;
  if (!product) return [{}, {}];
  let unit = product["sale_uom."];

  let { unit_price, price_w_tax } = await getSalePrices(
    activeRecord,
    parentRecord,
  );

  console.log(unit_price, price_w_tax, " validate ");
  const upToStore = {
    unit_price: unit_price,
    base_price: unit_price,
    unit: unit.id,
    description: product.name,
  };
  const upToRecord = {
    unit_price: unit_price,
    base_price: unit_price,
    unit: unit,
    amount: price_w_tax,
    total_amount: price_w_tax,
  };
  return [upToStore, upToRecord];
};

// const onChangeUnit = async (storeRec, activeRecord, parentRecord) => {
//   let { unit_price, price_w_tax } = await getSalePrices(
//     storeRec,
//     activeRecord,
//     parentRecord,
//   );
//   storeRec.unit_price = unit_price;
//   activeRecord.unit_price = unit_price;
//   storeRec.amount = price_w_tax;
//   activeRecord.amount = price_w_tax;
//   return storeRec, activeRecord;
// };

const onChangeQty = (activeRecord) => {
  let upToactiveRecord = {};
  if (activeRecord.quantity && activeRecord.amount) {
    const qty = activeRecord.quantity;
    if (activeRecord.discount) {
      const { amount, discount } = activeRecord;
      const amount_ = amount * qty - (amount * qty * discount) / 100;
      upToactiveRecord.total_amount = amount_;
    } else {
      upToactiveRecord.total_amount = activeRecord.amount * qty;
    }
  }

  return [{}, upToactiveRecord];
};

const searchUnit = (value, record) => {
  let dom = [["category", "=", record?.unit?.category]];
  if (value) {
    dom.push(["name", "ilike", `%${value}%`]);
  }
  return dom;
};

const getTotalAmount = (activeRecord) => {
  let total_amount = 0;
  if (activeRecord.amount && activeRecord.quantity) {
    total_amount = activeRecord.amount * activeRecord.quantity;
  }
  return total_amount;
};

const onChangeDeliveryFee = async (activeRecord) => {
  let upToStore = {};
  let upToActive = {};
  const { delivery_fee, quantity } = activeRecord;
  if (delivery_fee && quantity) {
    upToActive.total_amount = delivery_fee * quantity;
    upToActive.amount = delivery_fee;
    upToStore.unit_price = delivery_fee;
    upToStore.base_price = delivery_fee;
  }
  return [upToStore, upToActive];
};

const getView = () => {
  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
  let DictCtxView = {
    selectable: "one",
    model: "sale.line",
    row_selectable: false,
    table_action: ["delete", "add", "edit"],
    form_action: ["add", "edit"],
    target: "sale",
    webfields: {
      product: {
        type: "many2one",
        model: "product.product",
        recSearch: getProduct,
        withChange: onChangeProduct,
        attrs: [
          "id",
          "list_price",
          "name",
          "sale_price_taxed",
          "sale_uom.rec_name",
          "categories",
          "products_mix",
        ],
        required: true,
      },
      unit: {
        type: "many2one",
        model: "product.uom",
        recSearch: searchUnit,
        // withChange: onChangeUnit,
        readOnly: true,
        attrs: ["id", "name", "category", "factor"],
      },
      quantity: {
        type: "number",
        withChange: onChangeQty,
        required: true,
        default: 1,
      },
      note: { type: "text-area" },
      unit_price: { type: "number", readOnly: true },
      delivery_fee: {
        type: "number",
        visible: (rec) => rec.set_delivery_fee,
        withChange: onChangeDeliveryFee,
      },
      set_delivery_fee: { type: "boolean" },
      amount: { type: "number", readOnly: true },
      total_amount: {
        type: "number",
        readOnly: true,
        function: getTotalAmount,
      },
    },
    webtree: [
      { name: "product", width: "40%" },
      { name: "quantity", width: "10%" },
      { name: "amount", width: "23%" },
      { name: "total_amount", width: "23%" },
    ],
    webform: [
      { name: "product" },
      { name: "quantity", widget: "increment" },
      { name: "amount" },
      { name: "total_amount" },
      { name: "note" },
      {
        id: "fee",
        grid: [{ name: "set_delivery_fee" }, { name: "delivery_fee" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "buttons",
        grid: [
          {
            type: "button-custom-modal",
            Component: ProductCombo,
            color: "lime",
            icon: "fi fi-bs-hamburger-soda",
            name: "button_combo",
            button_ok: false,
          },
          {
            type: "button-custom-modal",
            Component: ProductCombine,
            color: "indigo",
            icon: "fi fi-rr-pizza-slice",
            name: "button_combine",
            button_ok: false,
          },
          {
            type: "button-custom-modal",
            Component: ProductDiscount,
            color: "rose",
            icon: "fi fi-rr-badge-percent",
            name: "button_discount",
            button_ok: false,
          },
        ],
        size: [1, 3],
        span: "col-span-2",
      },
    ],
  };
  return DictCtxView;
};

export default { ctxView: getView };
