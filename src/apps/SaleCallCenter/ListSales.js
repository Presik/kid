import React, { useState } from "react";
import store from "store";
import date from "date-and-time";

import ctxViewSale from "./ModelViewSale";
import QuickTable from "components/QuickTable";
import QuickForm from "components/QuickForm";
import TimeLine from "./TimeLine";

function ListOrder() {
  // const [saleId, setSaleId] = useState(null);
  const session = store.get("ctxSession");
  const [viewType, setViewType] = useState("list");
  const [activeRecord, setActiveRecord] = useState({});
  const [storeRec, setStoreRec] = useState({});
  const [action, setAction] = useState(null);
  const today = date.format(
    date.addHours(new Date(), -12, true),
    "YYYY-MM-DD HH:mm:ss",
  );

  // const today_ = tools.fmtDate2Tryton(today);
  const domain = [
    ["create_date", ">=", today],
    [
      "order_status",
      "in",
      ["draft", "commanded", "in_preparation", "dispatched", "requested"],
    ],
  ];

  async function onChangeView(event, action, rec) {
    let viewType_ = viewType;
    if (["edit", "add"].includes(action)) {
      viewType_ = "form";
    } else if (["editModal", "addModal"].includes(action)) {
      viewType_ = "modalForm";
    } else {
      viewType_ = "list";
    }
    if (!rec) {
      setActiveRecord({});
      setStoreRec({});
    } else {
      setActiveRecord(rec);
      setStoreRec({ id: rec.id });
    }

    setViewType(viewType_);
  }

  function onClickAction(event, action) {
    setAction(action);
    setTimeout(() => {
      setAction(null);
    }, 3000);
  }

  return (
    <div className="mt-4">
      {viewType === "list" ? (
        <QuickTable
          onChangeView={onChangeView}
          ctxView={ctxViewSale}
          domain={domain}
        />
      ) : (
        <>
          <TimeLine activeRecord={activeRecord} />
          <QuickForm
            activeRecord={activeRecord}
            storeRecord={storeRec}
            ctxView={ctxViewSale}
            action={action}
            onChangeView={onChangeView}
          />
        </>
      )}
    </div>
  );
}

export default ListOrder;
