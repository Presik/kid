// Sale model line

import dates from "tools/dates";
const today = new Date();

function formatTime(seconds) {
  const hours = Math.floor(seconds / 1000 / 60 / 60);
  const remainingMinutes = Math.floor(seconds / 1000 / 60) % 60;
  const remainingSeconds = Math.floor((seconds / 1000) % 60);

  const formattedHours = hours.toString().padStart(2, "0");
  const formattedMinutes = remainingMinutes.toString().padStart(2, "0");
  const formattedSeconds = remainingSeconds.toString().padStart(2, "0");

  return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
}

function get_time(rec) {
  let requested =
    dates.getTrytonDateTime2Js(rec["order_status_time."]?.requested, true) ??
    today;
  let commanded =
    dates.getTrytonDateTime2Js(rec["order_status_time."]?.commanded, true) ??
    today;

  const time = formatTime(commanded.getTime() - requested.getTime());
  return time;
}

const CtxViewLine = {
  form_action_add: "modal",
  model: "sale.line",
  row_selectable: false,
  table_action: [],
  form_action: [],
  webfields: {
    product: {
      type: "many2one",
      model: "product.product",
      required: true,
    },
    unit: {
      type: "many2one",
      model: "product.uom",
      readOnly: true,
    },
    quantity: {
      type: "number",
      required: true,
      default: 1,
    },
    note: { type: "text-area" },
    unit_price: { type: "number", readOnly: true },
    amount: { type: "number", readOnly: true },
    amount_w_tax: { type: "number", readOnly: true },
  },
  webtree: [
    { name: "product", width: "30%" },
    { name: "quantity", width: "10%" },
    { name: "amount", width: "15%" },
    { name: "amount_w_tax", width: "15%" },
    { name: "note", width: "30%" },
  ],
  webform: [
    { name: "product" },
    { name: "quantity" },
    { name: "amount" },
    { name: "amount_w_tax" },
    // { name: "comment" },
  ],
};

const stateColors = {
  draft: "rose",
  requested: "slate",
  commanded: "lime",
  dispatched: "sky",
};

const ctxViewSale = {
  row_selectable: false,
  // activeSearch: true,
  // filters: getFilters,
  // title: { model: true, field: "number" },
  tags: {
    order_status: stateColors,
  },
  limit: 200,
  model: "sale.sale",
  form_action: ["edit"],
  table_action: ["edit"],
  webfields: {
    number: { type: "char", readOnly: true },
    delivery: { type: "money", readOnly: true },
    party: {
      type: "many2one",
      model: "party.party",
      readOnly: { state: ["quotation"] },
    },
    source: {
      type: "many2one",
      model: "sale.source",
      readOnly: true,
    },
    consumer: {
      type: "many2one",
      model: "party.consumer",
      required: true,
    },
    saleman: {
      type: "many2one",
      model: "company.employee",
      readOnly: true,
    },
    lines: {
      type: "one2many",
      model: "sale.line",
      ctxView: CtxViewLine,
      readOnly: true,
    },
    sale_date: {
      type: "date",
      readOnly: true,
    },
    state: {
      type: "char",
      readOnly: true,
      translate: true,
      default: "draft",
    },
    order_status: {
      type: "char",
      readOnly: true,
      translate: true,
      default: "draft",
      tags: stateColors,
      // attrs: ["order_status_time.requested", "order_status_time.commanded"],
    },
    shop: {
      type: "many2one",
      model: "sale.shop",
      readOnly: true,
    },
    delivery_amount: { type: "number", readOnly: true },
    total_amount: { type: "number", readOnly: true },
    description: { type: "char", readOnly: true },
    kind: { type: "char", readOnly: true, translate: true },
    comment: { type: "text-area", readOnly: true },
    "order_status_time.requested": {
      type: "datetime",
      search: true,
      // function: get_time_requested,
    },
    "order_status_time.commanded": {
      type: "datetime",
      search: true,
      // function: get_time_commanded,
    },
    elapsed_time: {
      type: "char",
      function: get_time,
    },
  },
  webtree: [
    { name: "number", width: "20%" },
    { name: "sale_date", width: "25%" },
    { name: "shop", width: "25%" },
    { name: "party", width: "25%" },
    { name: "consumer", width: "25%" },
    { name: "salesman", width: "25%" },
    {
      name: "order_status",
      width: "30%",
      widget: "badge",
    },
    { name: "delivery_amount", width: "25%" },
    { name: "total_amount", width: "25%" },
    {
      name: "order_status_time.requested",
      width: "15",
      formatString: "HH:mm:ss",
    },
    {
      name: "order_status_time.commanded",
      width: "15",
      formatString: "HH:mm:ss",
    },
    { name: "elapsed_time", width: "15" },
  ],
  webform: [
    { name: "source" },
    { name: "shop" },
    { name: "kind" },
    { name: "consumer" },
    { name: "party" },
    { name: "sale_date" },
    { name: "delivery_amount" },
    { name: "lines", component: "modal" },
    { name: "total_amount" },
    { name: "order_status" },
    { name: "comment" },
  ],
};
export default ctxViewSale;
