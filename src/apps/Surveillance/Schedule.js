import React, { useState, useEffect } from "react";
import { FormattedMessage as FM } from "react-intl";
import date from "date-and-time";
import store from "store";

import QuickTable from "components/QuickTable";
import StdButton from "components/StdButton";
import IconButton from "components/IconButton";
import TwComboBox from "components/TwComboBox";
import ModalForm from "components/Modals/ModalForm";
import MenuScheduleReport from "./MenuScheduleReport";
import ModelShift from "./ModelShift";
import Sheet from "./Sheet";
import { useFormStore } from "store/formStore";
import funcs from "tools/functions";
import proxy from "api/proxy";
import modelLocation from "./ModelLocation";

const locationView = modelLocation.ctxView();
const HISTORY_PERIODS = -290;

function SurveillanceSchedule(props) {
  let [modeView, setModeView] = useState("locations"); // list or sheet
  let [messageCtx, setMessageCtx] = useState(null);
  let [domLocations, setDomLocations] = useState(null);
  let [oc, setOC] = useState(null);
  let [ocs, setOCs] = useState([]);
  let [period, setPeriod] = useState(null);
  let [periods, setPeriods] = useState(null);
  let [btnStartDisabled, setBtnStartDisabled] = useState(true);
  let [openModalShift, setOpenModalShift] = useState(false);
  let [selectedRows, setSelectedRows] = useState(new Map());
  let [filter, setFilter] = useState("");

  const { upActiveRecord, upStoreRecord, setActiveRecordFromId, resetRecord } =
    useFormStore();

  const multiselect = true;
  let toUpdate = new Map();
  let toRemove = [];
  let toCreate = {};

  useEffect(() => {
    _setPeriods();
    _setOCs();
  }, [props]);

  const _setPeriods = async () => {
    let start_period = date.addDays(new Date(), HISTORY_PERIODS);
    start_period = date.format(start_period, "YYYY-MM-DD");
    const dom = [["start_date", ">=", start_period]];
    const { data } = await proxy.search("account.period", dom, ["name"]);
    if (data) {
      const periods = funcs.recs2Combobox(data);
      setPeriods(periods);
      setPeriod(periods[0]["id"]);
    }
  };

  const onSetLocations = async (target) => {
    let center = target.oc;
    if (!center) {
      center = oc;
    }
    if (!center) {
      return;
    }
    const shDom = [
      ["operation_center", "=", center],
      ["period", "=", period],
    ];
    const { data: activesSch } = await proxy.search(
      "surveillance.schedule",
      shDom,
      ["location"],
    );
    const activesLocations = activesSch.map((val) => val.location);
    const dom = [["id", "in", activesLocations]];
    setDomLocations(dom);
  };

  const onStartSchedule = (target) => {
    setBtnStartDisabled(true);
    if (selectedRows.size > 0) {
      setModeView("sheet");
      setFilter("");
    } else if (target !== "") {
      setFilter(target);
      setModeView("sheet");
    }
  };

  const updateCtx = (type, value) => {
    if (type === "toRemove") {
      toRemove = value;
    } else if (type === "toCreate") {
      toCreate = value;
    } else if (type === "toUpdate") {
      toUpdate = value;
    } else {
      setMessageCtx(null);
    }
    setFilter("");
  };

  const toSave = async () => {
    const model = "surveillance.schedule.shift";
    if (toUpdate.size > 0) {
      const values = toUpdate.values();
      for (const val of values) {
        delete val.readOnly;
        const data = {
          model: model,
          storeRec: val,
        };
        await proxy.saveQuery(data);
      }
    }

    const messageCtx = {
      type: "msgInfo",
      msg: "board.records_saved",
    };

    setMessageCtx(messageCtx);
    if (Object.keys(toCreate).length > 0) {
      for (const rec of Object.values(toCreate)) {
        delete rec.id;
        delete rec.location;
        delete rec.readOnly;
        await proxy.create(model, rec);
      }
    }
    if (toRemove.length > 0) {
      const args = { model: model, ids: toRemove };
      await proxy.remove(args);
    }

    await toReload();
  };

  const onChangePeriod = (event, data) => {
    setPeriod(data.value);
    onSetLocations({ period: data.value });
  };

  const onChangeOC = (event, data) => {
    setOC(data.value);
    onSetLocations({ oc: data.value });
  };

  const toAdd = async () => {
    const newRecord = {
      id: 0,
    };
    setOpenModalShift(true);
    // setActiveRecordFromId(newRecord);
    upActiveRecord(newRecord);
    upStoreRecord(newRecord);
  };

  const toReload = async () => {
    toCreate = {};
    toRemove = [];
    toUpdate = new Map();
    setMessageCtx(null);
  };

  const _setOCs = async () => {
    // Get operation centers permitted for user
    const session = store.get("ctxSession");
    const domOC = [["id", "=", session.user]];
    const { data } = await proxy.search("res.user", domOC, [
      "name",
      "operation_centers",
    ]);
    if (data.length === 0) {
      return;
    }
    const op_centers_ids = data[0]["operation_centers"];
    const dom = [["id", "in", op_centers_ids]];
    const model = "company.operation_center";
    const { data: res } = await proxy.search(model, dom, ["name"]);
    if (res) {
      setOCs(funcs.recs2Combobox(res));
    }
  };

  function onDoubleClickCell(rec) {
    const ctxView = ModelShift.ctxView();
    setActiveRecordFromId(rec.id, ctxView.model, ctxView);
    setOpenModalShift(true);
  }

  const closeEditModal = () => {
    setOpenModalShift(false);
    resetRecord();
  };

  const toCloseSheet = () => {
    setModeView("locations");
    setSelectedRows(new Map());
    setMessageCtx(null);
  };

  function onDoubleClickRow(rec) {
    if (rec && rec.id) {
      props.onSelectedRecord(rec);
      props.onClose();
    }
  }

  function onClickRow(rec) {
    if (rec && multiselect) {
      if (selectedRows.has(rec.id)) {
        selectedRows.delete(rec.id);
      } else {
        selectedRows.set(rec.id, rec);
      }
    }
    if (selectedRows.size > 0) {
      setBtnStartDisabled(false);
    } else {
      setBtnStartDisabled(true);
    }
  }

  // function handleMultiselect(name) {
  //   if (name === "selectAll") {
  //     for (const record of records) {
  //       onClickRow(record[1]);
  //     }
  //   }
  // }

  function getActiveLocations() {
    let _locations = Array.from(selectedRows.values());
    _locations = _locations.map((loc) => loc.id);
    return _locations;
  }

  return (
    <div className="w-full h-full px-4">
      {messageCtx && (
        <div className="" id={`surveillance-${messageCtx.type}`}>
          <p className="">
            <FM id={messageCtx.msg} />
          </p>
        </div>
      )}
      <div className="flex gap-4 my-2">
        <div className="flex">
          <TwComboBox
            name="period"
            placeholder="Periodo"
            onChange={onChangePeriod}
            options={periods}
            value={store.period}
          />
        </div>
        <div className="flex justify-center">
          <TwComboBox
            name="oc"
            placeholder="Centro de Op."
            onChange={onChangeOC}
            options={ocs}
            value={store.oc}
          />
        </div>
        <div className="flex justify-center">
          <StdButton
            color="green"
            onClick={onStartSchedule}
            style="w-full"
            disabled={btnStartDisabled}
            content={"schedule.scheduling"}
          />
        </div>
        {modeView == "sheet" && (
          <>
            <IconButton
              key="return"
              color="rose"
              onClick={toCloseSheet}
              name="fi-br-cross-small"
              content="Return"
            />
            <StdButton
              color="blue"
              onClick={toAdd}
              size="w-40"
              iconRight="fi fi-br-add"
              content="board.button_add"
            />
            <StdButton
              color="amber"
              onClick={toSave}
              size="w-40"
              iconLeft="fi-sr-disk"
              content={"board.button_save"}
            />
            <MenuScheduleReport />
            {openModalShift && (
              <ModalForm
                open={openModalShift}
                onClose={closeEditModal}
                ctxView={ModelShift.ctxView()}
                level="main"
              />
            )}
          </>
        )}
      </div>
      <div>
        {modeView == "locations" && domLocations && (
          <QuickTable
            model={"surveillance.location"}
            ctxView={locationView}
            domain={domLocations}
            onDoubleClickRow={onDoubleClickRow}
            onClickRow={onClickRow}
          />
        )}
        {modeView == "sheet" && (
          <Sheet
            updateCtx={updateCtx}
            locations={getActiveLocations()}
            onDoubleClickCell={onDoubleClickCell}
            filter={filter}
            oc={oc}
            period={period}
          />
        )}
      </div>
    </div>
  );
}

export default SurveillanceSchedule;
