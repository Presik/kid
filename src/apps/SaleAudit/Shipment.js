import React, { Fragment } from "react";
import QuickTable from "components/QuickTable";
import { useRecords } from "hooks/records";
import funcs from "tools/functions";
import FullTable from "components/FullTable";

const ctxView = {
  model: "stock.shipment.out",
  selectable: false, // options: multi - one - false
  // activeSearch: true,
  // filters: getFilters,
  // pagination: [["sale_date", ">=", "2023-07-01"]],
  // domain: [["sale_date", ">=", "2023-06-01"]],
  limit: 50,
  form_action: [],
  table_action: [],
  webfields: {
    number: { type: "char", readOnly: true },
    reference: { type: "char", readOnly: true },
    effective_date: {
      type: "date",
      readOnly: true,
    },
    planned_date: {
      type: "date",
      readOnly: true,
    },
    state: {
      type: "char",
      readOnly: true,
      translate: true,
    },
    invoice_state: {
      type: "char",
      readOnly: true,
      translate: true,
    },
    warehouse_storage: {
      type: "many2one",
      model: "stock.location",
    },
    // description: { type: "char", readOnly: true },
  },
  webtree: [
    { name: "number", width: "20%" },
    { name: "reference", width: "20%" },
    { name: "effective_date", width: "20%" },
    { name: "planned_date", width: "20%" },
    { name: "state", width: "20%" },
    { name: "warehouse_storage", width: "20%" },
    // { name: "description", width: "35%" },
  ],
};

function Shipment({ record, name }) {
  const domain = [["id", "in", record[name] ?? []]];
  const model =
    name === "shipments" ? "stock.shipment.out" : "stock.shipment.out.return";
  const fields_names = funcs.getViewFields(ctxView, "list");
  let { data, isLoading } = useRecords(model, domain, fields_names);

  const ctxViewShipment = { ...ctxView, domain, model };
  return (
    <Fragment>
      <FullTable
        records={data ? data : []}
        model={model}
        ctxView={ctxViewShipment}
      />
    </Fragment>
  );
}

export default Shipment;
