// Sale model
import store from "store";

import saleLine from "./ModelSaleLine";
import tools from "tools/dates";
import proxy from "api/proxy";
import Shipment from "./Shipment";
import Invoice from "./Invoice";

let today = tools.dateToday();

const searchParty = (value) => {
  const session = store.get("ctxSession");
  let search = [
    "OR",
    [["agent.user", "=", session.user]],
    [["agent.user", "=", session.user]],
  ];
  if (value) {
    search[1].push(["name", "ilike", `%${value}%`]);
    search[2].push(["id_number", "ilike", `${value}%`]);
  }
  return search;
};

const getFilters = () => {
  return {
    today: [
      [
        "OR",
        ["sale_date", "=", `${today}`],
        ["create_date", ">=", `${today} 00:00:00`],
      ],
    ],
    pending: [
      "OR",
      ["state", "in", ["draft", "quotation"]],
      ["shipments.state", "not in", ["done", "cancelled"]],
    ],
    quotation: [["state", "=", "quotation"]],
    processing: [["state", "=", "processing"]],
    draft: [["state", "=", "draft"]],
  };
};
const stateColors = {
  none: "slate",
  quotation: "amber",
  processing: "sky",
  done: "lime",
  canceled: "rose",
};

const getView = (config) => {
  let DictCtxView = {
    selectable: false, // options: multi - one - false
    activeSearch: true,
    filters: getFilters,
    pagination: [],
    domain: [],
    tags: {
      state: stateColors,
    },
    limit: 50,
    form_action: ["open"],
    table_action: ["open"],
    model: "sale.sale",
    webfields: {
      number: { type: "char", readOnly: true, searchable: true },
      party: {
        type: "many2one",
        model: "party.party",
        required: true,
        readOnly: true,
        searchable: true,
      },
      sale_date: {
        type: "date",
        readOnly: true,
        searchable: true,
      },
      shipment_date: {
        type: "date",
        required: false,
        readOnly: true,
      },
      agent: {
        type: "many2one",
        model: "commission.agent",
        readOnly: true,
      },
      state: {
        type: "char",
        readOnly: true,
        translate: true,
        tags: stateColors,
      },
      invoice_state: {
        type: "char",
        readOnly: true,
        translate: true,
      },
      invoices: {
        type: "char",
        readOnly: true,
        color: "blue",
        search: true,
        desired_action: "count",
        Component: Invoice,
      },
      shipments: {
        type: "char",
        readOnly: true,
        color: "blue",
        search: true,
        desired_action: "count",
        Component: Shipment,
      },
      shipment_returns: {
        type: "char",
        readOnly: true,
        color: "blue",
        search: true,
        desired_action: "count",
        Component: Shipment,
      },
      shipment_state: {
        type: "char",
        readOnly: true,
        translate: true,
      },
      lines: {
        type: "one2many",
        model: "sale.line",
        ctxView: saleLine.ctxView(config),
        required: true,
        readOnly: { state: ["quotation"] },
      },
      total_amount: {
        type: "number",
        readOnly: true,
      },
      comment: { type: "text-area", readOnly: true },
      description: { type: "char", readOnly: true },
    },
    webtree: [
      { name: "number", width: "20%" },
      { name: "sale_date", width: "20%" },
      { name: "party", width: "35%" },
      { name: "agent", width: "35%" },
      { name: "total_amount", width: "10%" },
      { name: "state", width: "10%", widget: "circle" },
      { name: "invoices", width: "10%", widget: "button-custom-modal" },
      // { name: "value_invoiced", width: "10%" },
      // { name: "value_to_invoiced", width: "10%" },
      { name: "invoice_state", width: "10%" },
      { name: "shipments", width: "10%", widget: "button-custom-modal" },
      { name: "shipment_returns", width: "10%", widget: "button-custom-modal" },
      { name: "shipment_state", width: "10%" },
    ],
    webform: [
      { name: "agent" },
      {
        id: "infoSale",
        grid: [{ name: "number" }, { name: "shipment_date" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "party" },
      { name: "shipment_address" },
      { name: "price_list" },
      { name: "description" },
      { name: "lines", component: "modal" },
      { name: "comment" },
      { name: "total_amount" },
      { name: "state" },
      { name: "quote" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
