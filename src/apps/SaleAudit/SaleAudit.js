import React, { Fragment } from "react";

import Board from "components/Board";
import sale from "./ModelSale";
// import DateRangeFilter from "components/DateRangeFilter";

const SaleAudit = ({ config }) => {
  // function handleChangeDate(params) {
  //   console.log(params);
  // }

  return (
    // <div className="flex flex-col w-full my-4 pr-8">
    //   <DateRangeFilter action={handleChangeDate} />
    <Board ctxView={sale.ctxView(config)} />
    // </div>
  );
};

export default SaleAudit;
