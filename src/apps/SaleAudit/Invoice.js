import React, { Fragment } from "react";
import { useRecords } from "hooks/records";
import FullTable from "components/FullTable";

import funcs from "tools/functions";
import { classNames } from "tools/ui";
import {
  ArrowTrendingDownIcon,
  ArrowTrendingUpIcon,
  BanknotesIcon,
  CurrencyDollarIcon,
} from "@heroicons/react/20/solid";

const ctxView = {
  model: "account.invoice",
  selectable: false, // options: multi - one - false
  // activeSearch: true,
  // filters: getFilters,
  // pagination: [["sale_date", ">=", "2023-07-01"]],
  // domain: [["sale_date", ">=", "2023-06-01"]],
  limit: 50,
  form_action: [],
  table_action: [],
  webfields: {
    number: { type: "char", readOnly: true },
    party: {
      type: "many2one",
      model: "party.party",
      required: true,
      readOnly: true,
    },
    invoice_date: {
      type: "date",
      readOnly: true,
    },
    state: {
      type: "char",
      readOnly: true,
      translate: true,
    },
    invoice_state: {
      type: "char",
      readOnly: true,
      translate: true,
    },
    total_amount: {
      type: "number",
      readOnly: true,
    },
    amount_to_pay: {
      type: "number",
      readOnly: true,
    },
    description: { type: "char", readOnly: true },
  },
  webtree: [
    { name: "number", width: "20%" },
    { name: "invoice_date", width: "20%" },
    { name: "state", width: "20%" },
    { name: "party", width: "35%" },
    { name: "description", width: "35%" },
    { name: "total_amount", width: "10%" },
    { name: "amount_to_pay", width: "10%" },
  ],
};

const style =
  "flex flex-col flex-auto p-4 space-y-4 rounded-md divide-y-2 items-center h-24 bg-white shadow-md border-2 border-slate-200";
const formatter = Intl.NumberFormat("en-US");

function Invoice({ record }) {
  console.log(record, "inrgsa a render");
  const domain = [["id", "in", record.invoices ?? []]];
  let fields_names = funcs.getViewFields(ctxView, "list");
  fields_names = fields_names.concat([
    "lines.product.cost_price",
    "lines.amount",
    "lines.quantity",
  ]);

  let { data, isLoading } = useRecords(ctxView.model, domain, fields_names);

  let totalAmountSum = 0;
  let amountToPaySum = 0;
  let basePrice = 0;
  let baseCostPrice = 0;
  for (const invoice of data ?? []) {
    totalAmountSum += invoice.total_amount;
    amountToPaySum += invoice.amount_to_pay;
    console.log(invoice["lines."]);
    for (const line of invoice["lines."]) {
      console.log(line["product."]["cost_price"], "nm");
      basePrice += line.amount;
      baseCostPrice += line.quantity * line["product."]["cost_price"];
    }
  }

  let profit = 0;
  let amountProfit = 0;
  if (basePrice > 0 && baseCostPrice > 0) {
    amountProfit = Number(basePrice - baseCostPrice);
    profit = Number((amountProfit / basePrice) * 100).toFixed(2);
    amountProfit = formatter.format(amountProfit.toFixed(2));
  }
  const colorProfit = profit > 0 ? "text-green-500" : "text-rose-500";

  const percentageInvoiced = Number(
    (totalAmountSum * 100) / record?.total_amount,
  ).toFixed(2);
  const colorPercentInv =
    percentageInvoiced > 90 ? "text-green-500" : "text-rose-500";

  const percentagePaid = totalAmountSum
    ? Number((amountToPaySum * 100) / totalAmountSum).toFixed(2)
    : 0;
  const colorPercentPaid =
    percentageInvoiced <= 0 ? "text-green-500" : "text-rose-500";

  totalAmountSum = formatter.format(Number(totalAmountSum));

  amountToPaySum = formatter.format(Number(amountToPaySum));

  return (
    <Fragment>
      <div className="grid grid-cols-4 m-4 space-x-6 items-stretch">
        <div className="relative flex flex-col shadow-md px-4 pt-10 pb-4 rounded-md">
          <div className="flex flex-col">
            <span className="text-indigo-950 font-semibold text-xl">
              {formatter.format(record?.total_amount)}
            </span>
            <span className="text-slate-400 font-medium">Vendido </span>
          </div>
        </div>
        <div className="relative flex flex-col shadow-md px-4 pt-10 pb-4 rounded-md">
          <div className="flex flex-col">
            <span className="text-indigo-950 font-semibold text-xl">
              {totalAmountSum}
            </span>
            <span className="text-slate-400 font-medium">Facturado</span>
          </div>
          <BanknotesIcon
            className={classNames(
              "h5 w-5 absolute inset-y-2 left-4",
              colorPercentInv,
            )}
          />
          <span
            className={classNames(
              colorPercentInv,
              "font-semibold absolute inset-y-2 right-4",
            )}
          >
            {formatter.format(percentageInvoiced)}%
          </span>
        </div>
        <div className="relative flex flex-col shadow-md px-4 pt-10 pb-4 rounded-md">
          <div className="flex flex-col">
            <span className="text-indigo-950 font-semibold text-xl">
              {amountToPaySum}
            </span>
            <span className="text-slate-400 font-medium">Pend. a Pagar </span>
          </div>
          <CurrencyDollarIcon
            className={classNames(
              "h5 w-5 absolute inset-y-2 left-4",
              colorPercentPaid,
            )}
          />
          <span
            className={classNames(
              colorPercentPaid,
              "font-semibold absolute inset-y-2 right-4",
            )}
          >
            {formatter.format(percentagePaid)}%
          </span>
        </div>
        <div className="relative flex flex-col shadow-md px-4 pt-10 pb-4 rounded-md">
          <div className="flex flex-col">
            <span className="text-indigo-950 font-semibold text-xl">
              {amountProfit}
            </span>
            <span className="text-slate-400 font-medium">Utilidad</span>
          </div>
          {profit <= 0 ? (
            <ArrowTrendingDownIcon
              className={classNames(
                "h5 w-5 absolute inset-y-2 left-4",
                colorProfit,
              )}
            />
          ) : (
            <ArrowTrendingUpIcon
              className={classNames(
                "h5 w-5 absolute inset-y-2 left-4",
                colorProfit,
              )}
            />
          )}
          <span
            className={classNames(
              "font-semibold absolute inset-y-2 right-4",
              colorProfit,
            )}
          >
            {profit}%
          </span>
        </div>
      </div>
      <FullTable
        records={data ? data : []}
        model={ctxView.model}
        ctxView={ctxView}
      />
    </Fragment>
  );
}

export default Invoice;
