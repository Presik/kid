// Stock Move Line
import store from "store";

export default {
  model: "stock.move",
  row_selectable: false,
  form_action: [],
  table_action: [],
  target: "shipment",
  webfields: {
    product: {
      type: "many2one",
      model: "product.product",
      attrs: ["id", "default_uom.name", "name"],
      required: true,
    },
    quantity: { type: "float", decimalPlaces: 2, required: true },
    uom: { type: "many2one", model: "product.uom", readOnly: true },
    company: {
      default: store.get("ctxSession") && store.get("ctxSession").company,
    },
    from_location: {
      type: "many2one",
      model: "stock.location",
    },
    to_location: {
      type: "many2one",
      model: "stock.location",
    },
  },
  webtree: [
    { name: "product", width: "60%" },
    { name: "uom", width: "10%" },
    { name: "quantity", width: "20%" },
  ],
  webform: [{ name: "product" }, { name: "uom" }, { name: "quantity" }],
};
