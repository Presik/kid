// Room Housekeeping model
import React from "react";
import date from "date-and-time";

import sleepImg from "./sleep.png";
import travellerImg from "./traveller.png";
import pillowImg from "./pillow.png";
import checkedImg from "./checked.png";
import enterImg from "./enter.png";
import exitImg from "./exit.png";
import { classNames } from "tools/ui";

const base =
  "w-full h-full p-3 mb-3 border-2 border-zinc-300 rounded-xl border-dashed text-center";

const stateColors = {
  clean: "sky",
  dirty: "rose",
  not_authorized: "lime",
  maintenance: "gray",
};

const getFilters = () => {
  return {
    clean: [["state", "=", "clean"]],
    dirty: [["state", "=", "dirty"]],
    check_out_today: [["check_out_today", "!=", null]],
    all: [[]],
  };
};

function getCheckinWidget(record) {
  return getWidget("check_in", record);
}

function getCheckoutWidget(record) {
  return getWidget("check_out", record);
}

function getWidget(kind, record) {
  let title;
  let iconStatus = pillowImg;
  let personImg;
  let check_time;
  let size = "w-10 h-10 ";
  let titleStyle = "text-zinc-300";
  const {
    occupied_today,
    arrival_today,
    check_in_today,
    departure_today,
    check_out_today,
  } = record;
  if (kind === "check_in") {
    title = "Check In";
    if (!occupied_today) {
      if (arrival_today) {
        personImg = travellerImg;
        iconStatus = enterImg;
        titleStyle = "text-zinc-600";
        if (check_in_today) {
          iconStatus = checkedImg;
          check_time = check_in_today;
        }
      } else {
        size = "w-16 h-16";
      }
    } else {
      iconStatus = sleepImg;
      size = "w-16 h-16";
    }
  } else {
    title = "Check Out";
    if (!occupied_today) {
      if (departure_today) {
        personImg = sleepImg;
        iconStatus = exitImg;
        titleStyle = "text-zinc-600";
        if (check_out_today) {
          iconStatus = checkedImg;
          check_time = check_out_today;
        }
      } else {
        size = "w-16 h-16";
      }
    } else {
      iconStatus = sleepImg;
      size = "w-16 h-16";
    }
  }
  if (check_time) {
    check_time = date.parse(check_time, "HH:mm:ss", true);
    check_time = date.format(check_time, "hh:mm A");
  } else if (
    (departure_today && kind === "check_out") ||
    (arrival_today && kind === "check_in")
  ) {
    check_time = "_ _ : _ _";
  }

  if (record)
    return (
      <div key="widget-custom" className={base}>
        <h2 className={classNames("text-3xl mb-3 font-extrabold", titleStyle)}>
          {" "}
          {title}
        </h2>
        <div key="widget-custom-check" className="flex justify-center">
          {personImg && (
            <img key="sleep" className="w-20 h-20 mx-2" src={personImg} />
          )}
          <img
            key="icon-status"
            className={classNames("my-auto mx-4", size)}
            src={iconStatus}
          />
          {check_time && (
            <h2 className="my-auto text-3xl mx-2 text-stone-500 font-extrabold">
              {check_time}
            </h2>
          )}
        </div>
      </div>
    );
}

const getView = () => {
  let DictCtxView = {
    model: "hotel.room",
    table_action: ["open"], // options: ['open', 'delete', 'edit', 'add']
    form_action: ["edit"], // options: ['open', 'delete', 'edit', 'add']
    card_action: ["open"], // options: ['open', 'delete', 'edit', 'add']
    defaultView: "cards",
    filters: getFilters,
    activeSearch: true,
    domain: [], // Options: null or valid domain
    orderBy: [["code", "ASC"]],
    selectable: null, // Options for table rows: null, multi, one
    title: { field: "rec_name" },
    tags: {
      state: stateColors,
    },
    webfields: {
      name: { type: "char", readOnly: true, searchable: true },
      notes: { type: "text-area" },
      classification: {
        type: "many2one",
        model: "hotel.room.classification",
        readOnly: true,
      },
      cleaning_type: {
        type: "many2one",
        model: "hotel.room.cleaning_type",
        readOnly: true,
      },
      location: {
        type: "many2one",
        model: "hotel.location",
        readOnly: true,
      },
      housekeeping: {
        type: "many2one",
        model: "company.employee",
        readOnly: true,
      },
      state: {
        type: "char",
        label: false,
        readOnly: true,
        translate: true,
        tags: stateColors,
      },
      last_check_in: {
        type: "datetime",
        readOnly: true,
      },
      last_check_out: {
        type: "datetime",
        readOnly: true,
      },
      last_clean: {
        type: "datetime",
        readOnly: true,
      },
      check_out_widget: {
        type: "custom",
        function: getCheckoutWidget,
        depends: ["check_out_today", "departure_today", "occupied_today"],
      },
      check_in_widget: {
        type: "custom",
        function: getCheckinWidget,
        depends: ["check_in_today", "arrival_today", "occupied_today"],
      },
      check_in_today: {
        type: "time",
      },
      check_out_today: {
        type: "time",
      },
      arrival_today: {
        type: "boolean",
      },
      departure_today: {
        type: "boolean",
      },
      occupied_today: {
        type: "boolean",
      },
      clean: {
        type: "button",
        button_method: "clean",
        visible: [{ state: ["dirty"] }],
        onSuccessMsg: "AHORA LA HABITACION ESTA LIMPIA!",
        color: "blue",
      },
      not_authorized: {
        type: "button",
        button_method: "not_authorized",
        visible: [{ state: ["dirty"] }],
        onSuccessMsg: "SE MARCO QUE LA HAB. NO AUTORIZO LIMPIEZA!",
        color: "green",
      },
    },
    webtree: [
      { name: "name", width: "15%" },
      {
        name: "state",
        width: "10%",
        widget: "circle",
      },
      { name: "cleaning_type", width: "15%" },
      { name: "clean", width: "25%" },
      { name: "not_authorized", width: "25%" },
    ],
    webform: [
      { name: "classification" },
      { name: "location" },
      {
        id: "check_out_ctx",
        grid: [{ name: "check_out_widget" }],
        size: [1, 1],
        span: "col-span-1",
      },
      {
        id: "check_in_ctx",
        grid: [{ name: "check_in_widget" }],
        size: [1, 1],
        span: "col-span-1",
      },
      { name: "cleaning_type" },
      { name: "last_clean" },
      { name: "housekeeping" },
      { name: "state" },
      { name: "clean" },
      { name: "not_authorized" },
      { name: "notes" },
    ],
    webcards: {
      head: [
        {
          col: [{ name: "name", style: "font-bold text-lg" }],
          width: "w-11/12",
        },
        {
          col: [{ name: "state", widget: "circle" }],
        },
      ],
      content: [
        { row: [{ name: "classification" }] },
        { row: [{ name: "location" }] },
        {
          row: [
            { name: "cleaning_type", width: "w-3/5" },
            { name: "check_out_today", width: "w-2/5" },
          ],
        },
      ],
    },
  };

  return DictCtxView;
};

export default { ctxView: getView };
