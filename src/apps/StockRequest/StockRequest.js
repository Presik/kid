import React from "react";

import Board from "components/Board";
import requestView from "./ModelStockRequest";

const StockRequest = () => {
  return <Board ctxView={requestView} />;
};

export default StockRequest;
