// Stock Move Line
import store from "store";

const getProduct = (value) => {
  const search = [
    "OR",
    [
      ["template.name", "ilike", `%${value}%`],
      ["template.type", "=", "goods"],
    ],
    [
      ["code", "ilike", `${value}%`],
      ["template.type", "=", "goods"],
    ],
  ];
  return search;
};

const onChangeProduct = async (record, parentRec) => {
  let storeRec = {};
  let recUpdate = {};
  const activeRecord = parentRec?.record;
  const uom = record.product["default_uom."];
  if (uom) {
    recUpdate.uom = uom;
    storeRec.uom = uom.id;
  }
  if (activeRecord) {
    recUpdate.from_location = activeRecord.from_location;
    recUpdate.to_location = activeRecord.to_location;
    storeRec.from_location = activeRecord.from_location.id;
    storeRec.to_location = activeRecord.to_location.id;
  }
  return [storeRec, recUpdate];
};

export default {
  form_action_add: "modal",
  model: "stock.move",
  row_selectable: false,
  form_action: ["add"],
  table_action: ["edit", "add", "delete"],
  target: "shipment",
  webfields: {
    product: {
      type: "many2one",
      model: "product.product",
      recSearch: getProduct,
      withChange: onChangeProduct,
      attrs: ["id", "default_uom.name", "name"],
      required: true,
    },
    quantity: { type: "float", decimalPlaces: 2, required: true },
    uom: { type: "many2one", model: "product.uom", readOnly: true },
    company: {
      default: store.get("ctxSession") && store.get("ctxSession").company,
    },
    from_location: {
      type: "many2one",
      model: "stock.location",
    },
    to_location: {
      type: "many2one",
      model: "stock.location",
    },
  },
  webtree: [
    { name: "product", width: "60%" },
    { name: "uom", width: "10%" },
    { name: "quantity", width: "20%" },
  ],
  webform: [{ name: "product" }, { name: "uom" }, { name: "quantity" }],
};
