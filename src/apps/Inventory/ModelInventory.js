import store from "store";
import tools from "tools/common";

import WizardAddProduct from "./WizardAddProduct";
import inventoryLine from "./ModelInventoryLine";
import InventoryUpdate from "./InventoryUpdate";

const today = new Date();
const end_date = tools.fmtDate2Tryton(today);
const start_date = tools.dateAdd(today, "months", -2);

function isAdmin() {
  const session = store.get("ctxSession");
  let admin = false;
  for (var v of session.groups) {
    if (v.name === "Administración de logística") {
      admin = true;
    }
  }
  return admin;
}

const getFilters = () => {
  const session = store.get("ctxSession");
  const admin = isAdmin();
  let filters = {
    checkup: [
      ["state", "=", "checkup"],
      ["assign_to", "=", session.user_employee],
    ],
  };
  if (admin) {
    filters = {
      last_two_months: [
        ["date", "<=", `${end_date}`],
        ["date", ">=", `${start_date}`],
      ],
      draft: [["state", "=", "draft"]],
      checkup: [["state", "=", "checkup"]],
      pre_count: [["state", "=", "pre_count"]],
    };
  }
  return filters;
};

const searchAssign = (value) => {
  if (value === null) {
    return [];
  }
  const search = [
    "OR",
    [["party.name", "ilike", `%${value}%`]],
    [["party.id_number", "ilike", `${value}%`]],
  ];
  return search;
};

const getVisible = (name, record) => {
  const admin = isAdmin();
  if (record.state === "pre_count" && admin) return true;
  return false;
};

const getVisibleAddProduct = (record) => {
  const admin = isAdmin();
  if (record?.state == "draft" && admin) return true;
  return false;
};

const searchLocation = (value) => {
  let dom = [
    ["type", "=", "storage"],
    ["active", "=", true],
    ["rec_name", "ilike", "ZA%"],
  ];
  if (value) {
    dom.push([
      "OR",
      [["name", "ilike", `%${value}%`]],
      [["code", "ilike", `${value}%`]],
    ]);
  }
  return dom;
};

const getView = (config) => {
  // const session = store.get('ctxSession');
  let DictCtxView = {
    row_selectable: true,
    selectable: true,
    activeSearch: true,
    model: "stock.inventory",
    form_action: ["edit", "save"],
    table_action: ["edit", "add"],
    domain: null,
    filters: getFilters,
    webfields: {
      number: { type: "char", readOnly: true },
      location: {
        type: "many2one",
        model: "stock.location",
        recSearch: searchLocation,
        readOnly: { state: ["checkup", "pre_count"] },
        required: true,
      },
      date: {
        type: "date",
        readOnly: { state: ["checkup", "pre_count"] },
      },
      state: {
        type: "char",
        readOnly: true,
        translate: true,
        default: "draft",
      },
      assign_to: {
        type: "many2one",
        model: "company.employee",
        recSearch: searchAssign,
        readOnly: { state: ["checkup", "pre_count"] },
      },
      add_product: {
        type: "button-custom-modal",
        Component: WizardAddProduct,
        color: "blue",
        visible: getVisibleAddProduct,
        button_ok: false,
      },
      form_set_inventory: {
        type: "custom-fragment",
        component: InventoryUpdate,
        visible: (record) => record?.state === "checkup",
      },
      lines: {
        type: "one2many",
        model: "stock.inventory.line",
        ctxView: inventoryLine.ctxView(config, isAdmin),
        // readOnly: { state: ["checkup", "pre_count"] },
      },
      pre_count: {
        type: "button",
        button_method: "pre_count",
        visible: [{ state: "checkup" }],
      },
      checkup: {
        type: "button",
        button_method: "checkup",
        visible: [{ state: "draft" }],
      },
      confirm: {
        type: "button",
        button_method: "confirm",
        visible: getVisible,
      },
      cancel: {
        type: "button",
        button_method: "cancel",
        visible: getVisible,
        color: "rose",
      },
      comment: { type: "text-area" },
    },
    webtree: [
      { name: "number", width: "20%" },
      { name: "assign_to", width: "20%" },
      { name: "date", width: "15%" },
      { name: "location", width: "35%" },
      { name: "state", width: "10%" },
    ],
    webform: [
      { name: "number" },
      { name: "assign_to" },
      { name: "location" },
      { name: "date" },
      { name: "add_product" },
      {
        id: "set_inventory",
        grid: [{ name: "form_set_inventory" }],
        size: [1, 1],
        span: "col-span-2",
      },

      { name: "lines", component: "modal" },
      { name: "comment" },
      { name: "state" },
      { name: "pre_count" },
      { name: "checkup" },
      { name: "confirm" },
      { name: "cancel" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
