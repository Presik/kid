import React, { Fragment, useState } from "react";

import { useFormStore } from "store/formStore";
import DropdownField from "components/DropdownField";
import NumericField from "components/NumericField";
import StdButton from "components/StdButton";

const InventoryUpdate = ({ record }) => {
  const { storeRecord, activeRecord, upFieldActive, upFieldStore } =
    useFormStore();
  const [line, setLine] = useState("");
  const [addQuantity, setAddQuantity] = useState(0);
  const [subtractQuantity, setSubtractQuantity] = useState(0);
  // console.log(props, line, quantity, "fldkfldkf");
  const getLineProduct = (value) => {
    let dom = [["inventory", "=", record.id]];
    if (value) {
      const target = [
        "OR",
        ["product.code", "ilike", `%${value}%`],
        ["product.name", "ilike", `%${value}%`],
        ["product.barcode", "=", value],
      ];
      dom.push(target);
    }
    return dom;
  };

  const handleClick = (e, name) => {
    if (name !== "clean") {
      let line_ = { ...activeRecord.lines.get(line.id) };
      const quantity = addQuantity - subtractQuantity;
      if (!line_.quantity) {
        line_.quantity = quantity;
      } else {
        line_.quantity += quantity;
      }
      let _storeRecord = { ...storeRecord };
      if (!_storeRecord.lines) {
        _storeRecord.lines = new Map();
        _storeRecord.lines.set("write", new Map());
      }
      let to_write = _storeRecord.lines.get("write");
      to_write.set(line_.id, { quantity: line_.quantity, id: line_.id });

      let activeRecord_ = { ...activeRecord };
      activeRecord_.lines.set(line_.id, line_);

      upFieldActive("lines", activeRecord_.lines);
      upFieldStore("lines", _storeRecord.lines);
    }
    reset();
  };

  const reset = () => {
    setLine(null);
    setAddQuantity(null);
    setSubtractQuantity(null);
  };

  const handleProduct = (name, value) => {
    const line_ = { ...activeRecord.lines.get(value.id), ...value };
    setLine(line_);
  };

  const handleQuantity = (name, value) => {
    if (name === "addQuantity") {
      setAddQuantity(value);
    } else {
      setSubtractQuantity(value);
    }
  };

  return (
    <Fragment>
      <div className="flex flex-col md:flex-row md:space-x-4 mb-4">
        <div className="flex space-x-2 w-full md:w-3/6">
          <DropdownField
            name="product"
            label="stock.inventory.product"
            model="stock.inventory.line"
            placeholder="producto a buscar"
            recSearch={getLineProduct}
            onChange={handleProduct}
            value={line}
          />
          {/* </div>
        <div className="w-1/6"> */}
          <NumericField
            name="current_quantity"
            label="stock.inventory.current_quantity"
            value={line?.quantity || 0}
            readOnly={true}
          />
        </div>
        <div className="flex space-x-2 w-full md:w-2/6">
          <NumericField
            name="addQuantity"
            label="stock.inventory.add_quantity"
            value={addQuantity}
            onChange={handleQuantity}
          />
          <NumericField
            name="subtractQuantity"
            label="stock.inventory.subtract_quantity"
            value={subtractQuantity}
            onChange={handleQuantity}
          />
        </div>
        <div className="w-full md:w-1/6 flex justify-end space-x-2">
          <StdButton
            style={"mt-9"}
            content={"stock.inventory.confirm_quantity"}
            name={"confirm"}
            onClick={handleClick}
          />
          <StdButton
            style={"mt-9"}
            content={"stock.inventory.reset"}
            name={"clean"}
            onClick={handleClick}
            color={"slate"}
          />
        </div>
      </div>
    </Fragment>
  );
};

export default InventoryUpdate;
