// import proxy from 'proxy';
// import store from "store";

const getProduct = (value) => {
  const search = `['OR', [
      ('template.name', 'ilike', '%${value}%'),
      ('template.salable', '=', True),
    ], [
      ('code', 'ilike', '${value}%'),
      ('template.salable', '=', True),
    ],]`;
  return search;
};

const getDiference = (rec) => {
  const value = rec.quantity - rec.expected_quantity;
  if (Number.isNaN(value)) return;
  return value;
};

// function getValue(rec, field) {
//   console.log("ingresa a ejecucion", rec, field);
//   return rec["product."][field.name];
// }

const getColumns = (isAdmin) => {
  // const session = store.get("ctxSession");

  const admin = isAdmin();
  let columns = [];
  // for (var v of session.groups) {
  //   if (v.name === "Administración de logística") {
  //     admin = true;
  //   }
  // }

  if (admin) {
    columns = [
      { name: "product.code", width: "15%" },
      { name: "product", width: "25%" },
      { name: "product.description", width: "25%" },
      { name: "expected_quantity", width: "15%" },
      { name: "quantity", width: "15%", editable: true },
      { name: "diference", width: "5%" },
    ];
  } else {
    columns = [
      { name: "product.code", width: "15%" },
      { name: "product", width: "25%" },
      { name: "product.description", width: "25%" },
      { name: "quantity", width: "15%", editable: true },
    ];
  }

  return columns;
};

const getView = (config, isAdmin) => {
  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
  let DictCtxView = {
    selectable: "one",
    form_action: ["add"],
    table_action: ["delete"],
    model: "stock.inventory.line",
    webfields: {
      product: {
        type: "many2one",
        recName: "name",
        model: "product.product",
        recSearch: getProduct,
        readOnly: ["checkup", "pre_count"],
      },
      "product.description": {
        type: "char",
        readOnly: true,
      },
      "product.code": {
        type: "char",
        readOnly: true,
      },
      uom: {
        type: "many2one",
        model: "product.uom",
        readOnly: true,
      },
      expected_quantity: {
        type: "number",
        readOnly: true,
      },
      quantity: {
        type: "number",
        // readOnly: { state: ["draft", "confirm"] },
      },
      diference: {
        type: "number",
        readOnly: true,
        // visible: [{ state: "pre_count" }],
        function: getDiference,
      },
    },
    webtree: getColumns(isAdmin),

    webform: [
      { name: "product" },
      { name: "expected_quantity" },
      { name: "quantity" },
    ],
  };
  return DictCtxView;
};

export default { ctxView: getView };
