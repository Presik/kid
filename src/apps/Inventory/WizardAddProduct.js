import React, { Fragment } from "react";

import { useFormStore } from "store/formStore";
import { useIdSeq } from "store/idSeq";
import ModelProduct from "./ModelProduct.js";
import QuickTable from "components/QuickTable";
import StdButton from "components/StdButton.js";

function WizardAddProduct(props) {
  let selectedRows = new Map();
  const ctxView = ModelProduct.ctxView();
  const { storeRecord, activeRecord, upFieldActive, upFieldStore } =
    useFormStore();
  const { seq, increment } = useIdSeq();

  function onDoubleClickRow() {
    return;
  }

  function onClickRow(records) {
    if (records === null) {
      selectedRows = new Map();
    } else if (records instanceof Map) {
      selectedRows = new Map(records);
    } else {
      if (selectedRows.get(records.id)) {
        selectedRows.delete(records.id);
      } else {
        selectedRows.set(records.id, records);
      }
    }
  }

  function handleAccept() {
    let _storeRecord = { ...storeRecord };
    if (!_storeRecord.lines) {
      _storeRecord.lines = new Map();
      _storeRecord.lines.set("create", new Map());
    }
    let to_create = _storeRecord.lines.get("create");

    let activeRecord_ = { ...activeRecord };
    if (!activeRecord_.lines) {
      activeRecord_.lines = new Map();
    }

    const prods = Array.from(activeRecord_.lines.values()).map(
      (ItemRow) => ItemRow.product.id,
    );

    let sq = seq;
    for (let v of selectedRows.values()) {
      if (!prods.includes(v.id)) {
        const rec = {
          id: sq,
          product: {
            id: v.id,
            name: v.name,
            code: v.code,
            description: v.description,
          },
        };
        activeRecord_.lines.set(sq, rec);
        to_create.set(sq, { id: sq, product: v.id });
        sq += -1;
      }
    }
    increment(sq);
    upFieldActive("lines", activeRecord_.lines);
    upFieldStore("lines", _storeRecord.lines);
    props.onClose();
  }

  return (
    <Fragment>
      <QuickTable
        model={ctxView.model}
        ctxView={ctxView}
        selectable={ctxView.selectable}
        onDoubleClickRow={onDoubleClickRow}
        onClickRow={onClickRow}
        activeSearch={true}
      />
      <StdButton
        name={"button_add"}
        content={"button_add"}
        onClick={handleAccept}
        color={"bluePresik"}
        style={"mt-8"}
      />
    </Fragment>
  );
}

export default WizardAddProduct;
