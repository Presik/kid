import React, { Fragment, useRef, useState, useEffect } from "react";
import SignatureCanvas from "react-signature-canvas";
import StdButton from "components/StdButton";
import { useWizardStore } from "store/wizardStore";
import { useFormStore } from "store/formStore";
import ModelRentalBooking from "../ModelRentalBooking";
// import proxy from "api/proxy";
import proxy from "api/proxy";

const AddSignature = (props) => {
  let ctxView = ModelRentalBooking.ctxView();
  const signatureCanvasRef = useRef();
  let [btnDisabled, setBtnDisabled] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [cloudConf, setCloudConf] = useState({});
  let [imgSignature, setImgSignature] = useState(null);
  const { storeWizard, resetWizard } = useWizardStore();
  const { activeRecord, storeRecord, upStoreRecord, setActiveRecordFromId } =
    useFormStore();

  const session = proxy.getSession();

  console.log(props, " propsss");
  const getConf = async () => {
    const fields = [
      "cloudinary_api_key",
      "cloudinary_api_secret",
      "cloudinary_upload_preset",
      "cloudinary_upload_url",
      "cloudinary_name",
    ];
    const { data, error } = await proxy.search(
      "company.company",
      [["id", "=", session.company]],
      fields,
    );
    setCloudConf(data[0]);
  };

  const clearSignature = () => {
    signatureCanvasRef.current.clear();
    setBtnDisabled(true); // Deshabilita el botón después de borrar la firma
  };

  useEffect(() => {
    getConf();
  }, []);

  async function onRemove() {
    const value = null;
    props.onChange(props.name, value);
    setActive(true);
    setPhoto(null);
    setUpload({});
  }

  const saveSignature = async () => {
    const signatureDataURL = signatureCanvasRef.current.toDataURL();
    let _activeRecord = { ...activeRecord };
    _activeRecord.signatureImage = signatureDataURL;

    try {
      // Crear una nueva instancia de FormData para enviar la imagen a Cloudinary
      const formData = new FormData();
      formData.append("file", signatureDataURL);
      formData.append("upload_preset", cloudConf.cloudinary_upload_preset);
      const config = {
        method: "POST",
        body: formData,
      };

      const response = await fetch(
        cloudConf.cloudinary_upload_url + "upload",
        config,
      );
      const res = await response.json();

      if (res) {
        const model = "rental.booking";
        const args = {
          model: model,
          storeRec: {
            id: storeRecord.id,
            signature: res.url,
            equipment: storeRecord.equipment,
          },
        };

        let addAsignatureBooking = await proxy.saveQuery(args);
        if (addAsignatureBooking) {
          setImgSignature(res.url);
          setBtnDisabled(false);
        }
      } else {
        console.error(
          "Error al subir la imagen a Cloudinary:",
          response.statusText,
        );
      }
    } catch (error) {
      console.error("Error al subir la imagen a Cloudinary:", error);
    }
  };

  async function acceptCreate() {
    setIsLoading(true);
    // Aquí puedes realizar otras acciones antes de enviar la reserva a tu servidor.
  }

  const actionProcess = async () => {
    // setStore({});
    let _storeRecord = { ...storeRecord };

    let createService = await proxy.buttonMethod({
      model: "rental.booking",
      method: "process",
      ids: [_storeRecord.id],
    });
    console.log(createService);
    setIsLoading(false);
    setActiveRecordFromId(activeRecord.id, "rental.booking", ctxView);
    clearSignature();
  };

  return (
    <Fragment>
      <div className="text-lg max-h-96 overflow-y-scroll scroll-pl-6 pr-3 pl-3 mb-5">
        {/* ESTO DEBE SER UNA CONSULTA DE LAS PLANTILLAS */}
        <h1 className="font-bold my-5">RENTAL DAMAGE WAIVER</h1>
        <p>
          In consideration of being permitted to rent the electric bike
          (“E-Bike”) from date showed on rental agreement until date showed in
          my rental return confirmation, I hereby agree to the following:
        </p>

        <h2 className="font-bold my-5">ASSUMPTION OF RISK</h2>
        <p>
          I,Juana isabella chaves gomez confirm that I am voluntarily renting
          the E-bike. I understand and am fully aware of the potential risks and
          hazards involved in renting the above-mentioned Vehicle, which
          includes but is not limited to property damage, loss, injury, illness,
          and death. Having acknowledged such risks, I assume full
          responsibility for any liability that may arise during my rental
          period.
        </p>

        <h2 className="font-bold my-5">PAYMENT CONDITIONS</h2>
        <p>
          If I,Juana isabella chaves gomez lost, destroyed, or damaged the
          E-Bike during my rental period due to my negligence or misconduct, I
          understand that I have to pay to Rendon &amp; Aguirre Energy World Pty
          Ltd the amount of $2100.00 AUD due within thirty (30) days after my
          rental period.
        </p>

        <h2 className="font-bold my-5">LOSS AND DAMAGE WAIVER</h2>
        <p>
          If I,Juana isabella chaves gomez lost, destroyed, or damaged the
          E-Bike during my rental period due to my negligence or misconduct, I
          understand that If I pay 10AUD extra per week for a Loss and damage
          waiver, I have to pay to Rendon &amp; Aguirre Energy World Pty Ltd the
          amount of $ 1150.00 AUD due within thirty (30) days after my rental
          period.
        </p>
        <p>
          I,Juana isabella chaves gomez also acknowledge that a puncture is
          something that can happen to any bike at any time so every time an
          E-bike tire is flat I will pay $ 10.00 AUD to Rendon &amp; Aguirre
          Energy World Pty Ltd for tube replacement.
        </p>

        <h2 className="font-bold my-5">
          WAIVER OF LIABILITY AND INDEMNIFICATION
        </h2>
        <p>
          I,Juana isabella chaves gomez agree to indemnify, defend, and hold
          harmless Rendon &amp; Aguirre Energy World Pty Ltd, its employees,
          agents, and affiliates against all claims and liabilities in
          connection with my rental of the E-Bike unless the said event is
          caused by the negligence or willful misconduct of Rendon &amp; Aguirre
          Energy World Pty Ltd.
        </p>

        <h2 className="font-bold my-5">ADDITIONAL PROVISIONS</h2>
        <p>
          I,Juana isabella chaves gomez shall comply with all the policies
          related to my E-bike rental as set by Rendon &amp; Aguirre Energy
          World Pty Ltd in Renag / Rental Agreement document, and in any local,
          state, and federal laws applicable.
        </p>

        <p>
          I,Juana isabella chaves gomez have read and understood the contents of
          this document and I sign this willingly, fully aware that this is a
          release of liability.
        </p>

        <p>
          {" "}
          <a
            href="mailto:Email%3Aisabellachavesgomez@gmail.com"
            target="_blank"
          >
            Email:isabellachavesgomez@gmail.com
          </a>{" "}
        </p>
        <p> Client Name:Juana isabella chaves gomez </p>
        <p>
          {" "}
          Client Address:unit 3/58 Lothian Street, Annerley QLD, Australia{" "}
        </p>
        <p> Client Address:404827332 </p>
      </div>
      <div className="border border-dashed border-gray-400 p-3 col-span-2 mx-20">
        <SignatureCanvas
          ref={signatureCanvasRef}
          penColor="black"
          canvasProps={{
            width: 700,
            height: 120,
            className: "signature-canvas",
          }}
        />
        <button
          onClick={saveSignature}
          className="mr-5 bg-blue-presik rounded-full py-1 px-4 text-white"
        >
          Add
        </button>
        <button
          onClick={clearSignature}
          className="mr-5 bg-red-900  rounded-full py-1 px-4 text-white"
        >
          Clean
        </button>
      </div>
      <div className="py-5 gap-3 justify-center col-span-2 flex">
        <StdButton
          key="add"
          color="bluePresik"
          disabled={btnDisabled}
          loading={isLoading}
          onClick={actionProcess}
          size="w-1/3"
          content="rental.service.add_signature"
        />

        {/* <StdButton
          key="add"
          color="blue"
          // loading={isLoading}
          onClick={actionProcess}
          size="w-full"
          content="rental.service.send_contract"
        /> */}
      </div>
    </Fragment>
  );
};

export default AddSignature;
