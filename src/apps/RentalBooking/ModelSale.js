import proxy from "api/proxy";

import ModelSaleLine from "./ModelSaleLine";
import ModelParty from "./ModelParty";
import WizardAddPayment from "./WizardAddPayment";
import { ScheduledActivities } from "./components/ScheduledActivities";
import ModelWizardPayment from "./ModelWizardPayment";
import ModalWizzardActivity from "./components/ModalWizzardActivity";
import dates from "tools/dates";
import store from "store";

const defaultShop = async () => {
  const session = store.get("ctxSession");
  const dom = [["id", "=", session.shop]];
  const fields = ["id", "rec_name"];
  const val = await proxy.search("sale.shop", dom, fields, 1);
  return val[0];
};
const visibleBilling = (name, record) => {
  let res = false;
  if (record.state === "offer" && record.lines && record.lines.size > 0) {
    res = true;
  }
  return res;
};

const getView = (config) => {
  let DictCtxView = {
    model: "sale.sale",
    domain: [],
    form_action: ["save", "edit", "add"], // options: ['save', 'delete']
    table_action: ["open", "add"], // options: ['open', 'delete', 'edit', 'add']
    // orderBy: [["name", "ASC"]],
    limit: 1000,
    selectable: null, // Options for table rows: null, multi, one
    title: { field: "number", model: true },
    // tags: {
    //   order_status: stateColors,
    // },
    webfields: {
      id: { type: "integer", readOnly: true },
      name: { type: "name", readOnly: true },
      number: { type: "char", readOnly: true },

      delivery_amount: { type: "number", readOnly: true },
      party: {
        type: "many2one",
        model: "party.party",
        ctxView: ModelParty.ctxView(),
        required: true,
      },

      shop: {
        type: "many2one",
        model: "sale.shop",
        readOnly: true,
        default: defaultShop,
        required: true,
      },
      sale_date: {
        type: "date",
        readOnly: true,
        default: () => new Date(),
        required: true,
      },

      source: {
        type: "many2one",
        model: "sale.source",
        readOnly: true,
      },

      state: {
        type: "char",
        readOnly: true,
        translate: true,
        default: "draft",
      },

      total_amount: { type: "number", readOnly: true },

      price_list: {
        type: "many2one",
        recSearch: () => [],
        model: "product.price_list",
        required: true,
      },
      invoice_type: {
        type: "radio_group",
        options: [
          { value: "P", text: "POS" },
          { value: "1", text: "ELECTRONICA" },
        ],
        required: true,
      },
      lines: {
        type: "one2many",
        model: "sale.line",
        ctxView: ModelSaleLine.ctxView(config),
        // readOnly: true,
      },
      billing: {
        type: "button",
        button_method: "dash_create_order_call",
        color: "blue",
        visible: visibleBilling,
        onSuccessMsg: "ORDEN COMANDADA...!",
      },
      pay: {
        type: "button",
        button_method: "",
        visible: visibleBilling,
        color: "blue",
        onSuccessMsg: "ORDEN COMANDADA...!",
      },
      add_payment: {
        type: "button-wizard",
        Component: WizardAddPayment,
        color: "lime",
        icon: "fi fi-rr-add",
      },
      add_activity: {
        type: "button-wizard",
        Component: ModalWizzardActivity,
        color: "lime",
        icon: "fi fi-rr-add",
      },
    },
    webtree: [
      { name: "id", width: "20%" },
      { name: "number", width: "20%" },
      { name: "sale_date", width: "25%" },
      { name: "party", width: "25%" },
      { name: "delivery_amount", width: "25%" },
      { name: "total_amount", width: "25%" },
    ],
    webform: [
      { name: "party", widget: "search-add" },

      {
        id: "cunstomer",
        grid: [{ name: "number" }, { name: "shop" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "consumer_party",
        grid: [
          { name: "sale_date" },
          { name: "invoice_type" },
          { name: "price_list" },
          { name: "add_payment" },
        ],
        size: [1, 4],
        span: "col-span-2",
      },

      { name: "lines", component: "modal" },
      { name: "total_amount" },
      { name: "state", width: "7%" },

      { name: "billing" },
      // { name: "add_activity" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
