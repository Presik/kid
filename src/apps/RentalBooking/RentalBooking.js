import React, { Fragment, useState, useEffect } from "react";
import { FormattedMessage as FM } from "react-intl";
import date from "date-and-time";

import Board from "components/Board";
import DateRangeFilter from "components/DateRangeFilter";
import SectionRigth from "components/SectionRigth";
import QuickForm from "components/QuickForm";
import ModelRentalBooking from "./ModelRentalBooking";
import Screen from "components/Screen";
import WidgetSide from "apps/Booking/components/WidgetSide";
import ListPeopleCheckInWidget from "apps/Booking/components/ListPeopleCheckInWidget";
import { useFormStore } from "store/formStore";
import store from "store";
import tool from "tools/functions";

// import WidgetSideCustomers from "apps/Booking/components/WIdgetSideCustomers";
// import { dataListCustomers } from "apps/Booking/dataFake";
// import { ScheduledActivities } from "./components/ScheduledActivities";
// import WidgetSideStatusActivity from "./components/WidgetSideStatusActivity";
// import ModalWizzardActivity from "./components/ModalWizzardActivity";
const fmt = "YYYY-MM-DD";
var nextId = -1;

const RentalBooking = (props) => {
  const ctxView = ModelRentalBooking.ctxView(props.config); // Ensure rental.ctxView is a valid function or property
  const session = store.get("ctxSession");
  const [statusSection, setStatusSection] = useState(true);
  const [action, setAction] = useState(null);
  let [ctxViewSale, setCtxViewSale] = useState(ctxView);
  let [statusActivities, setStatusActivities] = useState(true);
  const [filterDay, setFilterDay] = useState(date.format(new Date(), fmt));

  let [booking, setBooking] = useState([]);
  let [tickets, setTickets] = useState([]);
  let [rooms, setRooms] = useState([]);

  let [record, setRecord] = useState(null);
  // let [openWizard, setOpenWizard] = useState(false);
  const { upStoreRecord, upActiveRecord } = useFormStore();

  const handleChangeDate = (value) => {
    let daySelected = date.format(new Date(value), "YYYY-MM-DD");
    setFilterDay(daySelected);
  };

  // async function getTickets() {
  //   let filterDay_ = new Date(filterDay);
  //   let filterDayStart = date.format(filterDay_, "YYYY-MM-DD 00:00:00");
  //   filterDay_.setDate(filterDay_.getDate() + 1);
  //   filterDay_.setHours(23, 59, 59);
  //   let filterDayEnd = date.format(filterDay_, "YYYY-MM-DD 23:59:59");

  //   let dom = [
  //     ["activity.date_start", ">=", filterDayStart],
  //     ["activity.date_start", "<=", filterDayEnd],
  //   ];
  //   // let dom = [];

  //   const fields = [
  //     "number",
  //     "party.name",
  //     "total_amount",
  //     "salesman",
  //     "invoice",
  //     "unit_price",
  //     "activity.quota",
  //     "activity.kind",
  //     "activity.kind.name",
  //     "activity.date_start",
  //   ];
  //   // const res = await proxy.search("sale_activity.ticket", dom, fields);

  //   // if (res) {
  //   //   console.log("----------------res", res);
  //   //   setTickets(res);setOpenWizard

  //   //   // const newDomain = [["lines.arrival_date", "=", filterDay]];
  //   //   // newCtxView.domain = newDomain;
  //   //   // setNewCtxView(newCtxView);
  //   // }
  // }

  // const handleChangeActivity = (id, dates) => {
  //   let domain = [["schedule.id", "=", id]];
  //   let ctxView_ = { ...ctxViewAc };
  //   ctxView_["domain"] = domain;
  //   setCtxViewActive(ctxView_);
  //   setOpenWizard(true);
  // };

  const handleChangeStatusActivities = (status) => {
    setStatusActivities(status);
  };

  useEffect(() => {
    // getTickets();
    resetNewRecord();
  }, [filterDay, props]);

  const resetNewRecord = async () => {
    nextId = nextId - 1;
    const [toStore, toActive] = await tool.getDefaults(nextId, ctxView);
    upStoreRecord(toStore);
    upActiveRecord(toActive);
    setRecord(toActive);
  };

  return (
    <Screen>
      <DateRangeFilter action={handleChangeDate} />
      {/* <ScheduledActivities
        currentDay={filterDay}
        // handleChangeActivity={handleChangeActivity}
        handleChangeStatusActivities={handleChangeStatusActivities}
        ctxView={ctxViewAc}
      /> */}

      {statusActivities && (
        <div
          className={`flex ${
            statusSection ? "flex-row justify-between" : "flex-col"
          }`}
        >
          <div
            className={
              statusSection == true
                ? "md:w-[65vw] xl:w-[70vw] "
                : "md:w-12/12 relative pr-10"
            }
          >
            <Board ctxView={ctxView} />
          </div>

          <SectionRigth
            position={"right"}
            widgets={["status", "numPeople"]}
            title="INFO BOOKING"
            bgColor={statusSection ? "bg-gray-100" : "bg-blue-presik"}
            status={statusSection}
            handleChangeStatus={setStatusSection}
          >
            {/* <WidgetSideStatusActivity
              title={<FM id="booking.widget.screen.huesped_house" />}
              background={"bg-gray-100"}
              name="statusHotel"
              tickets={tickets}
              day={filterDay}
            />

            <WidgetSide
              title={<FM id="booking.widget.screen.huesped_house" />}
              num={50}
              background={"bg-gray-100"}
              name="statusHotel"
              rooms={rooms}
              day={filterDay}
            />

            <WidgetSide
              title={<FM id="booking.widget.screen.huesped_house" />}
              num={3}
              background={"bg-gray-100"}
              name="statusPeopleHotel"
              rooms={rooms}
              day={filterDay}
            />

            <WidgetSideCustomers
              dataCustomers={dataListCustomers}
              title={<FM id="booking.widget.screen.last_bookings" />}
              data={booking}
              // day={filterDay}
            />
            <ListPeopleCheckInWidget
              title={<FM id="booking.widget.screen.more_options" />}
            /> */}
          </SectionRigth>
        </div>
      )}
    </Screen>
  );
};

export default RentalBooking;
