// Sale model line
import proxy from "api/proxy";

const getProduct = (value) => {
  const search = `['OR', [
      ('template.name', 'ilike', '%${value}%'),
      ('template.salable', '=', True),
    ], [
      ('code', 'ilike', '${value}%'),
      ('template.salable', '=', True),
    ],]`;
  return search;
};

const onChangeProduct = async (
  fieldValue,
  storeRec,
  activeRecord,
  parentRecord,
) => {
  if (fieldValue.list_price) {
    if (parentRecord && parentRecord.price_list) {
      const line = {
        product: activeRecord.product,
        price_list: parentRecord.price_list,
      };
      const { data: res } = await proxy.method(
        "sale.sale",
        "dash_get_line",
        line,
      );
      activeRecord.unit_price = res.unit_price;
      activeRecord.unit_price_w_tax = res.unit_price_w_tax;
      // activeRecord.commission_amount = res.commission_amount;
    } else {
      activeRecord.unit_price_w_tax = fieldValue.list_price;
      activeRecord.unit_price = fieldValue.list_price;
    }
  }
  return storeRec, activeRecord;
};

const onChangeQty = (qty, storeRec, activeRecord) => {
  if (activeRecord.unit_price_w_tax) {
    if (activeRecord.discount) {
      const { unit_price_w_tax, discount } = activeRecord;
      const amount =
        unit_price_w_tax * qty - (unit_price_w_tax * qty * discount) / 100;
      activeRecord.amount = amount;
    } else {
      activeRecord.amount = activeRecord.unit_price_w_tax * qty;
    }
  }
  return storeRec, activeRecord;
};

// const onChangeUnitPrice = (activeRecord, unitPrice) => {
//   if (activeRecord.quantity) {
//     if (activeRecord.discount) {
//       const {quantity, discount} = activeRecord;
//       const amount =
//         unitPrice * quantity - (unitPrice * quantity * discount) / 100;
//       activeRecord.amount = amount;
//     } else {
//       activeRecord.amount = unitPrice * activeRecord.quantity;
//     }
//   }
//   return activeRecord;
// };

// const onChangeDiscount = (activeRecord, discount) => {
//   if (activeRecord.unit_price_w_tax && activeRecord.quantity) {
//     const {unit_price_w_tax, quantity} = activeRecord;
//     activeRecord.amount =
//       unit_price_w_tax * quantity -
//       (unit_price_w_tax * quantity * discount) / 100;
//   }
//   return activeRecord;
// };

const getView = () => {
  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
  let DictCtxView = {
    row_selectable: false,
    form_action_add: "modal",
    table_action_update: true,
    webfields: {
      product: {
        type: "many2one",
        model: "product.product",
        recSearch: getProduct,
        withChange: onChangeProduct,
        attrs: ["id", "list_price", "name"],
        required: true,
      },
      quantity: { type: "integer", withChange: onChangeQty, required: true },
      amount: { type: "number", readOnly: true },
    },
    webtree: [
      { name: "product", width: "40%" },
      { name: "quantity", width: "10%" },
      { name: "amount", width: "23%" },
    ],
    webform: [{ name: "product" }, { name: "quantity" }],
  };

  return DictCtxView;
};

export default { ctxView: getView };
