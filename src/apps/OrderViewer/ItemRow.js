import React from "react";

import "./ItemRow.css";

function ItemRow(props) {
  return (
    <div className="order-viewer-item-row">
      <div className="order-viewer-item-qty">{props.quantity}</div>
      <div className="order-viewer-item-product">{props.product}</div>
      <div className="order-viewer-item-note">
        <span className="order-viewer-item-note-span">{props.note}</span>
      </div>
    </div>
  );
}

export default ItemRow;
