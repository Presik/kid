// Sale model
import store from "store";

import saleLine from "./ModelSaleLine";
import tools from "tools/common";

let today = tools.fmtDate2Tryton(new Date());

const searchParty = (value) => {
  // const session = store.get('ctxSession');
  const search = [
    "OR",
    [["name", "ilike", `%${value}%`]],
    [["id_number", "ilike", `${value}%`]],
  ];
  return search;
};

const searchTable = () => {
  const session = store.get("ctxSession");
  const search = `[
    ('state', '=', 'available'),
    ('shop', '=', ${session.shop})
  ]`;
  return search;
};

const getAddress = (record) => {
  if (record && record.hasOwnProperty("party.")) {
    const party_id = record["party."].id;
    const dom = [["party", "=", `${party_id}`]];
    return dom;
  }
};

const onChangeLines = (activeRecord, fieldValue) => {
  if (activeRecord.lines) {
    let total_amount = 0;
    for (let l of fieldValue.values()) {
      total_amount += l.amount;
    }
    activeRecord["total_amount"] = total_amount;
  }
  return activeRecord;
};

const getCardContentRender = (record) => {
  console.log("render record", record);
  return {
    titleCard: record.id + " " + record.state,
    contentMeta: "2 days ago",
    content: "this text have my card",
    extraContent: "validate my extra content",
    optionsMenu: [
      { id: 0, name: "View" },
      { id: 1, name: "View2" },
    ],
  };
};

const getDefaultFilter = (session) => {
  console.log(session, "session");
  return [
    ["order_status", "=", "commanded"],
    ["sale_date", "=", `${today}`],
  ];
  // ('shop', '=', ${session.shop}),
  // ('order_status', 'inin', "['commanded', 'in_preparation', 'dispatched']"),
};

const getFilters = (session) => {
  return {
    today: [
      ["sale_date", "=", `${today}`],
      ["create_uid", "=", session.user],
    ],
  };
};

const getView = (config) => {
  let DictCtxView = {
    activeSearch: true,
    filters: getFilters,
    defaultFilter: getDefaultFilter,
    groupColumns: [
      { id: "commanded", name: "Comandada" },
      { id: "in_preparation", name: "En preparacion" },
      { id: "dispatched", name: "Despachada" },
    ],
    webfields: {
      number: { type: "char", readOnly: true },
      table_assigned: {
        type: "many2one",
        model: "sale.shop.table",
        recSearch: searchTable,
        required: true,
        readOnly: ["quotation"],
      },
      party: {
        type: "many2one",
        model: "party.party",
        recSearch: searchParty,
        readOnly: ["quotation"],
      },
      shipment_address: {
        type: "many2one",
        model: "party.address",
        recSearch: getAddress,
        required: true,
        readOnly: ["quotation"],
      },
      sale_date: {
        type: "date",
        readOnly: ["quotation"],
      },
      state: {
        type: "char",
        readOnly: true,
        translate: true,
        default: "draft",
      },
      order_status: {
        type: "char",
        readOnly: true,
        translate: true,
        default: "draft",
      },
      lines: {
        type: "one2many",
        model: "sale.line",
        ctxView: saleLine.ctxView(config),
        withChange: onChangeLines,
        required: true,
      },
      total_amount: { type: "number", readOnly: true },
      // comment: {type: 'text-area', readOnly: ['quotation']},
      description: { type: "char", readOnly: ["quotation"] },
      quote: {
        type: "button",
        method: "create_sale",
        visible: ["draft"],
      },
      command: {
        type: "button",
        method: "command",
        // visible: true,
      },
    },
    webform: [
      { name: "table_assigned" },
      { name: "number" },
      { name: "lines", component: "modal" },
      { name: "total_amount" },
      { name: "order_status" },
      { name: "command" },
    ],
    webcard: getCardContentRender,
  };

  return DictCtxView;
};

export default { ctxView: getView };
