const data = {
  917: {
    id: "917",
    number: "PED-000006",
    table_assigned: "MESA 03",
    employee: "JHON CONNOR",
    status_order: "commanded",
    kind: "to_table",
    lines: [
      {
        id: 10,
        product: "CHEESE BURGUER",
        quantity: 2,
        note: "SIN PIÑA",
      },
      {
        id: 11,
        product: "BUDWEISER LIGHT",
        quantity: 5,
        note: null,
      },
    ],
  },
  919: {
    id: "919",
    number: "PED-000007",
    table_assigned: "MESA 11",
    employee: "SARAH SMITH",
    status_order: "commanded",
    kind: "to_table",
    lines: [
      {
        id: 12,
        product: "PIZZA ITALIAN SMALL",
        quantity: 1,
        note: "",
      },
      {
        id: 13,
        product: "ADICIONAL PEPPERONI",
        quantity: 1,
        note: null,
      },
      {
        id: 14,
        product: "ORANGE JUICE",
        quantity: 4,
        note: "SIN AZUCAR",
      },
    ],
  },
  920: {
    id: "920",
    number: "PED-000008",
    table_assigned: "MESA 17",
    employee: "RICHARD BUTTON",
    status_order: "commanded",
    kind: "to_table",
    lines: [
      {
        id: 15,
        product: "HATSU TE",
        quantity: 4,
        note: "",
      },
    ],
  },
};

export default data;
