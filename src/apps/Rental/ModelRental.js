import { format } from "date-and-time";
import ModelParty from "./ModelParty";
import ModelCheckList from "./ModelCheckList";
import { name } from "store/storages/cookieStorage";

const stateColors = {
  draft: "rose",
  booking: "amber",
  processed: "sky",
  confirmed: "lime",
};

const getView = (config) => {
  let DictCtxView = {
    model: "rental.service",
    form_action: ["edit"],
    table_action: ["add", "edit"],
    activeSearch: true,
    // title: { field: "schedule", model: true },
    domain: [],
    tags: {
      state: stateColors,
    },
    webfields: {
      number: { type: "char", readOnly: true },
      state: { type: "char", tags: stateColors, readOnly: true },
      start_date: { type: "date" },
      service_date: { type: "date" },
      odometer_start: { type: "date" },
      odometer_end: { type: "date" },
      hiring_time: { type: "integer" },

      // subdivision: {
      //   type: "many2one",
      //   recSearch: () => [],
      //   model: "country.subdivision",
      //   searchable: true,
      //   // readOnly: true,
      // },
      booking: {
        type: "many2one",
        model: "product.product",
        searchable: true,
        readOnly: true,

        // ctxView: ModelParty.ctxView(),
        // attrs: ["account_receivable"],
      },

      photo_link_agree: {
        type: "capture",
        help: "asdsad",
      },
      photo_link_party_id: {
        type: "capture",
        help: "asdsad",
      },
      check_list: {
        type: "one2many",
        model: "rental.service.product_check_list",
        ctxView: ModelCheckList.ctxView(config),
      },
      party: {
        type: "many2one",
        model: "party.party",
        ctxView: ModelParty.ctxView(),
        searchable: true,
        required: true,
        readOnly: true,
        // attrs:
      },
      "party.id_number": {
        type: "char",
        readOnly: true,
      },

      equipment: {
        type: "many2one",
        model: "maintenance.equipment",
        // ctxView: ModelParty.ctxView(),
        required: true,
        // readOnly: true,

        // attrs:
      },
      product: {
        type: "many2one",
        recSearch: () => [],
        model: "maintenance.equipment",
        searchable: true,
        // readOnly: true,
      },
      bank_account_number: { type: "char" },
      bank_bsb: { type: "char" },
      mobile: { type: "char" },
      email: { type: "char" },
      end_date: { type: "date" },
      comment: { type: "char" },
      issues_presented: { type: "char" },
      part_missing: { type: "char" },
      cover_prices: { type: "boolean" },
      confirm: {
        type: "button",
        button_method: "confirm",
        color: "green",
        icon: "fi fi-rr-add",
        // visible: visibleConfirmed,
      },
      cancelled: {
        type: "button",
        button_method: "cancel",
        color: "amber",
        icon: "fi fi-rr-add",
        // visible: visibleConfirmed,
      },
      renewed: {
        type: "button",
        button_method: "renew",
        color: "blue",
        icon: "fi fi-rr-add",
        // visible: visibleConfirmed,
      },
      done: {
        type: "button",
        button_method: "done",
        color: "blue",
        icon: "fi fi-rr-add",
        // visible: visibleConfirmed,
      },
    },
    webtree: [
      { name: "number", width: "30%" },
      { name: "booking", width: "30%" },
      { name: "party", width: "25%" },
      { name: "email", width: "30%" },
      { name: "mobile", width: "30%" },
      { name: "start_date", formatHour: false },
      { name: "end_date", width: "30%", formatHour: false },
      { name: "equipment" },
      { name: "state", widget: "badge" },
    ],
    webform: [
      { name: "number" },
      { name: "booking" },
      { name: "service_date" },
      { name: "party" },
      { name: "bank_account_number" },
      { name: "bank_bsb" },
      { name: "service_date" },
      { name: "equipment" },
      { name: "hiring_time" },
      { name: "start_date" },
      { name: "end_date" },
      { name: "product" },
      { name: "check_list" },
      { name: "issues_presented" },
      { name: "odometer_start" },
      { name: "photo_link_party_id" },
      { name: "photo_link_agree" },
      { name: "cover_prices" },
      { name: "odometer_end" },
      { name: "part_missing" },
      { name: "state" },

      {
        id: "buttoms",
        grid: [
          { name: "confirm" },
          { name: "renewed" },
          { name: "cancelled" },
          { name: "done" },
        ],
        size: [1, 4],
        span: "col-span-2",
      },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
