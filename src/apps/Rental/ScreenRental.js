import React, { Fragment, useState } from "react";
import ModelRental from "./ModelRental"; // Ensure this import is correct
import Board from "components/Board"; // Ensure this import is correct
import Screen from "components/Screen"; // Ensure this import is correct
import SectionRigth from "components/SectionRigth"; // Ensure this import is correct
import date from "date-and-time";
import DateRangeFilter from "components/DateRangeFilter"; // Ensure this import is correct

const ScreenRental = (props) => {
  const ctxView = ModelRental.ctxView(props.config); // Ensure rental.ctxView is a valid function or property

  const [filterDay, setFilterDay] = useState(
    date.format(new Date(), "YYYY-MM-DD"),
  );
  const [statusSection, setStatusSection] = useState(true);

  const handleChangeDate = (value) => {
    let daySelected = date.format(new Date(value), "YYYY-MM-DD");
    setFilterDay(daySelected);
  };

  return (
    <Screen>
      <DateRangeFilter handleChangeDate={handleChangeDate} />

      {statusSection && (
        <div
          className={`flex ${
            statusSection ? "flex-row justify-between" : "flex-col"
          }`}
        >
          <div
            className={
              statusSection
                ? "md:w-[65vw] xl:w-[70vw]"
                : "md:w-12/12 relative pr-10"
            }
          >
            <Board ctxView={ctxView} />
          </div>

          <SectionRigth
            position={"right"}
            widgets={["status", "numPeople"]}
            title="INFO RENTAL"
            bgColor={statusSection ? "bg-gray-100" : "bg-blue-presik"}
            status={statusSection}
            handleChangeStatus={setStatusSection}
          ></SectionRigth>
        </div>
      )}
    </Screen>
  );
};

export default ScreenRental;
