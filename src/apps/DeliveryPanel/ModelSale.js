// import React, { useEffect, useState } from "react";
// import { Table } from "semantic-ui-react";
// import FullTable from "components/FullTable";

const ViewSale = {
  model: "sale.sale",
  form_action: [],
  table_action: [],
  webfields: {
    number: { type: "char", readonly: true },
    status: { type: "char", readonly: true },
    message: { type: "char", readonly: true },
  },
  webtree: [
    { name: "number", width: "15%" },
    { name: "status", width: "15%" },
    { name: "message", width: "70%" },
  ],
};

export default ViewSale;
