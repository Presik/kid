import React, { useState, useEffect } from "react";
import { Button, Modal, Table } from "semantic-ui-react";
import store from "store";

import BodyTable from "components/Table/BodyTable";
import "./ModalDeliveryMan.css";
import proxy from "api/proxy";

const ctxView = {
  webfields: {
    party: { type: "many2one", readonly: true },
    number_plate: { type: "char", readonly: true },
  },
  webtree: [
    { name: "party", width: "35%" },
    { name: "number_plate", width: "15%" },
  ],
};

function ModalDeliveryMan(props) {
  const [records, setRecords] = useState([]);
  useEffect(() => {
    const model = "sale.shop";
    const session = store.get("ctxSession");
    const shop = session.shop;
    const dom = `[('id', '=', ${shop})]`;
    async function getRecords() {
      const { data } = await proxy.search(model, dom, ["delivery_man"]);
      if (data) {
        const delivery_man = data[0].delivery_man;
        setRecords(delivery_man.slice());
      }
    }
    getRecords();
  }, []);

  const onSelecRow = (rec) => {
    props.onClose(rec);
  };

  const buttons = [
    <Button
      key="close"
      color="red"
      className="modal-button"
      onClick={props.onClose}
    >
      CANCELAR
    </Button>,
  ];

  return (
    <Modal open={props.open}>
      <Modal.Header className="modal-header">
        <h2 className="modal-header-text">DOMICILIARIO</h2>
      </Modal.Header>
      <Modal.Content className="modal-content">
        <Table sortable={true} striped unstackable selectable color="olive">
          <BodyTable
            ctxView={ctxView}
            records={records}
            onClickRow={onSelecRow}
          />
        </Table>
      </Modal.Content>
      <Modal.Actions className="modal-actions">{buttons}</Modal.Actions>
    </Modal>
  );
}

export default ModalDeliveryMan;
