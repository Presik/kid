import React, { useState } from "react";

import store from "store";
import proxy from "api/proxy";
import tools from "tools/common";

import DropdownField from "components/DropdownField";
import BoardHeader from "components/BoardHeader";
import Button from "components/StdButton";
import QuickTable from "components/QuickTable";

import ModalPayment from "./ModalPayment";
import ModalInfo from "./ModalInfo";
import ViewModel from "./Model";
import CardTotal from "./CardTotal";

function DeliveryPanel() {
  const [delivery, setDelivery] = useState(null);
  const [action, setAction] = useState(null);
  const [modalInfo, setModalInfo] = useState(false);
  const [msg, setMsg] = useState("");
  const [modalPayment, setModalPayment] = useState(false);
  const [domain, setDomain] = useState(["create_date", "=", null]);
  const [selectedRows, setSelectedRows] = useState(new Map());
  const session = store.get("ctxSession");
  const yesterday = tools.dateAdd(new Date(), "days", -1);

  function handleDelivery(name, value) {
    setDelivery(value);
  }

  function handleAction(e, name) {
    if (name === "dispached" && !delivery) {
      setModalInfo(true);
      setMsg("missing_delivery");
      return;
    }
    let dom = [
      ["kind", "=", "delivery"],
      ["sale_date", ">=", `${yesterday}`],
      ["shop", "=", session.shop],
    ];
    if (name === "dispached") {
      dom = dom.concat([
        ["state", "in", ["confirmed", "processing"]],
        ["order_status", "in", ["commanded", "dispatched"]],
        ["payment_method", "!=", "all_paid"],
        ["delivery_party", "=", delivery?.id],
      ]);
    } else {
      dom = dom.concat([
        ["state", "in", ["draft", "quotation", "confirmed", "processing"]],
        ["delivery_party", "=", null],
        ["order_status", "=", "commanded"],
        ["invoice_type", "=", "P"],
      ]);
    }
    setSelectedRows(new Map());
    setDomain(dom);
    setAction(name);
  }

  function updateRecords() {
    setDomain([...domain]);
  }

  function getPropsCard() {
    let res = { total_amount: 0, invoices: 0, ids: [] };
    for (const item of Array.from(selectedRows.values())) {
      res.total_amount = res.total_amount + item.total_amount;
      res.invoices = res.invoices + 1;
      res.ids.push(item.id);
    }
    return res;
  }

  function onClickRow(selected) {
    if (!selected) {
      setSelectedRows(new Map());
      return;
    }

    if (selected instanceof Map) {
      setSelectedRows(new Map(selected));
      return;
    }

    let selectedRows_ = new Map(selectedRows);
    if (selectedRows.get(selected.id)) {
      selectedRows_.delete(selected.id);
    } else {
      selectedRows_.set(selected.id, selected);
    }
    setSelectedRows(selectedRows_);
  }

  function ordersByArrive() {
    setModalPayment(true);
  }

  function clearOptions() {
    setAction(null);
    setDelivery(null);
    setSelectedRows(new Map());
  }

  async function dispatchOrders() {
    if (!delivery) {
      setModalInfo(true);
      setMsg("missing_delivery");
      return;
    }

    if (selectedRows.size <= 0) {
      setModalInfo(true);
      setMsg("no_invoices_selected");
      return;
    }
    for (const row of selectedRows.values()) {
      if (row.state == "draft") {
        setModalInfo(true);
        setMsg("must_bill");
        return;
      }
    }
    await setOrderStatus("dispatched");
  }

  async function setOrderStatus(state) {
    if (selectedRows && delivery) {
      const model = ViewModel.model;
      const delivery_id = delivery.id;
      for (const [id] of selectedRows.entries()) {
        const args = {
          model: model,
          storeRec: {
            id: id,
            delivery_party: delivery_id,
            order_status: state,
          },
        };
        await proxy.saveQuery(args);
      }
    }
    clearOptions();
  }

  function onClose(option) {
    if (option === "modalInfo") {
      setModalInfo(false);
      setMsg("");
    } else if (option === "modalPayment") {
      setModalPayment(false);
      clearOptions();
    }
  }

  async function searchDelivery(value) {
    const { data, error } = await proxy.search(
      "sale.shop",
      [["id", "=", session.shop]],
      ["id", "delivery_man"],
    );
    let dom = [["id", "in", data[0].delivery_man]];
    if (value && value !== "") {
      dom.push(["rec_name", "ilike", `%${value}%`]);
    }
    return dom;
  }

  return (
    <div className="m-4 md:m-6 w-full">
      <BoardHeader ctxView={{ model: "delivery.panel" }} />
      <div className="flex flex-col md:flex-row gap-4 lg:gap-80 mb-6">
        <div className="w-full lg:w-2/4">
          <DropdownField
            name="delivery"
            recSearch={searchDelivery}
            model={"sale.delivery_party"}
            onChange={handleDelivery}
            value={delivery}
            label={"delivery.panel.delivery"}
          />
        </div>
        <div className="w-full lg:w-2/4">
          <CardTotal {...getPropsCard()} action={action} />
        </div>
      </div>
      <div className="flex flex-row gap-10">
        <Button
          size="w-60"
          style={"h-14"}
          content="delivery.panel.by_dispached"
          name="byDispached"
          color="amber"
          onClick={(event, name) => handleAction(event, name)}
        />
        <Button
          size="w-60"
          style={"h-14"}
          content="delivery.panel.dispached"
          color="blue"
          name={"dispached"}
          onClick={(event, name) => handleAction(event, name)}
        />
        {action === "byDispached" ? (
          <Button
            size="w-60"
            style={"h-14"}
            content="delivery.panel.send"
            color="green"
            onClick={dispatchOrders}
          />
        ) : action === "dispached" ? (
          <Button
            size="w-60"
            style={"h-14"}
            content="delivery.panel.confirm_send"
            color="green"
            onClick={ordersByArrive}
          />
        ) : (
          <></>
        )}
        <Button
          size="w-60"
          style={"h-14"}
          content="delivery.panel.close"
          color="slate"
          onClick={clearOptions}
        />
      </div>
      <div className={"my-6"}>
        {action && (
          <QuickTable
            ctxView={ViewModel}
            domain={domain}
            onClickRow={onClickRow}
            updateRecords={updateRecords}
          />
        )}
        {msg && <ModalInfo open={modalInfo} onClose={onClose} msg={msg} />}
        {modalPayment && (
          <ModalPayment
            open={modalPayment}
            {...getPropsCard()}
            onClose={onClose}
          />
        )}
      </div>
    </div>
  );
}

export default DeliveryPanel;
