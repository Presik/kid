import React, { useState, useEffect } from "react";
import store from "store";
import proxy from "api/proxy";

import BasicModal from "components/BasicModal";
import DropdownField from "components/DropdownField";
import Divider from "components/Form/Divider";
import ProgressBar from "./ProgressBar";

const ModalPayment = (props) => {
  const { ids } = props;
  const [processing, setProcessing] = useState(false);
  const [statement, setStatement] = useState(false);
  const [progress, setProgress] = useState(0);
  const amount = new Intl.NumberFormat().format(props.total_amount);
  const session = store.get("ctxSession");

  async function getStatements(debouncedValue) {
    let dom = [];
    const { data, error } = await proxy.search(
      "res.user",
      [["id", "=", session.user]],
      ["sale_device"],
    );
    if (data) {
      dom = [
        ["sale_device", "=", data[0].sale_device],
        ["state", "=", "draft"],
      ];
      if (debouncedValue) {
        dom.push(["rec_name", "ilike", `%${debouncedValue}%`]);
      }
    }
    return dom;
  }

  async function acceptModal() {
    setProcessing(true);
  }

  useEffect(() => {
    // const totalRecords = props.ids.length;
    // const totalRecords = ids.length;

    const processRecords = async () => {
      if (ids.length > 0) {
        for (let i = 0; i < ids.length; i++) {
          await proxy.methodCall({
            model: "account.statement",
            method: "multipayment_invoices",
            args: [
              {
                statement_id: statement,
                sale_ids: [ids[i]],
              },
            ],
          });
          const progress = i + 1;
          setProgress(progress); // Actualizar el estado de la barra de progreso
        }
      }
      closeProcessing();
    };

    if (processing) {
      processRecords();
    }
  }, [ids, processing]);

  function closeProcessing() {
    setTimeout(() => {
      setProcessing(false);
      props.onClose("modalPayment");
    }, 1000);
  }

  function closeModal() {
    props.onClose("modalPayment");
  }

  function selectedItem(field, value) {
    setStatement(value.id);
  }

  return (
    <BasicModal
      open={true}
      buttons={["cancel", "ok"]}
      onClose={closeModal}
      onAccept={acceptModal}
      title={"CONFIRME LA FORMA DE PAGO"}
      width={"w-5/6 sm:my-8 sm:w-2/3 sm:p-6"}
      height={"h-[60vh]"}
    >
      <Divider />
      <div className="h-[30vh]">
        <h2 className="font-semibold h">Total a recibir: {amount}</h2>
        {!processing ? (
          <DropdownField
            name="statement"
            model="account.statement"
            recSearch={getStatements}
            onChange={selectedItem}
            labeled={false}
            placeholder="Seleccione el medio de pago..."
          />
        ) : (
          <>
            <div className="flex flex-col md:flex-row items-center justify-center gap-4 mb-4">
              <h3 className="text-slate-800 font-semibold">
                Espere un momento... procesando las facturas!
              </h3>
              <div className="flex items-center justify-center rounded-full w-10 h-10 bg-gradient-to-tr from-indigo-400 via-cyan-400 to-white animate-spin">
                <div className="h-7 w-7 rounded-full bg-gray-200"></div>
              </div>
            </div>
            <div className="text-right"></div>
            <ProgressBar progress={progress} maxValue={ids.length} />
          </>
        )}
      </div>
    </BasicModal>
  );
};

export default ModalPayment;
