// Purchase model line
import proxy from "api/proxy";

const getProduct = (value) => {
  const search = [
    "OR",
    [
      ["template.name", "ilike", `%${value}%`],
      ["template.purchasable", "=", true],
    ],
    [
      ["code", "ilike", `${value}%`],
      ["template.purchasable", "=", true],
    ],
  ];
  return search;
};

const onChangeProduct = async (activeRecord) => {
  const storeRec = {};
  let recordToUpdate = {
    description: "",
    taxes: {},
    unit_price: 0,
    quantity: 0,
    amount: 0,
  };
  if (activeRecord?.product) {
    const taxes = await getTaxes(activeRecord.product.id);
    recordToUpdate.description = activeRecord.product.name;
    recordToUpdate.taxes = taxes;
  }
  return [storeRec, recordToUpdate];
};

const onChangeQty = (activeRecord) => {
  const storeRec = {};
  let recordToUpdate = {};
  if (activeRecord.unit_price) {
    let price_w_tax = activeRecord.unit_price;
    if (activeRecord.taxes) {
      if (activeRecord.taxes.percentage > 0) {
        price_w_tax *= 1 + activeRecord.taxes.percentage;
      }
      if (activeRecord.taxes.fixed > 0) {
        price_w_tax += activeRecord.taxes.fixed;
      }
    }
    const amount = price_w_tax * activeRecord.quantity;
    recordToUpdate.amount = amount;
  }
  return [storeRec, recordToUpdate];
};

const getTaxes = async (product) => {
  const { data } = await proxy.methodCall({
    model: "purchase.purchase",
    method: "dash_get_taxes",
    args: [{ id: product }],
  });
  return data;
};

const onChangeUnitPrice = (activeRecord) => {
  const storeRec = {};
  let recordToUpdate = {};
  if (activeRecord.quantity) {
    let price_w_tax = activeRecord.unit_price;
    if (activeRecord.taxes.percentage > 0) {
      price_w_tax *= 1 + activeRecord.taxes.percentage;
    }
    if (activeRecord.taxes.fixed > 0) {
      price_w_tax += activeRecord.taxes.fixed;
    }
    const amount = price_w_tax * activeRecord.quantity;
    recordToUpdate.amount = amount;
  }
  return [storeRec, recordToUpdate];
};

const getView = () => {
  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
  let DictCtxView = {
    form_action_add: "modal",
    model: "purchase.line",
    form_action: ["delete", "add", "edit"],
    table_action: ["edit", "add"],
    row_selectable: false,
    validate_state: true,
    target: "purchase",
    webfields: {
      product: {
        type: "many2one",
        model: "product.product",
        recSearch: getProduct,
        withChange: onChangeProduct,
        attrs: ["id", "name"],
        required: true,
        readOnly: ["quotation"],
      },
      quantity: {
        type: "number",
        withChange: onChangeQty,
        required: true,
        decimalPlaces: 2,
      },
      unit: { type: "number", readOnly: ["quotation"] },
      unit_price: {
        type: "number",
        withChange: onChangeUnitPrice,
        required: true,
        decimalPlaces: 4,
      },
      description: { type: "char", readOnly: true },
      amount: { type: "number", readOnly: true, decimalPlaces: 2 },
    },
    webtree: [
      { name: "product", width: "30%" },
      { name: "quantity", width: "10%" },
      { name: "unit_price", width: "15%" },
      { name: "amount", width: "15%" },
      { name: "description", width: "30%" },
    ],
    webform: [
      { name: "product" },
      { name: "quantity", component: "increment" },
      { name: "unit_price" },
      { name: "amount" },
      { name: "description" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
