// Surveillance Schedule
import tools from "tools/common";

let today = tools.fmtDate2Tryton(new Date());

const searchLocation = () => {
  const dom = `[
      ('type', '=', 'storage'),
    ]`;
  return dom;
};

const getFilters = (session) => {
  return {
    today: `[
      ('create_date', '>=', '${today} 00:00:00'),
      ('create_uid', '=', ${session.user}),
    ]`,
  };
};

export default {
  model: "surveillance.schedule.shift",
  filters: getFilters,
  webfields: {
    guard: {
      type: "many2one",
      recSearch: searchLocation,
      model: "stock.location",
      readOnly: ["request"],
      required: true,
    },
    // to_location: {
    //   type: 'many2one',
    //   model: 'stock.location',
    //   default: getToLocation,
    //   readOnly: true,
    //   required: true,
    // },
    // effective_date: {
    //   type: 'date',
    //   readOnly: ['request'],
    //   required: true,
    // },
    // state: {type: 'char', readOnly: true, translate: true, default: 'draft'},
    // comment: {type: 'text-area', readOnly: ['quotation']},
    // wait: {
    //   type: 'button',
    //   method: 'dash_wait',
    //   visible: ['draft'],
    // },
  },
  webtree: [
    { name: "guard", width: "20%" },
    // {name: 'from_location', width: '20%'},
    // {name: 'to_location', width: '35%'},
    // {name: 'effective_date', width: '10%'},
    // {name: 'state', width: '10%'},
  ],
  webform: [
    { name: "guard", width: "20%" },
    // {name: 'from_location', width: '20%'},
    // {name: 'to_location', width: '35%'},
    // {name: 'effective_date', width: '10%'},
    // {name: 'state', width: '10%'},
  ],
};
