// // import React, { Component } from "react";
// // import { Grid, Button, Input } from "semantic-ui-react";
// // import store from "store";
// //
// // import intl from "i18n/messages";
// // import TakeCapture from "components/Form/TakeCapture";
// // import proxy from "api/proxy";
// // import "./StaffControlAccess.css";
// // import Loading from 'components/Tools/Loading';
// // import ctxView from './ModelStaffControlAccess';
//
// class StaffControlAccess extends Component {
//   constructor(props) {
//     super(props);
//     this.setShiftKind = this.setShiftKind.bind(this);
//     this.onEnter = this.onEnter.bind(this);
//     this.onChange = this.onChange.bind(this);
//     this.searchClicked = this.searchClicked.bind(this);
//     this.onTakePhoto = this.onTakePhoto.bind(this);
//     this.state = {
//       session: store.get("ctxSession"),
//       employee_id: "",
//       employee: "NOMBRE DEL EMPLEADO",
//       employee_active: false,
//     };
//   }
//
//   async getTranslation() {
//     const messages_ = await intl.messages();
//     return messages_;
//   }
//
//   async onTakePhoto(event, value) {
//     console.log("onTakePhoto....", value);
//   }
//
//   async searchClicked(event, value) {
//     const { employee_id } = this.state;
//     console.log("searchClicked....", event, value, employee_id);
//     this.setState({
//       employee: "FABIAN DUARTE",
//       employee_active: true,
//     });
//   }
//
//   async onEnter() {
//     console.log("onEnter....");
//   }
//
//   async onChange(event, data) {
//     console.log("onEnter....", data.value);
//     this.setState({
//       employee_id: data.value,
//     });
//   }
//
//   async setShiftKind() {
//     const res = await proxy.search("surveillance.shift.kind", "[]", [
//       "name",
//       "code",
//       "start",
//       "end",
//       "total_time",
//     ]);
//     let kindShifts = new Map();
//     res.forEach((s) => {
//       let rec = {
//         key: s.id,
//         text: s.code,
//         value: s.code,
//         record: {
//           id: s.id,
//           code: s.code,
//           start: s.start,
//           total_time: s.total_time,
//         },
//       };
//       kindShifts.set(s.code, rec);
//     });
//     this.setState({ kindShifts });
//   }
//
//   // async setPositions() {
//   //   const res = await proxy.search('surveillance.location.position', '[]', [
//   //     'name',
//   //     'code',
//   //     'start',
//   //     'end',
//   //     'total_time',
//   //   ]);
//   //   let kindShifts = new Map();
//   //   res.forEach((s) => {
//   //     let rec = {
//   //       key: s.id,
//   //       text: s.code,
//   //       value: s.code,
//   //       record: {
//   //         id: s.id,
//   //         code: s.code,
//   //         start: s.start,
//   //         total_time: s.total_time,
//   //       },
//   //     };
//   //     kindShifts.set(s.code, rec);
//   //   });
//   //   this.setState({kindShifts});
//   // }
//
//   render() {
//     const { employee, employee_active } = this.state;
//     return (
//       <Grid>
//         <Grid.Row className="staff-access-row">
//           <Input
//             readonly={false}
//             name="guard"
//             className="staff-access-input-id"
//             placeholder="INGRESE SU NÚMERO DE CÉDULA"
//             onChange={this.onChange}
//             icon={{
//               name: "search",
//               circular: true,
//               link: true,
//               onClick: this.searchClicked,
//               inverted: true,
//             }}
//           />
//         </Grid.Row>
//         <Grid.Row className="staff-access-row">
//           <p className="staff-access-employee-name">{employee}</p>
//         </Grid.Row>
//         <Grid.Row className="staff-access-row">
//           <TakeCapture
//             disabled={employee_active}
//             name="employee"
//             onChange={this.onTakePhoto}
//             expand={16}
//           />
//         </Grid.Row>
//         <Grid.Row className="staff-access-row">
//           <Button
//             key="Enter"
//             color="teal"
//             disabled={!employee_active}
//             className="staff-control-button"
//             onClick={() => this.onEnter()}
//           >
//             ENTRAR
//           </Button>
//         </Grid.Row>
//         <Grid.Row className="staff-access-row">
//           <Button
//             key="Exit"
//             color="pink"
//             disabled={!employee_active}
//             className="staff-control-button"
//             onClick={() => this.onEnter()}
//           >
//             SALIR
//           </Button>
//         </Grid.Row>
//       </Grid>
//     );
//   }
// }
//
// export default StaffControlAccess;
