import React, { Fragment, useState, useEffect } from "react";
import FormWizard from "components/Form/FormWizard";
import { useWizardStore } from "store/wizardStore";
import { useFormStore } from "store/formStore";
import proxy from "api/proxy";
import dates from "tools/dates";
import ModelWizardPayment from "./ModelWizardPayment";
import ModelSale from "./ModelSale";
import func from "../../tools/functions";
import { useBtnLoading } from "store/btnLoading";
import { array } from "prop-types";

const WizardAddPayment = ({ onClose }) => {
  let [ctxViewAc, setCtxViewAc] = useState(null);
  const { storeWizard, resetWizard } = useWizardStore();
  const [arrayNoAvalaible, setArrayNoAvalaible] = useState([]);
  const { activeRecord, storeRecord, upStoreRecord, setActiveRecordFromId } =
    useFormStore();

  const { setButtonLoading } = useBtnLoading();

  const ctxViewSale = ModelSale.ctxView();
  useEffect(() => {
    // Llama a validateQuote cuando el componente se monta
    validateQuote();
  }, []);

  const handleAddPayment = async () => {
    if (!activeRecord.party) {
      onClose(false, "missing_party");
      return;
    }

    let recordId = activeRecord.id;
    if (activeRecord.state !== "done") {
      const statusValidate = await validateQuote();
      console.log(statusValidate, "state");
      if (statusValidate == false) {
        console.log("paso por aca ");
        return false;
      }
    }

    if (recordId < 0) {
      recordId = await createSaleRecord();
    }
    if (activeRecord.invoice_type == "M") {
      let _ctxView = { ...ctxView };

      onClose();
      setActiveRecordFromId(recordId, "sale.sale", ctxViewSale);
      resetWizard();
      setButtonLoading(false);
      return false;
    }

    const toStore = {
      statement: storeWizard.statement.id,
      statement: storeWizard.statement.id,
      amount: storeWizard.amount,
      number: storeWizard.voucher,
      date: dates.dateToday(),
      description: activeRecord.number,
      party: activeRecord.party.id,
      account: activeRecord.party.account_receivable,
      source: `sale.sale,${recordId}`,
      sale: recordId,
    };

    const { data, error } = await createStatementLine(toStore);

    if (data) {
      const _storeRecord = { ...storeRecord };
      _storeRecord.sale_id = recordId;
      const saleDoneError = await callSaleDoneMethod(recordId);
      onClose(true, saleDoneError);
      setActiveRecordFromId(recordId, "sale.sale", ctxViewSale);
      resetWizard();
      setButtonLoading(false);
    }

    return true;
  };

  const validateQuote = async () => {
    const store = { ...storeRecord };
    const fieldsActi = [
      "id",
      "time_start",
      "time_end",
      "available",
      "schedule.kind.name",
    ];
    let to_create = store.lines.get("create");
    const origenes = [];
    const AllActivities = [];
    for (const [key, line] of to_create) {
      let id_origen = line.origin.split(",");
      AllActivities.push({
        id_acti: id_origen[1],
        id: line.id,
        quantity: line.quantity,
        origin: parseInt(id_origen[1]),
      });
      origenes.push(id_origen[1]);
    }
    const { data: dataQuote, error: error } = await proxy.browse(
      "sale_activity.activity",
      origenes,
      fieldsActi,
    );
    // Filtrar array2 según las condiciones
    const newArray = dataQuote.filter((item2) => {
      const matchingItem1 = AllActivities.find(
        (item1) => item1.id_acti === item2.id.toString(),
      );
      if (matchingItem1 && matchingItem1.quantity <= item2.available) {
        return false; // No cumple la condición
      }
      return true; // Cumple la condición
    });
    console.log("todas las actividades", AllActivities);
    console.log("VALIDACION DE HORAS", dataQuote);
    // ESTE ES EL NUEVO ARRAY QUE NO CUMPLE CON LAS CONDICIONES
    console.log(newArray);
    if (newArray.length === 0) {
      return true;
    } else {
      setArrayNoAvalaible(newArray);
      setButtonLoading(false);
      return false;
    }
  };

  const createSaleRecord = async () => {
    const model = "sale.sale";
    const _activeRecord = { ...activeRecord };
    const _storeRecord = { ...storeRecord };
    _storeRecord.status = "processing";
    const _data = func.recToTryton(_storeRecord);
    const args = {
      model: "sale.sale",
      method: "create_sale",
      args: [_data],
    };
    const { data: res, error } = await proxy.methodCall(args);
    console.log(error, "errores");
    return res.record.id;
  };

  const createStatementLine = async (toStore) => {
    const model = "account.statement.line";
    const fields_names = ["id"];
    return await proxy.create(model, toStore, fields_names);
  };

  const callSaleDoneMethod = async (recordId) => {
    const sale = { sale_id: recordId };
    const argSaleDone = {
      model: "sale.sale",
      method: "faster_process",
      args: [sale],
    };
    const { data } = await proxy.methodCall(argSaleDone);
    // self.Sale.reconcile_invoice({'sale_id': self.sale_to_post_id})

    const argSalePost = {
      model: "sale.sale",
      method: "faster_post_invoice",
      args: [sale],
    };

    const { dataPost } = await proxy.methodCall(argSalePost);

    const argSaleReconcile = {
      model: "sale.sale",
      method: "reconcile_invoice",
      args: [sale],
    };

    const { dataReconcile, errorReconcile } = await proxy.methodCall(
      argSaleReconcile,
    );
    return dataReconcile;
  };

  const ctxView = ModelWizardPayment.ctxView();

  return (
    <Fragment>
      {arrayNoAvalaible.map((item, key) => {
        return (
          <div
            key={key}
            className="bg-red-400 p-3 rounded-md shadow-sm text-center mb-1"
          >
            <p className="">
              <span className="font-medium">
                {item["schedule."]["kind."].name}
              </span>{" "}
              <span>cuenta con </span>
              <span className="font-medium  text-black text-lg">
                {" "}
                {item.available}
              </span>
              {"  "}
              de disponibilidad
            </p>
          </div>
        );
      })}
      <FormWizard
        key="hotel-booking-wizard-payment"
        ctxView={ctxView}
        parentRec={activeRecord}
        handleButton={handleAddPayment}
      />
    </Fragment>
  );
};

export default WizardAddPayment;
