const getType = () => {
  const options = [
    { id: "phone", name: "Teléfono" },
    { id: "mobile", name: "Móvil" },
    { id: "fax", name: "Fax" },
    { id: "email", name: "Correo electrónico" },
    { id: "website", name: "Sitio Web" },
    { id: "skype", name: "Skype" },
    { id: "sip", name: "SIP" },
    { id: "irc", name: "IRC" },
    { id: "jabber", name: "Jabber" },
    { id: "other", name: "Otro" },
  ];

  return options;
};

const getView = () => {
  let ctxView = {
    model: "party.addresses",
    selectable: "one",
    activeSearch: true,
    filters: null,
    form_action: ["save", "add"],
    table_action: ["add", "edit"],

    // selectable: "one",
    webfields: {
      street: {
        type: "char",
        // translate: true,
      },
      postal_code: {
        type: "char",
        // translate: true,
        default: "",
      },
      party: {
        type: "many2one",
        model: "party.party",
        recSearch: () => [],
      },
      country_code: {
        type: "many2one",
        model: "party.country_code",
        recSearch: () => [],
      },
      department_code: {
        type: "many2one",
        model: "party.department_code",
        recSearch: () => [],
      },

      // nationality: {
      //   type: "many2one",
      //   model: "country.country",
      //   recSearch: () => [],
      // },
      city_code: {
        type: "many2one",
        model: "party.city_code",
        recSearch: () => [],
      },
    },
    webtree: [
      { name: "street" },
      { name: "country_code" },
      { name: "department_code" },
      { name: "city_code" },
      // { name: "nationality" },
      // { name: "postal_code" },
      // { name: "amount", width: "15%" },
      // { name: "description", width: "30%" },
    ],
    webform: [
      { name: "street" },
      { name: "country_code" },
      { name: "department_code" },
      { name: "city_code" },
      // { name: "nationality" },
      // { name: "postal_code" },
    ],
  };

  return ctxView;
};

export default { ctxView: getView };
