import proxy from "api/proxy";
import store from "store";

import ModelSaleLine from "./ModelSaleLine";
import ModelParty from "./ModelParty";
import WizardAddPayment from "./WizardAddPayment";
import ProductDiscount from "./components/ProductDiscount";
import fetchLocalAPI from "printing/printing";
import { ModalWizzardFolios } from "./components/ModalWizardFolios";
const session = store.get("ctxSession");

const stateColors = {
  draft: "gray",
  processing: "amber",
  done: "lime",
  cancelled: "rose",
  transferred: "sky",
};

const onPrintBrowser = async (record) => {
  const model = "sale.sale";
  const fieldsActi = ["id", "time_start", "time_end"];
  const session = store.get("ctxSession");
  const args = { user_id: session.user };
  const { data: ctxPrinting } = await proxy.methodCall({
    model: model,
    method: "get_printing_context",
    args: [args],
  });

  console.log(ctxPrinting, "este es el ctx del print");
  const argSale = {
    type_doc: "invoice",
    sale_id: record.id,
  };
  const { data: sale } = await proxy.methodCall({
    model: model,
    method: "get_data",
    args: [argSale],
  });
  // const apiPrint = ctxPrinting.local_printer;
  console.log(sale);
  let _sale = record;
  _sale.order = sale?.order;
  _sale.payment_term = sale?.payment_term;
  _sale.create_date = new Date(record.sale_date);
  _sale.create_date = _sale.create_date.toLocaleDateString();
  _sale.party_address = record.party.address;
  _sale.total_amount = _sale["lines."].reduce((total, item) => {
    return total + item.base_price * item.quantity;
  }, 0);

  if (record.state !== "transferred" && record.invoice_type != "M") {
    const argSale = {
      type_doc: "invoice",
      sale_id: record.id,
    };

    let origins = record["lines."].map((item) => item.origin.id);

    const { data: activity, error: errorActivity } = await proxy.browse(
      "sale_activity.activity",
      origins,
      fieldsActi,
    );
    _sale = { ...record };
    _sale.party = record.party.rec_name;
    _sale.party_id_number = record.party.id_number;
    _sale.party_phone = record.party.mobile;

    _sale.untaxed_amount = _sale.total_amount;
    const _saleLine = _sale["lines."].map((act) => {
      let matchingActivity = activity.find((a) => a.id === act.origin.id);
      let timeStart = matchingActivity?.time_start;
      timeStart = timeStart.slice(0, -3);
      let name = act.product.name;
      const dataInvoice = {
        name: `${timeStart} ${name}`,
        origin: act.origin,
        unit_price_w_tax: act.amount,
        amount_w_tax: act.product.amount_w_tax,
        quantity: act.quantity,
        taxes: act.taxes,
        discount: act.discount,
      };
      return dataInvoice;
    });
    _sale.lines = _saleLine;
  } else {
    _sale.party = record.party.rec_name;
    _sale.party_id_number = record["party."].id_number;
    _sale.party_address = record["party."].address;
    _sale.party_phone = record["party."].mobile;
    _sale.payment_term = "Transferido";

    let origins2 = record["lines."].map((item) => item.origin);
    let ids = origins2.map((item) => item.id);
    const { data: activity2, error: errorActivity } = await proxy.browse(
      "sale_activity.activity",
      ids,
      fieldsActi,
    );
    let _saleLine = _sale["lines."].map((act) => {
      let matchingActivity = activity2.find((a) => a.id === act.origin.id);

      let timeStart = matchingActivity ? matchingActivity?.time_start : "";
      timeStart = timeStart.slice(0, -3);
      let name = act.product.name;
      return {
        name: `${timeStart} ${name}`,
        origin: act.origin,
        unit_price_w_tax: act.amount,
        amount_w_tax: act.amount,
        quantity: act.quantity,
        taxes: act.product.taxes,
        discount: act.discount,
      };
    });

    _sale.lines = _saleLine;
  }

  let html = `
    <div style="margin-top: 0.5cm; margin-bottom: 0.5cm; display: flex; justify-content: space-between; border-top: 1px dashed; border-bottom: 1px dashed; padding: 2mm;">
      <div>Articulo</div>
      <div>Subtotal</div>
    </div>
    ${_sale.lines
      .map(
        (item) => `
      <div style="display: flex; justify-content: space-between;margin-bottom: 0.1cm;">
        <div style="width:70%">${item?.name} ${
          item?.quantity != 1 ? "x " + item?.quantity : ""
        }</div>
        <div style="width:30% text-align:rigth">${item?.unit_price_w_tax}</div>
      </div>
    `,
      )
      .join("")}
    <div style="display: flex; justify-content: space-between;">
      <div>Impuestos:</div>
      <div style="text-align: right;">0</div>
    </div>
    <div style="display: flex; justify-content: space-between; margin-top: 0.5cm; ">
      <div>Total:</div>
      <div style="text-align: right;">
        <div style="border-top: 1px dashed; width: 20mm; margin-bottom: 0.1cm; "></div>
        ${_sale?.total_amount}
  
      </div>
    </div>
    <div style="margin-top: 0.3cm; text-align: center"><b>Por favor llegar al punto de encuentro 15
    minutos antes de la actividad</b></div>
  `;

  let htmlTax;
  if (record.state != "transferred" && record.invoice_type != "M") {
    htmlTax = `<div>DETALLE DE IMPUESTOS</div>
  <div
    style="${styleSeparator}; border-top:1px dashed ; border-botton:1px dashed; margin-top: 0.5cm;
  margin-bottom: 0.5cm;"
  ></div>
  <div style="display: flex; gap: 0.2cm; flex-direction: column">
    <div style="display: flex; justify-content: space-between">
      <div>MEDIO PAGO</div>
      <div># APROB</div>
      <div>VALOR</div>
    </div>
    ${sale.payments
      .map(
        (item) => `
        <div style="display: flex; justify-content: space-between">
        <div>${item.name}</div>
        <div>${item?.voucher || ""} </div>
        <div>${item.amount} </div>
      </div>
    `,
      )
      .join("")}
  
  </div>
  <div
    style="${styleSeparator}; border-top:1px dashed ; border-botton:1px dashed; margin-top: 0.5cm;
margin-bottom: 0.5cm;"
  ></div>
  <div style="display: flex; justify-content: space-between">
    <div>No ITEMS:</div>
    <div>${_sale.lines.length}</div>
  </div>
  <div style="display: flex; justify-content: space-between">
    <div>No PEDIDO:</div>
    <div>${_sale.order}</div>
  </div>
  <div style="display: flex; justify-content: space-between">
    <div>CAJA No.:</div>
    <div>${ctxPrinting.sale_device}</div>
  </div>
  <div style="display: flex; justify-content: space-between">
    <div>CAJERO:</div>
    <div>${ctxPrinting.user}</div>
  </div>
  <div
  style="${styleSeparator}; border-top:1px dashed ; border-botton:1px dashed; margin-top: 0.5cm;
margin-bottom: 0.5cm;"
></div>
<div>Autorizacion de facturacion ${
      ctxPrinting.authorizations[sale.invoice_type].kind
    } No ${ctxPrinting.authorizations[sale.invoice_type].number} del ${
      ctxPrinting.authorizations[sale.invoice_type].start_date_auth
    }, habilita desde ${
      ctxPrinting.authorizations[sale.invoice_type].to_auth
    } </div>
<div>${sale.invoice_type == "1" ? sale.cufe : ""}</div>
`;
  }

  const styleSeparator =
    " border-top: 1px dashed; margin-top:0.5cm; margin-bottom:0.5cm";
  const contenidoHTML = `    <div style="width: 62mm; padding: 0.1cm; font-family: monospace; font-size: 2.8mm">
  <div style="text-align: center">
    <div style="margin-bottom: 0.5cm">${ctxPrinting.header}</div>  
    <div>${ctxPrinting.company}</div>
    <div>${ctxPrinting.shop}</div>
    <div>NIT: ${ctxPrinting.id_number} ${ctxPrinting.regime_tax}</div>
    <div>${ctxPrinting.street}</div>
    <div>${ctxPrinting.city} Telefono: ${ctxPrinting.phone}</div>
  </div>
  <div style="margin-top: 0.5cm">${
    record.state != "transferred"
      ? `FACTURA DE VENTA No. ${sale?.number}`
      : `CARGOS DE FOLIO No. ${_sale.number}`
  }</div>
  <div>Fecha: ${_sale.create_date}</div>
  <div style="${styleSeparator}"></div>
  <div>
    <div>Cliente: ${_sale.comment || _sale.party}</div>
    <div>ID: ${_sale.party_id_number}</div>
    <div>Direccion: ${_sale.party_address}</div>
    <div>Telefono: ${_sale.party_phone}</div>
    <div>Forma de Pago: ${_sale.payment_term}</div>
  </div>
  ${html}

  <div
    style="${styleSeparator}; border-top:1px dashed ; border-botton:1px dashed; margin-top: 0.5cm;
    margin-bottom: 0.5cm;"
  ></div>
  ${htmlTax || ""}
  <div>Fecha de creacion: ${_sale.create_date} </div>
  <div style="margin-top: 0.5cm; text-align:center">SOFTWARE TRYTON - www.presik.com</div>
</div>`;

  const ventana = window.open("", "", "width=500,height=600");
  ventana.document.open();
  ventana.document.write(contenidoHTML);
  ventana.document.close();
  ventana.print();
};

const onPrint = async (record) => {
  const model = "sale.sale";
  const session = store.get("ctxSession");
  const args = { user_id: session.user };
  const { data: ctxPrinting } = await proxy.methodCall({
    model: model,
    method: "get_printing_context",
    args: [args],
  });

  const apiPrint = ctxPrinting.local_printer;
  const argSale = {
    type_doc: "invoice",
    sale_id: record.id,
  };
  const { data: sale } = await proxy.methodCall({
    model: model,
    method: "get_data",
    args: [argSale],
  });
  const origins = sale.lines.map((item) => item.origin);
  const fieldsActi = ["id", "time_start", "time_end"];
  const { data: activity, error: errorActivity } = await proxy.browse(
    "sale_activity.activity",
    origins,
    fieldsActi,
  );
  let _sale = { ...sale };
  const _saleLine = sale.lines.map((act) => {
    const matchingActivity = activity.find((a) => a.id === act.origin);
    let timeStart = matchingActivity ? matchingActivity.time_start : "";
    timeStart = timeStart.slice(0, -3);
    let name = act.name;
    return {
      name: `${timeStart} ${name}`,
      origin: act.origin,
      unit_price_w_tax: act.unit_price_w_tax,
      amount_w_tax: act.amount_w_tax,
      quantity: act.quantity,
      taxes: act.taxes,
      discount: act.discount,
    };
  });
  _sale.lines = _saleLine;

  const obj = { record: _sale, context: ctxPrinting };
  const printer = new fetchLocalAPI(apiPrint);
  const { data, error } = await printer.post("/print_invoice", obj);
  window.alert(data ?? error);
};

const defaultShop = async () => {
  const dom = [["id", "=", session.shop]];
  const fields = ["id", "rec_name", "payment_term"];
  const { data, error } = await proxy.search("sale.shop", dom, fields, 1);
  return data[0];
};

const defaultPaymentTerm = async () => {
  // const dom = [["id", "=", session.shop]];
  const dom = [];
  const fields = ["id", "rec_name"];
  const { data, error } = await proxy.search(
    "account.invoice.payment_term",
    dom,
    fields,
    1,
  );
  return data[0];
};

const defaultListPrice = async () => {
  const dom = [["id", "=", session.shop]];
  const fields = ["id", "rec_name"];
  const { data, error } = await proxy.search(
    "product.price_list",
    dom,
    fields,
    1,
  );
  return data[0];
};

const onChangeLines = (activeRecord, fieldValue) => {
  if (activeRecord.lines) {
    let total_amount = 0;
    for (let l of fieldValue.values()) {
      total_amount += l.amount;
    }
    activeRecord["total_amount"] = total_amount;
  }
  return activeRecord;
};

const visiblePrint = (name, record) => {
  let res = false;
  if (
    (record.state === "draft" && record.invoice_type == "M" && record.number) ||
    record.state === "processing" ||
    record.state === "transferred" ||
    record.state === "done"
  ) {
    res = true;
  }
  return res;
};

const visibleFolio = (name, record) => {
  let res = false;
  if (record.state === "draft" && record.lines && record.lines.size > 0) {
    res = true;
  }
  return res;
};

const disabledField = (record) => {
  let res = false;
  if (record.state !== "draft") {
    res = true;
  }
  return res;
};

const visibleDiscount = (name, record) => {
  let res = false;
  if (
    record.state === "draft" &&
    record.lines &&
    record.lines.size > 0 &&
    record.price_list &&
    record.party
  ) {
    res = true;
  }
  return res;
};

const visiblePay = (name, record) => {
  let res = false;
  if (
    record.lines &&
    record.lines.size > 0 &&
    // record.price_list &&
    record.party &&
    record.residual_amount != 0 &&
    record.state !== "transferred"
  ) {
    res = true;
  }
  return res;
};

const getSaleDevice = async () => {
  const session = await store.get("ctxSession");
  const dom = [["id", "=", session.user]];
  const fields = ["id", "sale_device", "rec_name"];
  const { data } = await proxy.search("res.user", dom, fields);
  if (data[0].sale_device) {
    let domDevice = [["id", "=", data[0].sale_device]];
    const { data: dataDevice, error } = await proxy.search(
      "sale.device",
      [domDevice],
      ["id", "rec_name"],
    );
    return dataDevice[0];
  }
  return false;
};

const getOptionsAsync = async () => {
  const res = await proxy.getFields("sale.sale", []);
  console.log("--------resultadosale", res);
};
// getOptionsAsync();
const getFilters = (session) => {
  return {
    draft: [["state", "=", "draft"]],
    done: [["state", "=", "done"]],
    trasnferred: [["state", "=", "transferred"]],
    // my_sales: [["create_uid", "=", `${session.user}`]],
  };
};
// const getTotalAmount = (record) => {
//   const lines = record?.["lines."];
//   const linesActive = record?.["lines"];
//   if (lines) {
//     const totalAmount = lines.reduce((total, item) => {
//       return total + item.amount * item.quantity;
//     }, 0);

//     return Number(totalAmount).toLocaleString("es", { useGrouping: true });
//   }
//   if (linesActive !== undefined && !lines) {
//     const totalAmount = [...linesActive.values()].reduce(
//       (accumulator, currentValue) => {
//         const amount = parseFloat(currentValue.amount);
//         return accumulator + amount;
//       },
//       0,
//     );
//     return Number(totalAmount).toLocaleString("es", { useGrouping: true });
//   }
// };

const getView = (config) => {
  let DictCtxView = {
    model: "sale.sale",
    activeSearch: true,
    filters: getFilters,
    domain: [["shop", "=", session.shop]],
    form_action: ["save", "edit", "add"], // options: ['save', 'delete']
    table_action: ["open"], // options: ['open', 'delete', 'edit', 'add']
    // pagination: [],
    limit: 100,
    tags: {
      state: stateColors,
    },
    selectable: null, // Options for table rows: null, multi, one
    title: { field: "number", model: true },
    webfields: {
      id: { type: "integer", readOnly: true },
      name: { type: "name", readOnly: true },
      number: { type: "char", readOnly: true, searchable: true },
      "party.name": {
        type: "char",
        searchable: true,
      },
      "party.address": {
        type: "char",
      },
      "party.id_number": {
        type: "char",
        searchable: true,
      },
      "party.addresses": {
        type: "char",
      },
      "party.phone": {
        type: "char",
        // searchable: true,
      },
      "party.mobile": {
        type: "char",
        call: ["mobile", "whatsapp"],
        // searchable: true,
      },
      "party.account_payable": {
        type: "many2one",
      },
      "party.account_receivable": {
        type: "many2one",
      },
      party: {
        type: "many2one",
        model: "party.party",
        ctxView: ModelParty.ctxView(),
        required: true,
        searchable: true,
        default: {
          id_number: "222222222222",
          id: 34438,
          addresses: [27934],
          mobile: "",
          phone: "1",
          address: "VILLAVICENCIO",
          account_payable: 514,
          account_receivable: 334,
          rec_name: "CONSUMIDOR FINAL",
        },
        attrs: [
          "account_receivable",
          "mobile",
          "account_payable",
          "address",
          "addresses",
          "phone",
          "id_number",
        ],
        readOnly: disabledField,
      },

      create_uid: {
        type: "many2one",
        model: "res.user",
        readOnly: true,
      },
      payment_term: {
        type: "many2one",
        required: true,
        model: "account.invoice.payment_term",
        // options: getOptions(),
        translate: true,
        default: defaultPaymentTerm,
        readOnly: true,
      },
      shop: {
        type: "many2one",
        model: "sale.shop",
        readOnly: true,
        default: defaultShop,
        required: true,
      },
      invoice_address: {
        type: "many2one",
        model: "party.address",
        recSearch: () => [],
      },

      sale_date: {
        type: "date",
        readOnly: true,
        default: () => new Date(),
        required: true,
      },

      source: {
        type: "many2one",
        model: "sale.source",
        readOnly: true,
      },

      state: {
        type: "char",
        readOnly: true,
        translate: true,
        default: "draft",
        tags: stateColors,
      },

      residual_amount: {
        type: "number",
        readOnly: true,
      },
      paid_amount: {
        type: "number",
        readOnly: true,
      },
      total_amount: {
        type: "number",
        readOnly: true,
        // recibar esta funcion si es necesaria para el proceso
        // function: getTotalAmount,
      },
      comment: {
        type: "char",
        searchable: true,
        readOnly: disabledField,
      },
      sale_device: {
        type: "many2one",
        model: "sale.device",
        recSearch: () => [],
        required: true,
        default: getSaleDevice,
        readOnly: true,
      },

      price_list: {
        type: "many2one",
        recSearch: () => [],
        model: "product.price_list",
        required: true,
        default: defaultListPrice,
        readOnly: true,
      },

      invoice_type: {
        type: "radio-group",
        options: [
          { value: "P", text: "POS" },
          { value: "1", text: "ELECTRONICA" },
          { value: "M", text: "AC" },
        ],
        default: "P",
        required: true,
        readOnly: disabledField,
      },
      lines: {
        type: "one2many",
        model: "sale.line",
        ctxView: ModelSaleLine.ctxView(config),
        withChange: onChangeLines,
      },
      pay: {
        type: "button",
        button_method: "",
        visible: ProductDiscount,
        color: "blue",
        onSuccessMsg: "ORDEN COMANDADA...!",
      },
      add_payment: {
        type: "button-wizard",
        Component: WizardAddPayment,
        color: "lime",
        icon: "fi fi-rr-add",
        style: "w-10",
        visible: visiblePay,
        resetWizard: false,
      },
      folios: {
        type: "button-wizard",
        Component: ModalWizzardFolios,
        color: "blue",
        icon: "fi fi-rr-add",
        style: "w-10",
        visible: visibleFolio,
        resetWizard: false,
      },
      transferred_events: {
        type: "button-wizard",
        // Component: ModalWizzardFolios,
        color: "yellow",
        icon: "fi fi-rr-add",
        style: "w-10",
        visible: visibleFolio,
        resetWizard: false,
      },
      add_discount: {
        type: "button-wizard",
        Component: ProductDiscount,
        color: "blue",
        icon: "fi fi-rr-add",
        visible: visibleDiscount,
        resetWizard: false,
      },
      pay_method: {
        type: "button",
        method: "create_sale",
        visible: visiblePay,
        style: "p-22",
      },
      print: {
        type: "button",
        method: onPrint,
        visible: visiblePrint,
        // size: "w-32",
        color: "amber",
        iconRight: "fi fi-rr-print",
      },

      printBrowser: {
        type: "button",
        method: onPrintBrowser,
        visible: visiblePrint,
        // size: "w-32",
        color: "amber",
        iconRight: "fi fi-rr-print",
      },
    },
    webtree: [
      { name: "number", width: "20%" },
      { name: "party.name" },
      { name: "comment" },
      { name: "party.id_number" },
      { name: "party.mobile", widget: "call-action" },
      { name: "create_uid" },
      { name: "sale_date" },
      { name: "paid_amount" },
      { name: "state", widget: "badge" },
      // { name: "print", width: "8%" },
    ],
    webform: [
      { name: "party", widget: "search-add-new" },
      {
        id: "cunstomer",
        grid: [{ name: "number" }, { name: "sale_date" }],
        size: [1, 2],
        span: "col-span-1",
      },
      {
        id: "consumer_party",
        grid: [
          { name: "comment" },

          { name: "invoice_type" },
          { name: "shop" },
          // { name: "price_list" },
          { name: "sale_device" },
          // { name: "payment_term" },
          { name: "add_discount" },
        ],
        size: [1, 4],
        span: "col-span-2",
      },
      { name: "lines", component: "modal" },

      {
        id: "paid",
        grid: [
          { name: "total_amount" },
          { name: "paid_amount" },
          { name: "residual_amount" },
          { name: "state", width: "7%" },
        ],
        size: [1, 4],
        span: "col-span-2",
      },
      {
        id: "actions",
        // grid: [{ name: "add_payment" }],
        grid: [
          { name: "add_payment" },
          // { name: "print" },
          { name: "printBrowser" },
          { name: "folios" },
          // { name: "transferred_events" },
        ],
        size: [1, 4],
        span: "col-span-2",
      },

      // { name: "add_activity" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
