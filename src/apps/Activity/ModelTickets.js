// Party model
import party from "./ModelParty";
import ActivityModel from "./ModelActivity";

const getView = () => {
  let DictCtxView = {
    model: "sale_activity.ticket",
    form_action: ["edit", "add", "save"],
    table_action: ["edit"],
    domain: [],
    webfields: {
      // number: { type: "char" },
      salesman: { type: "many2one", model: "company.employee" },

      // name: { type: "char", editable: true, editableTree: true },
      activity: {
        type: "many2one",
        model: "sale_activity.activity",
        // ctxView: ModelActivity.ctxView(),
        searchable: true,

        // readOnly: true,
      },
      party: {
        type: "many2one",
        model: "party.party",
        searchable: true,
        ctxView: party.ctxView(),
        attrs: ["account_receivable"],
      },
      price_list: {
        type: "many2one",
        model: "product.price_list",
        ctxView: ActivityModel.ctxView(),
      },
      id: {
        type: "char",
        readOnly: true,
      },
      number: {
        type: "char",
        readOnly: true,
      },
      rec_name: {
        type: "number",
        readOnly: true,
      },

      total_amount: {
        type: "char",
        readOnly: true,
      },
      create_date: {
        type: "char",
        readOnly: true,
      },
    },
    webtree: [
      { name: "rec_name", width: "30%" },
      { name: "party", width: "30%" },
      { name: "price_list", width: "30%" },
      { name: "activity", width: "30%" },
      { name: "salesman", width: "30%" },
      { name: "create_date", width: "30%" },
      { name: "total_amount", width: "30%" },
      // { name: "party", width: "30%" },
      // { name: "price_list", width: "30%" },
    ],
    webform: [
      { name: "party", widget: "search-add" },
      { name: "activity" },
      { name: "total_amount" },
      { name: "rec_name" },
      { name: "number" },
      // {name:}
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
