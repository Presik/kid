import store from "store";
import proxy from "api/proxy";

async function getStatements(value) {
  const ctxSession = store.get("ctxSession");

  let dom = [];
  const { data, error } = await proxy.search(
    "res.user",
    [["id", "=", ctxSession.user]],
    ["sale_device"],
  );
  if (data) {
    dom = [
      ["sale_device", "=", data[0].sale_device],
      ["state", "=", "draft"],
    ];
    if (value) {
      dom.push(["rec_name", "ilike", `%${value}%`]);
    }
  }
  return dom;
}

const getView = () => {
  let DictCtxView = {
    model: "hotel.booking.add_payment",
    form_action: [],
    table_action: [],
    webfields: {
      statement: {
        type: "many2one",
        model: "account.statement",
        readOnly: false,
        required: true,
        recSearch: getStatements,
        // default: getStatements,
      },
      amount: {
        type: "number",
        readOnly: false,
        required: true,
        // function: defaultAmount,
        // default: 50000,
      },
      voucher: {
        type: "char",
        readOnly: false,
        // required: true,
      },
      pay: {
        type: "button",
        // method: addPayment, This method is added in custom model
        color: "green",
        iconRight: "fi fi-rr-dollar",
        visible: true,
      },
    },
    webform: [
      { name: "statement" },
      { name: "amount" },
      { name: "voucher" },

      {
        id: "paid",
        grid: [{ name: "pay" }],
        size: [1, 3],
        span: "col-span-2",
      },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
