import proxy from "api/proxy-old";
import ProductDiscount from "./components/ProductDiscount";

// Sale model line
const searchUnit = (value, record) => {
  let dom = [["category", "=", record?.unit?.category]];
  if (value) {
    dom.push(["name", "ilike", `%${value}%`]);
  }
  return dom;
};

const onChangeAmount = (activeRecord) => {
  let upToStore = {};
  let upToActive = {};
  if (activeRecord.amount && activeRecord.quantity) {
    const { amount, quantity } = activeRecord;
    upToActive.total_amount = amount * quantity;
    upToActive.unit_price = amount;
  }
  return [upToStore, upToActive];
};

const getSalePrices = async (activeRecord, parentRecord) => {
  const unit_price = await proxy.methodInstance({
    model: "product.product",
    method: "_get_sale_unit_price",
    instance: activeRecord?.product?.id ?? activeRecord.product,
    args: [activeRecord.quantity],
    ctx: {
      price_list: parentRecord?.price_list?.id,
      uom: activeRecord.unit ?? null,
      customer: parentRecord?.party?.id,
    },
  });
  if (!activeRecord.product) {
    return {};
  }

  const price_w_tax = await proxy.methodCall({
    model: "sale.sale",
    method: "dash_get_amount_w_tax",
    args: [
      {
        product: activeRecord.product["id"],
        list_price: unit_price,
      },
    ],
  });
  // const factor = activeRecord.unit.factor; // ???? bug
  return {
    unit_price: Number(unit_price),
    price_w_tax: Number(price_w_tax).toFixed(2),
  };
};

function accDelete(name, rec) {
  let res = false;
  console.log(res, "-----------------------desde el local", name);

  if (rec && rec.state !== "requested") {
    console.log("rec .....", rec);
    res = false;
  }
  return res;
}

function accEdit(name, rec) {
  console.log(res, "desde el local");
  let res = false;
  if (rec && rec.status_order === "draft") {
    console.log("rec .....", rec);
    res = false;
  }
  return res;
}

function visibleDiscount(name, rec) {
  let res = false;
  if (rec.status_order !== "requested") {
    res = true;
  }
  return res;
}
const getView = () => {
  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
  let DictCtxView = {
    form_action_add: "modal",
    model: "sale.line",
    row_selectable: false,
    table_action: ["delete"],
    form_action: ["add", "delete"],
    access: {
      delete: accDelete,
      edit: accDelete,
    },
    otherActions: [],
    selectable: true,
    webfields: {
      product: {
        type: "many2one",
        model: "product.product",
        attrs: [
          "id",
          "list_price",
          "name",
          // "sale_price_taxed",
          // "sale_uom.rec_name",
          // "categories",
        ],
        required: true,
      },
      "product.code": {
        type: "char",
        readOnly: true,
        // readOnly: true,
        // attrs: ["id", "name", "category", "factor"],
      },
      unit: {
        type: "many2one",
        model: "product.uom",
        recSearch: searchUnit,
        // readOnly: true,
        // attrs: ["id", "name", "category", "factor"],
      },
      quantity: {
        type: "number",
        required: true,
        default: 1,
      },
      base_price: {
        type: "number",
      },
      discount: { type: "char", readOnly: true },
      status_order: {
        type: "char",
        readOnly: true,
        default: "draft",
        translate: true,
      },
      unit_price: { type: "number" },
      sale_price_taxed: { type: "number" },
      amount: { type: "number", readOnly: true, withChange: onChangeAmount },
      "origin.time_start": { type: "char" },
      "origin.time_end": { type: "char" },
      origin: { type: "many2one", readOnly: true },
      add_discount: {
        type: "button",
        method: () => {
          return <p>hola mundo</p>;
        },
        color: "blue",
        icon: "fi fi-rr-add",
        visible: visibleDiscount,
      },
    },
    webtree: [
      { name: "add_discount", width: "40%" },
      { name: "product", width: "40%" },
      { name: "origin.time_start", width: "23%" },
      { name: "origin.time_end", width: "23%" },
      { name: "quantity", width: "10%" },
      { name: "unit", width: "23%" },
      { name: "base_price", width: "23%" },
      // { name: "discount", width: "23%" },
      { name: "amount", width: "23%" },
      { name: "status_order", width: "10%" },
    ],
    webform: [
      { name: "product" },
      { name: "unit" },
      { name: "quantity" },
      { name: "amount" },
    ],
  };
  return DictCtxView;
};

export default { ctxView: getView };
