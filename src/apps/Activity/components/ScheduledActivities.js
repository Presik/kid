import React, { useEffect, useState } from "react";
import proxy from "api/proxy";
import date from "date-and-time";

import imgRidding from "../../../assets/img/caballo.svg";
import imgRiddingNigth from "../../../assets/img/ridding-nigth.png";
import imgRiven from "../../../assets/img/riven.svg";
import imgWalk from "../../../assets/img/walking.svg";
import imgBuggy from "../../../assets/img/buggie.svg";
import imgRappel from "../../../assets/img/rappel.svg";
import imgGanado from "../../../assets/img/ganado.svg";
import imgAmanecer from "../../../assets/img/amacener.svg";
import imgLlanero from "../../../assets/img/llanero.svg";
import imgDelta from "../../../assets/img/delta.svg";
import SectionNoData from "components/Tools/SectionNoData";
import ModalWizardActi from "./ModalWizardActi";
import ModalWizzardActivity from "./ModalWizzardActivity";
import ItemCard from "./ItemCard";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import { SearchActivities } from "./SearchActivities";

const imagesKind = [
  {
    code: "cabalgata-noche",
    image: imgRidding,
  },
  { code: "pasaporte-aldea", image: imgBuggy },
  { code: "rio-casique", image: imgRiven },
  { code: "cabalgata-rancho", image: imgRiddingNigth },
  { code: "caminata", image: imgWalk },
  { code: "rappel", image: imgRappel },
  { code: "travesia", image: imgAmanecer },
  { code: "delta", image: imgDelta },
  { code: "desafio-vaquiano", image: imgGanado },
  { code: "ganado", image: imgGanado },
  { code: "cultura-llanera", image: imgLlanero },
];

export const ScheduledActivities = (props) => {
  let [activityData, setActivityData] = useState([]);
  const [originalActivityData, setOriginalActivityData] = useState([]);
  let [record, setRecord] = useState(null);
  let [onOpen, setOnOPen] = useState(false);
  let [ctxViewActive, setCtxViewActive] = useState(null);

  let day = new Date(props.currentDay);
  let dayFormatted = date.format(day, "YYYY-MM-DD");
  let dayFormattedEnd = date.format(day, "YYYY-MM-DD");
  let dom = [["date_activity", "=", dayFormatted]];

  const handleChangeActivity = (id, dates) => {
    let domain = [["schedule.id", "=", id]];
    let ctxView_ = { ...props.ctxView };
    ctxView_["domain"] = domain;
    setCtxViewActive(ctxView_);
    setOnOPen(true);
  };

  useEffect(() => {
    getActivity();
  }, [props.currentDay]);

  const addImages = (id) => {
    let image = imagesKind.find((kind) => kind.code == id);
    return image?.image;
  };

  const resetActivityData = () => {
    setActivityData(originalActivityData);
  };

  const renderCategories = () => {
    let itemsPerSlide;

    if (window.innerWidth >= 1000) {
      itemsPerSlide = 5;
    } else if (window.innerWidth >= 500) {
      itemsPerSlide = 4;
    } else {
      itemsPerSlide = 2;
    }

    const totalSlides = Math.ceil(activityData.length / itemsPerSlide);
    const slides = [];

    for (let i = 0; i < totalSlides; i++) {
      const start = i * itemsPerSlide;
      const end = start + itemsPerSlide;
      const slideCategories = activityData.slice(start, end);

      slides.push(
        <div key={i} className="grid grid-cols-5 gap-3">
          {slideCategories.map((activity, key) => (
            <ItemCard
              activity={activity}
              keyId={key}
              key={key}
              handleChangeActivity={handleChangeActivity}
              dayFormatted={dayFormatted}
              dayFormattedEnd={dayFormattedEnd}
            />
          ))}
        </div>,
      );
    }

    return slides;
  };

  async function getActivity() {
    const fields = [
      "kind",
      "kind.name",
      "kind.image",
      "kind.code",
      "kind.employee.party.name",
      "kind.product.list_price",
      "kind.product.sale_price_w_tax",
      "kind.product.image_url",
      "kind.product.images",
      "date_activity",
      "activities",
      "activities.quota",
      "activities.available",
      "activities.state",
      "id",
    ];
    const { data: res } = await proxy.search(
      "sale_activity.schedule",
      dom,
      fields,
    );
    if (res) {
      const res_ = res.map((activity) => ({
        id: activity.id,
        name: activity["kind."].name ?? "",
        date_activity: activity.date_activity,
        kind: activity["kind."].id,
        activities: activity["activities."],
        // price: activity["kind."]?.["product."].list_price,
        price: activity["kind."]?.["product."].sale_price_w_tax,
        image: addImages(activity["kind."].code),
        img: activity["kind."]?.image,
      }));

      setActivityData(res_);
      setOriginalActivityData(res_);
    }
  }
  if (activityData.length != 0 && originalActivityData.length != 0) {
    props.handleChangeStatusActivities(true);
    return (
      <div
        className={`mt-5 mx-4 mb-3 border-b border-gray-300 pb-12 ${
          props.statusShedule ? "block" : "hidden"
        }`}
      >
        <SearchActivities
          data={activityData}
          setActivityData={setActivityData}
          resetActivityData={resetActivityData}
        />

        <Carousel
          showThumbs={false}
          showStatus={false}
          showIndicators={true}
          emulateTouch={true}
          showArrows={false}
          renderIndicator={(clickHandler, isSelected, index, label) => {
            if (isSelected) {
              return (
                <li
                  className="custom-dot active"
                  aria-label={`Slide ${label} - Current Slide`}
                />
              );
            }
            return (
              <li
                className="custom-dot"
                onClick={clickHandler}
                onKeyDown={clickHandler}
                value={index}
                key={index}
                role="button"
                tabIndex={0}
                title={`${label}`}
                aria-label={`Slide ${label}`}
              />
            );
          }}
        >
          {renderCategories()}
        </Carousel>

        <ModalWizardActi
          key="button-wizard"
          model="sale_activity.activity"
          label={"Horarios de actividades"}
          ctxView={ctxViewActive}
          onAccept={() => console.log("asdsd")}
          onClose={() => setOnOPen(onOpen ? false : true)}
          record={record}
          view={props.view}
          Component={ModalWizzardActivity}
          onOpen={onOpen}
        />
      </div>
    );
  } else {
    props.handleChangeStatusActivities(false);
    return (
      // <p>asd</p>
      <SectionNoData text="No hay actividades programadas para este día" />
    );
  }
};
