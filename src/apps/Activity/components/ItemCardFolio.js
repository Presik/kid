import { ArrowRightCircleIcon } from "@heroicons/react/20/solid";
import imgFolio from "../../../assets/img/folios.svg";
import React from "react";

const ItemCardFolio = ({ folio, keyId, handleChangeFolio }) => {
  return (
    <div
      key={keyId} // Ensure to provide a unique key for each element in the array
      className={`group h-36 hover:shadow-md bg-gray-200 shadow-sm relative active:scale-95 focus:bg-blue-presik cursor-pointer overflow-hidden text-bluePresik  rounded-md flex `}
      onClick={() => handleChangeFolio(folio)}
      title={folio.name}
    >
      <div className="w-1/4 flex items-center">
        <img src={imgFolio} className="mx-auto ml-1" />
      </div>
      <div className="p-3 flex flex-col justify-between w-full">
        <div className="space-y-1 divide-y divide-gray-300" id="headerCard">
          <span className="text-sm font-semibold uppercase line-clamp-2 text-left">
            {folio["room."].name}
          </span>
          <span className="text-[12px] flex flex-col pt-2 items-start align-middle">
            <span>{folio["booking."]["party."]?.name}</span>
            <span>
              <span className="font-medium">#ID:</span>
              {folio["booking."]["party."]?.id_number}
            </span>
          </span>
        </div>
        <div className="flex flex-col items-start ">
          <span>
            <span className="font-medium">#Booking:</span>{" "}
            {folio["booking."]?.rec_name}
          </span>
          {/* <span>
            <span className="font-medium">Disponibilidad:</span>{" "}
          </span> */}

          <ArrowRightCircleIcon
            className={`w-10  absolute right-1.5 bottom-1.5 text-bluePresik bg group-hover:scale-125 transform duration-150 `}
          />
        </div>
      </div>
    </div>
  );
};

export default ItemCardFolio;
