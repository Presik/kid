import React, { useState, useEffect, Fragment } from "react";
import proxy from "api/proxy";
import { useFormStore } from "store/formStore";
import { FormattedMessage as FM } from "react-intl";
import ModelFolioWizard from "../ModelFolioWizard";
import FullTable from "components/FullTable";
import StdButton from "components/StdButton";
import func from "tools/functions";

const fields = [
  "arrival_date",
  "departure_date",
  "booking.party",
  "booking.party.name",
  "booking.party.id_number",
  "booking.rec_name",
  "room",
  "room.name",
  "party",
  "party.name",
  "party.id_number",
  "product",
  "product.name",
  "unit_price",
  "charges",
  "charges.date_service",
  "charges.product",
  "charges.product.rec_name",
  "charges.quantity",
  "charges.unit_price_w_tax",
  "charges.amount",
  "charges.status",
];
var nextId = -1;

export const ModalWizzardFolios = ({
  ctxView,
  handleStatusShedule,
  onClose,
}) => {
  const ctxViewFolio = ModelFolioWizard.ctxView();
  const [statusActiveFolio, setStatusActiveFolio] = useState(true);
  const {
    upStoreRecord,
    upActiveRecord,
    activeRecord,
    storeRecord,
    setActiveRecordFromId,
    resetRecord,
  } = useFormStore();
  let [folios, setFolios] = useState(null);
  const [arrayNoAvalaible, setArrayNoAvalaible] = useState([]);
  let [selectFolio, setSelectedFolio] = useState(null);
  const getFolios = async () => {
    const { data: dataFolio, error: errorFolios } = await proxy.methodCall({
      model: "hotel.folio",
      method: "get_current_folios",
      args: [],
    });
    dataFolio;
    const mapRecs = new Map();

    if (dataFolio) {
      dataFolio.forEach((folio) => {
        folio.id = folio.id;
        folio["room."] = {
          code: folio["room."].name,
          id: folio["room."].id,
        };
        mapRecs.set(folio.id, folio);
      });
      console.log("data de reserva:", mapRecs);
      setFolios(mapRecs);
    }
  };

  const addFolio = async (record) => {
    const statusValidate = await validateQuote();
    console.log(statusValidate, "status");
    if (statusValidate == false) {
      return false;
    }
    const _activeRecord = { ...activeRecord };
    const _storeRecord = { ...storeRecord };
    _activeRecord.party = record["main_guest."];
    // _activeRecord.reference = record["booking."].number;
    // _storeRecord.reference = record["booking."].number;
    _activeRecord.id_folio = record.id;
    _storeRecord.party = record["main_guest."];
    upActiveRecord(_activeRecord);
    upStoreRecord(_storeRecord);
    setSelectedFolio(record);
    // onClose();
  };

  ctxViewFolio.webfields.add.method = addFolio;

  const createSaleRecord = async () => {
    const statusValidate = await validateQuote();
    console.log(statusValidate, "status");
    if (statusValidate == false) {
      console.log("paso por aca ");
      return false;
    }
    const model = "sale.sale";
    let _storeRecord = { ...storeRecord };
    _storeRecord.status = "transferred";
    _storeRecord.state = "transferred";
    const _data = func.recToTryton(_storeRecord);
    const args = {
      model: "sale.sale",
      method: "create_sale",
      args: [_data],
    };

    const { data: res, error } = await proxy.methodCall(args);
    if (res) {
      const { data: resTransfer, error } = await proxy.methodCall({
        model: "sale.sale",
        method: "transfer_to_folio",
        args: [res.record.id, selectFolio.id],
      });
      if (resTransfer) {
        setActiveRecordFromId(res.record.id, "sale.sale", ctxView);
        onClose();
      }
    }
    console.log(error, "errores");

    // return res.record.id;
  };

  const cancelledFolio = () => {
    let _activeRecord = { ...activeRecord };
    let _storeRecord = { ...storeRecord };
    _activeRecord.party = null;
    _storeRecord.party = null;
    upActiveRecord(_activeRecord);
    upStoreRecord(_storeRecord);
    onClose();
  };

  const validateQuote = async () => {
    const store = { ...storeRecord };
    const fieldsActi = [
      "id",
      "time_start",
      "time_end",
      "available",
      "schedule.kind.name",
    ];
    let to_create = store.lines.get("create");
    const origenes = [];
    const AllActivities = [];
    for (const [key, line] of to_create) {
      let id_origen = line.origin.split(",");
      AllActivities.push({
        id_acti: id_origen[1],
        id: line.id,
        quantity: line.quantity,
        origin: parseInt(id_origen[1]),
      });
      origenes.push(id_origen[1]);
    }
    const { data: dataQuote, error: error } = await proxy.browse(
      "sale_activity.activity",
      origenes,
      fieldsActi,
    );
    // Filtrar array2 según las condiciones
    const newArray = dataQuote.filter((item2) => {
      const matchingItem1 = AllActivities.find(
        (item1) => item1.id_acti === item2.id.toString(),
      );
      if (matchingItem1 && matchingItem1.quantity <= item2.available) {
        return false; // No cumple la condición
      }
      return true; // Cumple la condición
    });
    console.log("todas las actividades", AllActivities);
    console.log("VALIDACION DE HORAS", dataQuote);
    // ESTE ES EL NUEVO ARRAY QUE NO CUMPLE CON LAS CONDICIONES
    console.log(newArray);
    if (newArray.length === 0) {
      return true;
    } else {
      setArrayNoAvalaible(newArray);
      return false;
    }
  };

  useEffect(() => {
    validateQuote();
    getFolios();
  }, []);

  return (
    <Fragment>
      <div className="mb-5">
        {arrayNoAvalaible.map((item, key) => {
          return (
            <div
              key={key}
              className="bg-red-400 p-3 rounded-md shadow-sm text-center mb-1"
            >
              <p className="">
                <span className="font-medium">
                  {item["schedule."]["kind."].name}
                </span>{" "}
                <span>cuenta con </span>
                <span className="font-medium  text-black text-lg">
                  {" "}
                  {item.available}
                </span>
                {"  "}
                de disponibilidad
              </p>
            </div>
          );
        })}
      </div>
      <FullTable
        records={folios}
        isLoading={false}
        limit={30}
        // updateRecords={updateRecords}
        onChangeView={() => console.log("cambio de vista")}
        ctxView={ctxViewFolio}
      />
      {selectFolio && (
        <Fragment>
          <div className="mt-10">
            <div className="font-medium text-lg mb-3">Datos de habitacion</div>
            <div>Habitacion: {selectFolio["room."].code}</div>
            <div>Reserva: {selectFolio["booking."].number}</div>
            <div>Huesped: {selectFolio["main_guest."].name}</div>
            <div>Numero ID: {selectFolio["main_guest."].id_number}</div>
            <div>Telefono: {selectFolio["main_guest."].mobile}</div>
          </div>
          <div className="flex mx-auto space-x-3 justify-center mt-10">
            <StdButton
              content="Cargar a la habitación"
              color="bluePresik"
              style="uppercase w-[200px]"
              onClick={() => createSaleRecord()}
            />
            <StdButton
              content="Cancelar"
              color="rose"
              style="uppercase w-[200px] "
              onClick={() => cancelledFolio()}
            />
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};
