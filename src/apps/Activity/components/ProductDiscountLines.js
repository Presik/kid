import React, { useState } from "react";
import { useFormStore } from "store/formStore";
import { useWizardStore } from "store/wizardStore";

import { Fragment } from "react";
import StdButton from "components/StdButton";
import SelectionField from "components/SelectionField";
import NumericField from "components/NumericField";
import TextField from "components/TextField";

function ProductDiscountItem({ record, onClose }) {
  const {
    storeRecord,
    activeRecord,
    upFieldActive,
    upFieldStore,
    upActiveRecord,
  } = useFormStore();
  const { upActiveWizard, upStoreWizard } = useWizardStore();

  let [discount, setDiscount] = useState({});

  const handleChanged = (name, value) => {
    const _value = { ...discount };
    _value[name] = value;
    setDiscount(_value);
  };

  const addDiscount = () => {
    let _record = { ...activeRecord };
    let _storeRecord = { ...storeRecord };
    let _currentLine = { ...record };
    if (discount.type.id == "percentage" && discount.discount_value > 100) {
      alert("Este numero de porcentaje no es valido");
      return false;
    }
    let to_create = _storeRecord.lines.get("create");
    let _discount = discount.discount_value;
    if (discount.type.id == "fixed") {
      _discount = discount.discount_value;
      for (const [key, line] of _record.lines) {
        if (line.id === _currentLine.id) {
          line.amount = line.amount - _discount;
          // line.discount = parseInt(_discount);
        }
      }
      for (const [key, line] of to_create) {
        if (line.id === _currentLine.id) {
          if (line.base_price < _discount && _currentLine.quantity == 1) {
            alert(
              "El precio de descuento es mayor para esta línea de producto",
            );
            return false;
          }
          // let precioOriginalPorProducto = line.base_price;
          let price_discount = line.base_price - _discount / line.quantity;
          let balanced_price = line.base_price - price_discount;
          line.unit_price = line.base_price - balanced_price;
          console.log("este es el valor ajustado  " + line.unit_price);
        }
      }
    } else {
      _discount = 100 - discount.discount_value;
      for (const [key, line] of _record.lines) {
        if (line.id === _currentLine.id) {
          line.amount = (_discount * line.amount) / 100;
          line.unit_price = (_discount * line.unit_price) / 100;
          line.sale_price_taxed = (_discount * line.sale_price_taxed) / 100;
          line.discount = parseInt(discount.discount_value);
        }
      }
      for (const [key, line] of to_create) {
        if (line.id === _currentLine.id) {
          line.unit_price = (_discount * line.unit_price) / 100;
        }
      }
    }

    upFieldActive("lines", _record.lines);
    upFieldStore("lines", _storeRecord.lines);
    const totalAmount = [..._record.lines.values()].reduce(
      (accumulator, currentValue) => {
        const amount = parseFloat(currentValue.amount);
        return accumulator + amount;
      },
      0,
    );
    _record.total_amount = totalAmount.toFixed(2);
    upActiveRecord(_record);
    upStoreWizard({
      amount: parseInt(totalAmount.toFixed(2)),
    });
    upActiveWizard({
      amount: parseInt(totalAmount.toFixed(2)),
    });
    onClose();
  };

  return (
    <Fragment>
      <div className="px-4 sm:px-6 lg:px-8 py-20 md:col-span-2 bg-blue">
        <h2 className="text-lg font-bold">
          {record.product.name} | {record.amount}
        </h2>
        <p>Agrega descuentos de porcentaje o fijo para este producto.</p>
        <div className="flex justify-center space-x-4">
          <SelectionField
            label="Tipo de descuento"
            name="type"
            options={[
              { id: "percentage", name: "Porcentaje" },
              { id: "fixed", name: "Valor fijo" },
            ]}
            value={discount?.type}
            onChange={handleChanged}
          />

          {/* <TextField
            label="Valor"
            name="discount_value"
            value={discount?.discount_value}
            onChange={handleChanged}
            placeholder="0"
          /> */}
          <NumericField
            label="Valor"
            name="discount_value"
            value={discount?.discount_value}
            onChange={handleChanged}
            placeholder="0"
          />
        </div>
        <div className="mt-5">
          <StdButton
            content={"Calcular descuento"}
            onClick={addDiscount}
            color="bluePresik"
            style="mx-auto min-w-[200px] uppercase"
          />
        </div>
      </div>
    </Fragment>
  );
}

export default ProductDiscountItem;
