import React, { Fragment } from "react";
// import { FormattedMessage as FM } from "react-intl";

import BasicModal from "components/BasicModal";
// import { useWizardStore } from "store/wizardStore";
// import { useFormStore } from "store/formStore";

function ModalWizardActi({
  Component,
  label,
  // icon,
  record,
  onAccept,
  model,
  ctxView,
  parentRec,
  onOpen,
  onClose,
  view,
  reset = false,
}) {
  function handleClose() {
    onClose(false);
  }

  return (
    <Fragment>
      <BasicModal
        title={label}
        // open={onOpen}
        open={onOpen}
        onClose={handleClose}
        buttons={[]}
        titleSize="text-2xl"
        width="w-1/2"
      >
        <Component
          parentRec={parentRec}
          model={model}
          record={record}
          onClose={() => handleClose()}
          ctxView={ctxView}
          view={view}
        />
      </BasicModal>
    </Fragment>
  );
}

export default ModalWizardActi;
