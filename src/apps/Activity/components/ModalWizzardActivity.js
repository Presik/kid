import React, { useState, useEffect } from "react";
// import GridCards from "components/GridCards";
import FullTable from "components/FullTable";
import proxy from "api/proxy";
import { useIdSeq } from "store/idSeq";
import { useWizardStore } from "store/wizardStore";
import { useFormStore } from "store/formStore";

const ModalWizzardActivity = ({
  onClose,
  parentRec,
  model,
  record,
  ctxView,
  view,
}) => {
  let [records, setRecords] = useState(null);
  let [roomsSelected, setRoomsSelected] = useState(new Map());
  let [wizStore, setWizStore] = useState({});
  const {
    storeRecord,
    activeRecord,
    upFieldStore,
    upFieldActive,
    upActiveRecord,
    upStoreRecord,
  } = useFormStore();
  const { storeWizard, upActiveWizard, upStoreWizard } = useWizardStore();
  const { seq, increment } = useIdSeq();
  useEffect(() => {
    fillActivities(wizStore);
  }, [ctxView]);

  function updateRecords(field, value, recId) {
    let _records = new Map(records);
    let category = _records.get(recId);
    category[field] = value;
    setRecords(_records);
  }

  const addActivity = async (record) => {
    if (record.quantity > record.available) {
      alert(`Solo existen ${record.available} cupos para esta hora.`);
      return false;
    }

    const { data: activityAvailable } = await proxy.browse(
      "sale_activity.activity",
      [record.id],
      ["available"],
    );

    if (record.quantity > activityAvailable[0].available) {
      let msg =
        activityAvailable[0].available == 0
          ? `Ya no existen cupos disponibles para esta actividad.`
          : `Solo existen ${activityAvailable[0].available} cupos para hora.`;
      alert(msg);
      return false;
    }
    const product = record["schedule."]["kind."]["product."];
    const sequence = seq;
    let line_ = {
      origin: {
        time_start: record.time_start,
        time_end: record.time_end,
        id: `sale_activity.activity,${record.id}`,
      },
      id: sequence,
      quantity: record.quantity,
      unit: product["sale_uom."],
      sale_price_taxed: product["sale_price_taxed"],
      sale_price_w_tax: product["sale_price_w_tax"],
      list_price: product["list_price"],
      base_price: product["sale_price_w_tax"],
      discount: 0,
      product: product,
      // origin: `sale_activity.activity,${record.id}`,
    };
    let lineStore_ = {
      id: sequence,
      quantity: record.quantity,
      unit: product["sale_uom."]["id"],
      product: product.id,
      base_price: product["sale_price_w_tax"],
      origin: `sale_activity.activity,${record.id}`,
    };

    let _activeRecord = { ...activeRecord };
    let lines = _activeRecord.lines;
    let _storeRecord = { ...storeRecord };
    if (!_storeRecord.lines) {
      _storeRecord.lines = new Map();
      _storeRecord.lines.set("create", new Map());
    }
    let to_create = _storeRecord.lines.get("create");
    line_["unit_price"] = product.sale_price_w_tax.toFixed(2);
    lineStore_["unit_price"] = product.sale_price_w_tax.toFixed(2);
    line_["amount"] = (record.quantity * line_.sale_price_w_tax).toFixed(2);
    to_create.set(sequence, lineStore_);
    lines.set(sequence, { ...line_ });
    increment(sequence + -1);

    upFieldActive("lines", lines);
    upFieldStore("lines", _storeRecord.lines);
    const totalAmount = [...lines.values()].reduce(
      (accumulator, currentValue) => {
        const amount = parseFloat(currentValue.amount);
        return accumulator + amount;
      },
      0,
    );
    if (totalAmount >= 212060) {
      alert("El valor aplica para factura electrónica.");
      console.log(_activeRecord);
      _activeRecord.invoice_type = "1";
      _storeRecord.invoice_type = "1";
    }

    _activeRecord.total_amount = totalAmount.toFixed(2);
    upActiveRecord(_activeRecord);
    upStoreRecord(_storeRecord);

    upStoreWizard({
      amount: parseInt(totalAmount.toFixed(2)),
    });
    upActiveWizard({
      amount: parseInt(totalAmount.toFixed(2)),
    });
    onClose();
  };

  ctxView.webfields.add.method = addActivity;

  const fillActivities = async (store) => {
    const today = new Date();
    const formattedTime = today.toLocaleTimeString("en-US", {
      hour12: false,
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    });
    console.log(formattedTime);

    const fields = [
      "quota",
      "time_start",
      "time_end",
      "available",
      "id",
      "state",
      "schedule",
      "schedule.date_activity",
      "schedule.kind",
      "schedule.kind.name",
      "schedule.kind.product.name",
      "schedule.kind.product.sale_price_taxed",
      "schedule.kind.product.list_price",
      "schedule.kind.product.sale_uom.name",
      "schedule.kind.product.sale_price_w_tax",
    ];
    const { data: res, error } = await proxy.search(
      "sale_activity.activity",
      ctxView.domain,
      fields,
    );

    res.sort(orderArray);

    const mapRecs = new Map();
    res.forEach((act) => {
      console.log(today, "dia ", act.time_start, formattedTime);

      if (
        act.state == "scheduled" &&
        act.time_start >= formattedTime &&
        act.available > 0
      ) {
        act.id = act.id;
        (act.quantity = parseInt(1)), mapRecs.set(act.id, act);
      }
    });

    if (res) {
      setRecords(mapRecs);
    }
  };

  const orderArray = (a, b) => {
    const hour1 = a.time_start;
    const hour2 = b.time_start;
    if (hour1 < hour2) {
      return -1;
    }
    if (hour1 > hour2) {
      return 1;
    }
    return 0;
  };

  return (
    <div id="hotel-booking-wizard-rooms" className="block">
      <FullTable
        records={records}
        isLoading={false}
        limit={30}
        updateRecords={updateRecords}
        onChangeView={() => console.log("cambio de vista")}
        ctxView={ctxView}
      />
    </div>
  );
};

export default ModalWizzardActivity;
