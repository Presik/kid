import React, { useState, useEffect, Fragment } from "react";
import proxy from "api/proxy";
import ItemCardFolio from "./ItemCardFolio";
import QuickForm from "components/QuickForm";
import tool from "tools/functions";
import { useFormStore } from "store/formStore";
import { FormattedMessage as FM } from "react-intl";

const fields = [
  "arrival_date",
  "departure_date",
  "booking.party",
  "booking.party.name",
  "booking.party.id_number",
  "booking.rec_name",
  "room",
  "room.name",
  "party",
  "party.name",
  "party.id_number",
  "product",
  "product.name",
  "unit_price",
  "charges",
  "charges.date_service",
  "charges.product",
  "charges.product.rec_name",
  "charges.quantity",
  "charges.unit_price_w_tax",
  "charges.amount",
  "charges.status",
];
var nextId = -1;

export const Folios = ({ ctxView, handleStatusShedule }) => {
  const [folios, setFolios] = useState(null);
  const [statusActiveFolio, setStatusActiveFolio] = useState(true);
  const {
    upStoreRecord,
    upActiveRecord,
    activeRecord,
    setActiveRecordFromId,
    resetRecord,
  } = useFormStore();
  let [record, setRecord] = useState(null);

  async function getBooking() {
    let dom = [
      // ["lines.arrival_date", ">=", filterDay],
      // ["lines.arrival_date", "<=", nextDayFormatted],
    ];

    const { data, error } = await proxy.search("hotel.folio", dom, fields);

    if (data) {
      console.log("data de reserva:", data);
      setFolios(data);
    }
  }
  const handleChangeFolio = (folio) => {
    setActiveRecordFromId(folio.id, ctxView.model, ctxView);
    handleStatusShedule(true);
    setStatusActiveFolio(false);
  };

  const resetNewRecord = async () => {
    nextId = nextId - 1;
    const [toStore, toActive] = await tool.getDefaults(nextId, ctxView);
    upStoreRecord(toStore);
    upActiveRecord(toActive);
    setRecord(toActive);
    // resetRecord(true);
  };

  useEffect(() => {
    getBooking();
    resetNewRecord();
  }, []);

  const onChangeViewShedule = () => {
    handleStatusShedule(false);
    setStatusActiveFolio(true);
  };

  return (
    <Fragment>
      {/* <h1 className="text-zinc-500 text-4xl md:col-span-2 lg:col-span-2 xl:col-span-2 my-5">
        Folios
      </h1> */}
      {statusActiveFolio == true ? (
        <div className=" mt-10 mx-4 mb-3 border-b border-gray-300 pb-12">
          <p className="text-gray-600 my-5">
            <FM id="activity.folio.charges.title" />
          </p>
          <div className="grid grid-cols-4  gap-4">
            {folios &&
              folios.map((item, key) => (
                <ItemCardFolio
                  folio={item}
                  keyId={key}
                  handleChangeFolio={handleChangeFolio}
                />
              ))}
          </div>
        </div>
      ) : (
        record && (
          <QuickForm
            ctxView={ctxView}
            level="main"
            onChangeView={onChangeViewShedule}
          />
        )
      )}
    </Fragment>
  );
};
