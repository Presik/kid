import React from "react";
import { useQuery } from "@tanstack/react-query";
import { useFormStore } from "store/formStore";
import { useFormChildStore } from "store/formChildStore";
import { useWizardStore } from "store/wizardStore";
import proxy from "api/proxy-old";
import { Fragment } from "react";

function ProductDiscount({ record, onClose }) {
  // const [selected, setSelected] = useState([]);
  const {
    storeRecord,
    activeRecord,
    upFieldActive,
    upFieldStore,
    upActiveRecord,
  } = useFormStore();
  const { upActiveWizard, upStoreWizard } = useWizardStore();
  const shopId = activeRecord?.shop?.id;
  const queryProductDiscount = useQuery(
    ["productDiscount", shopId],
    async () => {
      let discounts = [];
      if (shopId) {
        discounts = await proxy.search(
          "sale.shop",
          [["id", "=", shopId]],
          ["discounts.name", "discounts.type_discount", "discounts.discount"],
        );
      }

      return discounts;
    },
  );

  const handleSelect = async (discount) => {
    let _record = { ...activeRecord };
    let _storeRecord = { ...storeRecord };

    let to_create = _storeRecord.lines.get("create");
    const _discount = 100 - discount.discount;
    let totalLineAmount = 0;
    for (const [key, line] of _record.lines) {
      line.amount = (_discount * line.amount) / 100;
      line.unit_price = (_discount * line.unit_price) / 100;
      line.sale_price_taxed = (_discount * line.sale_price_taxed) / 100;
      line.discount = parseInt(discount.discount);
      totalLineAmount += line.amount;
    }
    _record.total_amount = totalLineAmount == 0 ? 1 : totalLineAmount;

    for (const [key, line] of to_create) {
      line.unit_price = (_discount * line.unit_price) / 100;
    }
    upActiveRecord(_record);
    upFieldActive("lines", _record.lines);
    upFieldStore("lines", _storeRecord.lines);
    upStoreWizard({
      amount: parseInt(_record.total_amount.toFixed(2)),
    });
    upActiveWizard({
      amount: parseInt(_record.total_amount.toFixed(2)),
    });
    onClose(true, false);
  };

  return (
    <Fragment>
      <div className="px-4 sm:px-6 lg:px-8 py-2 md:col-span-2 bg-blue">
        <div className="grid grid-cols-2 md:grid-cols-3 gap-4 pt-2">
          {queryProductDiscount.data?.[0]["discounts."].map(
            (discount, index) => {
              return (
                <div
                  className={
                    "group bg-blue-presik rounded-md cursor-pointer text-center  px-2 py-2 relative overflow-hidden"
                  }
                  key={discount.id}
                  onClick={() => {
                    handleSelect(discount);
                  }}
                  name={index}
                >
                  <div className="absolute hidden group-hover:block transition h-full w-full backdrop-blur-sm left-0 top-0" />
                  <div
                    key={discount.id}
                    className="pointer-events-none flex-wrap p-2 border h-full border-white py-8 border-dashed text-white"
                  >
                    <h3 className="font-semibold text-[17px]">
                      {discount.name}
                    </h3>
                    <span className="text-gray-500 text-[30px]">
                      {discount.discount}{" "}
                      {(discount.type_discount = "discount" ? "%" : "")}
                    </span>
                  </div>
                </div>
              );
            },
          )}
        </div>
      </div>
    </Fragment>
  );
}

export default ProductDiscount;
