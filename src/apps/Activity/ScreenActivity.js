import React, { Fragment, useState, useEffect } from "react";
import { FormattedMessage as FM } from "react-intl";
import date from "date-and-time";

import { useFormStore } from "store/formStore";
import tool from "tools/functions";
import DateRangeFilter from "components/DateRangeFilter";
import SectionRigth from "components/SectionRigth";
import QuickForm from "components/QuickForm";
import { ScheduledActivities } from "./components/ScheduledActivities";
import WidgetSideStatusActivity from "./components/WidgetSideStatusActivity";
import StdButton from "components/StdButton";
import proxy from "api/proxy";
import QuickTable from "components/QuickTable";
import PureModal from "components/Modals/PureModal";
import ProductDiscountItem from "./components/ProductDiscountLines";

const fmt = "YYYY-MM-DD";

var nextId = -1;
const viewDefault = {
  view: "create",
  status: true,
};

const ScreenActivity = (props) => {
  const dayToday = new Date();
  let { ctxView, ctxViewAc, permits } = props;
  const [statusSection, setStatusSection] = useState(true);
  let [statusActivities, setStatusActivities] = useState(true);
  const [filterDay, setFilterDay] = useState(dayToday);
  let [tickets, setTickets] = useState([]);
  let [statusShedule, setStatusShedule] = useState(true);
  let [record, setRecord] = useState(null);
  let [recordLines, setRecordLines] = useState(null);
  let [addDiscount, setAddDiscount] = useState(false);
  const { upStoreRecord, upActiveRecord, activeRecord } = useFormStore();
  const handleChangeDate = (value) => {
    if (permits) {
      let daySelected = new Date(value);
      daySelected.setDate(daySelected.getDate() + 1);
      let _daySelected = date.format(new Date(daySelected), "YYYY-MM-DD");
      setFilterDay(_daySelected);
    }
  };
  const [view, setView] = useState("create");
  const [viewDiscount, setViewDiscount] = useState(false);
  const [viewFolio, setViewFolio] = useState(false);
  // const history = useHistory();

  async function getShedule() {
    let filterDay_ = new Date(filterDay);
    filterDay_.setDate(filterDay_.getDate());
    let filterDayStart = date.format(filterDay_, "YYYY-MM-DD");
    let dom = [
      ["schedule.date_activity", "=", filterDayStart],
      ["state", "=", "scheduled"],
    ];
    const fields = ["state", "available", "quota", "schedule.date_activity"];
    const { data: res, error } = await proxy.search(
      "sale_activity.activity",
      dom,
      fields,
    );
    if (res) {
      setTickets(res);
    }
  }

  const handleChangeActivity = (id, dates) => {
    let domain = [["schedule.id", "=", id]];
    let ctxView_ = { ...ctxViewAc };
    ctxView_["domain"] = domain;
    setCtxViewActive(ctxView_);
    setOpenWizard(true);
  };
  const handleChangeStatusActivities = (status) => {
    setStatusActivities(status);
  };
  const handleView = (viewActive) => {
    if (viewActive) {
      // setView(viewActive);
    }
  };
  const handleViewDiscount = () => {
    setViewDiscount(viewDiscount ? false : true);
  };

  const handleViewFolio = () => {
    setViewFolio(viewFolio ? false : true);
    setStatusShedule(false);
  };

  useEffect(() => {
    getShedule();
    resetNewRecord();
  }, [filterDay, props, view]);

  const resetNewRecord = async () => {
    nextId = nextId - 1;
    const [toStore, toActive] = await tool.getDefaults(nextId, ctxView);
    let _toStore = { ...toStore };
    let _toActive = { ...toActive };
    upStoreRecord(_toStore);
    upActiveRecord(_toActive);
    setRecord(_toActive);
    // upStoreWizard({
    //   amount: parseInt(_toActive.total_amount.toFixed(2)),
    // });
    // upActiveWizard({
    //   amount: parseInt(_toActive.total_amount.toFixed(2)),
    // });
  };

  const handleStatusShedule = (status) => {
    setStatusShedule(status);
  };

  // add functions in add_discount in sale_lines
  const addDiscountItem = (record) => {
    console.log("estoy aca ", record);
    setAddDiscount(true);
    setRecordLines(record);
  };
  ctxView.webfields.lines.ctxView.webfields.add_discount.method =
    addDiscountItem;
  return (
    <Fragment>
      <DateRangeFilter action={handleChangeDate} />
      {view === "create" && activeRecord.id < 0 && (
        <ScheduledActivities
          currentDay={filterDay}
          handleChangeStatusActivities={handleChangeStatusActivities}
          ctxView={ctxViewAc}
          view={view}
          statusShedule={statusShedule}
        />
      )}

      {statusActivities && (
        <div
          className={`flex ${
            statusSection ? "flex-row justify-between" : "flex-col"
          }`}
        >
          <div
            className={
              statusSection == true
                ? "md:w-[65vw] xl:w-[70vw] "
                : "md:w-12/12 relative pr-10"
            }
          >
            {view === "create" ? (
              <QuickForm ctxView={ctxView} level="main" />
            ) : view === "history" ? (
              <QuickTable ctxView={ctxView} />
            ) : (
              <p>Seleccionar algo</p>
            )}
          </div>
          <div>
            {props.permits && (
              <SectionRigth
                position={"right"}
                widgets={["status", "numPeople"]}
                title="INFO ACTIVIDADES"
                bgColor={statusSection ? "bg-gray-100" : "bg-blue-presik"}
                status={statusSection}
                handleChangeStatus={setStatusSection}
              >
                <WidgetSideStatusActivity
                  title={<FM id="booking.widget.screen.huesped_house" />}
                  background={"bg-gray-100"}
                  name="statusHotel"
                  tickets={tickets}
                  day={filterDay}
                />
              </SectionRigth>
            )}

            <SectionRigth
              position={"right"}
              widgets={["status", "numPeople"]}
              title=""
              bgColor={statusSection ? "bg-gray-100" : "bg-blue-presik"}
              status={statusSection}
              handleChangeStatus={false}
            >
              <StdButton
                content={view == "create" ? "REINICIAR VENTA" : "NUEVA VENTA"}
                size="w-full"
                color={view == "create" ? "rose" : "green"}
                onClick={() => {
                  resetNewRecord();
                  setView("create");
                  setStatusShedule(true);
                }}
                // disabled={
                //   view == "create" && activeRecord.id < 0 ? true : false
                // }
              />

              <StdButton
                content={"HISTORIAL"}
                size="w-full"
                onClick={() => setView("history")}
                color={"bluePresik"}
                disabled={view == "history" ? true : false}
              />
            </SectionRigth>
          </div>
        </div>
      )}

      <PureModal
        open={addDiscount ? true : false}
        onClose={() => {
          setAddDiscount(false);
        }}
        record={recordLines}
      >
        <ProductDiscountItem
          record={recordLines}
          onClose={() => {
            setAddDiscount(false);
          }}
        />
      </PureModal>
    </Fragment>
  );
};

export default ScreenActivity;
