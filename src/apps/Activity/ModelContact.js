const getType = () => {
  const options = [
    { id: "phone", name: "Teléfono" },
    { id: "mobile", name: "Móvil" },
    { id: "fax", name: "Fax" },
    { id: "email", name: "Correo electrónico" },
    { id: "website", name: "Sitio Web" },
    { id: "skype", name: "Skype" },
    { id: "sip", name: "SIP" },
    { id: "irc", name: "IRC" },
    { id: "jabber", name: "Jabber" },
    { id: "other", name: "Otro" },
  ];

  return options;
};

const getView = () => {
  let ctxView = {
    model: "party.contact_mechanisms",
    selectable: "one",
    activeSearch: true,
    filters: null,
    form_action: ["save", "add"],
    table_action: ["add", "edit"],

    // selectable: "one",
    webfields: {
      type: {
        type: "selection",
        options: getType(),
        // translate: true,
      },
      value: {
        type: "char",
        // translate: true,
      },
      name: {
        type: "char",
        // translate: true,
      },
      party: {
        type: "many2one",
        model: "party.party",
        recSearch: () => [],
      },
    },
    webtree: [
      { name: "type" },
      { name: "name", width: "30%" },
      { name: "value" },

      // { name: "amount", width: "15%" },
      // { name: "description", width: "30%" },
    ],
    webform: [
      { name: "type" },
      { name: "name", width: "30%" },
      { name: "value" },
    ],
  };

  return ctxView;
};

export default { ctxView: getView };
