import React, { Fragment } from "react";
import FormWizard from "components/Form/FormWizard";
import { useWizardStore } from "store/wizardStore";
import { useFormStore } from "store/formStore";
import proxy from "api/proxy";
import dates from "tools/dates";
import ModelWizardPayment from "./ModelWizardPayment";
import ModelSale from "./ModelSale";
import func from "../../tools/functions";

const WizardAddPayment = ({ onClose }) => {
  const { storeWizard, resetWizard } = useWizardStore();
  const { activeRecord, storeRecord, upStoreRecord, setActiveRecordFromId } =
    useFormStore();
  let ctxViewSale = ModelSale.ctxView();
  async function addPayment() {
    if (!activeRecord.party) {
      onClose(false, "missing_party");
      return;
    }
    let recordId = activeRecord.id;

    if (recordId < 0) {
      const model = "sale.sale";
      const _activeRecord = { ...activeRecord };
      const _storeRecord = { ...storeRecord };
      _storeRecord.status = "processing";
      const _data = func.recToTryton(_storeRecord);
      const args = {
        model: "sale.sale",
        method: "create_sale",
        args: [_data],
      };
      const { data: res, error } = await proxy.methodCall(args);
      console.log(error, "errores");
      // return true;

      recordId = res.record.id;
      upStoreRecord(res.record);
    }

    let toStore = {
      statement: storeWizard.statement.id,
      amount: storeWizard.amount,
      number: storeWizard.voucher,
      date: dates.dateToday(),
      description: activeRecord.number,
      party: activeRecord.party.id,
      account: activeRecord.party.account_receivable,
      source: `sale.sale,${recordId}`,
      sale: recordId,
      // source: `sale.sale,${activeRecord.id}`,
      // sale: activeRecord.id,
    };

    // si no funciona descomentar esto
    const model = "account.statement.line";
    const fields_names = ["id"];
    const { data, error } = await proxy.create(model, toStore, fields_names);

    if (data) {
      const _storeRecord = { ...storeRecord };
      _storeRecord.sale_id = recordId;
      const argSaleDone = {
        model: "sale.sale",
        // method: "faster_post_invoice",
        method: "faster_process",
        args: [_storeRecord],
      };
      const { data: saleDone, error } = await proxy.methodCall(argSaleDone);
      onClose(true, error);
      setActiveRecordFromId(recordId, "sale.sale", ctxViewSale);
      resetWizard();
    }

    return true;
  }

  let ctxView = ModelWizardPayment.ctxView();
  // ctxView.webfields.pay.method = addPayment;

  return (
    <Fragment>
      <FormWizard
        key="hotel-booking-wizard-payment"
        ctxView={ctxView}
        parentRec={activeRecord}
        handleButton={addPayment}
      />
    </Fragment>
  );
};

export default WizardAddPayment;
