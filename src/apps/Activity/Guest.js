// Guest model

const GUEST = [
  { id: "adult", name: "adult" },
  { id: "child", name: "child" },
];

const SEX = [
  { id: "female", name: "female" },
  { id: "male", name: "male" },
];

const TYPES = [
  { id: "11", name: "registro_civil" },
  { id: "12", name: "tarjeta_identidad" },
  { id: "13", name: "cedula_ciudadania" },
  { id: "21", name: "tarjeta_extranjeria" },
  { id: "22", name: "cedula_extranjeria" },
  { id: "31", name: "nit" },
  { id: "41", name: "pasaporte" },
  { id: "42", name: "tipo_documento_extranjero" },
  { id: "47", name: "pep" },
  { id: "50", name: "nit_otro_pais" },
  { id: "91", name: "nuip" },
];

function getRequiredByMainGuest(record) {
  console.log("update REuired....", record);
  const res = record ? record.main_guest : false;
  return res;
}

const getView = () => {
  let DictCtxView = {
    model: "hotel.folio.guest",
    form_action: ["add"],
    table_action: ["add", "delete", "edit"],
    orderBy: [["name", "ASC"]],
    selectable: null, // Options for table rows: null, multi, one
    target: "folio",
    // domain: [], // Options: null or valid domain
    // limit: 10,
    webfields: {
      name: { type: "char", required: true },
      type_guest: {
        type: "selection",
        options: GUEST,
        required: true,
        translate: true,
        default: { id: "adult", name: "adult" },
      },
      type_document: {
        type: "selection",
        options: TYPES,
        required: true,
        translate: true,
      },
      sex: {
        type: "selection",
        options: SEX,
        required: true,
        translate: true,
        default: { id: "male", name: "male" },
      },
      id_number: { type: "char", required: true },
      mobile: { type: "char", required: true },
      email: { type: "char", required: true },
      address: { type: "char", required: getRequiredByMainGuest },
      birthday: { type: "date" },
      visa_date: { type: "date" },
      visa_number: { type: "char" },
      profession: { type: "char" },
      notes: { type: "text-area" },
      main_guest: {
        type: "boolean",
        translate: true,
      },
      origin_country: {
        type: "many2one",
        model: "country.country",
      },
      target_country: {
        type: "many2one",
        model: "country.country",
      },
      nationality: {
        type: "many2one",
        model: "country.country",
        required: true,
      },
    },
    webtree: [
      { name: "name", width: "25%" },
      { name: "type_guest", width: "15%" },
      { name: "type_document", width: "10%" },
      { name: "id_number", width: "10%" },
      { name: "mobile", width: "10%" },
      { name: "email", width: "10%" },
      { name: "main_guest", width: "5%" },
    ],
    webform: [
      { name: "name" },
      {
        id: "info1",
        grid: [{ name: "type_guest" }, { name: "main_guest" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "type_document" },
      { name: "id_number" },
      {
        id: "info2",
        grid: [{ name: "mobile" }, { name: "sex" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "email" },
      { name: "address" },
      { name: "nationality" },
      { name: "origin_country" },
      { name: "target_country" },
      { name: "birthday" },
      { name: "profession" },
      {
        id: "info4",
        grid: [{ name: "visa_date" }, { name: "visa_number" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "notes" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
