import ModelContact from "./ModelContact";
import ModelAddress from "./ModelAddress";

const getOptions = () => {
  const options = [
    // { id: "", name: "Seleccionar" },

    { id: "11", name: "Registro Civil de Nacimiento" },
    { id: "12", name: "Tarjeta de Identidad" },
    { id: "13", name: "Cedula de Ciudadania" },
    { id: "21", name: "Tarjeta de Extranjeria" },
    { id: "22", name: "Cedula de Extranjeria" },
    { id: "31", name: "NIT" },
    { id: "41", name: "Pasaporte" },
    { id: "42", name: "Tipo de Documento Extranjero" },
    { id: "47", name: "PEP" },
    { id: "50", name: "NIT de otro pais" },
    { id: "91", name: "NUIP" },
    { id: "", name: "" },
  ];

  return options;
};

const getOptionsSex = () => {
  const options = [
    // { id: "", name: "Seleccionar" },

    { id: "male", name: "Masculino" },
    { id: "female", name: "Femenino" },
    { id: "", name: "" },
  ];

  return options;
};
const save = (record) => {
  console.log("save", record);
};

const getView = (config) => {
  let ctxView = {
    model: "party.party",
    selectable: "one",
    activeSearch: true,
    filters: null,
    form_action: ["save", "edit", "add"],
    table_action: ["add"],

    // selectable: "one",
    webfields: {
      name: { type: "char", searchable: true },
      id_number: { type: "char", searchable: true },
      account_payable: { type: "boolean" },
      type_document: {
        type: "selection",
        options: getOptions(),
        // translate: true,
        default: "",
      },
      sex: {
        type: "selection",
        options: getOptionsSex(),
        // translate: true,
        default: "",
      },
      check_digit: {
        type: "integer",
      },
      email: {
        type: "char",
      },
      mobile: {
        type: "char",
        // searchable: true,
      },
      phone: {
        type: "char",
      },
      birthday: {
        type: "date",
      },
      // address: {
      //   type: "char",
      // },
      // country: {
      //   type: "many2one",
      //   model: "party.country_code",
      //   recSearch: () => [],
      // },
      // nationality: {
      //   type: "many2one",
      //   model: "country.country",
      //   recSearch: () => [],
      // },
      // city: {
      //   type: "many2one",
      //   model: "party.city",
      //   recSearch: () => [],
      // },

      account_receivable: {
        type: "many2one",
        model: "account.account",
        readOnly: true,
      },

      name: { type: "char", searchable: true },
      contact_mechanisms: {
        type: "one2many",
        model: "party.contact_mechanism",
        ctxView: ModelContact.ctxView(config),
        // readOnly: true,
      },
      addresses: {
        type: "one2many",
        model: "party.address",
        ctxView: ModelAddress.ctxView(config),
        // readOnly: true,
      },
    },
    webtree: [
      { name: "name", width: "30%" },
      { name: "id_number", width: "10%" },
      { name: "account_receivable", width: "15%" },
      // { name: "amount", width: "15%" },
      // { name: "description", width: "30%" },
    ],
    webform: [
      { name: "name" },
      { name: "type_document" },
      { name: "id_number" },
      // { name: "address" },
      { name: "email" },
      { name: "mobile" },
      { name: "sex" },
      { name: "phone" },
      { name: "birthday" },
      { name: "account_receivable" },
      { name: "contact_mechanisms" },
      { name: "addresses" },
    ],
  };

  return ctxView;
};

export default { ctxView: getView };
