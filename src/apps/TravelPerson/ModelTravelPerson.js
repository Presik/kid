// Travel Person model
import store from "store";
import date from "date-and-time";

import tools from "tools/dates";
import proxy from "api/proxy";

let today = tools.dateToday();
const todayConvert = new Date(today);
// let month = todayConvert.setMonth(todayConvert.getMonth() - 1);
// const _month = tools.fmtDate2Tryton(new Date(month));
const todayMonthStart = new Date();
let start_month = todayMonthStart.setDate(1);
const _start_month = tools.fmtDate2Tryton(new Date(start_month));

const todayMonthEnd = new Date();
let end_month = todayMonthEnd.setDate(30);
const _end_month = tools.fmtDate2Tryton(new Date(end_month));

// const weekEnd = new Date();
let next_days = date.addDays(new Date(), 7);
const _next_days = tools.fmtDate2Tryton(next_days);

const getAgent = async () => {
  const session = await store.get("ctxSession");
  const dom = [["user", "=", session.user]];
  const fields = ["id", "rec_name"];
  const { data } = await proxy.search("commission.agent", dom, fields, 1);
  return data[0];
};

const searchSale = (value) => {
  console.log("search sale....", value);
  let res = "";
  if (value["sale."]) {
    res = value["sale."]["number"];
  }
  return res;
};

const searchParty = (value) => {
  const session = store.get("ctxSession");
  let search = [["agent.user", "=", session.user]];
  if (value) {
    const dom = [
      "OR",
      ["name", "ilike", `%${value}%`],
      ["id_number", "ilike", `${value}%`],
    ];
    search.push(dom);
  }
  return search;
};

const getAddress = (value, record) => {
  const party_id = record?.party?.id;

  let dom = party_id ? [["party", "=", party_id]] : [];
  if (value > 2) {
    dom.push(["rec_name", "ilike", `%${value}%`]);
  }
  return dom;
};

const onChangeAddress = (activeRec) => {
  const toStore = { invoice_address: activeRec.shipment_address.id };
  const toActive = { invoice_address: activeRec.shipment_address };
  return [toStore, toActive];
};

const onChangeParty = (activeRec) => {
  let toStore = {};
  let toActive = {};
  if (activeRec.party) {
    const priceList = activeRec.party["sale_price_list."];
    toStore = { price_list: activeRec.party.sale_price_list };
    toActive = { price_list: priceList };

    const invoice_type = activeRec.party.invoice_type;
    if (invoice_type) {
      toStore["invoice_type"] = invoice_type;
      toActive["invoice_type"] = invoice_type;
    }
  }
  return [toStore, toActive];
};

const getFilters = (session) => {
  console.log("_month....", _start_month, _end_month);
  return {
    today: [
      [
        "OR",
        ["arrival_date", "=", `${today}`],
        ["create_date", ">=", `${today} 00:00:00`],
      ],
      // ["agent.user", "=", session.user],
    ],
    month: [
      [
        ["arrival_date", ">=", _start_month],
        ["arrival_date", "<=", _end_month],
      ],
    ],
    week: [
      [
        ["arrival_date", ">=", today],
        ["arrival_date", "<=", _next_days],
      ],
    ],
    // month_6: [
    //   [
    //     ["arrival_date", "<=", `${today}`],
    //     ["arrival_date", ">=", `${_month_6}`],
    //   ],
    //   // ["agent.user", "=", session.user],
    // ],
  };
};

const getTotalAmount = (record) => {
  let totalAmount = 0;
  record?.lines?.forEach((item) => {
    totalAmount = totalAmount + parseFloat(item.amount_w_tax);
  });
  return totalAmount;
};

const getVisible = (name, record) => {
  if (record?.state === "draft") return true;
  return false;
};

const disabledEdit = (name, record) => {
  let res = false;
  if (record.state === "draft") {
    res = true;
  }
  return res;
};

const getView = (config) => {
  const session = store.get("ctxSession");
  // ["arrival_date", "=", today],
  let DictCtxView = {
    selectable: false, // options: multi - one - false
    activeSearch: true,
    filters: getFilters,
    domain: [
      // ["agent.user", "=", session.user],
    ],
    form_action: ["add", "edit"],
    table_action: ["edit", "add"],
    model: "sale.line.person",
    webfields: {
      name: { type: "char", readOnly: true },
      phone: { type: "char", readOnly: true },
      email: { type: "char", readOnly: true },
      id_number: { type: "char", readOnly: true },
      arrival_date: {
        type: "date",
        readOnly: true,
        searchable: true,
        // readOnly: { state: ["quotation"] },
      },
      sale: {
        type: "many2one",
        model: "sale.sale",
        function: searchSale,
        // withChange: onChangeParty,
        // required: true,
        // readOnly: { state: ["quotation", "processing"] },
        attrs: ["number"],
      },
      line: {
        type: "many2one",
        model: "sale.line",
        readOnly: true,
        searchable: true,
        attrs: ["sale", "sale.number", "rec_name"],
        // recSearch: getAddress,
        // withChange: onChangeAddress,
        // required: true,
        // readOnly: { state: ["quotation"] },
        // readOnly: { state: ["quotation", "processing"] },
      },
      //
      // shipment_date: {
      //   type: "date",
      //   required: false,
      //   // readOnly: { state: ["quotation"] },
      //   readOnly: { state: ["quotation", "processing"] },
      // },
      // agent: {
      //   type: "many2one",
      //   model: "commission.agent",
      //   default: getAgent,
      //   readOnly: true,
      // },
      // price_list: {
      //   type: "many2one",
      //   model: "product.price_list",
      //   recSearch: () => "[]",
      //   // readOnly: { state: ["quotation"] },
      //   readOnly: { state: ["quotation", "processing"] },
      // },
      // state: {
      //   type: "char",
      //   readOnly: true,
      //   translate: true,
      //   default: "draft",
      // },
      // invoice_state: {
      //   type: "char",
      //   readOnly: true,
      //   translate: true,
      //   // default: "draft",
      // },
      // shipment_state: {
      //   type: "char",
      //   readOnly: true,
      //   translate: true,
      //   // default: "draft",
      // },
      //
      // reference: {
      //   type: "char",
      //   readOnly: { state: ["quotation", "processing"] },
      // },
      // invoice_type: {
      //   type: "radio-group",
      //   default: "P",
      //   options: [
      //     { value: "P", text: "POS" },
      //     { value: "1", text: "ELECTRONICA" },
      //   ],
      //   readOnly: { state: ["quotation", "processing"] },
      // },
      // lines: {
      //   type: "one2many",
      //   model: "sale.line",
      //   ctxView: saleLine.ctxVself.product.rec_nameiew(config),
      //   required: true,
      //   // readOnly: { state: ["quotation"] },
      //   readOnly: { state: ["quotation", "processing"] },
      //
      //   // withChange: getTotalAmount,
      // },
      // total_amount_cache: {
      //   type: "number",
      //   readOnly: true,
      //   function: getTotalAmount,
      //   search: true,
      // },
      // total_amount: {
      //   type: "number",
      //   readOnly: true,
      // },
      // comment: {
      //   type: "text-area",
      //   readOnly: { state: ["quotation", "processing"] },
      // },
      // description: {
      //   type: "char",
      //   readOnly: { state: ["quotation", "processing"] },
      // },
      // quote: {
      //   type: "button",
      //   method: "create_sale",
      //   visible: getVisible,
      //   // save: true,
      // },
      // draft: {
      //   type: "button",
      //   method: "draft",
      //   visible: getVisible,
      //   // save: true,
      // },
    },
    webtree: [
      { name: "name", width: "20%" },
      { name: "phone", width: "20%" },
      { name: "email", width: "20%" },
      { name: "id_number", width: "15%" },
      { name: "arrival_date", width: "15%" },
      { name: "sale", width: "10%" },
      { name: "line", width: "10%" },
    ],
    webform: [
      { name: "name" },
      { name: "id_number" },
      {
        id: "infoSale",
        grid: [{ name: "phone" }, { name: "email" }],
        size: [1, 3],
        span: "col-span-1",
      },
      { name: "arrival_date" },
      // { name: "shipment_address" },
      // {
      //   id: "infoSale2",
      //   grid: [{ name: "price_list" }, { name: "invoice_type" }],
      //   size: [1, 2],
      //   span: "col-span-1",
      // },
      // { name: "description" },
      // { name: "lines", component: "modal" },
      // { name: "comment" },
      // { name: "total_amount_cache" },
      // { name: "state" },
      // { name: "quote" },
      // { name: "draft" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
