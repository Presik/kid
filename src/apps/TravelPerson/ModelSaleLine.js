// Sale model line
import proxy from "api/proxy";

const getProduct = (value) => {
  const search = [
    "OR",
    [
      ["template.name", "ilike", `%${value}%`],
      ["template.salable", "=", true],
    ],
    [
      ["code", "ilike", `${value}%`],
      ["template.salable", "=", true],
    ],
  ];
  return search;
};

const getSalePrices = async (activeRecord, parentRecord) => {
  const unit = activeRecord.unit?.id ?? activeRecord.product["sale_uom."].id;
  const { data: unit_price } = await proxy.methodInstance({
    model: "product.product",
    method: "_get_sale_unit_price",
    instance: activeRecord?.product?.id,
    args: [Number(activeRecord.quantity) ?? 1],
    ctx: {
      price_list: parentRecord?.price_list?.id,
      uom: unit,
      customer: parentRecord?.party?.id,
    },
  });
  if (!activeRecord.product) {
    return {};
  }
  const { data: price_w_tax } = await proxy.methodCall({
    model: "sale.sale",
    method: "dash_get_amount_w_tax",
    args: [
      {
        product: activeRecord.product["id"],
        list_price: unit_price,
      },
    ],
  });
  // const factor = activeRecord.unit.factor; // ???? bug
  return { unit_price: unit_price, price_w_tax: price_w_tax };
};

const onChangeProduct = async (activeRecord, parentRec) => {
  const parentRecord = parentRec.record;
  const product = activeRecord?.product;
  let toActive = {};
  let toStore = {};
  if (product) {
    let unit = product["sale_uom."];
    let { unit_price, price_w_tax } = await getSalePrices(
      activeRecord,
      parentRecord,
    );
    toActive.unit_price = unit_price;
    toActive.unit = unit;
    toActive.unit_price_w_tax = price_w_tax;
    toActive.amount_w_tax = price_w_tax;

    toStore.unit_price = unit_price;
    toStore.unit = unit.id;
  }
  return [toStore, toActive];
};

const onChangeUnit = async (activeRecord, parentRec) => {
  const parentRecord = parentRec.record;
  let toActive = {};
  let toStore = {};
  let { unit_price, price_w_tax } = await getSalePrices(
    activeRecord,
    parentRecord,
  );
  toActive.unit_price = unit_price;
  toActive.unit_price_w_tax = price_w_tax;
  toStore.unit_price = unit_price;
  toStore.unit_price_w_tax = price_w_tax;
  return [toStore, toActive];
};

const onChangeQty = (activeRecord) => {
  let toActive = {};
  if (activeRecord.quantity && activeRecord.unit_price_w_tax) {
    const qty = activeRecord.quantity;
    if (activeRecord.discount) {
      const { amount, discount } = activeRecord;
      const amount_ = amount * qty - (amount * qty * discount) / 100;
      toActive.amount_w_tax = amount_;
    } else {
      toActive.amount_w_tax = activeRecord.unit_price_w_tax * qty;
    }
  }
  return [{}, toActive];
};

const onChangeUnitPrice = async (storeRec, activeRecord) => {
  if (activeRecord.quantity && activeRecord.unit_price) {
    const { quantity, unit_price } = activeRecord;
    if (activeRecord.discount) {
      // fix me calculation discount
      const amount = unit_price - (unit_price * activeRecord.discount) / 100;
      activeRecord.unit_price_w_tax = amount;
      activeRecord.amount_w_tax = amount * quantity;
    } else {
      const { data: price_w_tax } = await proxy.methodCall({
        model: "sale.sale",
        method: "dash_get_amount_w_tax",
        args: {
          product: activeRecord.product["id"],
          list_price: unit_price,
        },
      });
      activeRecord.unit_price_w_tax = price_w_tax;
      activeRecord.amount_w_tax = price_w_tax * quantity;
    }
  }
  return storeRec, activeRecord;
};

const searchUnit = (value, record) => {
  let dom = [["category", "=", record?.unit?.category]];
  if (value) {
    dom.push(["name", "ilike", `%${value}%`]);
  }
  return dom;
};

const getTotalAmount = (activeRecord) => {
  let total_amount = 0;
  if (activeRecord.unit_price_w_tax * activeRecord.quantity) {
    total_amount = activeRecord.unit_price_w_tax * activeRecord.quantity;
  }
  total_amount = total_amount.toLocaleString("es", { useGrouping: true });
  return total_amount;
};

const onChangeDiscount = (storeRec, activeRecord) => {
  if (activeRecord.unit_price_w_tax && activeRecord.quantity) {
    const { amount, quantity } = activeRecord;
    activeRecord.total_amount = amount * quantity;
  }
  return storeRec, activeRecord;
};

const getView = (config) => {
  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
  let DictCtxView = {
    form_action_add: "modal",
    model: "sale.line",
    row_selectable: false,
    table_action: ["edit", "add"],
    form_action: ["add", "delete", "edit"],
    target: "sale",
    selectable: true,
    webfields: {
      product: {
        type: "many2one",
        model: "product.product",
        recSearch: getProduct,
        withChange: onChangeProduct,
        attrs: [
          "id",
          "list_price",
          "name",
          "sale_price_taxed",
          "sale_uom.rec_name",
        ],
        required: true,
      },
      unit: {
        type: "many2one",
        model: "product.uom",
        recSearch: searchUnit,
        withChange: onChangeUnit,
        readOnly: true,
        attrs: ["id", "name", "category", "factor"],
      },
      quantity: {
        type: "number",
        withChange: onChangeQty,
        required: true,
        default: 1,
      },
      unit_price: { type: "number", readOnly: true },
      unit_price_w_tax: { type: "number", readOnly: true },
      amount_w_tax: { type: "number", readOnly: true },
      discount: { type: "number", withChange: onChangeDiscount },
    },
    webtree: [
      { name: "product", width: "40%" },
      { name: "unit", width: "10%" },
      { name: "quantity", width: "10%" },
      { name: "amount_w_tax", width: "23%" },
    ],
    webform: [
      { name: "product" },
      { name: "unit" },
      { name: "quantity" },
      { name: "unit_price" },
      { name: "unit_price_w_tax" },
      { name: "amount_w_tax" },
    ],
  };

  if (config && config.allow_discount) {
    DictCtxView["webfields"]["discount"] = {
      type: "number",
      withChange: onChangeDiscount,
    };
    DictCtxView["webtree"].insert(3, { name: "discount", width: "4%" });
    DictCtxView["webform"].insert(3, { name: "discount" });
  }
  if (config && config.allow_manual_pricing) {
    DictCtxView["webfields"]["unit_price"] = {
      type: "number",
      required: true,
      withChange: onChangeUnitPrice,
    };
  }
  return DictCtxView;
};

export default { ctxView: getView };
