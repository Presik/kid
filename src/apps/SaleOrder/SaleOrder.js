import React from "react";

import Board from "components/Board";
import saleOrder from "./ModelSale";

const SaleOrder = ({ config }) => {
  const ctxView = saleOrder.ctxView(config);

  return <Board ctxView={ctxView} />;
};

export default SaleOrder;
