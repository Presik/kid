// Sale model
import store from "store";

import saleLine from "./ModelSaleLine";
import tools from "tools/dates";
import proxy from "api/proxy";

let today = tools.dateToday();
const todayConvert = new Date(today);
let month = todayConvert.setMonth(todayConvert.getMonth() - 1);
let _month = tools.fmtDate2Tryton(new Date(month));
let month_3 = todayConvert.setMonth(todayConvert.getMonth() - 3);
let _month_3 = tools.fmtDate2Tryton(new Date(month_3));
let month_6 = todayConvert.setMonth(todayConvert.getMonth() - 6);
let _month_6 = tools.fmtDate2Tryton(new Date(month_6));

const getAgent = async () => {
  const session = await store.get("ctxSession");
  const dom = [["user", "=", session.user]];
  const fields = ["id", "rec_name"];
  const { data } = await proxy.search("commission.agent", dom, fields, 1);
  return data[0];
};

const searchParty = (value) => {
  const session = store.get("ctxSession");
  let search = [["agent.user", "=", session.user]];
  if (value) {
    const dom = [
      "OR",
      ["name", "ilike", `%${value}%`],
      ["id_number", "ilike", `${value}%`],
    ];
    search.push(dom);
  }
  return search;
};

const getAddress = (value, record) => {
  const party_id = record?.party?.id;

  let dom = party_id ? [["party", "=", party_id]] : [];
  if (value > 2) {
    dom.push(["rec_name", "ilike", `%${value}%`]);
  }
  return dom;
};

const onChangeAddress = (activeRec) => {
  const toStore = { invoice_address: activeRec.shipment_address.id };
  const toActive = { invoice_address: activeRec.shipment_address };
  return [toStore, toActive];
};

const onChangeParty = (activeRec) => {
  let toStore = {};
  let toActive = {};
  if (activeRec.party) {
    const priceList = activeRec.party["sale_price_list."];
    toStore = { price_list: activeRec.party.sale_price_list };
    toActive = { price_list: priceList };

    const invoice_type = activeRec.party.invoice_type;
    if (invoice_type) {
      toStore["invoice_type"] = invoice_type;
      toActive["invoice_type"] = invoice_type;
    }
  }
  return [toStore, toActive];
};

const getFilters = (session) => {
  return {
    today: [
      [
        "OR",
        ["sale_date", "=", `${today}`],
        ["create_date", ">=", `${today} 00:00:00`],
      ],
      ["agent.user", "=", session.user],
    ],
    month: [
      [
        ["sale_date", "<=", `${today}`],
        ["sale_date", ">=", `${_month}`],
      ],
      ["agent.user", "=", session.user],
    ],
    month_3: [
      [
        ["sale_date", "<=", `${today}`],
        ["sale_date", ">=", `${_month_3}`],
      ],
      ["agent.user", "=", session.user],
    ],
    month_6: [
      [
        ["sale_date", "<=", `${today}`],
        ["sale_date", ">=", `${_month_6}`],
      ],
      ["agent.user", "=", session.user],
    ],
  };
};

const getTotalAmount = (record) => {
  let totalAmount = 0;
  record?.lines?.forEach((item) => {
    totalAmount = totalAmount + parseFloat(item.amount_w_tax);
  });
  return totalAmount;
};

const getVisible = (name, record) => {
  if (record?.state === "draft") return true;
  return false;
};

const disabledEdit = (name, record) => {
  let res = false;
  if (record.state === "draft") {
    res = true;
  }
  return res;
};

const getView = (config) => {
  const session = store.get("ctxSession");
  let DictCtxView = {
    selectable: false, // options: multi - one - false
    activeSearch: true,
    filters: getFilters,
    domain: [
      ["sale_date", "=", today],
      ["agent.user", "=", session.user],
    ],
    form_action: ["add", "edit"],
    table_action: ["edit", "add"],
    model: "sale.sale",
    webfields: {
      number: { type: "char", readOnly: true },
      party: {
        type: "many2one",
        model: "party.party",
        // fix this
        recSearch: searchParty,
        withChange: onChangeParty,
        required: true,
        readOnly: { state: ["quotation", "processing"] },
        attrs: ["invoice_type", "sale_price_list", "sale_price_list.name"],
      },
      sale_date: {
        type: "date",
        readOnly: true,
        readOnly: { state: ["quotation"] },
      },
      shipment_date: {
        type: "date",
        required: false,
        // readOnly: { state: ["quotation"] },
        readOnly: { state: ["quotation", "processing"] },
      },
      shipment_address: {
        type: "many2one",
        model: "party.address",
        recSearch: getAddress,
        withChange: onChangeAddress,
        required: true,
        // readOnly: { state: ["quotation"] },
        readOnly: { state: ["quotation", "processing"] },
      },
      agent: {
        type: "many2one",
        model: "commission.agent",
        default: getAgent,
        readOnly: true,
      },
      price_list: {
        type: "many2one",
        model: "product.price_list",
        recSearch: () => "[]",
        // readOnly: { state: ["quotation"] },
        readOnly: { state: ["quotation", "processing"] },
      },
      state: {
        type: "char",
        readOnly: true,
        translate: true,
        default: "draft",
      },
      invoice_state: {
        type: "char",
        readOnly: true,
        translate: true,
        // default: "draft",
      },
      shipment_state: {
        type: "char",
        readOnly: true,
        translate: true,
        // default: "draft",
      },

      reference: {
        type: "char",
        readOnly: { state: ["quotation", "processing"] },
      },
      invoice_type: {
        type: "radio-group",
        default: "P",
        options: [
          { value: "P", text: "POS" },
          { value: "1", text: "ELECTRONICA" },
        ],
        readOnly: { state: ["quotation", "processing"] },
      },
      lines: {
        type: "one2many",
        model: "sale.line",
        ctxView: saleLine.ctxView(config),
        required: true,
        // readOnly: { state: ["quotation"] },
        readOnly: { state: ["quotation", "processing"] },

        // withChange: getTotalAmount,
      },
      total_amount_cache: {
        type: "number",
        readOnly: true,
        function: getTotalAmount,
        search: true,
      },
      total_amount: {
        type: "number",
        readOnly: true,
      },
      comment: {
        type: "text-area",
        readOnly: { state: ["quotation", "processing"] },
      },
      description: {
        type: "char",
        readOnly: { state: ["quotation", "processing"] },
      },
      quote: {
        type: "button",
        method: "create_sale",
        visible: getVisible,
        // save: true,
      },
      // draft: {
      //   type: "button",
      //   method: "draft",
      //   visible: getVisible,
      //   // save: true,
      // },
    },
    webtree: [
      { name: "number", width: "20%" },
      { name: "agent", width: "20%" },
      { name: "sale_date", width: "20%" },
      { name: "party", width: "35%" },
      { name: "total_amount", width: "10%" },
      { name: "invoice_state", width: "10%" },
      { name: "shipment_state", width: "10%" },
      { name: "state", width: "10%" },
    ],
    webform: [
      { name: "agent" },
      {
        id: "infoSale",
        grid: [
          { name: "shipment_date" },
          { name: "reference" },
          { name: "number" },
        ],
        size: [1, 3],
        span: "col-span-1",
      },
      { name: "party" },
      { name: "shipment_address" },
      {
        id: "infoSale2",
        grid: [{ name: "price_list" }, { name: "invoice_type" }],
        size: [1, 2],
        span: "col-span-1",
      },
      { name: "description" },
      { name: "lines", component: "modal" },
      { name: "comment" },
      { name: "total_amount_cache" },
      { name: "state" },
      { name: "quote" },
      // { name: "draft" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
