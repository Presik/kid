import { create } from "zustand";

import proxy from "api/proxy";

const fieldsCats = ["category", "category.name", "category.name_icon"];
const modelCats = "sale.shop-product.category";
const orderCats = [["category.name", "ASC"]];
const fieldsPts = [
  "name",
  "list_price",
  "default_uom",
  "sale_price_taxed",
  "products_mix",
  "products_mix.name",
  "products_mix.list_price",
  "products_mix.default_uom",
  "products_mix.sale_price_taxed",
];
const modelPts = "product.product";
const orderPts = [["code", "ASC"]];
const limit = 100;

export const useStoreProducts = create((set, get) => ({
  categories: null,
  products: {},
  unavailables: [],
  setCategories: async (shopId) => {
    const cats = get().categories;
    const _unavailables = get().unavailables;
    const noProducts = await getUnavailables(_unavailables);
    if (noProducts) {
      let catsProducts = get().products;
      for (const pdts of Object.values(catsProducts)) {
        for (const pdtId of noProducts) {
          pdts.delete(pdtId);
        }
      }
    }
    if (cats) return cats;

    const domain = [["shop", "=", shopId]];
    const { data, error } = await proxy.search(
      modelCats,
      domain,
      fieldsCats,
      100,
      orderCats,
    );
    if (!error) {
      set({ categories: data });
    }
  },
  setProducts: async (categoryId) => {
    let products = get().products;
    if (products) {
      const pts = products[categoryId];
      if (pts) {
        return pts;
      }
    }
    const domain = [
      ["categories", "=", categoryId],
      ["template.salable", "=", true],
      ["template.active", "=", true],
    ];
    const { data, error } = await proxy.search(
      modelPts,
      domain,
      fieldsPts,
      limit,
      orderPts,
    );
    if (data) {
      const list = data.map((rec) => [rec.id, rec]);
      products[categoryId] = new Map(list);
      set({ products: products });
    }
  },
}));

async function getUnavailables(current) {
  let res;
  const dom = [
    ["active", "=", false],
    ["template.salable", "=", true],
    ["template.type", "=", "goods"],
    ["id", "not in", current],
  ];
  const { data, error } = await proxy.search("product.product", dom, ["id"]);
  if (data.length > 0) {
    res = data.map((rec) => rec.id);
  }
  return res;
}
