import React, { useState, Fragment } from "react";
import store from "store";

import RestCategories from "./RestCategories";
import RestProducts from "./RestProducts";
import IconButton from "components/IconButton";

import date from "date-and-time";
import dates from "tools/dates";

const now = new Date();
const lastHours = date.addHours(now, -18);
const _date = dates.fmtDatetime2Tryton(lastHours);

function MenuRest(props) {
  const sessionUser = store.get("ctxSession");
  const [view, setView] = useState("categories");
  const [message, setMessage] = useState(null);
  const [category, setCategory] = useState(null);

  function onChange(_view, record) {
    setMessage(null);
    if (_view) {
      setView(_view);
      if (_view === "products" && record) {
        setCategory(record);
      }
    }
  }

  return (
    <div id="modal-menu-rest-order" className="">
      {view === "categories" ? (
        <RestCategories sessionUser={sessionUser} onChange={onChange} />
      ) : (
        <Fragment>
          <div className="flex">
            <IconButton
              onClick={() => onChange("categories")}
              color="stone"
              name="fi fi-rr-arrow-small-left"
              tooltip="board.button_back"
            />
            {message && (
              <div className="flex text-sky-700 mx-auto my-auto font-bold gap-x-3">
                {message}
                <i
                  key="check"
                  className="flex text-lime-500 text-2xl text-bold my-auto fi fi-br-check"
                />
              </div>
            )}
          </div>
          <RestProducts
            sessionUser={sessionUser}
            category={category}
            record={props.record}
            updateMsg={setMessage}
          />
        </Fragment>
      )}
    </div>
  );
}

export default MenuRest;
