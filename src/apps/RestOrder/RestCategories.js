import React from "react";

import RestCardCategory from "./RestCardCategory";
import { useStoreProducts } from "./storeProducts";

function RestCategories({ onChange, sessionUser }) {
  const { categories, setCategories } = useStoreProducts();
  setCategories(sessionUser.shop);

  if (!categories) return null;

  return (
    <div className="grid grid-cols-2 w-full">
      {categories.map((rec, idx) => (
        <RestCardCategory
          key={idx}
          onChange={onChange}
          record={rec["category."]}
        />
      ))}
    </div>
  );
}

export default RestCategories;
