import React, { useState } from "react";

import RestCardProduct from "./RestCardProduct";
import OptionsModal from "./OptionsModal";
import { useRecords } from "hooks/records";
import { useStoreProducts } from "./storeProducts";

function RestProducts({ category, record, updateMsg }) {
  const { products, setProducts } = useStoreProducts();
  const [openModal, setOpenModal] = useState(false);
  const [options, setOptions] = useState(null);
  setProducts(category.id);

  if (!products[category.id]) return null;

  function closeModal() {
    setOptions(null);
    setOpenModal(false);
  }

  function handleModal(opts) {
    setOpenModal(true);
    setOptions(opts);
  }

  console.log("_products.......", products[category.id]);
  const _products = Array.from(products[category.id].values());
  console.log("_products.......", _products);
  return (
    <div className="grid grid-cols-2 sm:grid-cols-3 w-full my-3 sm:px-2">
      {_products.map((pdt, idx) => (
        <RestCardProduct
          key={idx}
          product={pdt}
          updateMsg={updateMsg}
          handleModal={handleModal}
          closeModal={closeModal}
        />
      ))}
      {options && (
        <OptionsModal
          open={openModal}
          options={options}
          onClose={closeModal}
          updateMsg={updateMsg}
        />
      )}
    </div>
  );
}

export default RestProducts;
