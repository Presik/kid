import React, { useState } from "react";

import RestProducts from "./RestProducts";
import OptionsModal from "./OptionsModal";
import tools from "tools/functions";
import { useFormStore } from "store/formStore";
import { useIdSeq } from "store/idSeq";

function RestCardProduct({ product, updateMsg, handleModal, optional }) {
  const { storeRecord, activeRecord, upFieldActive, upFieldStore } =
    useFormStore();
  const { seq, increment } = useIdSeq();

  function onSelect() {
    increment();
    let line = {
      id: seq,
      product: { id: product.id, name: product.name },
      unit_price: product.list_price.toString(),
      quantity: 1,
      sale_price_taxed: product.sale_price_taxed,
      status_order: "draft",
      unit: product.default_uom,
    };
    if (optional) {
      line.sale_price_taxed = 0;
      line.unit_price = 0;
    }
    updateMsg(product.name);
    let _activeRecord = { ...activeRecord };
    let lines = _activeRecord.lines;
    lines.set(seq, line);
    upFieldActive("lines", lines);

    let _storeRecord = { ...storeRecord };
    if (!_storeRecord.lines) {
      _storeRecord.lines = new Map();
      _storeRecord.lines.set("create", new Map());
    }
    let to_create = _storeRecord.lines.get("create");
    let _line = { ...line };
    _line.product = product.id;
    delete _line.total_amount;
    delete _line.sale_price_taxed;
    to_create.set(seq, _line);
    upFieldStore("lines", _storeRecord.lines);
    const options = product["products_mix."];
    if (options && options.length > 0) {
      handleModal(options);
    }
  }

  return (
    <div
      className={
        "overflow-hidden mx-2 my-2 cursor-pointer rounded-lg border-[1px] border-gray-200 active:scale-95 active:bg-lime-100"
      }
      onClick={() => onSelect()}
    >
      <div className="py-1 px-3 h-12 bg-stone-700">
        <p className="text-xs md:text-sm text-left text-stone-200 mx-auto my-auto font-medium line-clamp-2">
          {product.name}
        </p>
      </div>
      <div className="flex bg-stone-600 h-18">
        <div className="bg-lime-300 w-3 h-3 my-auto ml-4 rounded-full" />
        <p className="text-xs md:text-base text-right ml-auto mr-3 py-[4px] text-stone-200 mx-auto my-auto">
          {tools.fmtMoney(product.sale_price_taxed)}
        </p>
      </div>
    </div>
  );
}

export default RestCardProduct;
