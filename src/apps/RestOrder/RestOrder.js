import React from "react";

import Board from "components/Board";
import restOrder from "./Sale";

const SaleOrder = ({ config }) => {
  return <Board ctxView={restOrder.ctxView(config)} />;
};

export default SaleOrder;
