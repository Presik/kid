import React, { useState } from "react";
import { FormattedMessage as FM } from "react-intl";
import date from "date-and-time";

import Screen from "components/Screen";
import Board from "components/Board";
import DateRangeFilter from "components/DateRangeFilter";
import WidgetSide from "./components/WidgetSide";
import WidgetSideStatusHotel from "./components/WidgetSideStatusHotel";
import SectionRigth from "components/SectionRigth";
import ListPeopleCheckInWidget from "./components/ListPeopleCheckInWidget";
import WidgetSideCustomers from "./components/WIdgetSideCustomers";
import WidgetCheckInSide from "./components/WidgetCheckInSide";
import opportunityModel from "./ModelOpportunity";
import { dataListCustomers } from "apps/Booking/dataFake";

const fmt = "YYYY-MM-DD";

const Opportunity = (props) => {
  const { config } = props;
  const ctxView = opportunityModel.ctxView(config);
  const [statusSection, setStatusSection] = useState(true);
  const [rooms, setRooms] = useState([]);
  const [filterDay, setFilterDay] = useState(date.format(new Date(), fmt));
  let [booking, setBooking] = useState([]);

  return (
    <Screen>
      <div
        className={`flex ${
          statusSection ? "flex-row justify-between " : "flex-col"
        }`}
      >
        <Board ctxView={ctxView} />
        <SectionRigth
          position="right"
          widgets={["status", "numPeople"]}
          title="360° SMART HOTEL"
          bgColor={statusSection ? "bg-gray-100" : "bg-blue-presik"}
          status={statusSection}
          handleChangeStatus={setStatusSection}
        >
          <WidgetSideStatusHotel
            title={<FM id="booking.widget.screen.huesped_house" />}
            background={"bg-gray-100"}
            name="statusHotel"
            rooms={rooms}
            day={filterDay}
          />
          <WidgetCheckInSide
            title="Estado"
            background={"bg-gray-100"}
            rooms={rooms}
            day={filterDay}
          />
          <WidgetSide
            title={<FM id="booking.widget.screen.huesped_house" />}
            num={50}
            background={"bg-gray-100"}
            name="statusHotel"
            rooms={rooms}
            day={filterDay}
          />
          <WidgetSide
            title={<FM id="booking.widget.screen.huesped_house" />}
            num={3}
            background={"bg-gray-100"}
            name="statusPeopleHotel"
            rooms={rooms}
            day={filterDay}
          />
          <WidgetSideCustomers
            dataCustomers={dataListCustomers}
            title={<FM id="booking.widget.screen.last_bookings" />}
            data={booking}
            // day={filterDay}
          />
          <ListPeopleCheckInWidget
            title={<FM id="booking.widget.screen.more_options" />}
          />
        </SectionRigth>
      </div>
    </Screen>
  );
};

export default Opportunity;
