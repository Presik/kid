// Opportunity model line

const getView = () => {
  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
  let DictCtxView = {
    model: "crm.person",
    form_action: ["add", "edit"],
    table_action: ["add", "edit"],
    validate_state: true,
    webfields: {
      name: { type: "char", required: true },
      id_number: { type: "char" },
      phone: { type: "char" },
      email: { type: "char" },
    },
    webtree: [
      { name: "name", width: "30%" },
      { name: "id_number", width: "15%" },
      { name: "phone", width: "15%" },
      { name: "email", width: "30%" },
    ],
    webform: [
      { name: "name" },
      { name: "id_number" },
      { name: "phone" },
      { name: "email" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
