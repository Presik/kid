import React, { Disclosure } from "@headlessui/react";
import { FormattedMessage as FM } from "react-intl";
import imgDespegar from "assets/apps/despegar.png";
import imgBooking from "assets/apps/booking.png";
import imgExpedia from "assets/apps/expedia.png";
import imgHouse from "assets/apps/house.png";
import { ReactComponent as IconHouse } from "assets/apps/iconHouse.svg";
import ItemRoom from "./ItemRoom";
import ButtonActionInfo from "./ButtonActionInfo";

const iconOta = {
  DESPEGAR: imgDespegar,
  BOOKING: imgBooking,
  EXPEDIA: imgExpedia,
  HOUSE: imgHouse,
};

function WidgetSideCustomers({ title, data }) {
  const styleLabel = "text-gray-500 font-semibold text-sm";
  const styleValue = "";

  const headerCustomer = (
    name,
    image = undefined,
    room = 0,
    channel = { name: "HOUSE", id: 0 },
  ) => {
    if (!image) {
      image = `https://ui-avatars.com/api/?name=${name}&background=0D8ABC&color=fff`;
    }
    return (
      <div className="flex  items-center justify-stretch relative">
        <div className="flex items-center w-full space-x-3">
          <img src={image} className="w-10 h-10 rounded-full"></img>
          <span className="truncate">{name}</span>
        </div>

        <span className="flex text-gray-500  relative justify-end">
          <IconHouse
            className={`w-12  text-transparent relative   mr-1 ${
              room == 0 ? "opacity-50" : "fill-white"
            }`}
          />
          {room !== 0 ? (
            <span className="text-bluePresik rounded-full absolute p-0  flex justify-center items-center text-[10px] top-[6px] right-[35px] w-[20px] ">
              {room}
            </span>
          ) : null}

          <img
            src={
              iconOta[channel?.name] ? iconOta[channel?.name] : iconOta["HOUSE"]
            }
            title="despegar"
            className="w-6 relative"
          />
        </span>
      </div>
    );
  };

  return (
    <div className="bg-white border-gray-100 shadow-sm p-3 flex flex-col  rounded-lg divide-y">
      <h3 className="truncate text-xl font-bold text-sky-800">{title}</h3>

      {data.slice(0, 4).map((customer, key) => {
        const {
          contact = "",
          number = "",
          booking_date,
          total_amount,
          state,
          image,
        } = customer;

        const name = customer["party."]?.name
          ? customer["party."]?.name
          : "N/ A";
        const phone = customer["party."]?.phone;
        const email = customer["party."]?.email;

        return (
          <div key={customer.number}>
            <Disclosure key={key}>
              <Disclosure.Button className="py-2 w-full text-left cursor-pointer uppercase">
                {headerCustomer(
                  name,
                  image,
                  customer["lines."].length,
                  customer["channel."],
                )}
              </Disclosure.Button>
              <Disclosure.Panel className="py-2">
                <h4 className="truncate text-sm font-bold text-gray-600 mb-4">
                  <FM id="booking.widget.customers.title" />
                </h4>
                {customer.contact && (
                  <h3
                    className="mb-2 text-white bg-blue-presik py-1 px-3 -mt-2 rounded-full truncate"
                    title={
                      <FM id="booking.widget.customers.name_contact.placeholder" />
                    }
                  >
                    <FM id="booking.widget.customers.name_contact" />{" "}
                    <span className="font-medium">{contact}</span>
                  </h3>
                )}

                <div className="grid grid-cols-2 mb-3 capitalize">
                  <div className="space-y-3">
                    <div className="flex flex-col">
                      <span className={styleLabel}>
                        <FM id="booking.widget.customers.number" />
                      </span>
                      <span className={styleValue}>{number}</span>
                    </div>
                    <div className="flex flex-col">
                      <span className={styleLabel}>
                        <FM id="booking.widget.customers.booking_date" />
                      </span>
                      <span className={styleValue}>{booking_date}</span>
                    </div>
                  </div>

                  <div className="space-y-3">
                    <div className="flex flex-col">
                      <span className={styleLabel}>
                        <FM id="booking.widget.customers.total_amount" />
                      </span>
                      <span className={styleValue}>{total_amount}</span>
                    </div>
                    <div className="flex flex-col">
                      <span className={styleLabel}>
                        <FM id="booking.widget.customers.state" />
                      </span>
                      <span className={styleValue}>{state}</span>
                    </div>
                  </div>
                </div>
                <div className="text-gray-500 flex flex-col gap-1">
                  <div className="flex space-x-3 justify-between">
                    <span className={styleLabel}>
                      <FM id="booking.widget.customers.rooms" />
                    </span>
                    <div className="flex space-x-3 items-center">
                      <div
                        id="circle"
                        className="w-4 h-4 mx-auto rounded-full border-[1px] bg-lime-300 border-lime-500"
                      ></div>{" "}
                      <span className="text-xs">
                        <FM id="hotel.room.clean" />
                      </span>
                      <div
                        id="circle"
                        className="w-4 h-4 mx-auto rounded-full border-[1px] bg-rose-300 border-rose-500"
                      ></div>
                      <span className="text-xs">
                        <FM id="hotel.room.dirty" />
                      </span>
                    </div>
                  </div>

                  {customer["lines."].map((room, key) => {
                    // return getRoom(room["room."], key);
                    return <ItemRoom room={room["room."]} key={key} />;
                  })}
                </div>
                <hr className="mt-4" />

                <h4 className="truncate text-sm font-bold text-gray-600 my-4">
                  <FM id="booking.widget.customers.info_huesped" />
                </h4>
                <div className="grid grid-cols-2 ">
                  <div className="space-y-3">
                    <div className="flex flex-col">
                      <span className={styleLabel}>
                        <FM id="booking.widget.customers.name" />
                      </span>
                      <span className={styleValue}>{name}</span>
                    </div>

                    <div className="flex flex-col">
                      <span className={styleLabel}>
                        <FM id="booking.widget.customers.phone" />
                      </span>
                      <span className={styleValue}>{phone}</span>
                    </div>
                  </div>

                  <div className="space-y-3">
                    <div className="flex flex-col">
                      <span className={styleLabel}>
                        <FM id="booking.widget.customers.booking_hour" />
                      </span>
                      <span className={styleValue}>8:OO AM</span>
                    </div>

                    <div className="flex flex-col">
                      <span className={styleLabel}>
                        <FM id="booking.widget.customers.email" />
                      </span>
                      <span className={styleValue}>{email}</span>
                    </div>
                  </div>
                </div>

                <ButtonActionInfo
                  phone={phone}
                  whatsapp={phone}
                  email={email}
                />
              </Disclosure.Panel>
            </Disclosure>
          </div>
        );
      })}

      <span className="pt-3 text-center text-gray-400">
        <FM id="booking.widget.screen.view_more" />
      </span>
    </div>
  );
}

export default WidgetSideCustomers;
