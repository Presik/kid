import React from "react";
import date from "date-and-time";
import { FormattedMessage as FM } from "react-intl";

// import { IconButton } from "./Constants/constants";

const WidgetSide = ({ name, title, size = "w-full", rooms, day }) => {
  let numToday = 0;
  let numTomorrow = 0;
  let nextDay = new Date(day);
  nextDay.setDate(nextDay.getDate() + 2);
  let nextDayFormatted = date.format(nextDay, "YYYY-MM-DD");
  let roomToday = rooms.filter((room) => {
    return (
      room["arrival_date"] == day && room["registration_state"] == "check_in"
    );
  });

  let roomTomorrow = rooms.filter((room) => {
    return room["arrival_date"] == nextDayFormatted;
  });

  if (name === "statusHotel") {
    let guestsToday = roomToday.flatMap((room) => room["guests"]);
    let guestsTomorrow = roomTomorrow.flatMap((room) => room["guests"]);
    numToday = guestsToday.length;
    numTomorrow = guestsTomorrow.length;
  }
  if (name === "statusPeopleHotel") {
    numToday = roomToday.length;
    numTomorrow = roomTomorrow.length;
  }

  return (
    <div
      className={`bg-white border-gray-100 shadow-sm p-3 flex items-center space-x-3 rounded-lg  ${size}`}
    >
      <span className="w-1/2 font-normal pr-2 text-slate-600">{title}</span>

      <div className="flex w-full justify-center space-x-3">
        <span className="flex flex-col items-center space-x-1 py-2 w-1/2 bg-sky-200 text-sky-700 rounded-md">
          <span className="text-2xl text-sky-700 font-medium">{numToday}</span>{" "}
          <span className="font-normal">
            <FM id="booking.widget_side.today" />
          </span>
        </span>

        <span className="flex flex-col items-center space-x-1 py-2 w-1/2 bg-gray-200 text-gray-700 rounded-md">
          <span className="text-2xl text-gray-700 font-medium">
            {numTomorrow}
          </span>{" "}
          <span>
            <FM id="booking.widget_side.tomorrow" />
          </span>
        </span>
      </div>
    </div>
  );
};

export default WidgetSide;
