import React from "react";

import { classNames } from "tools/ui";

const base =
  "group shadow-sm rounded-md px-2 cursor-pointer py-3 flex flex-col justify-center items-center -space-y-2 text-white w-full";

const WidgetCheckInSide = ({ rooms, day }) => {
  let checkIn = rooms.filter((room) => {
    return (
      room["arrival_date"] == day && room["registration_state"] == "check_in"
    );
  });
  let checkOut = rooms.filter((room) => {
    return (
      room["arrival_date"] == day && room["registration_state"] === "check_out"
    );
  });
  let pending = rooms.filter((room) => {
    return (
      room["arrival_date"] == day && room["registration_state"] === "pending"
    );
  });

  const dataCheckIn = {
    checkIn: {
      num: checkIn.length,
      name: "Check In",
      color: "text-lime-700",
      bgColor: "bg-lime-200",
    },
    checkOut: {
      num: checkOut.length,
      name: "Check Out",
      color: "text-amber-800",
      bgColor: "bg-amber-200",
    },
    cancelled: {
      num: pending.length,
      name: "Pending",
      color: "text-rose-700",
      bgColor: "bg-rose-200",
    },
  };

  const title = "text-3xl font-semibold text-center";
  const subtitle = "text-xs uppercase text-center";
  const getItemStatus = (data) => {
    const color = data.color;
    return (
      <div className={classNames(base, data.bgColor)}>
        <div className="flex flex-col ">
          <span className={classNames(title, color)}>{data.num}</span>
          <span className={classNames(subtitle, data.color)}>{data.name}</span>
        </div>
      </div>
    );
  };

  dataCheckIn.checkIn.num = 7;
  dataCheckIn.checkOut.num = 11;
  dataCheckIn.cancelled.num = 1;
  console.log("dataCheckIn.checkIn....", dataCheckIn);
  return (
    <div className="bg-white border-gray-100 shadow-sm p-3 rounded-lg overflow-hidden">
      <div className="grid grid-cols-3 justify-between gap-3">
        {getItemStatus(dataCheckIn.checkIn)}
        {getItemStatus(dataCheckIn.checkOut)}
        {getItemStatus(dataCheckIn.cancelled)}
      </div>
    </div>
  );
};

export default WidgetCheckInSide;
