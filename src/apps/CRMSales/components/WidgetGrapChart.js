import React from "react";
import { Pie, Bar } from "react-chartjs-2";

// import { IconButton } from "..components/Constants/constants";

const WidgetGrapCharts = (props) => {
  const { title, size = "w-full", typeChart } = props;

  // Datos de ejemplo para los tipos de medios
  const data = {
    labels: ["Teléfono", "Correo", "Llamada", "Ota"],
    datasets: [
      {
        data: [30, 20, 15, 35], // Cantidad de cada tipo de medio
        backgroundColor: [
          "#fecdd3", // Color para Teléfono
          "#fde68a", // Color para Correo
          "#d9f99d", // Color para Llamada
          "#bae6fd", // Color para Otra
        ],
      },
    ],
  };

  const options = {
    plugins: {
      legend: {
        display: false, // Desactivar la visualización de la leyenda (labels)
      },
    },
  };

  return (
    <div
      className={`bg-white border-gray-100 shadow-sm p-3 flex-col space-x-1 overflow-hidden  space-y-2 rounded-lg ${size}`}
    >
      <h3 className="truncate text-xl font-bold text-sky-800">{title}</h3>
      <hr />
      {typeChart == "pie" && (
        <div
          style={{ height: "10vw", width: "17vw", maxWidth: "100%;" }}
          className="flex justify-center"
        >
          <Pie data={data} />
        </div>
      )}

      {typeChart == "bar" && (
        <div
          className="flex justify-center"
          style={{ height: "10vw", width: "17vw", maxWidth: "100%;" }}
        >
          <Bar data={data} options={options} />
        </div>
      )}
    </div>
  );
};

export default WidgetGrapCharts;
