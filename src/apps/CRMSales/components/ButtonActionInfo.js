import React from "react";
import {
  EnvelopeIcon,
  DevicePhoneMobileIcon,
  ChatBubbleLeftIcon,
} from "@heroicons/react/20/solid";
import { FormattedMessage as FM } from "react-intl";
import { ReactComponent as WhatsappIcon } from "assets/apps/iconwhatsapp.svg";

const ButtonActionInfo = (phone, email) => {
  const handleClickCall = () => {
    window.location.href = `tel:${phone}`;
    console.log("Llamando...");
  };

  const handleClickEmail = (email) => {
    window.location.href = `mail:${email}`;
  };

  const handleClickWhatsApp = (phone) => {
    const linckWhatsappBase = "https://api.whatsapp.com/send?phone=" + phone;
    window.location.href = `tel:${linckWhatsappBase}`;
  };

  return (
    <div className="flex justify-between items-center divide-x rounded-md shadow-sm mt-8 overflow-hidden">
      <div className="bg-gray-50 w-1/4 flex flex-col justify-center  items-center p-3 text-center space-y-2 hover:bg-blue-presik group cursor-pointer ">
        <DevicePhoneMobileIcon
          className="w-6 text-center group-hover:text-white"
          onClick={() => handleClickCall()}
        />

        <span className="text-sm text-gray-500">
          <FM id="booking.widget.customers.call" />
        </span>
      </div>
      <div
        className="bg-gray-50 w-1/4 flex flex-col justify-center items-center p-3 text-center space-y-2 hover:bg-blue-presik group cursor-pointer "
        onClick={() => handleClickWhatsApp(phone)}
      >
        <WhatsappIcon className="w-6 h-6 text-center group-hover:fill-white" />

        <span className="text-sm text-gray-500">Whatsapp</span>
      </div>
      <div
        className="bg-gray-50 w-1/4 flex flex-col justify-center items-center p-3 text-center space-y-2 hover:bg-blue-presik group cursor-pointer "
        onClick={() => handleClickEmail(email)}
      >
        <EnvelopeIcon className="w-6  text-center group-hover:text-white" />
        <span className="text-sm text-gray-500">
          <FM id="booking.widget.customers.email" />
        </span>
      </div>
      <div className="bg-gray-50 opacity-50 w-1/4 flex flex-col justify-center items-center p-3 text-center space-y-2  group  ">
        <ChatBubbleLeftIcon className="w-6 text-center " />
        <span className="text-sm text-gray-500">SMS</span>
      </div>
    </div>
  );
};

export default ButtonActionInfo;
