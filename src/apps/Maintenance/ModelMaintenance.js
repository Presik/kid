const stateColors = {
  excellent: "amber",
  good: "lime",
  acceptable: "sky",
  bad: "rose",
};

const getView = (config) => {
  let DictCtxView = {
    model: "maintenance.equipment",
    form_action: ["edit"],
    table_action: ["add", "edit"],
    activeSearch: true,
    // title: { field: "schedule", model: true },
    domain: [],
    tags: {
      state: stateColors,
    },
    webfields: {
      model_num: { type: "char" },
      vin: { type: "char" },
      gps_serial: { type: "char" },
      active: { type: "boolean" },
      code: { type: "char" },
      serial: { type: "char" },
      state: { type: "char", tags: stateColors, readOnly: true },
      product: {
        type: "many2one",
        required: true,
        model: "product.template",
        readOnly: true,
      },
    },
    webtree: [
      { name: "model_num" },
      { name: "product" },
      { name: "vin" },
      { name: "gps_serial" },
      { name: "serial" },
      { name: "active" },
      { name: "state", widget: "badge" },
    ],
    webform: [
      { name: "active" },
      { name: "model_num" },
      { name: "product" },
      { name: "vin" },
      { name: "gps_serial" },
      { name: "serial" },
      { name: "state", widget: "badge" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
