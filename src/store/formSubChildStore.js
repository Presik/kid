import { create } from "zustand";

import { getFromId } from "store/common";
import funcs from "tools/functions";
import proxy from "api/proxy";

export const useFormSubChildStore = create((set, get) => ({
  seqId: 0,
  model: null,
  storeSubChild: null, // Used for store the data to send to API
  activeSubChild: null, // Used for render UI purposes
  upStoreSubChild: (rec) => set(() => ({ storeSubChild: { ...rec } })),
  upActiveSubChild: (rec) => set(() => ({ activeSubChild: { ...rec } })),
  newSubChild: async (ctxView, parentRec) => {
    const [toStore, toActive] = await funcs.getDefaults(0, ctxView);
    let target = {};
    if (parentRec) {
      target[ctxView.target] = parentRec.record.id;
    }
    set((state) => ({
      seqId: state.seqId - 1,
      storeSubChild: { ...toStore, ...target, id: state.seqId },
      activeSubChild: { ...toActive, ...target, id: state.seqId },
    }));
  },
  setActiveSubChildFromId: async (id, model, ctxView) => {
    const res = await getFromId(id, model, ctxView);
    set(() => ({
      activeSubChild: res,
      storeSubChild: { id: id },
    }));
  },
  saveSubChild: async (model) => {
    const store = get().storeSubChild;
    const { data } = await proxy.saveQuery({
      model: model,
      storeRec: get().storeSubChild,
    });
    return data;
  },
  resetSubChild: () =>
    set(() => ({
      storeSubChild: null,
      activeSubChild: null,
      model: null,
    })),
}));
