import { create } from "zustand";

import { getFromId } from "store/common";
import proxy from "api/proxy";

// Examples
// activeRecord:
// {
//   id: 1,
//   party: {id: 36, name: APPLE INC},
//   lines: { *(Map)*
//       -2: {id: -2, product: 31, qty: 3, unit_price: 8300},
//      422: {id: 422, product: 109, qty: 1, unit_price: 500}
//   }
// }

// storeRecord:
// {
//   id: 1,
//   party: 36,
//   lines: { *(Map)*
//       create: Map(
//         -2: {id: -2, product: 236, qty: 3, unit_price: 8300},
//         -3: {id: -3, product: 111, qty: 18, unit_price: 2000}
//       )},
//       write: Map(
//         29: {id: 29, product: 109, qty: 4, unit_price: 500}
//       ),
//       delete: [45],
//   }
// }

export const useFormStore = create((set, get) => ({
  storeRecord: {}, // Used for store the data to send to API
  activeRecord: {}, // Used for render UI purposes
  ctxRecord: {}, // model, ctxView
  upStoreRecord: (rec) => set(() => ({ storeRecord: { ...rec } })),
  upActiveRecord: (rec) => set(() => ({ activeRecord: { ...rec } })),
  setActiveRecordFromId: async (id, model, ctxView) => {
    const res = await getFromId(id, model, ctxView);
    set({
      activeRecord: res,
      storeRecord: { id: id },
      ctxRecord: { model, ctxView },
    });
  },
  upFieldActive: (field, value) => {
    set((state) => ({
      activeRecord: updateActive(field, value, state),
    }));
  },
  upFieldStore: (field, value) => {
    set((state) => ({
      storeRecord: updateStore(field, value, state),
    }));
  },
  saveRecord: async () => {
    const { model } = get().ctxRecord;
    const store = { ...get().storeRecord };
    const { data, error } = await proxy.saveQuery({
      model: model,
      storeRec: store,
    });
    if (!error) {
      console.log("saveRecord..........{}");
      set({ storeRecord: {} });
    }
    return data;
  },
  resetRecord: (resetForm) => {
    console.log("resetRecord..........{}");
    if (resetForm) {
      set({ storeRecord: {}, activeRecord: {} });
    } else {
      const storeRec = get().storeRecord;
      set({ storeRecord: { id: storeRec.id } });
    }
  },
  reloadRecord: async () => {
    const { model, ctxView } = get().ctxRecord;
    const rec = get().activeRecord;
    await get().setActiveRecordFromId(rec.id, model, ctxView);
  },
}));

const updateStore = (fieldName, value, state) => {
  let res = { ...state.storeRecord };
  // let _value = value;
  // console.log("updateStore...", value);
  // if (typeof value === "object") {
  //   if (!(value instanceof Map)) {
  //     _value = value.id;
  //   }
  // }
  res[fieldName] = value;
  return res;
};

const updateActive = (fieldName, value, state) => {
  let res = { ...state.activeRecord };
  res[fieldName] = value;
  return res;
};
