import { create } from "zustand";

import { getFromId } from "store/common";
// import proxy from "api/proxy";

export const useWizardStore = create((set) => ({
  storeWizard: {}, // Used for store the wizard data
  activeWizard: {}, // Used for render UI purposes
  upStoreWizard: (rec) => set(() => ({ storeWizard: { ...rec } })),
  upActiveWizard: (rec) => set(() => ({ activeWizard: { ...rec } })),
  resetWizard: () => set(() => ({ activeWizard: {}, storeWizard: {} })),
  updateWizard: (rec) =>
    set((state) => ({
      activeWizard: { ...state.activeWizard, ...rec },
      storeWizard: { ...state.storeWizard, ...rec },
    })),
  setActiveWizardFromId: async (id, model, ctxView) => {
    const res = await getFromId(id, model, ctxView);
    set({
      activeWizard: res,
      storeWizard: { id: id },
    });
  },
}));
