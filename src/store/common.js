import funcs from "tools/functions";
import proxy from "api/proxy";

export const getFromId = async (rec_id, model, ctxView) => {
  const fields = funcs.getViewFields(ctxView, "form");
  const { data: records } = await proxy.browse(model, [rec_id], fields);
  return funcs.recordToJs(records[0], ctxView.webfields);
};
