import { create } from "zustand";

import { getFromId } from "store/common";
import funcs from "tools/functions";
import proxy from "api/proxy";

export const useFormChildStore = create((set, get) => ({
  seqId: 0,
  model: null,
  storeChild: null, // Used for store the data to send to API
  activeChild: null, // Used for render UI purposes
  upStoreChild: (rec) => set(() => ({ storeChild: { ...rec } })),
  upActiveChild: (rec) => set(() => ({ activeChild: { ...rec } })),
  setActiveChildFromId: async (id, model, ctxView) => {
    const res = await getFromId(id, model, ctxView);
    set(() => ({
      activeChild: res,
      storeChild: { id: id },
    }));
  },
  newChild: async (ctxView, parentRec) => {
    const [toStore, toActive] = await funcs.getDefaults(0, ctxView);
    set((state) => ({
      seqId: state.seqId - 1,
      storeChild: { ...toStore, id: state.seqId },
      activeChild: { ...toActive, id: state.seqId },
    }));
  },
  saveChild: async (model) => {
    const { data } = await proxy.saveQuery({
      model: model,
      storeRec: get().storeChild,
    });
    return data;
  },
  incrementSeqChild: (Id) => {
    set((state) => ({
      seqId: Id ? Id : state.seqId - 1,
    }));
  },
  resetChild: () =>
    set(() => ({
      storeChild: {},
      activeChild: {},
      model: null,
    })),
}));
