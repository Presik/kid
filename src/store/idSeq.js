import { create } from "zustand";

export const useIdSeq = create((set) => ({
  seq: -1,
  increment: (value) => set((state) => ({ seq: value ?? state.seq - 1 })),
  reset: () => set({ seq: -1 }),
}));
