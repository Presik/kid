import { create } from "zustand";

export const useBtnLoading = create((set) => ({
  loadingBtn: false,
  setButtonLoading: (value) =>
    set(() => ({
      loadingBtn: value,
    })),
}));
