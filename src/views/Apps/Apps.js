import React from "react";
import { Outlet } from "react-router-dom";

function ExtApps({ session }) {
  return (
    <div className="w-full h-full px-5">
      <Outlet context={{ session }} />
    </div>
  );
}

export default ExtApps;
