import store from "store";

function ctxStore() {
  const ctxSession = store.get("ctxSession");
  let ctx = {};
  if (ctxSession) {
    ctx = {
      company: ctxSession.company,
      user: ctxSession.user,
      currency: ctxSession.currency,
      timezone: ctxSession.timezone,
      session: ctxSession.session,
    };
  }
  return ctx;
}

export default ctxStore;
