import React from "react";

import FormField from "./FormField";
import intl from "i18n/messages";
import { classNames, colors } from "tools/ui";

const style =
  "h-10 rounded-md w-full py-2 px-2 text-gray-700 border border-slate-300 focus:outline-none focus:border focus:border-cyan-500";

// h-[2.4rem]
/**
 *
 * @param {*} props -
 * @param {Boolean} readOnly -
 * @param {String} type -
 * @param {String} label -
 * @param {String} name -
 * @param {String} placeholder -
 * @param {String} value -
 * @param {Function} onChange -
 * @param {Boolean} translate -
 * @returns Component TextField
 */

function TextField(props) {
  let editedValue = props.value ?? "";
  if (props.value && props.translate) {
    const _value = `${props.model}.${props.value}`;
    editedValue = intl.msgs[_value] || props.value;
  }
  let _color = props.color ?? "bg-white";

  if (props.readOnly) {
    _color = colors.readOnly;
  }
  let _style = style;
  if (props.className) {
    _style = props.className;
  }

  function onChange(event) {
    let _value = event.target.value;
    if (props.type === "integer") {
      _value = parseInt(_value);
    } else if (props?.uppercase) {
      _value = _value.toUpperCase();
    }
    if (props.specialCharacters && props.type === "char") {
      _value = _value.replace(/[^\w\s]|[\dñ]/gi, "");
      _value = _value.replace("  ", " ").toUpperCase();
    }
    editedValue = _value;
    props.onChange(props.name, _value);
  }

  return (
    <FormField {...props}>
      <div className="relative rounded-md">
        <input
          key={props.name}
          id={props.name}
          name={props.name}
          className={classNames(_color, _style)}
          type={props.type || "text"}
          value={editedValue}
          onChange={onChange}
          onBlur={props.onblur ? onChange : () => ""}
          readOnly={props.readOnly ?? false}
          placeholder={props.placeholder ?? ""}
        />
        {props.icon || null}
      </div>
    </FormField>
  );
}

export default TextField;
