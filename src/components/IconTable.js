import React from "react";
import {
  PencilSquareIcon,
  TrashIcon,
  ExclamationTriangleIcon,
  EllipsisVerticalIcon,
} from "@heroicons/react/24/outline";

import { classNames } from "tools/ui";

const ICONS = {
  edit: [PencilSquareIcon, "text-cyan-500"],
  open: [PencilSquareIcon, "text-cyan-500"],
  delete: [TrashIcon, "text-rose-500"],
  info: [EllipsisVerticalIcon, "text-gray-700"],
  warning: [ExclamationTriangleIcon, "text-amber-600"],
};

function IconTable({ name, record, onChangeView }) {
  function onClick(e) {
    e.stopPropagation();
    onChangeView(e, name, record);
  }

  const [Icon, color] = ICONS[name];
  return (
    <div className="flex my-auto">
      <button className="mx-auto" key={name} onClick={onClick}>
        <Icon className={classNames("h-6 w-6", color)} aria-hidden="true" />
      </button>
    </div>
  );
}

export default IconTable;
