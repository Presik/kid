import React from "react";

import FormGrid from "components/Form/FormGrid";
import Form from "components/Form";
import { useWizardStore } from "store/wizardStore";

/**
 *
 * @param {object} props - component props
 * @param {object} props.ctxView - object for render record
 * @param {object} props.handleButton - object record to show
 * @param {boolean} props.parentRec - true or false
 * @return {Component} - Modal Form for show form record
 */
function FormWizard({ ctxView, handleButton }) {
  const { storeWizard } = useWizardStore();

  return (
    <FormGrid>
      <Form
        key="wizard-form"
        ctxView={ctxView}
        webform={ctxView.webform}
        handleButton={handleButton}
        recordForm={storeWizard}
        level="wizard"
      />
    </FormGrid>
  );
}

export default FormWizard;
