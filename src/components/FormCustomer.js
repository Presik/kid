import React, { Fragment, useState, useEffect } from "react";
import proxy from "api/proxy";
import { useFormStore } from "store/formStore";

import TextField from "components/TextField";
import SelectionField from "components/SelectionField";
import StdButton from "components/StdButton";
import TwComboBox from "components/TwComboBox";
import { checkRequiredForm } from "tools/form";
import { TYPE_DOC, SEX } from "ext-apps/WebBookingHotel/constants";
import funcs from "tools/functions";
import PureModal from "./Modals/PureModal";

const FormGuest = ({ onClose }) => {
  let [btnDisabled, setBtnDisabled] = useState(true);
  let [store, setStore] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [openModal, setOpenModal] = useState(true);
  const [countries, setCountries] = useState([]);
  const [departments, setDepartments] = useState([]);
  let [city, setCity] = useState([]);
  const { activeRecord, storeRecord, upStoreRecord, upActiveRecord } =
    useFormStore();
  const storeRequired = [
    "name",
    // "sex",
    "id_number",
    "email",
    "mobile",
    "address",
    // "nationality",
    // "country",
  ];

  useEffect(() => {
    dataCountry();
  }, []);

  const dataCountry = async () => {
    const dom = [];
    const { data } = await proxy.search("party.country_code", dom, [
      "name",
      "id",
    ]);
    if (data) {
      console.log(data);
      const countries = funcs.recs2Combobox(data);
      setCountries(countries);
    }
  };

  async function acceptCreate() {
    console.log(store, "datos aca");
    let _activeRecord = { ...activeRecord };
    let _storeRecord = { ...storeRecord };

    setIsLoading(true);
    if (store) {
      let address = [
        [
          "create",
          [
            {
              street: store.address,
              city_code: store.city_code,
              department_code: store.department_code,
              country_code: store.country_code,
            },
          ],
        ],
      ];

      let contact_mechanisms = [
        [
          "create",
          [
            {
              type: "mobile",
              value: store.mobile,
            },
            {
              type: "email",
              value: store.email,
            },
          ],
        ],
      ];

      let toStore = {
        name: store.name,
        id_number: store.id_number,
        type_document: 13,
        addresses: address,
        sex: store.sex.id,
        contact_mechanisms,
      };

      const fieldsNames = ["id"];
      const { data, error } = await proxy.create(
        "party.party",
        toStore,
        fieldsNames,
      );

      if (data) {
        console.log("ahora intentare pasar por el booking");

        const { data: customerData, error } = await proxy.browse(
          "party.party",
          [data[0]],
          [
            "name",
            "id_number",
            "phone",
            "account_receivable",
            "mobile",
            "account_payable",
            "address",
            "addresses",
          ],
        );
        console.log("todos los datos del usuario", customerData);
        _storeRecord.party = data[0];
        _activeRecord.party = customerData[0];
        upActiveRecord(_activeRecord);
        upStoreRecord(_storeRecord);
        onCloseModal();
      } else {
        alert(error);
      }

      setIsLoading(false);
    }
    setIsLoading(false);
  }

  const onCloseModal = () => {
    setOpenModal(false);
    onClose(false);
    // navigate(-1);
  };

  const onChange = (field, value) => {
    store[field] = value;
    const newStore = { ...store };
    const res = checkRequiredForm(newStore, storeRequired);
    setBtnDisabled(!res);
    setStore(newStore);
  };

  const handelCancel = async () => {
    setIsLoading(false);
    navigate(-1);
  };

  const onChangeCountry = async (event, value) => {
    store[event] = value.value;
    const newStore = { ...store };
    const res = checkRequiredForm(newStore, storeRequired);
    setBtnDisabled(!res);
    setStore(newStore);

    if (value.value === 48 && event === "country_code") {
      const { data } = await proxy.search(
        "party.department_code",
        [],
        ["name", "id"],
      );
      if (data) {
        const _departments = funcs.recs2Combobox(data);
        console.log("departamentos", data);
        setDepartments(_departments);
      }
    } else if (event === "department_code") {
      const { data } = await proxy.search(
        "party.city_code",
        [["department", "=", value.value]],
        ["name", "id", "department"],
      );
      if (data) {
        const _city = funcs.recs2Combobox(data);
        console.log("ciudades", data);
        setCity(_city);
      }
    }
  };

  return (
    <PureModal open={openModal} onClose={onCloseModal}>
      <div className="p-10">
        <h2 className="text-blue-presik font-semibold text-center text-lg">
          Agrega los datos de el cliente
        </h2>
        <form className="grid grid-cols-3 gap-x-3">
          <div className="col-span-2">
            <TextField
              name="name"
              label="app.booking.name"
              value={store.name || ""}
              required={true}
              onChange={onChange}
              translate={true}
            />
          </div>
          <div className="col-span-1">
            <SelectionField
              name="sex"
              label="app.booking.sex"
              value={store.sex}
              onChange={onChange}
              // required={true}
              options={SEX}
              translate={true}
            />
          </div>
          <div className="col-span-3 flex space-x-3">
            <SelectionField
              name="type_document"
              label="app.booking.type_document"
              value={store.type_document}
              onChange={onChange}
              required={true}
              options={TYPE_DOC}
              translate={true}
            />
            <TextField
              name="id_number"
              label="app.booking.id_number"
              value={store.id_number || ""}
              required={true}
              onChange={onChange}
              translate={true}
            />
          </div>
          <div className="col-span-3 flex space-x-3">
            <TextField
              name="mobile"
              label="app.booking.mobile"
              value={store.mobile || ""}
              required={true}
              onChange={onChange}
              translate={true}
            />
            <TextField
              name="email"
              label="app.booking.email"
              value={store.email || ""}
              required={true}
              onChange={onChange}
              translate={true}
            />
          </div>
          <div className="col-span-3 flex space-x-3">
            <TwComboBox
              name="country_code"
              label="app.booking.country"
              placeholder="País"
              onChange={onChangeCountry}
              options={countries}
              value={store.country_code || ""}
            />
            <TwComboBox
              name="department_code"
              label="app.booking.department"
              placeholder="Departamento"
              onChange={onChangeCountry}
              options={departments}
              readonly={true}
            />
            <TwComboBox
              name="city_code"
              placeholder="Ciudad"
              label="app.booking.city"
              onChange={onChangeCountry}
              options={city}

              // onChange={onChange}
            />
          </div>
          <div className="col-span-3">
            <TextField
              name="address"
              label="app.booking.address"
              value={store.address || ""}
              required={true}
              onChange={onChange}
              translate={true}
            />
          </div>
          <div className="py-5 gap-3 col-span-3">
            <StdButton
              key="add"
              color="bluePresik"
              disabled={false}
              loading={isLoading}
              onClick={acceptCreate}
              size="w-[200px]"
              content="sale_activity.add_customer"
              style="mx-auto px-10 "
            />
          </div>
        </form>
      </div>
    </PureModal>
  );
};

export default FormGuest;
