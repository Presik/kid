import React from "react";

import LoadingIcon from "components/LoadingIcon.js";

function Loading() {
  return (
    <div className="block mx-auto h-16 py-24">
      <LoadingIcon />
      <p className="mx-10 my-4 text-gray-500 text-xl text-center">
        Loading . . .
      </p>
    </div>
  );
}

export default Loading;
