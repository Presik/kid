import React from "react";
import { FormattedMessage as FM } from "react-intl";

import ColForm from "components/ColForm";

function CheckboxField(props) {
  let checked = props.value ?? false;

  function handleCheck() {
    if (props.readonly) {
      return;
    }
    let child;
    if (props.recId) {
      child = { id: props.recId };
    }
    props.onChange(props.name, !checked, child);
    checked = !checked;
  }

  return (
    <ColForm {...props}>
      <label key={props.name} className="flex mt-10 pl-1">
        <input
          id={props.name}
          type="checkbox"
          name={props.name}
          onChange={handleCheck}
          readOnly={props.readOnly}
          disabled={props.readOnly}
          checked={checked}
          className="h-5 w-5 rounded-lg accent-sky-600"
        />
        <p className="text-sm ml-4">
          <FM id={props.label} key={props.name} />
        </p>
      </label>
    </ColForm>
  );
}

export default CheckboxField;
