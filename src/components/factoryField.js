import React from "react";

import TextField from "components/TextField";
import NumericField from "components/NumericField";
import DropdownField from "components/DropdownField";
import DateField from "components/DateField";
import SubTable from "components/SubTable";
import SelectionField from "components/SelectionField";
import TextAreaField from "components/TextAreaField";
import CheckboxField from "components/CheckboxField";
import ButtonForm from "components/ButtonForm";
import DatetimeField from "components/DatetimeField";
import DividerField from "components/Form/Divider";
import CloudImage from "components/CloudImage";
import MultiSelectionField from "components/MultiSelectionField";
import ImageField from "components/ImageField";
import ButtonModal from "components/ButtonModal";
import SearchField from "components/SearchField";
import RadioGroupField from "components/RadioGroupField";
import TakeCapture from "components/TakeCapture";
import FileField from "components/FileField";
import IntegerField from "./IntegerField";
import ButtonCustomModal from "./ButtonCustomModal";
import ModalWizard from "components/Modals/ModalWizard";
import functions from "tools/functions";
import FloatField from "./FloatField";

/**
 *
 * @param {Object} field - Define props for render field
 * @param {String} model
 * @param {Object} record
 * @param {String} typeView
 * @param {Function} saveStore
 */

function factoryField({
  field,
  model,
  typeView,
  handleButton,
  updateStore,
  record,
  parentRec,
  ctxView,
  level,
}) {
  async function update(fieldName, value) {
    updateStore(field, fieldName, value);
  }

  let value;
  let label = field.name;
  if (typeof field.function === "function") {
    value = field.function(record, field);
  } else if (record && record[field.name] !== undefined) {
    value = record[field.name];
  } else if (field.default) {
    if (record.id <= 0 || !record.id) {
      if (typeof field.default === "function") {
        // Insert here activeRecord or childRecord
        value = field.default();
      } else {
        value = field.default;
      }
    }
  }
  let error = Boolean(null && Boolean(!value));
  let readOnly = false;
  if (typeof field.readOnly === "boolean") {
    readOnly = field.readOnly;
  } else if (record && typeof field.readOnly === "function") {
    readOnly = field.readOnly(record);
  } else if (record && field.readOnly) {
    for (const [keyField, valuesField] of Object.entries(field.readOnly)) {
      if (valuesField.includes(record[keyField])) {
        readOnly = true;
      }
    }
  }
  let { name } = field;
  const key = field.name;
  if (typeView !== "tree") {
    label = `${model}.${label}`;
  }
  field["onChange"] = update;
  field["parentRec"] = parentRec;
  field = { ...field, error, value, readOnly, record, typeView, key, label };

  const fieldExcludes = ["button", "button-wizard"];
  if (!fieldExcludes.includes(field.type) && record && field.visible) {
    // Field visible always must be a function
    const isVisible = field.visible(record);
    if (!isVisible) {
      return null;
    }
  }
  const errors = {};
  let _records = [];
  const raw_name = `${name}.`;
  if (record[name]) {
    _records = record[name];
  } else if (record[raw_name]) {
    _records = record[raw_name];
  }

  if (["char", "email", "money"].includes(field.type)) {
    return <TextField key={name} {...field} value={value} model={model} />;
  } else {
    switch (field.type) {
      case "date":
        return (
          <DateField
            key={name}
            widget
            min={new Date().toISOString().split("T")[0]}
            error={errors.date?.message}
            {...field}
          />
        );
      case "icon": {
        const Icon = field.icon(record);
        return Icon;
      }
      case "integer":
        return <NumericField key={name} {...field} />;
      case "float":
        return <FloatField key={name} {...field} />;
      case "number":
        if (field.widget === "increment") {
          return <IntegerField key={name} {...field} />;
        } else {
          return <NumericField key={name} {...field} />;
        }
      case "datetime":
        return <DatetimeField key={name} {...field} />;
      case "text-area":
        return <TextAreaField key={name} {...field} />;
      case "input_file":
        return <FileField key={name} {...field} />;
      case "boolean":
        return <CheckboxField key={name} {...field} />;
      case "divider":
        return <DividerField key={name} {...field} />;
      case "button-custom-modal":
        return <ButtonCustomModal key={name} {...field} />;
      case "one2many":
        let _level = null;
        if (level === "main") {
          _level = "child";
        } else if (level === "child") {
          _level = "subchild";
        }
        if (field.widget === "button_modal") {
          return (
            <ButtonModal
              key={name}
              name={name}
              icon={field.icon}
              label={field.label}
              model={field.model}
              ctxView={field.ctxView}
              color={field.color}
              records={_records}
              parentRec={{
                target: field.ctxView.target,
                record: record,
                model: model,
                ctxView: ctxView,
              }}
              level={_level}
            />
          );
        } else {
          if (record[raw_name]) {
            value = record[raw_name];
            field["value"] = value;
          }
          if (value && Array.isArray(value)) {
            value = functions.recsToMap(value);
          }
          return (
            <SubTable
              id="subtable"
              key={name}
              ctxView={field.ctxView}
              records={record[name] || new Map()}
              model={field.model}
              updateStore={update}
              fieldName={name}
              parentRec={{
                target: field.ctxView.target,
                record: record,
                model: model,
                ctxView: ctxView,
              }}
              readOnly={readOnly}
              level={_level}
            />
          );
        }
      case "many2one": {
        let obj_name = name;
        if (typeof record[name] === "number") {
          obj_name = `${name}.`;
        }
        if (record[obj_name]) {
          value = record[obj_name];
        }
        field["value"] = value;

        if (
          field.widget === "input" ||
          field.widget === "search" ||
          field.widget === "search-add" ||
          field.widget === "search-add-new"
        ) {
          let _value = "";
          if (field.value && field.value !== "") {
            if (field.value.id) {
              _value = field.value.name || field.value.rec_name;
            } else {
              _value = field.value;
            }
          }
          return (
            <SearchField
              key={name}
              {...field}
              value={_value}
              ctxView={field.ctxView}
              parentRec={{
                target: field.ctxView.target,
                record: record,
                model: model,
                ctxView: ctxView,
              }}
            />
          );
        } else {
          let _domain = field.domain;
          if (_domain && field.attrs) {
            const attrs = field.attrs[0];
            let target = record && record[attrs];
            if (attrs && target && target.id) {
              _domain = _domain.replace("1", target.id);
            } else {
              _domain = false;
            }
          }
          return (
            <DropdownField
              key={name}
              {...field}
              record={record}
              searchDom={_domain}
            />
          );
        }
      }
      case "selection":
        return <SelectionField key={name} {...field} />;
      case "multi-selection":
        return <MultiSelectionField key={name} {...field} />;
      case "button":
        field.value = name;
        field.handleButton = handleButton;
        return <ButtonForm key="button-form" {...field} />;
      case "button-wizard":
        field.value = name;
        field.storeRec = record; // activeRecord;
        return (
          <ModalWizard
            key="button-wizard"
            {...field}
            model={model}
            parentRec={{ name: name, record: record, ctxView: ctxView }}
            ctxView={field.ctxView}
            resetWizardModal={field.resetWizard == false ? false : true}
          />
        );
      case "custom":
        return field.function(record); // CustomComponent
      case "custom-fragment": {
        const Component = field.component;
        return <Component record={record} />;
      }
      case "capture":
        return <TakeCapture key={name} {...field} />;
      case "zone-image":
        return <CloudImage key={name} {...field} />;
      case "image":
        return <ImageField key={name} {...field} />;
      case "radio-group":
        return <RadioGroupField key={name} {...field} />;
      default:
        console.log("Warning unknown type field!", name, field.type);
    }
  }
}

export default factoryField;
