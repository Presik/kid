import React, { useState, useEffect } from "react";
import { FormattedMessage as FM } from "react-intl";
import store from "store";

import StdButton from "components/StdButton";
import PureModal from "components/Modals/PureModal";
import Form from "components/Form";
import proxy from "api/proxy";
import funcs from "tools/functions";
import { useWizardStore } from "store/wizardStore";

function ModalReport({ ctxView, parentRec, onClose, open }) {
  const session = store.get("ctxSession");
  const { webform, reportName } = ctxView;
  const { storeWizard } = useWizardStore();

  const onPrintReport = async () => {
    let storeStd = funcs.recToTryton(storeWizard);
    storeStd.company = session.company;
    const { reportUrl, nameFile } = await proxy.report({
      report: reportName,
      data: storeStd,
      records: [],
    });
    funcs.createReportLink(reportUrl, nameFile);
    onClose();
  };

  const buttonPrint = (
    <StdButton
      key="print-report"
      color="blue"
      onClick={() => onPrintReport()}
      content="button.print"
      style="ml-auto"
    />
  );

  return (
    <PureModal
      open={open}
      width="w-1/2"
      btnCloseActive={true}
      onClose={onClose}
      title={
        <h1 className="font-bold text-3xl text-stone-700">
          <FM id={reportName} />
        </h1>
      }
    >
      <div className="p-5">
        <Form
          ctxView={ctxView}
          webform={webform}
          parentRec={parentRec}
          level="wizard"
        />
        <div className="flex mt-5">{buttonPrint}</div>
      </div>
    </PureModal>
  );
}

export default ModalReport;
