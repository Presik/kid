import React, { Fragment, useState } from "react";
import { FormattedMessage as FM } from "react-intl";

import BasicModal from "components/BasicModal";
import ModalMsg from "components/Modals/ModalMsg";
import { BUTTON_COLORS } from "components/Constants/constants";
import { classNames } from "tools/ui";
import { useFormStore } from "store/formStore";
import { useWizardStore } from "store/wizardStore";
import { useBtnLoading } from "store/btnLoading";
import funcs from "tools/functions";

const style =
  "flex my-4 mx-auto w-full h-12 text-center border-[1px] justify-center rounded-xl active:scale-95 active:shadow-sm";

function ModalWizard({
  Component,
  label,
  icon,
  color,
  record,
  onAccept,
  model,
  ctxView,
  parentRec,
  visible,
  name,
  defaults,
  resetWizardModal,
}) {
  const [open, setOpen] = useState(false);
  const [msg, setMsg] = useState(null);
  const { activeRecord, setActiveRecordFromId } = useFormStore();
  const { storeWizard, resetWizard, upStoreWizard, upActiveWizard } =
    useWizardStore();
  const { setButtonLoading } = useBtnLoading();

  const _color = color ?? "blue";
  const [bgColor, txtColor, border] = BUTTON_COLORS[_color];

  function onCloseMsg() {
    setMsg(null);
  }

  function onClose(update, error) {
    if (error) {
      setMsg(error);
      resetWizard();
      return;
    }
    if (update && activeRecord.id > 0) {
      setActiveRecordFromId(activeRecord.id, model, parentRec.ctxView);
    }
    setOpen(false);
    setButtonLoading(false);
    if (resetWizardModal) {
      resetWizard();
    }
  }

  let _visible = funcs.isVisible(visible, record, name);
  if (!_visible) return null;

  function openModal(reset = true) {
    if (resetWizardModal) {
      resetWizard();
    }
    if (ctxView?.defaults) {
      // This method set default values for the wizard
      ctxView.defaults();
    }
    setOpen(true);
  }

  return (
    <Fragment>
      <button
        onClick={openModal}
        className={classNames(style, bgColor, txtColor, border)}
      >
        <p className={classNames("w-42 ml-auto my-auto")}>
          <FM id={label} />
        </p>
        {icon && (
          <i
            className={classNames(
              "mr-auto ml-6 text-xl inline-flex my-auto",
              icon,
              txtColor,
            )}
          />
        )}
      </button>
      <BasicModal
        title={label}
        open={open}
        onClose={onClose}
        buttons={[]}
        titleSize="text-2xl"
        width="w-full sm:w-4/5 md:w-3/4 lg:w-2/3"
      >
        <Component
          parentRec={parentRec}
          model={model}
          record={record}
          onClose={onClose}
          ctxView={ctxView}
        />
      </BasicModal>
      {msg && (
        <ModalMsg
          open={true}
          type="error"
          msg={msg}
          buttons={["close"]}
          onClose={onCloseMsg}
        />
      )}
    </Fragment>
  );
}

export default ModalWizard;
