import React from "react";

import { classNames } from "tools/ui";

const base =
  "group shadow-sm rounded-md px-2 cursor-pointer py-3 flex flex-col justify-center items-center -space-y-2 text-white w-full";

const title = "text-3xl font-semibold text-center";
const subtitle = "text-xs uppercase text-center";

const COLORS = {
  lime: {
    color: "text-lime-700",
    bgColor: "bg-lime-200",
  },
  amber: {
    color: "text-amber-800",
    bgColor: "bg-amber-200",
  },
  rose: {
    color: "text-rose-700",
    bgColor: "bg-rose-200",
  },
};

const WidgetsCardInfo = ({ records, day }) => {
  function getCardInfo(rec) {
    const color = rec.color;
    return (
      <div className={classNames(base, rec.bgColor)}>
        <div className="flex flex-col ">
          <span className={classNames(title, color)}>{rec.num}</span>
          <span className={classNames(subtitle, rec.color)}>{rec.name}</span>
        </div>
      </div>
    );
  }

  return (
    <div className="bg-white border-gray-100 shadow-sm p-3 rounded-lg overflow-hidden">
      <div className="grid grid-cols-3 justify-between gap-3">
        {records.map((rec) => getCardInfo(rec))}
      </div>
    </div>
  );
};

export default WidgetsCardInfo;
