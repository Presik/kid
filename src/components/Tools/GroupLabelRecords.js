import React from "react";

function GroupLabels({ records, onClick }) {
  return (
    <div>
      {records.map((rec, idx) => {
        return (
          <span key={idx} color="grey" onClick={() => onClick(rec)}>
            {rec.name}
            {rec.weight}
          </span>
        );
      })}
    </div>
  );
}

export default GroupLabels;
