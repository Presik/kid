import React from "react";
import { FormattedMessage as FM } from "react-intl";

function Loading() {
  return (
    <div>
      <FM id="board.loading" />
    </div>
  );
}

export default Loading;
