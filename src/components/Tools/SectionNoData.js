import React from "react";
import { NoSymbolIcon, CalendarDaysIcon } from "@heroicons/react/20/solid";

const SectionNoData = ({ text = null, icon = null }) => {
  return (
    <div className="h-32 w-full bg-blue-presik shadow-md rounded-md my-5 p-2">
      <div className=" border border-dashed flex items-center justify-center just  w-full h-full">
        <CalendarDaysIcon className="w-16 text-yellow-300" />
        <h2 className="w-[500px]  text-white text-center text-3xl flex flex-col">
          {text}
        </h2>
      </div>
    </div>
  );
};

export default SectionNoData;
