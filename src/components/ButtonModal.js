import React, { useState } from "react";
import { FormattedMessage as FM } from "react-intl";

import BasicModal from "components/BasicModal";
import SubTable from "components/SubTable";
import { BUTTON_COLORS_DARK } from "components/Constants/constants";
import { classNames } from "tools/ui";
import funcs from "tools/functions";
import proxy from "api/proxy";

const base =
  "flex w-28 h-18 mb-auto mx-auto p-3 rounded-xl shadow-md active:scale-95 active:shadow-sm";

function ButtonModal(props) {
  const [open, setOpen] = useState(false);
  const [records, setRecords] = useState([]);

  let [bgColor, txtColor] = BUTTON_COLORS_DARK.blue;
  if (props.color) {
    [bgColor, txtColor] = BUTTON_COLORS_DARK[props.color];
  }

  function onClose() {
    setOpen(false);
  }

  function updateStore() {
    console.log("Update store vacio....!");
  }

  async function onClick() {
    if (props.records && props.records.length > 0) {
      const recordsIds = props.records.map((rec) => rec.id);
      const fields_names = funcs.getViewFields(props.ctxView, "list");
      const res = await proxy.browse(props.model, recordsIds, fields_names);
      setRecords(res);
    }
    setOpen(true);
  }

  return (
    <div onClick={onClick} className={classNames(base, bgColor, txtColor)}>
      <div className="mx-auto text-center">
        {props.icon && (
          <i
            key={props.icon}
            className={classNames("h-4 w-4 text-xl my-auto", props.icon)}
          />
        )}
        <span className="flex my-auto text-xs font-medium ml-auto">
          <FM id={props.label} />
        </span>
      </div>
      <BasicModal
        title={props.label}
        open={open}
        onClose={onClose}
        titleSize="text-2xl"
        width="w-1/2"
      >
        <SubTable
          id="subtable"
          key={props.name}
          ctxView={props.ctxView}
          records={props.records || new Map()}
          model={props.model}
          updateStore={updateStore}
          fieldName={props.name}
          parentRec={props.parentRec}
          level={props.level}
        />
      </BasicModal>
    </div>
  );
}

export default ButtonModal;
