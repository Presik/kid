import React from "react";

const Screen = (props) => {
  return <div className="w-full">{props.children}</div>;
};

export default Screen;
