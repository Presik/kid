import React, { useState } from "react";
import { FormattedMessage as FM } from "react-intl";

function ButtonComponent(props) {
  const [open, setOpen] = useState(false);

  function onOpen() {
    setOpen(!open);
  }

  return (
    <div
      onClick={() => onOpen()}
      className="grid w-36 h-16 bg-gradient-to-tr from-sky-500 to-blue-500 my-auto text-center rounded-xl shadow-xl active:scale-95 active:shadow-sm"
    >
      <span className="text-white my-auto font-bold">
        <FM id={props.label} />
      </span>
      {props.component(props, open, onOpen)}
    </div>
  );
}

export default ButtonComponent;
