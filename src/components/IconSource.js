import React from "react";

const IconSource = ({ data }) => {
  let image = data;

  return <img src={image} className="w-6 h-6 rounded-full cursor-pointer" />;
};

export default IconSource;
