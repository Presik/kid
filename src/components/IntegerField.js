import React, { useState, useEffect } from "react";
import { PlusCircleIcon, MinusCircleIcon } from "@heroicons/react/20/solid";

import FormField from "./FormField";
import { classNames, colors } from "tools/ui";

const base =
  "rounded-none w-full border-t text-center border-b border-slate-300 py-2 px-3 text-gray-700 focus:outline-none focus:border focus:border-cyan-500 ";

function IntegerField(props) {
  const [value, setValue] = useState("");

  function onChange(event) {
    let _value = event.target.value;
    _value = _value.toString().replace(/\./g, "");
    if (isNaN(_value)) {
      return;
    }
    _value = Number(_value);
    setValue(_value);
    props.onChange(props.name, _value, props.recordid);
  }

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  let _color = "bg-white";
  if (props.color === "gray") {
    _color = "bg-gray-100";
  }
  if (props.readOnly) {
    _color = colors.readOnly;
  }

  let _value = "";
  if (value) {
    _value = value.toLocaleString("es", { useGrouping: true });
  }

  const increment = (e) => {
    e.preventDefault();
    if (props.readOnly) return;
    setValue((prevValue) => {
      const newValue = prevValue + 1;
      onChange({ target: { value: newValue } });
      return newValue;
    });
  };

  const decrement = (e) => {
    e.preventDefault();
    if (props.readOnly) return;
    if (value > 0) {
      setValue((prevValue) => {
        const newValue = prevValue - 1;
        onChange({ target: { value: newValue } });
        return newValue;
      });
    }
  };

  const iconStyle = "w-7 h-7 text-stone-700";

  const stBtn =
    "border-slate-300 border-[1px] px-3 py-1 bg-zinc-100 active:bg-zinc-200 focus:outline-none";
  const leftBtn = "rounded-l-md";
  const rightBtn = "rounded-r-md";

  const btnMinus = (
    <button className={classNames(stBtn, leftBtn)} onClick={decrement}>
      <MinusCircleIcon className={iconStyle} />
    </button>
  );

  const btnPlus = (
    <button className={classNames(stBtn, rightBtn)} onClick={increment}>
      <PlusCircleIcon className={iconStyle} />
    </button>
  );
  return (
    <FormField {...props}>
      <div className="flex items-stretch w-full h-10">
        {btnMinus}
        <input
          id={props.name}
          name={props.name}
          type="text"
          value={_value}
          className={classNames(base, _color)}
          onChange={(val) => onChange(val)}
          readOnly={props.readOnly}
        />
        {btnPlus}
      </div>
    </FormField>
  );
}

export default IntegerField;
