import React from "react";

import QuickForm from "components/QuickForm";
import ModalForm from "components/Modals/ModalForm";
import { useFormStore } from "store/formStore";
import { useWizardStore } from "store/wizardStore";

/**
 *
 * @param {object} props - component props
 * @param {object} props.ctxView - object for render record
 * @param {Function} props.onClose - function for close modal
 * @param {boolean} props.onSave - function for save action
 * @param {boolean} props.open - true or false
 * @return {Component} - Modal Form for show form record
 */
function ModalStdForm(props) {
  const { ctxView, onClose, open, recordForm, level, onSave } = props;
  const { activeRecord } = useFormStore();
  const { activeWizard } = useWizardStore();

  let _recordForm = recordForm;
  if (level === "wizard") {
    _recordForm = activeWizard;
  } else if (level === "main") {
    _recordForm = activeRecord;
  }

  return (
    <ModalForm
      recordForm={_recordForm}
      ctxView={ctxView}
      onClose={onClose}
      // onSave={onSave}
      open={open}
    >
      <QuickForm
        ctxView={ctxView}
        modal={true}
        onClose={onClose}
        recordForm={_recordForm}
        level="main"
      />
    </ModalForm>
  );
}

export default ModalStdForm;
