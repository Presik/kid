import React from "react";

function TwDivField({ children }) {
  return <div className="mb-6">{children}</div>;
}

export default TwDivField;
