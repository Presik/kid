import React, { Fragment } from "react";

import Paragraph from "components/Paragraph";
import GroupFields from "components/GroupFields";
import GridFields from "components/GridFields";
import factoryField from "components/factoryField";
import { useFormStore } from "store/formStore";
import { useFormChildStore } from "store/formChildStore";
import { useFormSubChildStore } from "store/formSubChildStore";
import { useWizardStore } from "store/wizardStore";

function Form(props) {
  const { ctxView, webform, parentRec, level } = props;
  const { webfields, model } = ctxView;

  const { activeRecord, storeRecord, upActiveRecord, upStoreRecord } =
    useFormStore();
  const { storeChild, activeChild, upActiveChild, upStoreChild } =
    useFormChildStore();
  const { storeSubChild, activeSubChild, upActiveSubChild, upStoreSubChild } =
    useFormSubChildStore();
  const { activeWizard, storeWizard, upActiveWizard, upStoreWizard } =
    useWizardStore();

  let upActive;
  let upStore;
  if (level === "child") {
    upActive = upActiveChild;
    upStore = upStoreChild;
  } else if (level === "subchild") {
    upActive = upActiveSubChild;
    upStore = upStoreSubChild;
  } else if (["wizard", "many2one"].includes(level)) {
    upActive = upActiveWizard;
    upStore = upStoreWizard;
  }

  let record = {};
  let _parentRec = parentRec;
  if (["wizard", "many2one"].includes(level)) {
    record = activeWizard;
    _parentRec = { record: activeRecord };
  } else if (level === "main" || !level) {
    record = activeRecord;
  } else if (level === "child") {
    _parentRec = { record: activeRecord };
    record = activeChild;
  } else if (level === "subchild") {
    if (activeSubChild) {
      record = activeSubChild;
    }
  }

  function handleButton(btnName) {
    props.handleButton(btnName);
  }

  async function updateStore(field, fieldName, value) {
    if (["wizard", "many2one"].includes(level)) {
      console.log("storeWizard....", storeWizard);
      activeWizard[fieldName] = value;
      storeWizard[fieldName] = value;
      if (field.withChange) {
        const [withStore, withActive] = await field.withChange(_activeWizard);
        storeWizard = { ..._storeWizard, ...withStore };
        activeWizard = { ..._activeWizard, ...withActive };
      }
      upStoreWizard(storeWizard);
      upActiveWizard(activeWizard);
      return;
    }
    if (level === "main") {
      let _storeRecord = { ...storeRecord };
      let _activeRecord = { ...activeRecord };
      _activeRecord[fieldName] = value;
      _storeRecord[fieldName] = value;
      if (field.withChange) {
        const [withStore, withActive] = await field.withChange(_activeRecord);
        _storeRecord = { ..._storeRecord, ...withStore };
        _activeRecord = { ..._activeRecord, ...withActive };
      }
      upStoreRecord(_storeRecord);
      upActiveRecord(_activeRecord);
    } else {
      let _storeChild;
      let _activeChild;
      if (level === "child") {
        _storeChild = { ...storeChild };
        _activeChild = { ...activeChild };
      } else {
        // subchild
        _storeChild = { ...storeSubChild };
        _activeChild = { ...activeSubChild };
      }
      _activeChild[fieldName] = value;
      if (!_storeChild.id) {
        _storeChild.id = _activeChild.id;
      }
      if (value instanceof Object && value.id) {
        _storeChild[fieldName] = value.id;
      } else {
        _storeChild[fieldName] = value;
      }
      if (field.withChange) {
        _activeChild[fieldName] = value;
        const [withStore, withActive] = await field.withChange(
          _activeChild,
          _parentRec,
        );
        _storeChild = { ..._storeChild, ...withStore };
        _activeChild = { ..._activeChild, ...withActive };
      }
      upActive(_activeChild);
      upStore(_storeChild);
    }
    console.log("storeSubChild...", storeSubChild);
  }

  let formActive = [];

  let _webform = webform;
  if (!webform) {
    _webform = ctxView.webform;
  }
  for (const field of _webform) {
    let component;
    let valueFm = `model.${model}.${field.name}`;
    if (field.widget === "text") {
      component = <Paragraph key={field.name} text={valueFm} />;
    } else if (field.widget === "img") {
      component = (
        <img
          key="form-img"
          alt="img-form"
          src={field.img_link}
          style={{ margin: "auto" }}
        />
      );
    } else if (field.widget === "group") {
      const groupForm = Form({
        ctxView: ctxView,
        webform: field.children,
        handleButton: handleButton,
        record: record,
        parentRec: parentRec,
        level: level,
      });
      component = <GroupFields key={field.name}>{groupForm}</GroupFields>;
    } else if (field.grid) {
      const groupForm = Form({
        ctxView: ctxView,
        webform: field.grid,
        handleButton: handleButton,
        record: record,
        parentRec: _parentRec,
        level: level,
      });
      component = (
        <GridFields id="grid-fields" key={field.id} {...field}>
          {groupForm}
        </GridFields>
      );
    } else {
      const fieldProps = {
        ...webfields[field.name],
        ...field,
        parent: _parentRec,
      };
      component = factoryField({
        field: fieldProps,
        model: model,
        handleButton: handleButton,
        updateStore: updateStore,
        record: record,
        parentRec: _parentRec,
        ctxView: ctxView,
        level: level,
      });
    }
    formActive.push(component);
  }
  return <Fragment>{formActive}</Fragment>;
}

export default Form;
