import React from "react";
import { FormattedMessage as FM } from "react-intl";

import ButtomModalBodyTable from "components/ButtomModalBodyTable";
import ButtonTable from "./ButtonTable";
import ButtonLink from "components/ButtonLink";
import Circle from "components/Circle";
import Badge from "components/Badge";
import IconSource from "./IconSource";
import TextCell from "./TableFields/TextCell";
import IntegerCell from "./TableFields/IntegerCell";
import TableCell from "./TableCell";
import TextAreaCell from "./TableFields/TextAreaCell";
import SelectionCell from "./TableFields/SelectionCell";
import BooleanCell from "./TableFields/BooleanCell";
import NumericCell from "./TableFields/NumericCell";
import DropdownCell from "./TableFields/DropdownCell";
import ButtonCustomModal from "./TableFields/ButtonCustomModal";
import FloatCell from "./TableFields/FloatCell";
import CallAction from "./CallAction";
import dates from "tools/dates";

function getChar(value, field, model, key, rec) {
  if (field.editable) {
    return [<TextCell key={key} value={value} {...field} />];
  }
  let color;
  let _value = value;
  let align = "text-left";
  if (field.widget) {
    align = "text-center";
    if (field.widget === "circle") {
      color = "white";
      if (field.tags) {
        color = field.tags[value];
      } else if (field.color) {
        color = field.color;
      }
      _value = <Circle color={color} />;
    } else if (field.widget === "badge") {
      color = "white";
      if (field.tags) {
        color = field.tags[value];
      } else if (field.color) {
        color = field.color;
      }
      _value = <Badge color={color} value={value} model={model} />;
    } else if (field.widget === "button-custom-modal") {
      _value = (
        <ButtonCustomModal
          key={key}
          {...field}
          label={value}
          record={rec}
          model={model}
        />
      );
    } else if (field.widget === "menuItem") {
      _value = <ButtomModalBodyTable data={value} />;
    } else if (field.widget === "call-action") {
      _value = <CallAction key={key} value={value} />;
    } else if (field.widget.type === "sourceImg") {
      if (field.widget.data) {
        _value = <IconSource data={field.widget.data[value ? value : 0]} />;
      } else {
        _value = value;
      }
    }
  } else if (field.translate && value) {
    // className="bg-lime-200 whitespace-nowrap px-2 py-2 text-sm text-gray-900"
    _value = (
      <FM id={`${model}.${value}`} key={key}>
        {(msg) => <p key={key}>{msg}</p>}
      </FM>
    );
  }
  return [_value, align, color];
}

function getTextArea(value, field, key) {
  return <TextAreaCell key={key} field={field} value={value} />;
}

function getSelection(value, field, ctxView) {
  return <SelectionCell key={field.name} field={field} value={value} />;
}

function getDate(value) {
  if (!value) return;
  return dates.fmtDate(value, true);
}

function getDatetime(value, field) {
  const _value = dates.getTrytonDateTime2Js(value, true, field.formatString);
  // _value = dates.fmtDatetimeTable(_value, field.formatHour);
  return _value;
}

function getFloat(value, field, onChange, key) {
  return <FloatCell key={key} value={value} {...field} onChange={onChange} />;
}

function getNumber(value, field, onChange, key) {
  let align;
  let color;
  let _value;
  if (field.editable) {
    _value = (
      <NumericCell key={key} value={value} {...field} onChange={onChange} />
    );
  } else {
    align = "text-right";
    if (field.format === "percent") {
      value = value * 100;
    }
    const isNumber = (value) =>
      Number.isNaN(value) || typeof value === "undefined";
    _value = isNumber(value)
      ? ""
      : Number(value).toLocaleString("es", { useGrouping: true });
    if (typeof value === "number" && value < 0) {
      color = "text-rose-700";
    }
    if (field.format === "percent") {
      _value = `${_value} %`;
    }
  }

  return [_value, align, color];
}

function getMany2One(value, field, record, onChange, key) {
  const align = "text-left";
  let _value = "";
  let target;
  const raw_name = `${field.name}.`;
  if (record[raw_name]) {
    target = record[raw_name];
  } else {
    target = record[field.name];
  }
  if (field.editable) {
    _value = (
      <DropdownCell
        key={key}
        value={target}
        {...field}
        onChange={onChange}
        record={record}
      />
    );
  } else if (field.getIcon) {
    _value = field.getIcon(target);
  } else {
    if (target) {
      if (target.rec_name) {
        _value = target.rec_name;
      } else {
        _value = target.name;
      }
    }
  }

  return [_value, align];
}

function getButtonLink(value, field, key) {
  return (
    <ButtonLink
      key={key}
      value={value}
      icon="chat"
      data_source={field.data_source}
    />
  );
}

function getInteger(value, field, onChange, key) {
  if (field.editable) {
    return [
      <IntegerCell key={key} value={value} {...field} onChange={onChange} />,
    ];
  }
  return value;
}

function getButtonFile() {
  return <i className="w-6 h-6 bg-white text-rose-600 fi-rr-file" />;
}

function getBoolean(value, field, onChange, key) {
  if (field.editable) {
    return [
      <BooleanCell key={key} value={value} {...field} onChange={onChange} />,
    ];
  } else {
    return (
      // fix this @vic
      <input
        type="checkbox"
        className="h-5 w-5 rounded-full"
        checked={value ? true : false}
        disabled
      />
    );
  }
}

function getButton(value, field, name, model, rec, triggerAction, ctxView) {
  let label = `${model}.${name}`;
  let _value = "";

  if (rec && field.visible && !field.visible(name, rec)) {
    return null;
  }

  const buttonProps = {
    value: value,
    label: label,
    method: field.method,
    wizard: field.wizard,
    iconLeft: field.iconLeft,
    iconRight: field.iconRight,
    button_method: field.button_method,
    record: rec,
    triggerAction: triggerAction,
    color: field.color,
  };
  _value = <ButtonTable {...buttonProps} key={name} />;
  return _value;
}

function factoryCell({
  rec,
  field,
  key,
  model,
  triggerAction,
  onClickCell,
  onChange,
  ctxView,
}) {
  const { name, type } = field;
  let align = "text-center";
  let color;
  let value;
  let _value = rec[name];
  if (name.indexOf(".") > -1) {
    const arrField = name.split(".");
    let value_ = rec;
    for (let item of arrField) {
      if (!value_) {
        break;
      }
      value_ = value_[item] ?? value_[item + "."];
    }
    _value = value_;
  }
  if (field.function) {
    _value = field.function(rec);
  }
  switch (type) {
    case "char":
      [value, align, color] = getChar(_value, field, model, key, rec);
      break;
    case "text-area":
      value = getTextArea(_value, field, key);
      break;
    case "number":
      [value, align, color] = getNumber(_value, field, onChange, key);
      break;
    case "float":
      value = getFloat(_value, field, onChange, key);
      break;
    case "integer":
      value = getInteger(_value, field, onChange, name);
      break;
    case "selection":
      value = getSelection(_value, field, ctxView);
      break;
    case "date":
      value = getDate(_value, field);
      break;
    case "datetime":
      value = getDatetime(_value, field);
      break;
    case "many2one":
      [value, align] = getMany2One(_value, field, rec, onChange, key);
      break;
    case "input_file":
      value = getButtonFile();
      break;
    case "boolean":
      value = getBoolean(_value, field, onChange, name);
      break;
    case "button":
      value = getButton(
        _value,
        field,
        name,
        model,
        rec,
        triggerAction,
        ctxView,
      );
      break;
    case "link":
      value = getButtonLink(_value, field, key);
      break;
  }

  return (
    <TableCell
      key={name}
      selectable={field.selectable}
      record={rec}
      field={field.name}
      align={align}
      onClick={onClickCell}
      color={color}
    >
      {value}
    </TableCell>
  );
}

export default factoryCell;
