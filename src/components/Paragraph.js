import React from "react";
import { FormattedMessage as FM } from "react-intl";

function Paragraph(props) {
  return (
    <p key={props.text} className="mt-2 text-xl">
      <FM id={props.text} />
    </p>
  );
}

export default Paragraph;
