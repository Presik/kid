import React, { useState, useEffect } from "react";

import IconTable from "components/IconTable";
import factoryCell from "components/factoryCell";
import { triggerMethod } from "tools/helpers";

function BodyTable(props) {
  const {
    parentRecord,
    records,
    ctxView,
    selectAll,
    onChangeView,
    selectable,
    onClickCell,
    updateRecords,
    reloadRecords,
  } = props;
  const { webtree, webfields, model, table_action } = ctxView;
  const [selectedRows, setSelectedRows] = useState(new Map());

  const cellButtonStart = [];
  const cellButtonEnd = [];

  if (table_action) {
    for (const buttonType of table_action) {
      if (buttonType === "add") {
        continue;
      }
      if (buttonType === "edit" || buttonType === "open") {
        cellButtonStart.push(buttonType);
      } else {
        cellButtonEnd.push(buttonType);
      }
    }
  }

  const treeView = webtree.map((val) => {
    return { ...webfields[val.name], ...val };
  });

  function updateSelectedRecords() {
    let selectedRows_ = new Map();
    let rows = null;
    if (selectAll) {
      if (Array.isArray(records)) {
        records.map((item) => {
          selectedRows_.set(item.id, item);
        });
      } else {
        selectedRows_ = new Map(records);
      }
      rows = selectedRows_;
    }
    if (props.onClickRow) {
      props.onClickRow(rows);
    }
    setSelectedRows(selectedRows_);
  }

  useEffect(() => {
    //  What is the reason for to call this function??
    /**
     * This function is necessary to be able to select all the records,
     *  according to the order given from the parent component.
     * */
    updateSelectedRecords();
  }, [selectAll]);

  function actionButton(rec, value) {
    return (
      <td key={value} className="text-center w-11">
        <IconTable name={value} record={rec} onChangeView={onChangeView} />
      </td>
    );
  }

  async function triggerAction(record, method) {
    if (typeof method !== "string") {
      await method(record);
      return;
    }
    await triggerMethod(model, method, record, false);
    reloadRecords();
  }

  function onDoubleClickRow(rec) {
    if (props.onDoubleClick) {
      props.onDoubleClickRow(rec);
    }
  }

  function onClickRow(rec) {
    let selectedRows_ = new Map(selectedRows);
    if (selectable === "multi") {
      if (selectedRows.get(rec.id)) {
        selectedRows_.delete(rec.id);
      } else {
        selectedRows_.set(rec.id, rec);
      }
    } else {
      selectedRows_ = new Map([[rec.id, rec]]);
    }
    if (props.onClickRow) {
      props.onClickRow(rec);
    }
    setSelectedRows(selectedRows_);
  }

  function TableRow(idx, rec, treeView, parentRecord) {
    let _style = "odd:bg-white even:bg-zinc-100";
    if (selectable && selectedRows.get(rec.id)) {
      _style = "bg-lime-200";
    }

    function onChange(field, value) {
      if (updateRecords) {
        updateRecords(field, value, rec.id);
      }
    }

    return (
      <tr
        key={idx}
        disabled={props.readOnly}
        onClick={() => onClickRow(rec)}
        onDoubleClick={() => onDoubleClickRow(rec)}
        className={_style}
      >
        {cellButtonStart.map((value, index) => actionButton(rec, value, index))}
        {treeView.map((field, key) => {
          return factoryCell({
            rec,
            field,
            key,
            model,
            parentRecord,
            triggerAction,
            onClickCell,
            onChange,
          });
        })}
        {cellButtonEnd.map((value, index) => actionButton(rec, value, index))}
      </tr>
    );
  }

  return (
    <tbody>
      {records && !records.size !== 0
        ? Array.isArray(records)
          ? records.map(function (item, i) {
              return TableRow(i, item, treeView, parentRecord);
            })
          : Array.from(records.values()).map(function (item, i) {
              return TableRow(i, item, treeView, parentRecord);
            })
        : null}
    </tbody>
  );
}

export default BodyTable;
