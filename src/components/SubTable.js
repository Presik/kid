import React, { useState } from "react";

import Table from "components/Table";
import TableHeader from "components/TableHeader";
import BodyTable from "components/BodyTable";
import SubtableHeader from "components/SubtableHeader";
import ModalForm from "components/Modals/ModalForm";
import { useFormStore } from "store/formStore";
import { useFormChildStore } from "store/formChildStore";
import { useFormSubChildStore } from "store/formSubChildStore";
import funcs from "tools/functions";

function SubTable(props) {
  const { model, ctxView, parentRec, records, level, readOnly } = props;
  const { order, order_by } = ctxView;
  const [openModalForm, setOpenModalForm] = useState(false);
  const [openMode, setOpenMode] = useState("edit");

  // Child record opened in modal
  const { storeRecord, activeRecord, upFieldActive, upFieldStore } =
    useFormStore();
  const {
    newChild,
    activeChild,
    upActiveChild,
    upStoreChild,
    setActiveChildFromId,
  } = useFormChildStore();
  const { newSubChild, saveSubChild, setActiveSubChildFromId } =
    useFormSubChildStore();

  let _records;
  if (records) {
    _records = records;
  } else if (parentRec) {
    _records = parentRec[props.fieldName];
  }

  let orderDirection;
  if (order) {
    orderDirection = order === "asc" ? "ascending" : "descending";
  }

  function updateRecords(field, value, recordId) {
    const records = activeRecord[props.fieldName];
    let record = records.get(recordId);
    const storeTable = { ...storeRecord }[props.fieldName];
    let storeChild = storeTable?.get("write")?.get(recordId);
    if (!storeChild) {
      storeChild = { id: recordId };
    }
    storeChild[field] = value;
    record[field] = value;
    editRecord("write", record, storeChild);
  }

  async function handleAddModal() {
    if (level === "child") {
      setOpenMode("add");
      newChild(ctxView, parentRec);
    } else {
      newSubChild(ctxView, parentRec);
    }
    setOpenModalForm(true);
  }

  function setActiveChild(record) {
    upActiveChild(record);

    // FIXME debe buscar el store record en el main y fijarlo
    let storeTable = { ...storeRecord }[props.fieldName];

    if (record) {
      let writeMap;
      let createMap;
      if (!storeTable) {
        storeTable = new Map();
        writeMap = new Map();
        createMap = new Map();
        storeTable.set("write", writeMap);
        storeTable.set("create", createMap);
        if (record.id > 0) {
          writeMap.set(record.id, { id: record.id });
        } else {
          createMap.set(record.id, record);
        }
        upFieldStore(props.fieldName, storeTable);
        upStoreChild({ id: record.id });
      } else {
        let currentStore;
        if (record.id > 0) {
          writeMap = storeTable.get("write");
          currentStore = writeMap.get(record.id);
        } else {
          createMap = storeTable.get("create");
          currentStore = createMap.get(record.id);
        }
        upStoreChild(currentStore);
      }
    }
  }

  function onChangeView(event, action, record) {
    if (action === "delete") {
      editRecord("delete", record);
    } else {
      setOpenMode("edit");
      if (level === "child") {
        if (record.id > 0) {
          setActiveChildFromId(record.id, model, ctxView);
        } else {
          upActiveChild(record);
          const storeField = storeRecord[props.fieldName];
          const storeAction = storeField.get("create");
          const storeChildCurrent = storeAction.get(record.id);
          upStoreChild(storeChildCurrent);
        }
      } else if (level === "subchild") {
        setActiveSubChildFromId(record.id, model, ctxView);
      }
      setOpenModalForm(true);
    }
  }

  async function editRecord(action, record, newStore) {
    if (level === "child") {
      let _activeChild = record;

      let storeTable = { ...storeRecord }[props.fieldName];
      let activeTable = { ...activeRecord }[props.fieldName];
      let storeAction;
      if (storeTable) {
        storeAction = storeTable.get(action);
      } else {
        storeTable = new Map();
        if (action === "delete") {
          storeAction = [];
        } else {
          storeAction = new Map();
        }
        storeTable.set(action, storeAction);
      }

      let currentStore;
      if (newStore) {
        if (action !== "delete") {
          currentStore = storeAction.get(newStore.id);
        }
      }
      if (currentStore) {
        currentStore = { ...currentStore, ...newStore };
      } else {
        currentStore = newStore;
        if (!currentStore) {
          currentStore = useFormChildStore.getState().storeChild;
        }
      }

      if (action === "delete") {
        activeTable.delete(record.id);
        if (record.id <= 0) {
          // We need call to create because this records are new
          let to_create = storeTable.get("create");
          to_create.delete(record.id);
        } else {
          storeAction.push(record.id);
        }
      } else if (action === "create") {
        activeTable.set(record.id, record);
        const recStoreStd = funcs.recToTryton(currentStore);
        storeAction.set(record.id, recStoreStd);
      } else if (action === "write") {
        const recStoreStd = funcs.recToTryton(currentStore);
        activeTable.set(record.id, _activeChild);
        storeAction.set(record.id, recStoreStd);
      }
      upFieldStore(props.fieldName, storeTable);
      upFieldActive(props.fieldName, activeTable);
    } else if (level === "subchild") {
      const { data, error } = await saveSubChild(model);
      if (!error) {
        const { record, model, ctxView } = parentRec;
        await setActiveChildFromId(record.id, model, ctxView);
      }
    }
  }

  function handleSort() {}

  function onClose() {
    setOpenModalForm(false);
  }

  return (
    <div
      id="subtable"
      className="w-full sm:p-2 col-span-2 rounded-md border border-slate-400 border-dashed  overflow-scroll xl:overflow-visible"
    >
      <SubtableHeader
        ctxView={ctxView}
        handleAddModal={handleAddModal}
        readOnly={readOnly}
      />
      <Table>
        <TableHeader ctxView={ctxView} handleSort={handleSort} />
        <BodyTable
          ctxView={ctxView}
          records={_records}
          onChangeView={onChangeView}
          onClose={onClose}
          updateRecords={updateRecords}
          onClickRow={props.onClickRow}
          onDoubleClickRow={props.onDoubleClickRow}
          parent={parentRec}
          multiselect={props.multiselect}
        />
      </Table>
      {openModalForm && activeChild && (
        <ModalForm
          onClose={onClose}
          ctxView={ctxView}
          editRecord={editRecord}
          parentRec={parentRec}
          open={openModalForm}
          level={level}
          mode={openMode}
        />
      )}
    </div>
  );
}

export default SubTable;
