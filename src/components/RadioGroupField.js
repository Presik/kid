import React, { useState, useEffect } from "react";

import FormField from "./FormField";

function RadioGroupFieldActive(props) {
  const { options, name } = props;
  const [option, setOption] = useState(null);

  function handleField(value) {
    setOption(value);
    props.onChange(name, value);
  }

  useEffect(() => {
    setOption(props.value);
  }, [props.value]);

  return (
    <FormField {...props}>
      <ul className="items-center w-full text-sm font-medium text-gray-900 bg-white border border-slate-300 rounded-lg sm:flex dark:bg-gray-700 dark:border-gray-600 dark:text-white h-10 divide-x divide-gray-200">
        {options.map((item) => {
          return (
            <li
              key={item.value}
              className="w-full h-full border-b border-gray-200 sm:border-b-0  dark:border-gray-600 "
            >
              <div className="flex items-center pl-3 h-full">
                <input
                  key={`${item.value}-checkbox-list`}
                  id={`${item.value}-checkbox-list`}
                  type="radio"
                  value={item.value}
                  checked={option === item.value}
                  disabled={props.readOnly ?? false}
                  name={name}
                  onChange={(e) => {
                    handleField(item.value, e);
                  }}
                  className="w-4 h-4 border-gray-300 rounded"
                />
                <label
                  key={`${item.value}-checkbox-label`}
                  htmlFor={`${item.value}-checkbox-list`}
                  className="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                  onClick={() => {
                    handleField(item.value);
                  }}
                >
                  {item.text}
                </label>
              </div>
            </li>
          );
        })}
      </ul>
    </FormField>
  );
}

export default RadioGroupFieldActive;
