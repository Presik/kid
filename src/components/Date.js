import React, { useState, Fragment } from "react";
import { Menu, Transition } from "@headlessui/react";
import {
  CalendarDaysIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from "@heroicons/react/20/solid";
import date from "date-and-time";

import FormField from "./FormField";
import DATES from "./funcDates";
import { classNames } from "tools/ui";

/**
 *
 * @param {object} props -
 * @param {boolean} props.selection -
 * @param {string} props.name -
 * @param {string} props.label -
 * @param {string} props.value -
 * @param {Function} props.onChange -
 * @param {boolean} props.required -
 * @param {Map} props.options -
 * @returns Component DropDown
 */

function DateField(props) {
  // props.data_source must be a Map()
  const [selected, setSelected] = useState(props.value || "");

  function onClick(event) {
    const value = new Date(event.timeStamp);
    const _date = date.format(value, "YYYY-MM-DD");
    setSelected(_date);
  }

  // function selectedItem(event, data) {
  //   const val = String(data.value);
  //   const value = props.options.get(val);
  //   if (!value) {
  //     return;
  //   }
  //   let toStore = value.record ? value.record : value.value;
  //   if (props.onChange) {
  //     props.onChange(data.name, toStore, data.recordid);
  //   }
  //   setSelected(data.value);
  // }

  // let dataMap = [];
  // if (props.options) {
  //   dataMap = Array.from(props.options.values());
  // }

  // clearable
  return (
    <FormField {...props}>
      <Menu as="div" className="relative inline-block text-left w-full">
        <div className="flex">
          <Menu.Button className="h-12 inline-flex w-full rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm focus:outline-none focus:border focus:border-cyan-500">
            <CalendarDaysIcon
              className="text-start mr-1 h-6 w-6 my-auto"
              aria-hidden="true"
            />
            <p className="pl-4 text-base my-auto">{selected}</p>
          </Menu.Button>
        </div>

        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <div className="mt-10 text-center lg:col-start-8 lg:col-end-13 lg:row-start-1 lg:mt-9 xl:col-start-9">
            <div className="flex items-center text-gray-900">
              <button
                type="button"
                className="-m-1.5 flex flex-none items-center justify-center p-1.5 text-gray-400 hover:text-gray-500"
              >
                <span className="sr-only">Previous month</span>
                <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
              </button>
              <div className="flex-auto font-semibold">January</div>
              <button
                type="button"
                className="-m-1.5 flex flex-none items-center justify-center p-1.5 text-gray-400 hover:text-gray-500"
              >
                <span className="sr-only">Next month</span>
                <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
              </button>
            </div>
            <div className="mt-6 grid grid-cols-7 text-xs leading-6 text-gray-500">
              <div>M</div>
              <div>T</div>
              <div>W</div>
              <div>T</div>
              <div>F</div>
              <div>S</div>
              <div>S</div>
            </div>
            <div className="isolate mt-2 grid grid-cols-7 gap-px rounded-lg bg-gray-200 text-sm shadow ring-1 ring-gray-200">
              {DATES.map((day, dayIdx) => (
                <button
                  key={day.date}
                  type="button"
                  onClick={onClick}
                  className={classNames(
                    "py-1.5 hover:bg-gray-100 focus:z-10",
                    day.isCurrentMonth ? "bg-white" : "bg-gray-50",
                    (day.isSelected || day.isToday) && "font-semibold",
                    day.isSelected && "text-white",
                    !day.isSelected &&
                      day.isCurrentMonth &&
                      !day.isToday &&
                      "text-gray-900",
                    !day.isSelected &&
                      !day.isCurrentMonth &&
                      !day.isToday &&
                      "text-gray-400",
                    day.isToday && !day.isSelected && "text-indigo-600",
                    dayIdx === 0 && "rounded-tl-lg",
                    dayIdx === 6 && "rounded-tr-lg",
                    dayIdx === DATES.length - 7 && "rounded-bl-lg",
                    dayIdx === DATES.length - 1 && "rounded-br-lg",
                  )}
                >
                  <time
                    dateTime={day.date}
                    className={classNames(
                      "mx-auto flex h-7 w-7 items-center justify-center rounded-full",
                      day.isSelected && day.isToday && "bg-indigo-600",
                      day.isSelected && !day.isToday && "bg-gray-900",
                    )}
                  >
                    {day.date.split("-").pop().replace(/^0/, "")}
                  </time>
                </button>
              ))}
            </div>
          </div>
        </Transition>
      </Menu>
    </FormField>
  );
}

export default DateField;
