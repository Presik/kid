import React from "react";

import Divider from "components/Form/Divider";
import CardSectionRow from "components/Chart/CardSectionRow";

function SegmentCard({ children, header, header_meta, desc, desc_meta }) {
  return (
    <div className="">
      <div className="">
        {children}
        <Divider />
        <CardSectionRow
          content1={header}
          content2={header_meta}
          metaStyle="text-red-500"
        />
        {desc && (
          <CardSectionRow
            metaStyle="text-red-500"
            content1={desc}
            content2={desc_meta}
          />
        )}
      </div>
    </div>
  );
}

export default SegmentCard;
