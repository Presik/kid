import React, { useState, useEffect, Fragment } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, ChevronDownIcon } from "@heroicons/react/20/solid";
import { FormattedMessage as FM } from "react-intl";

import FormField from "./FormField";
import { classNames, colors } from "tools/ui";

/**
 *
 * @param {*} props -
 * @param {Boolean} selection -
 * @param {String} name -
 * @param {String} label -
 * @param {String} value -
 * @param {Function} onChange -
 * @param {Boolean} required -
 * @param {Map} options -
 * @returns Component DropDown
 */

const style =
  "h-10 text-left text-sm border border-slate-300 appearance-none pt-2 pb-2 px-3 w-full rounded-md focus:outline-none focus:border focus:border-cyan-500 min-w-[100px]";

function SelectionField(props) {
  const [selected, setSelected] = useState(props.value || "");

  let color = "bg-white";

  if (props.readOnly) {
    color = colors.readOnly;
    // styleDisabled = "pointer-events-none";
  }

  function selectedItem(item) {
    props.onChange(props.name, item, props.recordid);
  }

  const options = props.options ?? [];

  useEffect(() => {
    setSelected(props.value || "");
  }, [props.value]);

  return (
    <FormField {...props}>
      <Listbox value={selected} onChange={(item) => selectedItem(item)}>
        {({ open }) => (
          <Fragment>
            <div className="relative">
              <Listbox.Button className={classNames(style, color)}>
                {selected && (
                  <span className="block truncate">
                    {selected &&
                    props.translate &&
                    typeof selected === "object" ? (
                      <FM id={selected.name} />
                    ) : (
                      selected.name
                    )}
                  </span>
                )}
                <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                  <ChevronDownIcon
                    className="h-5 w-5 text-gray-400"
                    aria-hidden="true"
                  />
                </span>
              </Listbox.Button>

              <Transition
                show={open}
                as={Fragment}
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Listbox.Options className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                  {options.map((value) => (
                    <Listbox.Option
                      key={value.id}
                      className={({ active }) =>
                        classNames(
                          active ? "text-white bg-cyan-500" : "text-gray-900",
                          "relative cursor-default select-none py-2 pl-8 pr-4",
                        )
                      }
                      value={value}
                    >
                      {({ selected, active }) => (
                        <Fragment>
                          <span
                            className={classNames(
                              selected ? "font-semibold" : "font-normal",
                              "block truncate",
                            )}
                          >
                            {props.translate && value ? (
                              <FM id={value.name} />
                            ) : (
                              value.name
                            )}
                          </span>

                          {selected ? (
                            <span
                              className={classNames(
                                active ? "text-white" : "text-indigo-600",
                                "absolute inset-y-0 left-0 flex items-center pl-1.5",
                              )}
                            >
                              <CheckIcon
                                className="h-5 w-5"
                                aria-hidden="true"
                              />
                            </span>
                          ) : null}
                        </Fragment>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </div>
          </Fragment>
        )}
      </Listbox>
    </FormField>
  );
}

export default SelectionField;
