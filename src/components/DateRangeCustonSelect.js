import React, { useState } from "react";
import { FormattedMessage as FM } from "react-intl";
import { Popover } from "@headlessui/react";
import { CalendarDaysIcon } from "@heroicons/react/20/solid";

import DateField from "./DateField";
import StdButton from "./StdButton";

const DateRangeCustomSelect = (props) => {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);
  let [dateStart, setDateStart] = useState(null);
  let [dateEnd, setDateEnd] = useState(null);

  const onChange = (name, value) => {
    if (value) {
      let _value = new Date(value);
      // setDateStart("value");
      props.onClick(_value);
      setIsPopoverOpen(!isPopoverOpen);
    }
  };

  const togglePopover = () => {
    setIsPopoverOpen(!isPopoverOpen);
  };

  return (
    <div className="relative">
      <button
        onClick={() => setIsPopoverOpen(!isPopoverOpen)}
        className="group bg-sky-200 border border-sky-300 shadow-sm rounded-md px-7 cursor-pointer pt-2 h-[74px] pb-2 flex flex-col justify-center w-20 items-center space-y-1 active:scale-95 active:shadow-x hover:bg-blue-presik"
      >
        <CalendarDaysIcon className="w-10 group-hover:text-white" />
        <span className="text-xs font-semibold uppercase group-hover:text-yellow-300 pb-2 text-cyan-700">
          <FM id={"date_range.select"} key={"date_range.select"} />
        </span>
      </button>
      {isPopoverOpen && (
        <div className="bg-white p-6 shadow-sm rounded-md border border-gray-200 absolute z-10 -left-28 w-[300px] flex flex-col top-20">
          <DateField
            onChange={onChange}
            name="filter_date"
            label="date_range.filter_date"
          />
          {/* fecha final en desarrollo */}
          {/* <DateField /> */}
          {/* <div className="w-full">
            <StdButton
              content={"Filtrar"}
              style="mx-auto mt-4"
              onClick={togglePopover}
            />
          </div> */}
        </div>
      )}
    </div>
  );
};

export default DateRangeCustomSelect;
