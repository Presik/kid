import React, { useEffect, useState } from "react";

import { classNames, colors } from "tools/ui";

function NumericCell(props) {
  const [value, setValue] = useState(props.value ?? "");

  function onChange(event) {
    let _value = event.target.value;
    _value = _value.replace(/\./g, "");
    if (isNaN(_value)) {
      return;
    }
    _value = Number(_value);
    setValue(_value);
  }

  function handleBlur(event) {
    let _value = event.target.value;
    _value = _value.replace(/\./g, "");
    if (isNaN(_value)) {
      return;
    }
    _value = Number(_value);
    props.onChange(props.name, _value);
  }

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  let _color = props.readOnly ? colors.readOnly : props.color ?? "bg-white";

  let _value = "";
  if (!isNaN(value) && value !== null) {
    _value = value.toLocaleString("es", { useGrouping: true });
  }

  return (
    <input
      id={props.name}
      name={props.name}
      className={classNames(
        "h-9 rounded-md shadow-sm w-full text-right py-1 px-3 text-gray-700 border border-slate-300 focus:outline-none focus:border focus:border-cyan-500",
        _color,
      )}
      type="text"
      inputMode="numeric"
      value={_value}
      onBlur={handleBlur}
      onChange={onChange}
      readOnly={props.readOnly}
    />
  );
}

export default NumericCell;
