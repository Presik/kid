import React, { useEffect, useState } from "react";

import { classNames, colors } from "tools/ui";

function BooleanCell(props) {
  const [value, setValue] = useState(props.value ?? false);

  function onChange(event) {
    let _value = event.target.value;
    setValue(_value);
    props.onChange(props.name, _value, props.recordid);
  }

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  let _color = props.readOnly ? colors.readOnly : props.color ?? "bg-white";
  let _value = props.value;

  return (
    <input
      id={props.name}
      name={props.name}
      className={classNames("h-5 w-5 rounded-full", _color)}
      type="checkbox"
      inputMode="numeric"
      value={_value}
      checked={value ? true : false}
      // onBlur={handleBlur}
      onChange={onChange}
      readOnly={props.readOnly}
    />
  );
}

export default BooleanCell;
