import React from "react";

import intl from "i18n/messages";

// h-[2.4rem]
/**
 *
 * @param {*} props -
 * @param {Boolean} readOnly -
 * @param {String} type -
 * @param {String} name -
 * @param {String} placeholder -
 * @param {String} value -
 * @param {Function} onChange -
 * @param {Boolean} translate -
 * @returns Component TextField
 */

function TextCell(props) {
  let editedValue = props.value ?? "";
  if (props.value && props.translate) {
    const _value = `${props.model}.${props.value}`;
    editedValue = intl.msgs[_value] || props.value;
  }

  function onChange(event) {
    let _value = event.target.value;
    if (props.type === "integer") {
      _value = parseInt(_value);
    }
    editedValue = _value;
  }

  console.log("aqui ........", props.value, editedValue);
  return (
    <div className="">
      <input
        key={props.name}
        id={props.name}
        name={props.name}
        className={
          "w-full py-2 px-2 text-gray-700 rounded-md bg-inherit focus:outline-none focus:border focus:border-cyan-500"
        }
        type={props.type || "text"}
        value={editedValue}
        onChange={onChange}
        readOnly={props.readOnly ?? false}
        placeholder={props.placeholder ?? ""}
      />
      {props.icon || null}
    </div>
  );
}

export default TextCell;
