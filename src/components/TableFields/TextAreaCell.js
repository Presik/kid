import React from "react";

/**
 *
 * @param {*} props -
 * @param {Boolean} readOnly -
 * @param {String} type -
 * @param {String} name -
 * @param {String} placeholder -
 * @param {String} value -
 * @param {Function} onChange -
 * @param {Boolean} translate -
 * @returns Component TextField
 */

function TextAreaCell(props) {
  let value = props.value ?? "";

  function onChange(event) {
    console.log("Not implemented update in TextAreaCell!");
  }

  return (
    <textarea
      id={props.field.name}
      value={value}
      rows={3}
      onChange={onChange}
      className={"border-none focus:border-none border-0 bg-inherit"}
    />
  );
}

export default TextAreaCell;
