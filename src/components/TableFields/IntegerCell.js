import React from "react";
import { PlusCircleIcon, MinusCircleIcon } from "@heroicons/react/20/solid";
import { classNames } from "tools/ui";
const iconStyle = "w-5 h-5 text-stone-700";
const stBtn =
  "border-slate-300 border-[1px] px-3 py-1 bg-zinc-100 active:bg-zinc-200 focus:outline-none";
const base =
  "rounded-none w-full border-t text-center border-b border-slate-300 py-2 px-3 text-gray-700 focus:outline-none focus:border focus:border-cyan-500 ";
const _color = "bg-gray-100";

function IntegerCell(props) {
  let value = props.value ?? "";

  function onChange(event) {
    let _value = event.target.value;
    _value = _value.toString().replace(/\./g, "");

    if (isNaN(_value)) {
      return;
    }
    _value = Number(_value);
    value = _value;
    props.onChange(props.name, _value, props.recordid);
  }

  if (value) {
    value = value.toLocaleString("es", { useGrouping: true });
  }

  const increment = () => {
    let newValue;
    if (typeof value == "string") {
      newValue = Number(value) + 1;
    } else {
      newValue = value + 1;
    }
    onChange({ target: { value: newValue } });
  };

  const decrement = () => {
    if (value > 1) {
      let newValue;
      if (typeof value == "string") {
        newValue = Number(value) - 1;
      } else {
        newValue = value - 1;
      }
      onChange({ target: { value: newValue } });
    }
  };

  const leftBtn = "rounded-l-md";
  const rightBtn = "rounded-r-md";
  return (
    <div className="flex items-stretch w-full max-w-[140px] mx-auto">
      {!props.readOnly && (
        <button className={classNames(stBtn, leftBtn)} onClick={decrement}>
          <MinusCircleIcon className={iconStyle} />
        </button>
      )}
      <input
        id={props.name}
        name={props.name}
        className={classNames(base, _color)}
        type="text"
        value={value}
        onChange={onChange}
        readOnly={props.readOnly}
      />
      {!props.readOnly && (
        <button onClick={increment} className={classNames(stBtn, rightBtn)}>
          <PlusCircleIcon className={iconStyle} />
        </button>
      )}
    </div>
  );
}

export default IntegerCell;
