import React, { Fragment, useState } from "react";
import { FormattedMessage as FM } from "react-intl";

import BasicModal from "components/BasicModal";
import { BUTTON_COLORS } from "components/Constants/constants";
import { classNames } from "tools/ui";
import { useWizardStore } from "store/wizardStore";
import { useFormStore } from "store/formStore";

const style =
  "block my-4 mx-auto w-full h-12 text-center border-[1px] justify-center rounded-xl active:scale-95 active:shadow-sm";

function ButtonCustomModal({
  Component,
  label,
  icon,
  color,
  record,
  button_ok,
  onAccept,
}) {
  const [open, setOpen] = useState(false);
  const { activeWizard, storeWizard } = useWizardStore();
  const { activeRecord, storeRecord, upStoreRecord, upActiveRecord } =
    useFormStore();

  const _color = color ?? "blue";
  const [bgColor, txtColor, border] = BUTTON_COLORS[_color];

  function onClose() {
    setOpen(false);
  }

  function handleAccept() {
    if (Object.keys(storeWizard).length > 0) {
      const _storeRecord = { ...storeRecord, ...storeWizard };
      upStoreRecord(_storeRecord);
      const _activeRecord = { ...activeRecord, ...activeWizard };
      upActiveRecord(_activeRecord);
    }
    setOpen(false);
  }

  let buttons = [];
  if (button_ok !== false) {
    buttons = ["ok"];
  }

  return (
    <Fragment>
      <button
        onClick={() => setOpen(true)}
        className={classNames(style, bgColor, txtColor, border)}
      >
        <span className={classNames("w-42 ml-auto  my-auto")}>
          <FM id={label} />
        </span>
        {icon && (
          <i
            className={classNames(
              "mr-auto my-auto ml-6 text-xl",
              icon,
              txtColor,
            )}
          />
        )}
      </button>
      <BasicModal
        title={label}
        open={open}
        onClose={onClose}
        buttons={buttons}
        onAccept={handleAccept}
        titleSize="text-2xl"
        width="w-full sm:w-5/6 md:w-1/2"
      >
        <Component record={record} onClose={() => onClose()} />
      </BasicModal>
    </Fragment>
  );
}

export default ButtonCustomModal;
