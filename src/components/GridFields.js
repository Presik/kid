import React from "react";
import { classNames } from "tools/ui";

const base = "grid gap-x-2 mb-auto rounded-lg h-full";

function GroupFields(props) {
  let style = "";
  let cols = "";
  let rows = "";
  let border = "";
  if (props.border === "visible") {
    border = "border border-dashed border-stone-400 px-2";
  }
  if (props.size) {
    rows = props.size[0];
    cols = `md:grid-cols-${props.size[1]}`;
  }
  let span = "";
  if (props.span) {
    span = props.span;
  }
  return (
    <div
      key="form-group-fields"
      className={classNames(base, span, cols, rows, style, border)}
    >
      {props.children}
    </div>
  );
}

export default GroupFields;
