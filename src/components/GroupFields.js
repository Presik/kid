import React from "react";

function GroupFieldActives(props) {
  return (
    <div
      key="group-form"
      className="flex gap-2 mb-auto border border-dashed border-stone-400 p-2 rounded-lg"
    >
      {props.children}
    </div>
  );
}

export default GroupFieldActives;
