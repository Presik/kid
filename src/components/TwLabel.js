import React from "react";
import { FormattedMessage as FM } from "react-intl";

function TwLabel({ name, required }) {
  return (
    <label className="flex text-gray-600 text-md mb-1" htmlFor={name}>
      <FM id={name} />
      <p className="text-red-500">{required && "*"}</p>
    </label>
  );
}

export default TwLabel;
