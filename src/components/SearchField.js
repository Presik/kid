import React, { useState } from "react";
import { MagnifyingGlassIcon } from "@heroicons/react/20/solid";

import FormField from "components/FormField";
import SearchWindow from "components/SearchWindow";
import IconButton from "./IconButton";
import ModalForm from "components/Modals/ModalForm";
import { classNames, colors } from "tools/ui";
import { useFormStore } from "store/formStore";
import { useFormChildStore } from "store/formChildStore";
import { useWizardStore } from "store/wizardStore";
import FormCustomer from "components/FormCustomer";
const style =
  "flex group rounded-md w-full p-2 text-gray-700 border border-slate-300 focus-within:border focus-within:border-cyan-500";

function SearchField(props) {
  let { ctxView, parentRec, model } = props;
  const [placeholder, setPlaceholder] = useState(null);
  const [searchModal, openSearchModal] = useState(false);
  let [openRecord, setOpenRecord] = useState(false);
  let [mode, setMode] = useState(null);
  const { setActiveChildFromId, newChild } = useFormChildStore();
  const { activeRecord } = useFormStore();
  const { setActiveWizardFromId } = useWizardStore();

  let bg = "bg-white";
  if (props.readOnly) {
    bg = colors.readOnly;
  }

  // const onSaveAdd = async (action) => {
  //   let args = storeChild;
  //   args = funcs.recToTryton(args);
  //   const fields_names = funcs.getViewFields(ctxView, "form");
  //   let res;
  //   if (action === "add") {
  //     res = await proxy.create(model, args, fields_names);
  //     args.id = res[0];
  //     upFieldStore(props.name, args.id);
  //     upFieldActive(props.name, args);
  //   } else {
  //     const data = {
  //       model: model,
  //       storeRec: args,
  //     };
  //     res = await proxy.saveQuery(data);
  //   }
  //   setOpenRecord(false);
  //   // resetChild();
  //   console.log("NO DEBE EJECUTARSE............");
  // };

  function onSelectedRecord(rec) {
    props.onChange(props.name, rec);
  }

  function searchClicked() {
    if (props.readOnly) {
      return;
    }
    openSearchModal(true);
    setPlaceholder(null);
  }

  const onChangeView = async (mode) => {
    if (mode === "edit") {
      const recordId = activeRecord[props.name].id;
      await setActiveWizardFromId(recordId, model, ctxView);
    } else {
      // create / add
      await newChild(ctxView, parentRec);
    }
    setMode(mode);
    setOpenRecord(true);
  };

  function onCloseSearch() {
    openSearchModal(false);
  }

  function onCloseRecord() {
    setOpenRecord(false);
  }

  function selectedItem() {}

  let widgetIcon;
  if (props.widget == "search-add" || props.widget == "search-add-new") {
    if (props.value) {
      widgetIcon = (
        <IconButton
          onClick={() => onChangeView("edit")}
          color="blue"
          tooltip="board.button_field_edit"
          name="fi fi-rr-edit"
          style="ml-1"
        />
      );
    } else {
      widgetIcon = (
        <IconButton
          onClick={() => onChangeView("create")}
          color="blue"
          name="fi fi-rr-add"
          tooltip="board.button_field_create"
          style="ml-1"
        />
      );
    }
  }

  // const recordForm = activeRecord[props.name];
  if (parentRec) {
    parentRec.target = props.name;
  }
  return (
    <FormField {...props}>
      <div className="flex">
        <div className={classNames(style, bg)}>
          <input
            type="text"
            disabled={props.readOnly}
            name={props.name}
            key={props.name}
            value={props.value}
            placeholder={placeholder}
            onChange={selectedItem}
            className="w-full focus:border-none focus:outline-none"
          />
          <MagnifyingGlassIcon
            onClick={searchClicked}
            className="h-5 w-5 text-gray-500 active:text-sky-600 cursor-pointer active:scale-90"
            aria-hidden="true"
          />
        </div>
        {widgetIcon}
      </div>
      <SearchWindow
        key="search"
        modeSelect="one"
        open={searchModal}
        onClose={onCloseSearch}
        model={model}
        ctxView={ctxView}
        nameField={props.name}
        onSelectedRecord={onSelectedRecord}
      />
      {openRecord &&
        mode &&
        (props.widget !== "search-add-new" ? (
          <ModalForm
            level="many2one"
            open={openRecord}
            ctxView={ctxView}
            mode={mode}
            nameField={props.name}
            parentRec={parentRec}
            onClose={onCloseRecord}
          />
        ) : (
          <FormCustomer onClose={onCloseRecord} />
        ))}
    </FormField>
  );
}

export default SearchField;
