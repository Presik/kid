import React from "react";
import { ChevronUpIcon, XMarkIcon } from "@heroicons/react/20/solid";

import { BUTTON_COLORS } from "components/Constants/constants";

const style = `${BUTTON_COLORS.rose[0]} ${BUTTON_COLORS.rose[1]}`;
const styleOpen = "bg-white text-bluePresik";

const SectionRigth = ({
  children,
  title,
  bgColor,
  status,
  handleChangeStatus,
}) => {
  return (
    <div
      className={
        status == true
          ? "md:w-[25vw] xl:w-[22vw] block pr-5 custom-transition duration-500 sticky top-0"
          : "md:w-[344px] fixed h-[89px] bottom-[50%] pb-5 -right-[153px] rounded-b-md overflow-hidden transform rotate-90 animate-fade-left animate-ease-linear transition-all duration-500"
      }
    >
      {handleChangeStatus && (
        <button
          className="absolute right-3 top-6"
          onClick={() => handleChangeStatus(!status)}
        >
          {status ? (
            <XMarkIcon
              className={`${style} absolute right-4 top-4 w-9 rounded-full p-2 `}
            />
          ) : (
            <ChevronUpIcon
              className={`${styleOpen} absolute right-4 top-4 w-9 rounded-full p-2 transform rotate-180 `}
            />
          )}
        </button>
      )}

      <div
        className={`${bgColor} px-6 py-5 shadow-md space-y-5 rounded-lg mt-5`}
      >
        <h3
          className={`truncate text-2xl font-bold ${
            status ? "text-bluePresik" : "text-white"
          } `}
        >
          {title}
        </h3>
        {children}
      </div>
    </div>
  );
};

export default SectionRigth;
