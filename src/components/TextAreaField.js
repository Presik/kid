import React from "react";

import FormField from "./FormField";
import { classNames, colors } from "tools/ui";

function TextAreaField(props) {
  let editedText = props.value ?? "";

  function onChange(event) {
    if (props.readOnly) {
      return;
    }
    const _value = event.target.value;
    editedText = _value;
    props.onChange(props.name, _value);
  }
  let _color = "bg-white";
  if (props.readOnly) {
    _color = colors.readOnly;
  }
  const style =
    "block w-full max-h-20 rounded-md w-full py-3 pb-2 px-2 text-gray-700 border border-slate-300 focus:outline-none focus:border focus:border-cyan-500";
  return (
    <FormField {...props}>
      <textarea
        disabled={props.readonly}
        rows={4}
        id={props.name}
        key={props.name}
        name={props.name}
        value={editedText}
        onChange={onChange}
        placeholder={props.placeholder ?? ""}
        className={classNames(_color, style)}
      />
    </FormField>
  );
}

export default TextAreaField;
