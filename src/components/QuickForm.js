import React, { Fragment, useState } from "react";

import FormMessage from "components/Form/FormMessage";
import FormGrid from "components/Form/FormGrid";
import HeadForm from "components/Form/HeadForm";
import FooterForm from "components/Form/FooterForm";
import ModalMsg from "components/Modals/ModalMsg";
import Form from "components/Form";
import { validateForm } from "tools/form";
import funcs from "tools/functions";
import { useFormStore } from "store/formStore";
import { useFormChildStore } from "store/formChildStore";
import { useFormSubChildStore } from "store/formSubChildStore";
import { useWizardStore } from "store/wizardStore";
import { useBtnLoading } from "store/btnLoading";
import proxy from "api/proxy";

const MISSING_FIELDS = "board.missing_required_fields";

/**
 * create component Form
 * @param {Object} props
 * @param {Array} props.ctxView
 * @param {Function} props.modal
 * @returns
 */
function QuickForm(props) {
  // kind => modal, fixed
  // level => main, child, subchild, wizard, many2one
  const {
    ctxView,
    kind,
    onClose,
    onChangeView,
    level,
    parentRec,
    editRecord,
    mode,
  } = props;
  const { webfields, webform, model, title } = ctxView;
  const [message, setMessage] = useState(null);
  const [modalProps, setModalProps] = useState({
    open: false,
    msg: "",
    type: "info",
    origin: null,
  });
  const {
    storeRecord,
    activeRecord,
    setActiveRecordFromId,
    resetRecord,
    reloadRecord,
    upFieldActive,
    upFieldStore,
  } = useFormStore();
  const {
    activeChild,
    storeChild,
    resetChild,
    setActiveChildFromId,
    newChild,
  } = useFormChildStore();
  const {
    activeSubChild,
    storeSubChild,
    resetSubChild,
    setActiveSubChildFromId,
    newSubChild,
  } = useFormSubChildStore();
  const { activeWizard, storeWizard, resetWizard } = useWizardStore();
  const { setButtonLoading } = useBtnLoading();

  async function handleButton(btnName) {
    let button = webfields[btnName];
    if (["wizard", "many2one"].includes(level)) {
      button.method();
    } else if (button) {
      await onActionButton(button);
    }
    setButtonLoading(false);
  }

  // async function handleButton(btnName) {
  //   let button;
  //   if (["wizard", "many2one"].includes(level)) {
  //     button = webfields[btnName];
  //     button.method();
  //   } else if (webfields[btnName]) {
  //     console.log("porque esta aqui 1");
  //     await onActionButton(webfields[btnName]);
  //   }
  //   setButtonLoading(false);
  //   console.log("porque esta aqui 2");
  // }

  async function onActionButton(button) {
    let { record, store } = getStoreCtx(); // need reset ?
    if (button.method && typeof button.method !== "string") {
      button.method(record);
      return;
    }
    const size = Object.keys(store).length;
    if (!button || (button?.button_method && size > 1)) {
      const recordId = await onSave();
      if (!recordId) return;
      record.id = recordId;
    }
    let dataToAPI = {
      model: model,
      method: button.button_method ?? button.method,
    };
    let res;
    if (button.button_method) {
      dataToAPI.ids = [record.id];
      dataToAPI.method = button.button_method;
      res = await proxy.buttonMethod(dataToAPI);
    } else {
      dataToAPI.args = [store];
      dataToAPI.method = button.method;
      res = await proxy.methodCall(dataToAPI);
      if (res.data?.record) {
        record = res.data.record;
      }
      console.log("este record.......!", record);
    }
    handleAction(res, record.id, button);
  }

  function validate(record, store) {
    const requiredFields = funcs.getRequired(webfields, record);
    let action = record.id > 0 ? "update" : "create";
    const res = validateForm(action, record, requiredFields);
    if (res !== "ok") {
      setMessage({ type: "error", msg: MISSING_FIELDS });
      return;
    }
    store.id = record.id;
    return store;
  }

  function getStoreCtx() {
    let record, store, reset;
    if (level === "main") {
      record = activeRecord;
      store = { ...storeRecord };
      reset = resetRecord;
    } else if (level === "child") {
      record = activeChild;
      store = { ...storeChild };
      reset = resetChild;
    } else if (level === "subchild") {
      record = activeSubChild;
      store = { ...storeSubChild };
      reset = resetSubChild;
    } else if (level === "many2one") {
      record = activeWizard;
      store = { ...storeWizard };
      reset = resetWizard;
    }
    store = funcs.recToTryton(store);
    return { record, store, reset };
  }

  async function onSave(action) {
    const { record, store, reset } = getStoreCtx();

    // We need improvement this!, because editRecord is only for record related
    // from subtable, normally a child or subchild, the best way is split this
    // function to applying clean code
    if (action === "delete") {
      if (editRecord) {
        editRecord("delete", record, store);
      }
      onClose();
      return;
    } else if (action === "add") {
      if (editRecord) {
        const storeRec = validate(record, store);
        if (!storeRec) return;
        editRecord("create", record, store);
      }
      if (level === "child") {
        newChild(ctxView);
      } else if (level === "subchild") {
        newSubChild(ctxView, parentRec);
      }
      return;
    } else if (action === "accept") {
      if (level === "child" || level === "subchild") {
        if (record.id <= 0) {
          const storeRec = validate(record, store);
          if (!storeRec) return;
          editRecord("create", record, store);
        } else {
          editRecord("write", record, store);
        }
        onClose();
        return;
      }
    }

    const storeRec = validate(record, store);
    if (!storeRec) return;
    let dataToAPI = {
      model: model,
      storeRec: storeRec,
    };
    const res = await proxy.saveQuery(dataToAPI);
    let recId = record.id;
    if (Array.isArray(res.data)) {
      recId = res.data[0];
    } else if (res.data?.record) {
      recId = res.data.record.id;
    }

    handleAction(res, recId, null, action);
    if (onClose) {
      onClose();
      console.log("We need reset????????");
      reset();
    }
    if (!res.error) {
      setMessage({ type: "info", msg: "board.record_saved" });
      return recId;
    }
  }

  async function handleAction(response, recId, button, action) {
    if (response.error) {
      console.log("hubo un error....", response.error);
      let msgError = "";
      if (response.error instanceof TypeError) {
        // This is for network error or similars
        msgError = response.error.message;
      } else {
        msgError = response.error.error;
      }
      setModalProps({
        open: true,
        msg: msgError,
        type: "error",
        reset: false,
      });
      return;
    } else if (response?.data?.msg) {
      setModalProps({
        open: true,
        msg: response.data.msg,
        type: response.data.type,
        origin: button,
      });
    }
    console.log("ESTO NO SE DEBE EJECUTAR .................>>>!");
    if (recId > 0) {
      if (level === "main") {
        // recID will always be greater than 0 as when the record is created for the first time. fixme
        // El registro creado es mayor que cero pero si viene de uno a muchos esto no es necesario
        setActiveRecordFromId(recId, model, ctxView);
        // reloadRecord();
      } else if (level === "child") {
        if (action === "accept") {
          setActiveRecordFromId(recId, model, ctxView);
          // reloadRecord();
        } else {
          setActiveChildFromId(recId, model, ctxView);
        }
      } else if (level === "many2one") {
        // Fixme for child, subchild?
        if (recId > 0) {
          setMany2One(recId);
        }
      }
    }
  }

  async function setMany2One(recId) {
    // In the future maybe we can abstract this methods of this render function
    // This add the new record to field when this is created from SearchField
    const targetField = parentRec.target;
    let storeRec = {};
    if (parentRec && parentRec.target && parentRec.record.id > 0) {
      // This keep an auto save for new many2one added,
      // to existing main record
      storeRec.id = parentRec.record.id;
      storeRec[targetField] = recId;
      const args = {
        model: parentRec.model,
        storeRec: storeRec,
      };
      // here must to set new/selected record
      const { error } = await proxy.saveQuery(args);
      if (!error) {
        reloadRecord();
      }
    } else {
      // This add the data many2one to store of new main record
      const model = ctxView.model;
      const dom = ["id", "=", recId];
      const fields = ["id", "name", "rec_name"];
      const { data, error } = await proxy.search(model, dom, fields);
      if (data) {
        const rec = data[0];
        storeRec.id = recId;
        storeRec.rec_name = rec.name || rec.rec_name;
        upFieldActive(targetField, storeRec);
        upFieldStore(targetField, storeRec);
      }
    }
  }

  function onCloseModalMsg(button, reset) {
    console.log("Aqui onCloseModalMsg", reset);
    if (reset !== false) {
      console.log("reseteando.............");
      resetRecord(button?.resetForm);
    }
    setModalProps({
      open: false,
      msg: "",
      type: "info",
      origin: null,
    });
  }

  let recordForm;
  if (level === "main") {
    recordForm = activeRecord;
  } else if (level === "child") {
    recordForm = activeChild;
  } else if (level === "subchild") {
    recordForm = activeSubChild;
  } else if (level === "wizard") {
    recordForm = activeWizard;
  } else if (level === "many2one") {
    recordForm = activeWizard;
  }

  return (
    <Fragment>
      <HeadForm
        key="form-header"
        ctxView={ctxView}
        kind={kind}
        onChangeView={onChangeView}
        title={title}
        colspan={true}
        FooterForm
        onSave={onSave}
        onClose={onClose}
        recordForm={recordForm}
      />
      <FormMessage
        message={message}
        handleCloseMessage={() => setMessage(null)}
      />
      <FormGrid>
        <Form
          key="form"
          ctxView={ctxView}
          webform={webform}
          handleButton={handleButton}
          recordForm={recordForm}
          level={level}
        />
      </FormGrid>
      {(kind === "modal" || level === "many2one") && (
        <FooterForm
          ctxView={ctxView}
          handleButton={handleButton}
          onSave={onSave}
          mode={mode}
        />
      )}
      {modalProps.open && (
        <ModalMsg
          open={modalProps.open}
          type={modalProps.type}
          msg={modalProps.msg}
          reset={modalProps.reset}
          onClose={onCloseModalMsg}
          origin={modalProps.origin}
          buttons={["close"]}
        />
      )}
    </Fragment>
  );
}

export default QuickForm;
