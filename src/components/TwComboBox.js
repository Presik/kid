import React, { Fragment, useEffect, useState } from "react";
import { Combobox } from "@headlessui/react";
import { CheckIcon, ChevronDownIcon } from "@heroicons/react/24/solid";
import { useQuery } from "@tanstack/react-query";

import { classNames } from "tools/ui";
import FormField from "./FormField";
import proxy from "api/proxy";

// const people = [
//   { id: 1, name: 'Leslie Alexander' },
//   // More users...
// ]

/**
 * Field Form Selection .
 *
 * @param {string} name - property name of field type string
 * @param {string} label - property label of field type string
 * @param {boolean} readonly - property readonly type boolean
 * @param {string} model - property model from name model tryton type string
 * @param {object} selected - property value is data for options of select
 * @param {Array} options - property record is data of parent
 * @param {function} callback -  callback(nameField, dataField) called onChage field
 * @return {Component} - component selection
 */
export default function TwComboBox(props) {
  const [query, setQuery] = useState("");
  const [selectedOption, setSelectedOption] = useState("");

  let { data: filteredOptions } = useQuery([props.name, query], async () => {
    if (props.model) {
      const domain =
        query === ""
          ? []
          : props.recSearch
          ? props.recSearch(query)
          : [["rec_name", "ilike", `%${query}%`]];

      const fields = props.attrs
        ? ["id", "name", ...props.attrs]
        : ["id", "name"];
      return await proxy.search(props.model, domain, fields, 100);
    }
    return null;
  });

  if (!props.model) {
    filteredOptions =
      query === ""
        ? props.options
        : props.options.filter((option) => {
            return option.name.toLowerCase().includes(query.toLowerCase());
          });
  }

  let className =
    "h-10 border border-slate-300 appearance-none py-2 px-3 w-full rounded-md focus:outline-none focus:border focus:border-cyan-500";
  if (props.className) {
    className = props.className;
  }

  const handleField = (value) => {
    setSelectedOption(value);
    props.onChange(props.name, value);
  };

  useEffect(() => {
    setSelectedOption(props.selected || "");
  }, [props.selected]);

  return (
    <FormField {...props}>
      <Combobox as="div" value={selectedOption} onChange={handleField}>
        <div className="relative ">
          <Combobox.Input
            className={className}
            onChange={(event) => setQuery(event.target.value)}
            displayValue={() => selectedOption?.name}
          />
          <Combobox.Button className="absolute inset-y-0 right-0 flex items-center rounded-r-md px-2 focus:outline-none">
            <ChevronDownIcon
              className="h-5 w-5 text-gray-400"
              aria-hidden="true"
            />
          </Combobox.Button>

          {filteredOptions?.length > 0 && (
            <Combobox.Options className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
              {filteredOptions.map((option) => (
                <Combobox.Option
                  key={option.id}
                  value={option}
                  className={({ active }) =>
                    classNames(
                      "relative cursor-default select-none py-2 pl-3 pr-9",
                      active ? "bg-cyan-500 text-white" : "text-gray-900",
                    )
                  }
                >
                  {({ active, selected }) => (
                    <Fragment>
                      <span
                        className={classNames(
                          "block truncate",
                          selected && "font-semibold",
                        )}
                      >
                        {option.name}
                      </span>

                      {selected && (
                        <span
                          className={classNames(
                            "absolute inset-y-0 right-0 flex items-center pr-4",
                            active ? "text-white" : "text-indigo-600",
                          )}
                        >
                          <CheckIcon className="h-5 w-5" aria-hidden="true" />
                        </span>
                      )}
                    </Fragment>
                  )}
                </Combobox.Option>
              ))}
            </Combobox.Options>
          )}
        </div>
      </Combobox>
    </FormField>
  );
}
