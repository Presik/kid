import React from "react";

import imageEmpty from "assets/img/image_empty.svg";
import FormField from "./FormField";

function ImageField(props) {
  let size = "small";
  if (props.size) {
    size = props.size;
  }
  let src = props.value || imageEmpty;
  // src = imageEmpty;
  if (props.bytes && props.value) {
    src = "data:image/png;base64," + props.value;
  }

  return (
    <FormField {...props}>
      <img
        style={stylesCtx(size)}
        id={props.name}
        name={props.name}
        key={props.name}
        disabled={props.readonly}
        src={src}
      />
    </FormField>
  );
}

const stylesCtx = (size) => {
  let maxWidth, maxHeight;
  if (size === "small") {
    maxWidth = 100;
    maxHeight = 100;
  } else if (size === "middle") {
    maxWidth = 200;
    maxHeight = 200;
  } else {
    maxWidth = 300;
    maxHeight = 300;
  }
  return {
    maxWidth,
    maxHeight,
    margin: "auto",
  };
};

export default ImageField;
