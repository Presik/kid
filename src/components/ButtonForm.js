import React from "react";

import StdButton from "components/StdButton";
import funcs from "tools/functions";
import { useBtnLoading } from "store/btnLoading";

function ButtonForm(props) {
  // This option is unnecesary becouse factoryField resolve
  let visible = funcs.isVisible(props.visible, props.record, props.name);
  if (!visible) return null;

  const { loadingBtn, setButtonLoading } = useBtnLoading();

  async function onClick() {
    setButtonLoading(true);
    props.handleButton(props.name);
  }

  const _color = props.color ?? "green";

  return (
    <div className="py-4">
      <StdButton
        key={props.value}
        onClick={onClick}
        color={_color}
        content={props.label}
        disabled={props.disabled}
        name={props.name}
        loading={loadingBtn}
        iconRight={props.iconRight || false}
        iconLeft={props.iconLeft || false}
      />
    </div>
  );
}

export default ButtonForm;
