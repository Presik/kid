import React from "react";
import { FormattedMessage as FM } from "react-intl";

import HeaderCell from "components/MeTable/HeaderCell";

function TableHeader(props) {
  const { ctxView } = props;
  const { webtree, table_action } = ctxView;
  // order === "asc" ? "ascending" : "descending";
  let cellButtonStart = [];
  let cellButtonEnd = [];

  for (const btn of table_action) {
    if (btn === "add") continue;
    if (btn === "edit" || btn === "open") {
      cellButtonStart.push(btn);
    } else {
      cellButtonEnd.push(btn);
    }
  }

  function cellButtons(buttons) {
    return buttons.map((button) => {
      return (
        <th key={button} className="px-3">
          <FM id={`button_${button}`} />
        </th>
      );
    });
  }

  return (
    <thead className="sticky top-0 leading-6 ">
      <tr className="bg-amber-100">
        {cellButtons(cellButtonStart)}
        {webtree.map((field, index) => (
          <HeaderCell key={index} index={index} field={field} {...props} />
        ))}
        {cellButtons(cellButtonEnd)}
      </tr>
    </thead>
  );
}

export default TableHeader;
