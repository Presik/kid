import React, { useState } from "react";
import store from "store";

import BoardHeader from "./BoardHeader";
import QuickTable from "./QuickTable";
import QuickForm from "components/QuickForm";
import QuickCards from "./QuickCards";
import tool from "tools/functions";
import { useFormStore } from "store/formStore";

const VIEWS = {
  list: QuickTable,
  form: QuickForm,
  cards: QuickCards,
};
var nextId = -1;

/**
 * This component renders a table or a form from a context view object
 *
 * @param {object} props - component props
 * @param {Function} props.ctxView - object to render view
 * @returns {Component}
 */

function Board(props) {
  const session = store.get("ctxSession");
  const { ctxView } = props;
  let startView = "list";
  if (ctxView.defaultView) {
    startView = ctxView.defaultView;
  }
  const [viewType, setViewType] = useState(startView);
  const [action, setAction] = useState(null);
  const { upStoreRecord, upActiveRecord, setActiveRecordFromId } =
    useFormStore();
  const View = VIEWS[viewType];

  async function onChangeView(event, action, record) {
    let _viewType = viewType;
    if (action !== "return") {
      if (!record) {
        nextId = nextId - 1;
        const [toStore, toActive] = await tool.getDefaults(nextId, ctxView);
        upStoreRecord(toStore);
        upActiveRecord(toActive);
      } else {
        // upStoreRecord({ id: record.id });
        setActiveRecordFromId(record.id, ctxView.model, ctxView);
      }
    }
    if (["edit", "add"].includes(action)) {
      _viewType = "form";
    } else if (["editModal", "addModal"].includes(action)) {
      _viewType = "modalForm";
    } else {
      _viewType = "list";
    }
    setViewType(_viewType);
  }

  function onClickAction(event, action) {
    setAction(action);
  }

  return (
    <div id="board" className="mx-3 my-3 lg:mx-4 w-full">
      <BoardHeader
        ctxView={ctxView}
        viewType={viewType}
        onChangeView={onChangeView}
        onClickAction={onClickAction}
      />
      <View
        ctxView={ctxView}
        onChangeView={onChangeView}
        session={session}
        action={action}
        level="main"
      />
    </div>
  );
}

export default Board;
