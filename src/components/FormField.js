import React from "react";
import { FormattedMessage as FM } from "react-intl";

import ColForm from "components/ColForm";

function FormField(props) {
  let required = false;
  if (typeof props.required === "function") {
    required = props.required(props.record);
  } else if (props.required) {
    required = props.required;
  }

  return (
    <ColForm {...props}>
      {props.label && (
        <label
          className="flex text-sm mt-3 mb-0.5 ml-1 font-bold text-zinc-600 max-h-5"
          htmlFor={props.name}
        >
          <FM id={props.label} key={props.label} />
          {required && <p className="text-red-500">&nbsp; *</p>}
        </label>
      )}
      {props.children}
    </ColForm>
  );
}

export default FormField;
