import React, { useState } from "react";

import Table from "components/Table";
import BodyTable from "components/BodyTable";
import TableHeader from "components/TableHeader";
import Loading from "components/Loading";
import Pagination from "./Pagination";
import ModalForm from "components/Modals/ModalForm";
import { useFormStore } from "store/formStore";

/**
 * Component for create table with body and header
 *
 * @param {object} props - component props
 * @param {boolean} props.sortable - boolean define if column is sortable
 * @param {Objetc} props.ctxView - define model view
 * @param {Array} props.records - Array of records to fill in
 * @param {function} props.updateRecords - Function for update records parent
 * @param {function} props.onClickRow - Function for handle event onClickRow
 * @param {function} props.onClickCell - Function for handle event onClickCell
 * @param {boolean} props.paginate - Define if table is paginate
 * @return {Component} - table component
 */
function FullTable(props) {
  const {
    ctxView,
    records,
    limit,
    selectable,
    isLoading,
    reloadRecords,
    offset,
  } = props;
  const { model } = ctxView;
  const [openModalForm, setOpenModalForm] = useState(false);
  const [activePage, setActivePage] = useState(1);
  const { activeRecord, upStoreRecord, setActiveRecordFromId } = useFormStore();

  function handleSort() {
    // Add sort function
  }

  function onChangePage(arrow, page) {
    let _activePage = activePage;
    if (page) {
      _activePage = page;
    } else if (arrow === "next") {
      _activePage = activePage + 1;
    } else {
      _activePage = activePage - 1;
    }
    props.onChangePage(_activePage);
    setActivePage(_activePage);
  }

  async function onChangeView(event, action, _record) {
    if (action !== "delete") {
      setActiveRecordFromId(_record.id, model, ctxView);
    }
    if (ctxView.table_action.includes("open")) {
      upStoreRecord({ id: _record.id });
      setOpenModalForm(true);
    } else {
      props.onChangeView(event, action, _record);
    }
  }

  if (isLoading) return <Loading />;

  let mode;
  if (activeRecord && activeRecord.id > 0) {
    mode = "edit";
  }
  return (
    <div
      id="fulltable"
      className="overflow-x-scroll xl:overflow-visible max-h-fit block ring-1 ring-black ring-opacity-5 md:rounded-lg"
    >
      <Table>
        <TableHeader ctxView={ctxView} handleSort={handleSort} />
        <BodyTable
          {...props}
          ctxView={ctxView}
          selectable={selectable || ctxView.selectable}
          records={records}
          onChangeView={onChangeView}
          reloadRecords={reloadRecords}
        />
      </Table>
      {ctxView.pagination && (
        <Pagination
          lenRecords={records?.length}
          limit={limit}
          offset={offset}
          ctxView={ctxView}
          onChangePage={onChangePage}
        />
      )}
      {openModalForm && activeRecord && (
        <ModalForm
          ctxView={ctxView}
          onClose={() => setOpenModalForm(false)}
          open={openModalForm}
          level="main"
          mode={mode}
        />
      )}
    </div>
  );
}

export default FullTable;
