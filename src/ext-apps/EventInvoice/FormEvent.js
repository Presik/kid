import React, { useState } from "react";
import { FormattedMessage as FM } from "react-intl";

import StdButton from "components/StdButton";
import TextField from "components/TextField";
import TextAreaField from "components/TextAreaField";
import { color } from "theme";

function FormEvent(props) {
  const data = {
    firstname: "",
    familyname: "",
    jobtitle: "",
    organization: "",
    comments: "",
  };
  const [formData, setFormData] = useState(data);

  const handleChange = (field, value) => {
    formData[field] = value;
    setFormData(formData);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.onSubmit(formData);
  };

  return (
    <form onSubmit={(e) => handleSubmit(e)}>
      <div id="form-page" style={styles.row_main}>
        <div id="form-title" width={16} style={styles.col_title}>
          <h4 style={styles.title}>{props.title}</h4>
        </div>
        {/* {
              Object.keys(data).map((field)=>{
              <TextField
                name={field}
                label={<FM id={`eventinvoice.${field}`} />}
                variant="outlined"
                className="invoice-text"
                onChange={handleChange}
              />
              })
            } */}
        <TextField
          name="firstname"
          label={<FM id="eventinvoice.firstname" />}
          variant="outlined"
          className="invoice-text"
          onChange={handleChange}
        />
        <TextField
          name="familyname"
          label={<FM id="eventinvoice.familyname" />}
          variant="outlined"
          className="invoice-text"
          onChange={handleChange}
        />
        <TextField
          type="text"
          name="jobtitle"
          label={<FM id="eventinvoice.jobtitle" />}
          onChange={handleChange}
          className="eventinvoice-text"
          variant="outlined"
        />
        <TextField
          type="text"
          name="organization"
          label={<FM id="eventinvoice.organization" />}
          onChange={handleChange}
          className="invoice-text"
          variant="outlined"
        />
        <TextAreaField
          name="comment"
          label={<FM id="eventinvoice.comment" />}
          variant="outlined"
          onChange={handleChange}
        />

        <div width={16} className="invoice-col-button">
          <StdButton className="invoice-button" type="submit">
            <FM id="eventinvoice.button_enter" />
          </StdButton>
        </div>
      </div>
    </form>
  );
}

const styles = {
  row_main: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
    paddingBottom: 20,
    marginLeft: 10,
    marginRight: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  col_title: {
    height: 60,
    display: "flex",
    justifyContent: "center",
  },
  message: {
    lineHeight: "24px",
    color: color.salmon,
  },
  div_children: {
    display: "flex",
    flexWrap: "wrap",
    marginTop: 30,
  },
  title: {
    color: "rgb(149, 149, 149)",
    marginTop: "auto",
    marginBottom: "auto",
  },
};

export default FormEvent;
