import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useRecords } from "hooks/records";

import FormEvent from "./FormEvent";
import proxy from "api/proxy";

const fields_names = [
  "invoice_date",
  "number",
  "event.date_acknowledgment",
  "event.date_acceptance",
  "event.date_tacit_acceptance",
  "event.date_of_rejection",
];
const options = { accept: "Aceptacion", acuse: "Acuse", reject: "Rechazo" };

function InvoiceEvent() {
  const params = useParams();
  const { data } = useInvoice(params);
  const [contentInvoice, setContentInvoice] = useState("");
  const [form, setForm] = useState(true);
  const title = `${options[params.option]} de Factura Electronica`;

  async function handleSubmitEvent(formData) {
    setForm(false);
    // setContentInvoice(`Factura electronica ${data[0].number} rechazada`)
    formData["option"] = "reject";
    formData["id"] = data[0].id;
    sendEvent(formData);
  }

  async function sendEvent(args) {
    let event = {
      model: "account.invoice",
      method: "process_event",
      args: args,
    };
    proxy.method(event);
  }

  useEffect(() => {
    let contInvoice = "Documento Electronico no encontrado";
    let viewForm = true;
    if (data) {
      const invoice = data[0];
      if (invoice["event."]) {
        contInvoice = "";
        console.log("ingresa a esta linea continvoice");
        viewForm = false;
      } else {
        if (params.option === "reject") {
          contInvoice = `Factura Electronica ${invoice.number} Rechazada`;
        } else {
          sendEvent({ option: "accept", id: invoice.id });
          contInvoice = `Factura Electronica ${invoice.number} Aceptada Expresamente`;
        }
      }
    }
    setForm(viewForm);
    setContentInvoice(contInvoice);
  }, [data]);

  function onClose() {}

  return (
    <div>
      <div>
        {["acuse", "reject"].includes(params.option) && form ? (
          <FormEvent
            title={title}
            onSubmit={(formData) => handleSubmitEvent(formData)}
            handleClose={onClose}
          />
        ) : (
          <div className="container">
            <h2 className="title">{title}</h2>
            <p className="paragraph">{contentInvoice}</p>
          </div>
        )}
      </div>
    </div>
  );
}
export default InvoiceEvent;

function useInvoice(params) {
  return useRecords(
    "account.invoice",
    `[['cufe', '=', '${params.invoiceId}']]`,
    fields_names,
    1,
  );
}
