const TYPE_DOC = [
  { id: "passport", name: "passport" },
  { id: "driver_license", name: "driver_license" },
  { id: "medicare_card", name: "medicare_card" },
];
const SUBDIVISION = [
  // { id: "", name: "" },
  { id: "ACT", name: "Australian Capital Territory" },
  { id: "NSW", name: "New South Wales" },
  { id: "NT", name: "Northern Territory" },
  { id: "QLD", name: "Queensland" },
  { id: "SA", name: "South Australia" },
  { id: "TAS", name: "Tasmania" },
  { id: "VIC", name: "Victoria" },
  { id: "WA", name: "Western Australia" },
];

export { TYPE_DOC, SUBDIVISION };
