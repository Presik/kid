import React, { Fragment, useState, useRef } from "react";
import { useNavigate, useLocation } from "react-router-dom";

import TextField from "components/TextField";
import SelectionField from "components/SelectionField";
import Checkbox from "components/CheckboxField";
import StdButton from "components/StdButton";
import TwComboBox from "components/TwComboBox";
import DateField from "components/DateField";
import DatetimeField from "components/DatetimeField";
import ModalMsg from "components/Modals/ModalMsg";
import proxy from "api/proxy-old";
import func from "tools/functions";
import { checkRequiredForm } from "tools/form";
import { TYPE_DOC, SUBDIVISION } from "./constants";
import IntegerField from "components/IntegerField";
import SignatureCanvas from "react-signature-canvas";
import dates from "tools/dates";
import TextFieldGeocoding from "components/TextFieldGeocoding";
const STYLE_LABEL_ADDRESS = "bg-gray-100 rounded-md shadow-md  px-3 py-1";

function FormAddRentalBooking(props) {
  const location = useLocation();
  let [btnDisabled, setBtnDisabled] = useState(true);
  const [disabledLapse, setDisabledLapse] = useState(true);
  let [store, setStore] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [openModal, setOpenModal] = useState(null);
  const [error, setError] = useState(null);
  const navigate = useNavigate();
  const signatureCanvasRef = useRef();
  const storeRequired = [
    "first_name",
    "last_name",
    "doc_number",
    "email",
    "mobile",
    "type_document",
  ];

  async function acceptCreate() {
    var emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;

    var mobileRegex = /^[0-9]{10}$/;

    if (!emailRegex.test(store.email)) {
      setError("The email is not valid.");
      return false;
    }

    if (!mobileRegex.test(store.mobile)) {
      setError("Mobile phone number is not valid.");
      return false;
    }
    setIsLoading(true);
    if (Object.keys(store).length > 1) {
      const _store = func.recToTryton(store);
      const fieldsNames = ["id"];
      _store.state = "draft";
      _store.subdivision = 1;
      // _store.
      console.log("store sin tratamiento", store);
      console.log("store tratamiento con", _store);
      // return false;
      let res = await proxy.create("rental.booking", _store, fieldsNames);
      console.log(res[0], "este es el resssss");

      if (res) {
        setIsLoading(false);
        setError(null);
        setStore({});
        setOpenModal({
          msg: "Reservation Created!",
        });
        // props.onAdd();
        // setStore({});
        let sendValidation = await proxy.methodInstance({
          model: "rental.booking",
          method: "send_email_confirmation",
          instance: res[0],
        });
        console.log("res", sendValidation);
      } else {
        setOpenModal({
          msg: "Error al crear reserva",
        });
      }
    }
  }

  function onCloseModal() {
    setOpenModal(null);
    navigate(1);
  }
  const onChangeAdress = (field, value) => {
    let _storeRecord = { ...store };
    console.log(field, "value - ", value);
    _storeRecord.post_code = value.post_code;
    _storeRecord.city = value.city;
    _storeRecord.suburb = value.suburb;
    _storeRecord.address = value.address;
    setStore(_storeRecord);
  };
  function onChange(field, value) {
    if (field == "start_date" || field == "lapse_time") {
      if (field == "start_date") {
        setDisabledLapse(false);
      } else if (field == "lapse_time") {
        console.log(value, field);

        let end_date = new Date(store.start_date);
        console.log(store.start_date);
        end_date.setDate(end_date.getDate() + value * 7);
        store.end_date = end_date;
      }
    } else if (field == "subdivision") {
      value = value.id;
    }

    store[field] = value;
    const newStore = { ...store };
    const res = checkRequiredForm(newStore, storeRequired);
    setBtnDisabled(!res);
    setStore(newStore);
  }

  async function handelCancel() {
    setIsLoading(false);
    navigate(-1);
  }

  const onChangeWeek = (field, value) => {
    // store[field] = value;
    // const newStore = { ...store };
    // const res = checkRequiredForm(newStore, storeRequired);
    // setBtnDisabled(!res);
    // setStore(newStore);
  };

  const clearSignature = () => {
    signatureCanvasRef.current.clear();
  };

  const saveSignature = () => {
    const signatureDataURL = signatureCanvasRef.current.toDataURL();
    // Aquí puedes hacer algo con la URL de la firma, como enviarla a un servidor.
  };

  return (
    <form className="grid grid-cols-2 gap-5 w-full">
      {!props.id ? (
        <Fragment>
          {/* <p className="w-full col-span-2 text-gray-500">
            Fill your personal data with real information.
          </p> */}

          <TextField
            name="first_name"
            label="rental.booking.first_name"
            value={store.first_name || ""}
            required={true}
            onChange={onChange}
            translate={true}
            specialCharacters={true}
            type="char"
          />
          <TextField
            name="last_name"
            label="rental.booking.last_name"
            value={store.last_name || ""}
            required={true}
            onChange={onChange}
            translate={true}
            specialCharacters={true}
            type="char"
          />
          <SelectionField
            name="type_document"
            label="rental.booking.type_document"
            value={store.type_document}
            onChange={onChange}
            required={true}
            options={TYPE_DOC}
            translate={true}
          />
          <TextField
            name="doc_number"
            label="rental.booking.doc_number"
            value={store.doc_number || ""}
            required={true}
            onChange={onChange}
            translate={true}
          />
          <TextField
            name="mobile"
            label="rental.booking.mobile"
            value={store.mobile || ""}
            required={true}
            onChange={onChange}
            translate={true}
          />
          <TextField
            name="email"
            label="rental.booking.email"
            value={store.email || ""}
            required={true}
            onChange={onChange}
            translate={true}
            type="email"
          />
          <p className="w-full col-span-2 text-gray-500">
            Now choose the number of weeks you want to rent and select the start
            day
          </p>
          <div className="col-span-2 flex gap-5 p-3 bg-gray-100 rounded-md">
            <DatetimeField
              id="start_date"
              name="start_date"
              value={store.start_date}
              label="rental.booking.start_date"
              onChange={onChange}
              translate={true}
            />
            <div className="w-2/4">
              <IntegerField
                id="lapse_time"
                name="lapse_time"
                value={store.lapse_time || 1}
                label="rental.booking.week"
                onChange={onChange}
                translate={true}
                readOnly={disabledLapse}
              />
            </div>
            <DatetimeField
              id="end_date"
              name="end_date"
              value={store.end_date}
              label="rental.booking.end_date"
              // onChange={onChange}
              translate={true}
              readOnly={true}
            />
          </div>
          <div className="flex col-span-2 space-x-4">
            {store.subdivision && (
              <span className={STYLE_LABEL_ADDRESS}>
                <b>State: </b>
                {store.subdivision}
              </span>
            )}
            {store.city && (
              <span className={STYLE_LABEL_ADDRESS}>
                <b>City: </b>
                {store.city}
              </span>
            )}
            {store.suburb && (
              <span className={STYLE_LABEL_ADDRESS}>
                <b>Suburb: </b>
                {store.suburb}
              </span>
            )}
            {/* {store.address && (
              <span className={STYLE_LABEL_ADDRESS}>{store.address}</span>
            )} */}
            {store.post_code && (
              <span className={STYLE_LABEL_ADDRESS}>
                <b>Post code: </b>
                {store.post_code}
              </span>
            )}
          </div>
          <div className="col-span-2 flex gap-5 p-3 bg-gray-100 rounded-md z-0 relative">
            <div className="w-ful md:w-1/3 z-20">
              <SelectionField
                id="subdivision"
                name="subdivision"
                value={store.subdivision || ""}
                label="rental.booking.subdivision"
                onChange={onChange}
                // translate={true}
                options={SUBDIVISION}
              />
            </div>
            <TextFieldGeocoding
              name="address"
              label="rental.booking.address"
              value={store.address || ""}
              country="Australia" // Establece el país por defecto como Australia
              stateAcronym="NSW"
              // required={true}
              onChange={onChangeAdress}
              translate={true}
              apiGeocoding="eac36f481e2f4feba53554b8572ca819"
            />
          </div>
          {error && (
            <span className="grid py-2 gap-3 justify-center col-span-2 bg-red-300 w-full rounded-md px-20 text-red-800">
              {error}
            </span>
          )}
          <div className="grid py-5 gap-3 justify-center col-span-2">
            <StdButton
              key="add"
              color="blue"
              disabled={btnDisabled}
              loading={isLoading}
              onClick={acceptCreate}
              size="w-full"
              content="rental.service.appbooking"
            />
            {/* <StdButton
          key="cancel"
          color="red"
          onClick={handelCancel}
          size="w-full"
          noFill={true}
          content="Cancelled"
        /> */}
          </div>
        </Fragment>
      ) : (
        <Fragment>
          {" "}
          <div className="border border-gray-400 p-3 col-span-2 mx-20">
            <SignatureCanvas
              ref={signatureCanvasRef}
              penColor="black"
              canvasProps={{
                width: 500,
                height: 100,
                className: "signature-canvas",
              }}
            />
            <button onClick={clearSignature} className="mr-5">
              Clean
            </button>
            <button onClick={saveSignature}>Add</button>
          </div>
          <div className="grid py-5 gap-3 justify-center col-span-2">
            <StdButton
              key="add"
              color="blue"
              disabled={btnDisabled}
              loading={isLoading}
              onClick={acceptCreate}
              size="w-full"
              content="rental.service.appbooking"
            />
            {/* <StdButton
          key="cancel"
          color="red"
          onClick={handelCancel}
          size="w-full"
          noFill={true}
          content="Cancelled"
        /> */}
          </div>
        </Fragment>
      )}

      {openModal && (
        <ModalMsg
          open={true}
          onClose={onCloseModal}
          buttons={["close"]}
          type="success"
          msg={"Validate your email"}
        />
      )}

      {/* {props.id && (
        <ModalMsg
          open={true}
          onClose={onCloseModal}
          buttons={["close"]}
          type="success"
          msg={"Correo validado correctamente, continua con la reserva"}
        />
      )} */}
    </form>
  );
}

export default FormAddRentalBooking;
