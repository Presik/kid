import React, { Fragment } from "react";
import FormAddRentalBooking from "./FormAddRentalBooking";
import { useParams } from "react-router-dom";
// import Logo from "../../assets/img/logo_original.png";
import RentalRequirements from "./RentalRequirements";
import BookingMap from "./BookingMap";
import {
  ChatBubbleOvalLeftIcon,
  EnvelopeIcon,
} from "@heroicons/react/20/solid";
const Logo = "https://renagew.com/wp-content/uploads/2017/06/logo88.png";
const BACKGROUND =
  "https://renagew.com/wp-content/uploads/2023/05/20190908_085107-e1685533096893.jpg";

const STYLE_ITEM_CONTACT = "flex flex-col items-center gap-3";
const WebBookingRental = (props) => {
  const { id } = useParams();
  // console.log(id);

  return (
    <div className="grid grid-cols-2 h-screen">
      <img src={Logo} className="w-52 absolute top-3" />

      <div className="p-10 flex items-center">
        {id ? (
          <BookingMap id={id} />
        ) : (
          <FormAddRentalBooking {...props} id={id} />
        )}
      </div>
      <div
        className={`relative flex ${!id ? "px-20" : "px-0"} items-center`}
        style={{ backgroundImage: BACKGROUND, background: BACKGROUND }}
      >
        {!id ? (
          <Fragment>
            <RentalRequirements />

            <img
              src="https://renagew.com/wp-content/uploads/2023/05/20190908_085107-e1685533096893.jpg"
              className="w-full left-0 top-0 h-full object-cover absolute z-0"
            />
          </Fragment>
        ) : (
          <Fragment>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3540.3078343818847!2d153.02896525977167!3d-27.459674216332303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9159f6b92e98eb%3A0x240fe0bbf83ca1df!2s48%20Warren%20St%2C%20Fortitude%20Valley%20QLD%204006%2C%20Australia!5e0!3m2!1ses-419!2sco!4v1692224866442!5m2!1ses-419!2sco"
              width="100%"
              height="100%"
              allowfullscreen=""
              loading="lazy"
              referrerpolicy="no-referrer-when-downgrade"
            ></iframe>
            <div className="flex absolute w-full left-0 z-10 bg-blue-presik bottom-0 h-[10vh] text-white align-middle items-center px-10">
              <ul className="flex justify-between w-full ">
                <li className={STYLE_ITEM_CONTACT}>
                  <ChatBubbleOvalLeftIcon className="w-8 text-center" /> SMS :
                  0414 644 667
                </li>
                <li className={STYLE_ITEM_CONTACT}>
                  {" "}
                  <ChatBubbleOvalLeftIcon className="w-8 text-center" /> 0414
                  644 667
                </li>
                <li className={STYLE_ITEM_CONTACT}>
                  <EnvelopeIcon className="w-8 text-center" /> sales@renagew.com
                </li>
                <li className={STYLE_ITEM_CONTACT}>
                  <EnvelopeIcon className="w-8 text-center" /> renagewaustralia
                </li>
              </ul>
            </div>
          </Fragment>
        )}
      </div>
    </div>
  );
};

export default WebBookingRental;
