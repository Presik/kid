import React, { Fragment } from "react";
import { Switch, Route, Link } from "react-router-dom"; // Importa las rutas de React Router
import Header from "./layout/Header";
import Footer from "./layout/Footer";
import { MENU, PRODUCTS, TESTIMONIALS } from "./fakeData";
import NavPage from "./components/NavPage";
import PageRestaurant from "./pages/PageRestaurant";
import SliderProducts from "./components/SliderProducts";
import SectionWhy from "./components/SectionWhy";
import SectionResena from "./components/SectionResena";
import { NavCategories } from "./components/NavCategories";
import SliderTestimonials from "./components/SliderTestimonials";
import SectionPeople from "./components/SectionPeople";

const ScreenMelhous = () => {
  const SectionMain = () => {
    return (
      <div
        className="rounded-2xl  overflow-hidden my-5 relative bg-no-repeat bg-cover p-5 flex  md:h-64 "
        style={{
          backgroundImage:
            "url(https://images.unsplash.com/photo-1600891964599-f61ba0e24092?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8cmVzdGF1cmFudCUyMGZvb2R8ZW58MHx8MHx8fDA%3D&w=1000&q=80)",
        }}
      >
        <div className="bg-gradient-to-r from-black via-black to-transparent absolute w-full h-full left-0 top-0" />

        <div
          className="text-white p-10 w-full z-10 relative border flex flex-col md:flex-row md:space-x-20 items-center"
          style={{
            borderImage: "linear-gradient(to right, #ffcc00, #fdd01f4d)",
            borderImageSlice: 1,
          }}
        >
          <div>
            <h1 className="text-2xl font-semibold uppercase melhous-menu-title">
              Restaurante Bufifur
            </h1>
            <div className="border-y w-[120px] mb-3"></div>
            <h2 className="text-1xl font-semibold uppercase text-yellow-500 melhous-menu-title">
              EL mejor servicio
            </h2>
          </div>
          <p className="max-w-md text-left melhous-menu-description text-lg">
            Te invitamos a explorar y disfrutar de una amplia gama de platos
            deliciosos que hemos preparado con esmero. Además, descubre la
            belleza y comodidades de todos nuestros espacios comunes en el
            hotel, diseñados para hacer de tu estancia una experiencia
            inolvidable.
          </p>
          {/* <button className="py-2 px-8 bg-white rounded-full text-black">
            Button orden
          </button> */}
        </div>
        <div className="hidden md:block relative">
          {/* <ItemChildBlur top="48" rigth="72" text="DESAYUNOS" />
          <ItemChildBlur top="10" rigth="10" text="ALMUERZOS" />
          <ItemChildBlur top="32" rigth="44" text="ALMUERZOS" />
          <ItemChildBlur top="52" rigth="10" text="ALMUERZOS" />
          <ItemChildBlur top="20" rigth="96" text="ALMUERZOS" /> */}
        </div>
      </div>
    );
  };
  const ItemChildBlur = ({ top, rigth, text }) => {
    return (
      <div
        className={`bg-white/20 rounded-full   flex px-2 space-x-3 pr-4 py-2 absolute right-${rigth} top-${top} backdrop-blur-lg`}
      >
        <span className="h-5 w-5 bg-yellow-500 rounded-full"></span>
        <span> {text}</span>
      </div>
    );
  };
  return (
    <Fragment>
      <Header />
      <main className="container mx-auto min-h-[80vh] ">
        <section className="mt-16">
          <NavPage data={MENU} />
        </section>
        <section className="relative">
          <SectionMain />
        </section>
        <section>
          <hr className="border-t border-yellow-500 h-1 mt-10" />
          <SliderProducts data={PRODUCTS} />
        </section>

        <section
          className="bg-black h-[70vh] md:w-screen md:-left-40 md:mt-28 relative bg-no-repeat bg-cover "
          style={{
            backgroundImage:
              "url(https://res.cloudinary.com/dl4sdiukx/image/upload/v1697751539/i55sbvmcbyqv1lfxnfzs.jpg)",
          }}
        >
          <div className=" bg-gradient-to-l from-black/20 via-black/60 to-black/100 h-full w-full absolute" />

          <SectionResena />
        </section>
        <section>
          <SectionPeople />
        </section>
        <section>
          <SectionWhy />
        </section>
        <section>
          <SliderTestimonials data={TESTIMONIALS} />
        </section>
      </main>
      <Footer />
    </Fragment>
  );
};

export default ScreenMelhous;
