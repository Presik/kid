import React from "react";
import TitleSection from "../components/ui/TitleSection";
import TextField from "components/TextField";

const PageForm = () => {
  return (
    <div className="bg-[#f1eae2] max-w-3xl mx-auto py-5 px-5  rounded-2xl ">
      <div
        className="p-5 w-full h-full border min-h-[500px] flex flex-col justify-center items-center"
        style={{
          borderImage: "linear-gradient(to right, #ffcc00, #fdd01f4d)",
          borderImageSlice: 1,
        }}
      >
        <TitleSection text="Ingresa " />
        <form className="w-[400px] mx-auto grid grid-col-2">
          <TextField label="Nombre" />
          <TextField label="Password" />
        </form>
      </div>
    </div>
  );
};

export default PageForm;
