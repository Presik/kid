import React from "react";
import { Transition } from "@headlessui/react";
import { PRODUCTS } from "../fakeData";
import TitleSection from "../components/ui/TitleSection";
import { NavCategories } from "../components/NavCategories";

const PageRestaurant = (props) => {
  console.log(props, "hola");
  const data = PRODUCTS;

  const groupedData = new Map();

  data.forEach((item) => {
    const { category } = item;

    if (!groupedData.has(category)) {
      groupedData.set(category, []);
    }

    groupedData.get(category).push(item);
  });

  const groupedArray = [...groupedData.values()];

  return (
    <div className="space-y-10">
      <TitleSection text="MENÚ" />
      <NavCategories mode="left" />
      <div className="grid grid-cols-1 gap-4 md:grid-cols-2">
        {groupedArray.map((category, index) => (
          <Transition
            key={index}
            as={React.Fragment}
            show={true} // Puedes cambiar esto a true o false según tus necesidades
            enter="transition ease-in-out duration-300 transform opacity-0"
            enterFrom="opacity-0 translate-y-4"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in-out duration-300 transform opacity-0"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-4"
          >
            {(show) => (
              <div
                className={` px-5 py-5 rounded-lg md:w-full bg-[#f1eae2]  ${
                  show ? "opacity-100" : "opacity-0"
                }`}
              >
                <div className=" border border-yellow-500 h-full ">
                  <img
                    src={category[0].image}
                    className="w-full h-56 object-cover"
                  />
                  <div className="p-5">
                    <h3 className="md:text-2xl relative -top-11 w-fit  bg-black px-5 rounded-2xl py-2 text-yellow-500 melhous-menu-title uppercase">
                      {category[0].category}
                    </h3>
                    <p className="text-gray-900 melhous-menu-description">
                      {category[0].description}
                    </p>
                    <div className="">
                      {category.map((product, productIndex) => (
                        <div
                          key={productIndex}
                          className="flex items-end rounded-lg overflow-hidden active:scale-95 transition duration-100 cursor-pointer relative"
                        >
                          <div className=" to-transparent  pb-3 pt-5 w-full text-gray-900 flex flex-col md:flex-row space-x-5 items-center">
                            <div className="w-full md:w-auto">
                              <img
                                src={product.image}
                                className="md:w-[200px]  w-full h-28 object-cover rounded-md max-w-none"
                              />
                            </div>
                            <div className="w-full text-white mt-4 md:mt-0">
                              <div className=" flex space-x-5 items-center">
                                <h3 className="text-sm font-semibold md:min-w-max text-gray-700 melhous-menu-title uppercase  ">
                                  {product.name}
                                </h3>
                                <div className="w-full border-b border-dashed border-gray-800 hidden md:block"></div>
                                <div className=" md:relative absolute top-0 right-0 bg-black  md:bg-transparent p-2 md:p-0 rounded-full">
                                  <span className="text-3xl font-medium text-yellow-500 melhous-menu-title">
                                    <span className="text-xs text-gray-700 relative -top-2">
                                      $
                                    </span>
                                    {product.price}
                                  </span>
                                </div>
                              </div>
                              <p className="text-gray-600 text-[15px] melhous-menu-description">
                                {product.description}
                              </p>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            )}
          </Transition>
        ))}
      </div>
    </div>
  );
};

export default PageRestaurant;
