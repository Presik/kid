import React from "react";
const STYLE_ITEM_CONTAINER = "md:p-8 py-20 bg-cover space-y-2  relative w-full";
const STYLE_ITEM_TITLE =
  "md:text-4xl text-xl uppercase font-medium melhous-menu-title";
const COLOR_THEME = "bg-[#004a3c]/80";
// bg-[#004a3c]/80

const SectionWhy = () => {
  return (
    <div className="md:w-screen w-full md:h-[60vh]  md:mb-20 grid grid-cols-1  md:grid-cols-3 relative md:-left-40 text-white bg-black  justify-items-center place-items-center text-center ">
      <div
        className="h-full w-full flex flex-col justify-center relative "
        style={{
          backgroundImage:
            "url(http://demo.stairthemes.com/dine-inn/wp-content/uploads/sites/18/2023/09/image01.jpg)",
        }}
      >
        <div className="bg-black/80 h-full w-full absolute left-0 top-0 backdrop-blur-sm hover:backdrop-blur-0" />

        <div className={STYLE_ITEM_CONTAINER}>
          <h2 className={STYLE_ITEM_TITLE}>Desayuno incluido</h2>
          <p className="max-w-[300px] mx-auto melhous-menu-description text-[17px] md:text-[20px]  font-normal">
            Texto teto texto trexotas a asda sa dasd asd as dasda sdas dad
          </p>
        </div>
      </div>
      <div
        className="h-full w-full flex flex-col justify-center relative scale-110 z-10 "
        style={{
          backgroundImage:
            "url(https://res.cloudinary.com/dl4sdiukx/image/upload/v1697552452/tur0mrqxlrmp1jpc0hcd.jpg)",
        }}
      >
        <div
          className={`${COLOR_THEME} h-full w-full absolute left-0 top-0 backdrop-blur-sm hover:backdrop-blur-0`}
        />

        <div className={STYLE_ITEM_CONTAINER}>
          <h2 className={`${STYLE_ITEM_TITLE} text-yellow-500`}>
            Servicio 24/7
          </h2>
          <p className="max-w-[300px] mx-auto melhous-menu-description text-[17px] md:text-[20px] font-normal">
            Texto teto texto trexotas a asda sa dasd asd as dasda sdas dad
          </p>
        </div>
      </div>
      <div
        className="h-full w-full flex flex-col justify-center relative "
        style={{
          backgroundImage:
            "url(https://res.cloudinary.com/dl4sdiukx/image/upload/v1697552496/fr7i5a7bp8tiqqfyiaqx.jpg)",
        }}
      >
        <div className="bg-black/80 h-full w-full absolute left-0 top-0 backdrop-blur-sm hover:backdrop-blur-0" />

        <div className={STYLE_ITEM_CONTAINER}>
          <h2 className={STYLE_ITEM_TITLE}>espacios comodos</h2>
          <p className="max-w-[300px] mx-auto melhous-menu-description text-[17px] md:text-[20px] font-normal">
            Texto teto texto trexotas a asda sa dasd asd as dasda sdas dad
          </p>
        </div>
      </div>
    </div>
  );
};

export default SectionWhy;
