import { StarIcon } from "@heroicons/react/24/solid";
import React from "react";

const SectionResena = () => {
  const TextComment = ({
    left,
    top,
    scale,
    blur = false,
    text,
    name,
    image,
  }) => {
    return (
      <div
        className={`flex absolute ${left} ${top} ${scale} ${
          blur ? "blur-sm" : ""
        }  max-w-[350px] space-x-2`}
      >
        <img
          src={image}
          className="w-12 h-12 object-cover rounded-full border-2 border-yellow-500"
        />
        <div className="bg-white/80 backdrop-blur-lg p-3 rounded-md ">
          <span>{name}</span>
          <p className="text-[12px]">{text}</p>
        </div>
      </div>
    );
  };
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 md:p-8 h-full md:my-48  relative  ">
      <div className="z-20 hidden md:block ">
        <TextComment
          left="left-[50px]"
          top="-top-[60px]"
          text="¡Una experiencia increíble! El lugar es asombroso y la comida es deliciosa. Definitivamente, volveré pronto."
          image="https://img.freepik.com/free-photo/portrait-white-man-isolated_53876-40306.jpg?size=626&ext=jpg&ga=GA1.2.386372595.1697500800&semt=ais"
          scale="scale-110"
          name="OSCAR ALVAREZ"
        />

        <TextComment
          left="left-[30%]"
          top="top-[0px]"
          text="La estancia en este lugar fue maravillosa. El ambiente es encantador, y el personal es muy amable y servicial. ¡Recomiendo este hotel!"
          image="https://www.inboundcycle.com/hubfs/layout/v3/img/email/gmail-signatures/team-01/svercheval.jpg"
          scale="scale-50"
          name="LAURA MONTAÑO"
        />

        <TextComment
          left="-left-[0px]"
          top="top-[50%]"
          text="Un lugar perfecto para descansar y relajarse. Los servicios son excelentes, y la ubicación es ideal. ¡Sin duda, una experiencia inolvidable!"
          image="https://images.pexels.com/photos/614810/pexels-photo-614810.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
          name="FABIAN DIAZ"
        />

        <TextComment
          left="left-20%]"
          top="top-[70%]"
          text="Este lugar es un paraíso en la tierra. La comida es deliciosa, y la decoración es impresionante. Además, la atención es impecable. ¡Altamente recomendado!"
          image="https://static.wixstatic.com/media/ca0cf0_1195c4648655466cbf1d524da971e85e~mv2.jpg/v1/fill/w_422,h_434,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/Willy%20Eyzaguirre1_JPG.jpg"
          name="ALEXANDER FUQUEN"
          scale="scale-50"
          blur={true}
        />

        <TextComment
          left="left-[34%]"
          top="top-[100%]"
          text="No puedo decir suficientes cosas buenas sobre este hotel. Sus espacios comunes es sorprendente, y la comida es una delicia. "
          image="https://static.wixstatic.com/media/ca0cf0_1195c4648655466cbf1d524da971e85e~mv2.jpg/v1/fill/w_422,h_434,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/Willy%20Eyzaguirre1_JPG.jpg"
          scale="scale-110"
          name="VICTOR RUIZ"
        />

        <TextComment
          left="left-[35%]"
          top="top-[40%]"
          text="Mi estancia en este hotel superó todas mis expectativas. La decoración es elegante, la comida es deliciosa y el personal es amable. ¡Una experiencia inolvidable!"
          image="https://static.wixstatic.com/media/ca0cf0_4863d4a97c504c9d92d7feeddf061022~mv2.jpg/v1/crop/x_844,y_0,w_2640,h_2656/fill/w_422,h_434,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/Hernan%20Montenegro1_JPG.jpg"
          scale="scale-75"
          name="WILSON GOMEZ"
        />
      </div>
      <div className="text-white p-5 space-y-5 z-10 relative   h-full bg-black/70 md:rounded-2xl overflow-hidden backdrop-blur-md w-full">
        <div className=" h-full p-5 md:p-16 flex flex-col justify-center space-y-3">
          <h2 className="md:text-4xl uppercase font-bold melhous-menu-title md:leading-[52px] ">
            CONTAMOS CON LA MEJOR CALIFICACIÓN
          </h2>
          <div className="flex space-x-2">
            <StarIcon className="w-8 text-yellow-500" />
            <StarIcon className="w-8 text-yellow-500" />
            <StarIcon className="w-8 text-yellow-500" />
            <StarIcon className="w-8 text-yellow-500" />
            <StarIcon className="w-8 text-yellow-500" />
          </div>
          <p className=" melhous-menu-description md:text-lg">
            Contamos con una excelente calificacion y comentarios en todas las
            plataformas de reseñas.
          </p>
          <div>
            <h3 className="md:text-xl text-base uppercase  melhous-menu-title leading-[52px] mb-5">
              Descarga nuestra App
            </h3>
            <img
              src="https://www.pngmart.com/files/10/Google-Play-App-Store-PNG-Transparent-Image.png"
              className="w-[300px]"
            />
          </div>
          <button className="text-left underline text-yellow-500 melhous-menu-description uppercase">
            Ver calificaciones
          </button>
        </div>
      </div>

      <img
        src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697811024/zuzgxmwc5aztidot2mmu.png"
        className="absolute w-[50vw] -top-[45vh] rigth-[20vw] hidden md:block"
      />
    </div>
  );
};

export default SectionResena;
