import React from "react";
import { ArrowLeftIcon } from "@heroicons/react/20/solid";

const Button = ({ text, onclick, style, children }) => {
  return (
    <button
      onClick={() => goBack()}
      className="text-black bg-yellow-500 rounded-full py-2 pr-8 pl-3 flex space-x-3 uppercase max-w-fit items-center"
    >
      {children ? (
        children
      ) : (
        <ArrowLeftIcon className="w-5 text-yellow-500 bg-white rounded-full " />
      )}

      <span className={`melhous-menu-description ${style}`}>{text}</span>
    </button>
  );
};

export default Button;
