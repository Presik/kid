import React from "react";

const TitleSection = ({
  style,
  align = "text-center",
  text = "Hola mundo",
}) => {
  return (
    <div className="mx-auto  w-fit  mt-5 mb-5">
      <h2
        className={`uppercase md:text-3xl text-xl z-10 relative px-3 melhous-menu-title text-gray-900 ${align}`}
      >
        {text}
      </h2>
      <div className="w-full h-3 bg-yellow-500/50 skew-y-3 relative -top-4 rounded-l-md rounded-r-md " />
    </div>
  );
};

export default TitleSection;
