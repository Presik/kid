import { ArrowLeftIcon } from "@heroicons/react/20/solid";
import React, { useState, useEffect, Fragment } from "react";

const NavPage = ({ data }) => {
  let [view, setView] = useState(false);

  const renderView = (component) => {
    setView({ component }); // Quita "props" aquí
  };

  const goBack = () => {
    setView(null);
  };

  const ButtonBack = () => {
    return (
      <div className="container md:relative mx-auto flex justify-end  -mb-32">
        <button
          onClick={() => goBack()}
          className="text-black bg-yellow-500 rounded-full py-2 pr-4  md:pr-8 pl-3 flex md:space-x-3 space-x-2 uppercase max-w-fit  left-[100%] z-30 text-xs items-center"
        >
          <ArrowLeftIcon className="w-5 text-yellow-500 bg-white rounded-full " />

          <span>Volver</span>
        </button>
      </div>
    );
  };

  return (
    <Fragment>
      {view && <ButtonBack />}

      <div className="grid md:grid-cols-3  grid-cols-1 gap-x-4 gap-y-3 md:gap-y-6 mt-10 ">
        {view ? (
          // Renderizar el componente dinámicamente
          <div className="col-span-3 text-white">
            {React.createElement(view.component, data)}
          </div>
        ) : (
          <Fragment>
            {data.map((item, index) => (
              <div
                className={` text-white p-3 h-52 overflow-hidden relative rounded-2xl bg-cover ${
                  index == 0 ? "md:col-span-2  h-72" : ""
                } ${index == 1 ? " h-72" : ""}`}
                key={index}
                style={{
                  backgroundImage: `url(${item.image})`,
                }}
              >
                <div className="bg-gradient-to-r from-black md:via-black/80 via-black/80 to-transparent absolute w-full h-full left-0 top-0" />

                <div className="  p-2 h-full z-10 relative flex flex-col justify-center px-4 ">
                  <h2 className="text-[23px] melhous-menu-title uppercase">
                    {item.name}
                  </h2>
                  <p className="text-[18px]  melhous-menu-description md:max-w-[60%] max-w-[90%]">
                    {item.description}
                  </p>

                  <button
                    className="text-[#ffcc00] uppercase text-left mt-5 border border-yellow-500 max-w-fit px-5 rounded-2xl py-1 melhous-menu-title text-[13px] "
                    onClick={() =>
                      item.name == "Tu Reserva"
                        ? window.open(
                            "https://cloudx2.presik.com/loginCustomer",
                            "_blank",
                          )
                        : renderView(item.component, item.props)
                    }
                  >
                    Ver más ...
                  </button>
                </div>
              </div>
            ))}
          </Fragment>
        )}
      </div>
    </Fragment>
  );
};

export default NavPage;
