import React from "react";
import { Carousel } from "react-responsive-carousel";
import { ClockIcon, FireIcon } from "@heroicons/react/20/solid";
import TitleSection from "./ui/TitleSection";

const SliderTestimonials = ({ data }) => {
  const renderCategories = () => {
    let itemsPerSlide;

    if (window.innerWidth >= 1000) {
      itemsPerSlide = 3;
    } else if (window.innerWidth >= 500) {
      itemsPerSlide = 4;
    } else {
      itemsPerSlide = 1;
    }

    const totalSlides = Math.ceil(data.length / itemsPerSlide);
    const slides = [];

    for (let i = 0; i < totalSlides; i++) {
      const start = i * itemsPerSlide;
      const end = start + itemsPerSlide;
      const slideCategories = data.slice(start, end);

      slides.push(
        <article
          key={i}
          className="grid md:mt-10 grid-cols-1   md:grid-cols-3 gap-x-3  gap-y-20"
        >
          {slideCategories.map((testimonial, index) => (
            <div
              key={index}
              className=" bg-cover flex items-start rounded-lg overflow-hidden  "
              style={{ backgroundImage: `url(${testimonial.image})` }}
            >
              <div className="w-[50px] max-h-[40px] ">
                <img
                  src={testimonial.photo}
                  className="w-[50px] h-[50px] object-cover rounded-full"
                />
              </div>
              <div className="  px-5   w-full ">
                <h2 className="text-base text-left font-semibold w-max melhous-menu-title uppercase melhous-menu-title">
                  {testimonial.name}
                  <hr className="border-[0,5] border-gray-700 mb-2 mt-1 w-[70%]" />
                </h2>

                <div className="text-[13px] text-gray-700 font-thin text-left text-base melhous-menu-description">
                  {testimonial.text}
                </div>
              </div>
            </div>
          ))}
        </article>,
      );
    }

    return slides;
  };
  return (
    <div className="md:my-40 my-20">
      <TitleSection text="RESEÑAS" align="center" />
      <Carousel
        showThumbs={false}
        showStatus={false}
        showIndicators={false}
        emulateTouch={true}
        showArrows={false}
        autoPlay={true} // Hace que el carousel sea automático
        interval={5000} // Establece el tiempo de transición en milisegundos (5 segundos en este caso)
        infiniteLoop={true} // Hace que el carousel sea infinito
        renderIndicator={(clickHandler, isSelected, index, label) => {
          if (isSelected) {
            return (
              <li
                className="custom-dot active"
                aria-label={`Slide ${label} - Current Slide`}
              />
            );
          }
          return (
            <li
              className="custom-dot"
              onClick={clickHandler}
              onKeyDown={clickHandler}
              value={index}
              key={index}
              role="button"
              tabIndex={0}
              title={`${label}`}
              aria-label={`Slide ${label}`}
            />
          );
        }}
      >
        {renderCategories()}
      </Carousel>
    </div>
  );
};

export default SliderTestimonials;
