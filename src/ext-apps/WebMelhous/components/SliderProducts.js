import React from "react";
import { Carousel } from "react-responsive-carousel";
import { ClockIcon, FireIcon } from "@heroicons/react/20/solid";
import TitleSection from "./ui/TitleSection";

const SliderProducts = ({ data }) => {
  const renderCategories = () => {
    let itemsPerSlide;

    if (window.innerWidth >= 1000) {
      itemsPerSlide = 5;
    } else if (window.innerWidth >= 500) {
      itemsPerSlide = 4;
    } else {
      itemsPerSlide = 2;
    }

    const totalSlides = Math.ceil(data.length / itemsPerSlide);
    const slides = [];

    for (let i = 0; i < totalSlides; i++) {
      const start = i * itemsPerSlide;
      const end = start + itemsPerSlide;
      const slideCategories = data.slice(start, end);

      slides.push(
        <div
          key={i}
          className="grid grid-cols-2 md:grid-cols-5 gap-3 w-[150vw] md:w-[100%] rounded-md overflow-hidden"
        >
          {slideCategories.map((product, index) => (
            <article
              key={index}
              className="h-[400px]  bg-cover flex items-end rounded-lg overflow-hidden shadow-xl "
              style={{ backgroundImage: `url(${product.image})` }}
              title={product.name}
            >
              <div className=" bg-gradient-to-t from-black via-black/90 to-transparent px-5  pb-3 pt-10  w-full text-white">
                <h2 className="text-xs truncate w-full text-left font-semibold  melhous-menu-title uppercase">
                  {product.name}
                  <hr className="border-[0,5] border-gray-700 mb-2 mt-1 w-[70%]" />
                </h2>

                <div className="flex  justify-between">
                  <div className="text-[13px] text-yellow-500 font-medium text-left text-base">
                    $ {product.price}
                  </div>
                  <div className="flex space-x-1">
                    <ClockIcon className="w-5" />
                    <span>15 m</span>
                    <FireIcon className="w-5" />
                    <span>{product.category}</span>
                  </div>
                </div>
              </div>
            </article>
          ))}
        </div>,
      );
    }

    return slides;
  };
  return (
    <div className="mt-20 md:mb-48 mb-20">
      <TitleSection text="Populares" align="center" />
      <Carousel
        showThumbs={false}
        showStatus={false}
        showIndicators={true}
        emulateTouch={true}
        showArrows={false}
        renderIndicator={(clickHandler, isSelected, index, label) => {
          if (isSelected) {
            return (
              <li
                className="custom-dot active"
                aria-label={`Slide ${label} - Current Slide`}
              />
            );
          }
          return (
            <li
              className="custom-dot"
              onClick={clickHandler}
              onKeyDown={clickHandler}
              value={index}
              key={index}
              role="button"
              tabIndex={0}
              title={`${label}`}
              aria-label={`Slide ${label}`}
            />
          );
        }}
      >
        {renderCategories()}
      </Carousel>
    </div>
  );
};

export default SliderProducts;
