import React from "react";

const SectionPeople = () => {
  return (
    <div className="py-32 flex flex-col justify-center items-center space-y-5">
      <div className="h-[80px] w-[0.5px] bg-yellow-500" />
      <img
        className="w-20 h-20 rounded-full mt-10 object-cover"
        src="https://img.freepik.com/free-photo/portrait-white-man-isolated_53876-40306.jpg?size=626&ext=jpg&ga=GA1.2.386372595.1697500800&semt=ais"
      />
      <h2 className="uppercase text-xl md:text-3xl z-10 relative px-3 melhous-menu-title text-gray-900 text-center">
        COMUNICATE CON NOSOTROS
      </h2>
      <p className="max-w-2xl text-center melhous-menu-description ">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae
        ultrices justo. Maecenas purus dolor, vulputate ut volutpat at, eleifend
        a dolor.
      </p>

      <button
        className="text-black  text-left mt-3 border border-yellow-500 bg-yellow-500 max-w-fit px-5 rounded-2xl py-1 melhous-menu-title"
        onClick={() => renderView(item.component, item.props)}
      >
        CONTACTO
      </button>
    </div>
  );
};

export default SectionPeople;
