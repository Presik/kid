import { CircleStackIcon } from "@heroicons/react/20/solid";
import React, { Fragment } from "react";

export const NavCategories = ({ mode = "center" }) => {
  return (
    <Fragment>
      <div className="relative mt-16 hidden md:block">
        <div className="border-b border-yellow-500  absolute top-10 w-[60%]  right-0"></div>
        <div className="w-6 h-6 flex justify-center  items-center rounded-full border  border-yellow-500 bg-white  absolute text-yellow-500 top-[29px] right-[60%]">
          <div className=" bg-yellow-500  w-3 h-3 animate-ping rounded-full"></div>
        </div>
        <div
          className={`flex space-x-8 relative ${
            mode == "left" ? "justify-end pr-8" : "justify-center"
          }`}
        >
          <div className="border border-yellow-500 rounded-full p-5 bg-gray-100 ">
            <img
              src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697510604/fku2fdur1mg9egpvfrx3.png"
              className="w-10 h-10"
            />
          </div>
          <div className="border border-yellow-500 rounded-full p-5 bg-gray-100 ">
            <img
              src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697510604/fku2fdur1mg9egpvfrx3.png"
              className="w-10 h-10"
            />
          </div>
          <div className="border border-yellow-500 rounded-full p-5 bg-gray-100 ">
            <img
              src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697510604/fku2fdur1mg9egpvfrx3.png"
              className="w-10 h-10"
            />
          </div>
          <div className="border border-yellow-500 rounded-full p-5 bg-gray-100 ">
            <img
              src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697510604/fku2fdur1mg9egpvfrx3.png"
              className="w-10 h-10"
            />
          </div>
          <div className="border border-yellow-500 rounded-full p-5 bg-gray-100 ">
            <img
              src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697510604/fku2fdur1mg9egpvfrx3.png"
              className="w-10 h-10"
            />
          </div>
          <div className="border border-yellow-500 rounded-full p-5 bg-gray-100 ">
            <img
              src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697510604/fku2fdur1mg9egpvfrx3.png"
              className="w-10 h-10"
            />
          </div>
          <div className="border border-yellow-500 rounded-full p-5 bg-gray-100 ">
            <img
              src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697510604/fku2fdur1mg9egpvfrx3.png"
              className="w-10 h-10"
            />
          </div>
        </div>
      </div>
    </Fragment>
  );
};
