import React, { Fragment } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./layout/Header";
import Footer from "views/Footer";
import { PRODUCTS, MENU } from "./fakeData";
import NavPage from "./components/NavPage";
import PageRestaurant from "./pages/PageRestaurant";

const ScreenMelhous = () => {
  const SectionMain = () => {
    return (
      <div
        className="rounded-md my-5 relative bg-no-repeat bg-cover p-5 flex"
        style={{
          backgroundImage:
            "url(https://images.unsplash.com/photo-1600891964599-f61ba0e24092?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8cmVzdGF1cmFudCUyMGZvb2R8ZW58MHx8MHx8fDA%3D&w=1000&q=80)",
        }}
      >
        <div className="bg-gradient-to-r from-black via-black to-transparent absolute w-full h-full left-0 top-0" />

        <div
          className="text-white p-10 w-full space-y-5 z-10 relative border "
          style={{
            borderImage: "linear-gradient(to right, #ffcc00, #fdd01f4d)",
            borderImageSlice: 1,
          }}
        >
          <h1 className="text-2xl font-semibold uppercase">Titulo</h1>
          <p className="max-w-sm">
            Description texto aca para mejorar , Description texto aca para
            mejorar
          </p>
          <button className="py-2 px-8 bg-white rounded-full text-black">
            Button orden
          </button>
        </div>
        <div>
          <ItemChildBlur top="48" rigth="72" text="texto2" />
          <ItemChildBlur top="32" rigth="56" text="texto1" />
        </div>
      </div>
    );
  };

  const ItemChildBlur = ({ top = 48, rigth, text }) => {
    return (
      <div
        className={`bg-white/20 rounded-full  px-8 py-2 absolute right-${rigth} top-${top} backdrop-blur-lg`}
      >
        {text}
      </div>
    );
  };
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/hija" component={PageRestaurant} />
        <Route path="/" exact>
          {/* Contenido de la página principal */}
        </Route>
      </Switch>
      <main className="container mx-auto min-h-[80vh] ">
        <section>
          <SectionMain />
        </section>

        <section>
          <NavPage data={MENU} />

          {/* {PRODUCTS.map((product, index) => (
            <li key={index}>
              <p>Nombre: {product.name}</p>
              <p>Precio: {product.price}</p>
              <p>Categoría: {product.category}</p>
            </li>
          ))} */}
        </section>

        <section
          className="bg-black h-[70vh] w-screen -left-40 my-20 relative bg-no-repeat bg-cover"
          style={{
            backgroundImage:
              "url(https://images.unsplash.com/photo-1600891964599-f61ba0e24092?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8cmVzdGF1cmFudCUyMGZvb2R8ZW58MHx8MHx8fDA%3D&w=1000&q=80)",
          }}
        >
          <div className="grid grid-cols-2 p-8 h-full ">
            <div className="text-white p-5 space-y-5 z-10 relative   h-full bg-black/80 backdrop-blur-md">
              <div
                className="border h-full p-5 flex flex-col justify-center"
                style={{
                  borderImage: "linear-gradient(to right, #ffcc00, #fdd01f4d)",
                  borderImageSlice: 1,
                }}
              >
                <h2 className="text-4xl uppercase">Otro titulo</h2>
                <p>otro texto aca </p>
              </div>
            </div>
            <div></div>
          </div>
        </section>
      </main>
      <Footer />
    </Router>
  );
};

export default ScreenMelhous;
