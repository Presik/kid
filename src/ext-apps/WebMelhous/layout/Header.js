import { UserIcon } from "@heroicons/react/20/solid";
import LogoVersus from "./LogoVersus";
import LogoTerra from "./LogoTerra";
import { classNames } from "tools/ui";

const STYLE_HEADER = " w-screen relative top-0 -left-10 py-3 z-50";
// const property = "terra";
const property = "versus";

const ctxProperty = {
  versus: {
    logo: <LogoVersus />,
    colorPrimary: "bg-[#004a3c]",
    colorSecundary: "#f2ebe4",
  },
  terra: {
    logo: <LogoTerra />,
    colorPrimary: "bg-[#425719]",
    colorSecundary: "#f2ebe4",
  },
};

const Header = () => {
  return (
    <header
      className={classNames(STYLE_HEADER, ctxProperty[property].colorPrimary)}
    >
      <div className="flex justify-between px-14 mx-auto items-center ">
        <div></div>
        <div className="flex items-end flex-col">
          {ctxProperty[property].logo}
          <div className="flex relative -top-3">
            <span className="text-white font-medium">by</span>
            <img
              src="https://res.cloudinary.com/dl4sdiukx/image/upload/v1697488998/tecy4smfownu3bghmaqc.png"
              className="h-5 mx-auto relative top-0.5"
            />
          </div>
        </div>
        <div className="flex space-x-3 justify-end items-center">
          {/* <span className="text-yellow-500 underline text-[16px]">Login</span>
          <UserIcon className="w-8 h-8 p-1 bg-white text-yellow-500 rounded-full" /> */}
        </div>
      </div>
    </header>
  );
};

export default Header;
