// Folio Model

import ModelGuest from "./ModelGuest";

const getView = () => {
  let DictCtxView = {
    model: "hotel.folio",
    webfields: {
      room: {
        type: "many2one",
        model: "hotel.room",
        attrs: ["name", "classification.name"],
      },
      product: {
        type: "many2one",
        model: "product.product",
        attrs: ["rec_name"],
      },
      arrival_date: { type: "date" },
      departure_date: { type: "date" },
      nights_quantity: { type: "integer" },
      guests: { type: "one2many", ctxView: ModelGuest.ctxView() },
    },
    webcards: {
      head: [
        {
          col: [{ name: "room", style: "font-bold text-lg" }],
          width: "w-11/12",
        },
        {
          col: [{ name: "product", widget: "circle" }],
        },
        {
          col: [{ name: "arrival_date", style: "font-bold text-lg" }],
          width: "w-11/12",
        },
        {
          col: [{ name: "departure_date" }],
        },
      ],
      content: [],
    },
    webform: [],
  };

  return DictCtxView;
};

export default { ctxView: getView };
