// Booking Model
import ModelFolio from "./ModelFolio";

const getView = () => {
  let DictCtxView = {
    model: "hotel.booking",
    webfields: {
      number: { type: "char" },
      channel: {
        type: "many2one",
        model: "hotel.channel",
      },
      company: {
        type: "many2one",
        model: "company.company",
      },
      party: {
        type: "many2one",
        model: "party.party",
      },
      lines: {
        type: "one2many",
        ctxView: ModelFolio.ctxView(),
        defaultView: "cards",
      },
    },
    webform: [
      { name: "number" },
      { name: "channel" },
      { name: "company" },
      { name: "party" },
      { name: "lines" },
    ],
  };

  return DictCtxView;
};

export default { ctxView: getView };
