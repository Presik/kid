import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";

import QuickForm from "components/QuickForm";
import ModalMsg from "components/Modals/ModalMsg";
import ModelGuest from "./ModelGuest";
import proxy from "api/proxy";
import func from "tools/functions";
import { checkRequiredForm } from "tools/form";
import { useFormStore } from "store/formStore";

function FormGuest(props) {
  // window.scrollTo(0, 0); FIXME
  const ctxView = ModelGuest.ctxView();
  let [btnDisabled, setBtnDisabled] = useState(true);
  let [isLoading, setIsLoading] = useState(false);
  const [openModal, setOpenModal] = useState(null);
  const { storeRecord } = useFormStore();
  const navigate = useNavigate();

  async function addGuest(val) {
    setIsLoading(true);
    storeRecord.country = storeRecord.nationality;
    const { data, error } = await proxy.saveQuery({
      model: ctxView.model,
      storeRec: storeRecord,
    });
    setOpenModal({
      msg: "Huesped agregado exitosamente!",
    });
    props.onAdd();
    setIsLoading(false);
  }

  function onCloseReturn() {
    setOpenModal(null);
    navigate(-1);
  }

  async function handelCancel() {
    setIsLoading(false);
    navigate(-1);
  }

  ctxView.webfields.add_guest.method = addGuest;
  ctxView.webfields.cancel.method = onCloseReturn;

  return (
    <div>
      <QuickForm ctxView={ctxView} level="main" onChangeView={onCloseReturn} />
      {openModal && (
        <ModalMsg
          open={true}
          onClose={onCloseReturn}
          buttons={["close"]}
          type="success"
          msg={openModal.msg}
        />
      )}
    </div>
  );
}

export default FormGuest;
