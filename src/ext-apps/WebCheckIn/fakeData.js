// Fake data for testing web check-in

const data = {
  id: 12345,
  number: "BOK-023876",
  "party.name": "REBBECA SOFIA FERGUSSON",
  "channel.name": "EXPEDIA",
  lines: [
    {
      id: 1,
      "room.name": "307",
      "room.category": "DOBLE DELUXE",
      arrival_date: "2022-09-16",
      departure_date: "2022-09-19",
      nights_quantity: 3,
      guests: [],
    },
    {
      id: 2,
      "room.name": "405",
      "room.category": "DOBLE TWIN DELUXE",
      arrival_date: "2022-09-16",
      departure_date: "2022-09-19",
      nights_quantity: 3,
      guests: [],
    },
  ],
};

export default data;
