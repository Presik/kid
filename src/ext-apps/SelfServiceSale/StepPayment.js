import React, { useState, useEffect, useRef } from "react";

import HeaderStep from "./HeaderStep";
import { classNames } from "tools/ui";
import datafono from "./assets/datafono.png";
import printOrder from "./printOrder";
import proxy from "api/proxy";
import funcs from "tools/functions";

function StepPayment(props) {
  let [colorMsg, setColorMsg] = useState("text-lime-600");
  const [showImg, setShowImg] = useState(false);
  const isMounted = useRef(false);

  const _msg = "PROCESANDO TU COMPRA!!";
  let [msg, setMsg] = useState(_msg);

  async function activateTerminal(saleId) {
    setMsg("Sigue las instrucciones del datafono ...");
    setShowImg(true);
    const { data: res } = await proxy.methodCall({
      model: "sale.sale",
      method: "get_pay_from_card",
      args: [{ id: saleId }],
    });
    let response_pay;
    if (res["status"] === "ok") {
      for (let n = 1; n < 15; n++) {
        if (n === 1) {
          await new Promise((r) => setTimeout(r, 20000));
          const { data } = await proxy.methodCall({
            model: "sale.sale",
            method: "get_response_pay_from_card",
            args: [{ terminal: res["terminal"] }],
          });
          response_pay = data;
        } else {
          await new Promise((r) => setTimeout(r, 10000));
          const { data } = await proxy.methodCall({
            model: "sale.sale",
            method: "get_response_pay_from_card",
            args: [{ terminal: res["terminal"] }],
          });
          response_pay = data;
        }

        if (!isMounted.current || response_pay.status === "ok") {
          break;
        }
      }
    }
    setShowImg(false);
    return response_pay;
  }

  async function createSale() {
    let _sale = JSON.parse(JSON.stringify(props.storeCart));

    _sale["lines"] = funcs.cloneMap(props.storeCart.lines);
    for (const key of ["id", "payment_amount", "qr_code", "total_amount"]) {
      delete _sale[key];
    }
    const _lines = Array.from(_sale.lines.values()).map((val) => {
      val.product = val.product.id;
      return val;
    });
    _sale["lines"] = [["create", _lines]];
    const args = { model: "sale.sale", method: "create_sale", args: [_sale] };
    const res = await proxy.methodCall(args);
    return res;
  }

  async function getPaymentTerminal() {
    const { data } = await createSale();

    if (data?.record?.id) {
      setMsg("Compra procesada!!");
      props.changeCart("id", data.record.id);
      const response = await activateTerminal(data.record.id);

      if (response?.status !== "ok" || response?.code !== "0") {
        const _msg = response.msg;
        setMsg(_msg);
        setColorMsg("text-rose-700");
      } else {
        props.changeCart("pay", response.pay);
        printTicket({ sale_id: data.record.id, pay: response.pay });
        props.onChangeView(null, "ticket", null, data.record);
      }
    }
  }

  async function printTicket(args) {
    const { data } = await proxy.methodCall({
      model: "sale.sale",
      method: "process_pay_sale",
      args: [args],
    });
    if (data?.status == "ok") {
      printOrder(data);
    } else {
      console.log(data, "validate result");
    }
  }

  useEffect(() => {
    getPaymentTerminal();
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, []);

  let style = "text-5xl font-bold";
  return (
    <div>
      <HeaderStep label="PASO 2. REALIZA EL PAGO" />
      <div className="flex flex-col mt-60">
        <p className={classNames(style, colorMsg)}>{msg}</p>
        <p className="pt-3">
          {showImg && <img src={datafono} alt="datafono" />}
        </p>
      </div>
    </div>
  );
}

export default StepPayment;
