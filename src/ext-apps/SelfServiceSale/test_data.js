import proxy from "api/proxy";

const get_products = async (category) => {
  const fields_names = [
    "sale_price_w_tax",
    "list_price",
    "name",
    "images.image_url",
    "id",
  ];
  const { data: products } = await proxy.search(
    "product.product",
    [["categories", "in", [category]]],
    fields_names,
  );
  console.log("products...", products);
  return products;
};

const data = {
  203: {
    id: "203",
    name: "PIZZAS PEQUEÑAS",
    color: "green",
    image: "pizza",
    get_products: get_products,
    products: [
      {
        id: 10,
        product: "PIZZA QUESO",
        unit_price_w_tax: 45600,
        link: "cheese_pizza",
      },
      {
        id: 15,
        product: "PIZZA JAMON Y QUESO",
        unit_price_w_tax: 40800,
        link: "bacon_pizza",
      },
      {
        id: 11,
        product: "PIZZA PEPPERONI Y QUESO",
        unit_price_w_tax: 76800,
        link: "pepperoni_pizza",
      },
      {
        id: 12,
        product: "PIZZA POLLO CHAMPIÑONES",
        unit_price_w_tax: 36000,
        link: "mushroom_pizza",
      },
      {
        id: 14,
        product: "PIZZA HAWAIANA",
        unit_price_w_tax: 24100,
        link: "hawaiana_pizza",
      },
      {
        id: 16,
        product: "PIZZA CARNES",
        unit_price_w_tax: 34900,
        link: "meat_pizza",
      },
    ],
  },
  204: {
    id: "204",
    name: "PIZZAS MEDIANAS",
    color: "green",
    image: "pizza",
    get_products: get_products,
    products: [
      {
        id: 17,
        product: "PIZZA QUESO",
        unit_price_w_tax: 25600,
      },
      {
        id: 18,
        product: "PIZZA JAMON Y QUESO",
        unit_price_w_tax: 76800,
      },
      {
        id: 19,
        product: "PIZZA PEPPERONI Y QUESO",
        unit_price_w_tax: 26500,
      },
      {
        id: 20,
        product: "PIZZA POLLO CHAMPIÑONES",
        unit_price_w_tax: 16500,
      },
      {
        id: 21,
        product: "PIZZA HAWAIANA",
        unit_price_w_tax: 24100,
      },
      {
        id: 22,
        product: "PIZZA CARNES",
        unit_price_w_tax: 24100,
      },
    ],
  },
  205: {
    id: "205",
    name: "PANZEROTTIS",
    color: "yellow",
    image: "calzone",
    get_products: get_products,
    products: [
      {
        id: 17,
        product: "PANZEROTTI QUESO",
        unit_price_w_tax: 25600,
      },
      {
        id: 18,
        product: "PANZEROTTI JAMON Y QUESO",
        unit_price_w_tax: 76800,
      },
      {
        id: 19,
        product: "PANZEROTTI PEPPERONI Y QUESO",
        unit_price_w_tax: 26500,
      },
      {
        id: 20,
        product: "PANZEROTTI POLLO CHAMPIÑON",
        unit_price_w_tax: 16500,
      },
      {
        id: 21,
        product: "PANZEROTTI HAWAIANA",
        unit_price_w_tax: 24100,
      },
      {
        id: 22,
        product: "PANZEROTTI CARNES",
        unit_price_w_tax: 24100,
      },
    ],
  },
  206: {
    id: "206",
    name: "BEBIDAS",
    color: "skin",
    image: "soda",
    get_products: get_products,
    products: [
      {
        id: 16,
        product: "GASEOSA 1.5 LITRO",
        unit_price_w_tax: 25600,
      },
    ],
  },
};

export default data;
