import React, { useState, useEffect } from "react";

import ButtonProduct from "./ButtonProduct";
import BasicModal from "components/BasicModal";
// import cats from "./test_data";

function ModalProducts(props) {
  let [products, setProducts] = useState([]);

  function _setProducts() {
    const _products = props.products.map((product) => {
      if (product.salable) {
        return (
          <ButtonProduct key={product.id} {...product} onClick={onClick} />
        );
      }
    });
    setProducts(_products);
  }

  function onClick(value) {
    props.onClicked(value);
    props.onClose();
  }

  useEffect(() => {
    _setProducts();
  }, [props.cats]);

  return (
    <BasicModal
      width="w-11/12"
      height="h-5/6"
      open={props.open}
      title="self_service_sale.select_product"
      titleSize="text-4xl text-slate-600"
      onClose={props.onClose}
    >
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3  gap-4 pt-10 mb-4">
        {products}
      </div>
    </BasicModal>
  );
}

export default ModalProducts;
