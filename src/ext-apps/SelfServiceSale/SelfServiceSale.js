import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import Loading from "components/Loading.js";
import StepOrder from "./StepOrder";
import StepPayment from "./StepPayment";
import StepTicket from "./StepTicket";
import Cart from "./Cart";
import Button from "./ButtonAction";
import logoCompany from "./assets/logo.png";
import proxy from "api/proxy";

const stepsCtx = {
  order: StepOrder,
  payment: StepPayment,
  ticket: StepTicket,
};

// route access http://my.domain/app/DB/self_service_sale/id_shop

function SelfServiceSale(props) {
  const { shop } = useParams();
  let [step, setStep] = useState("order");
  let [cartReadOnly, setCartReadOnly] = useState(false);
  let [btnDisabled, setBtnDisabled] = useState(true);
  const [storeCart, setStoreCart] = useState(null);
  let [saleNumber, setSaleNumber] = useState(null);

  async function getShopCtx() {
    if (!shop) {
      return;
    }
    const dom = [["id", "=", shop]];
    const fields = ["name", "party", "company", "product_categories"];
    let { data: res } = await proxy.search("sale.shop", dom, fields);
    if (res) {
      const _shop = res[0];
      const _data = {
        id: 0,
        kind: "take_away",
        party: _shop.party,
        shop: _shop.id,
        company: _shop.company,
        currency: _shop.currency,
        lines: new Map(),
        total_amount: 0,
        payment_amount: 0,
        state: "draft",
        qr_code: "www.presik.com",
      };
      setStoreCart(_data);
    }
  }

  // async function validateApp() {
  //   await proxy.method({
  //     model: "dash.app.self_service_sale",
  //     method: "validate_app",
  //     args: { shop: shop },
  //   });
  // }

  useEffect(() => {
    // validateApp();
    getShopCtx();
  }, [props]);

  async function onChangeView(e, nextStep, resetStore, data) {
    // if (nextStep === "order") {
    //   setCartReadOnly(false);
    // } else {
    //   setCartReadOnly(true);
    // }
    let nextStep_ = nextStep;
    if (nextStep === "ticket") {
      setSaleNumber(data.number);
    } else if (nextStep === "cancel") {
      nextStep_ = "order";
      await cancelSale();
    }
    setStep(nextStep_);
    if (resetStore) {
      await getShopCtx();
      setBtnDisabled(true);
    }
  }

  async function cancelSale() {
    await proxy.buttonMethod({
      model: "sale.sale",
      method: "cancel",
      ids: [storeCart.id],
    });
  }

  function changeCart(field, value) {
    let storeCart_ = { ...storeCart, ...{} };
    storeCart_[field] = value;
    setStoreCart(storeCart_);
    if (storeCart_.lines.size > 0) {
      setBtnDisabled(false);
    } else {
      setBtnDisabled(true);
    }
  }

  const StepComponent = stepsCtx[step];

  const BUTTON = {
    order: (
      <Button
        name="payment"
        content="IR A PAGAR"
        color="blue"
        disabled={btnDisabled}
        onClick={onChangeView}
      />
    ),
    payment: (
      <Button
        name="cancel"
        content="CANCELAR"
        color="blue"
        onClick={(e, name) => {
          onChangeView(e, name, true);
        }}
      />
    ),
    ticket: (
      <>
        {/* <Button
          name="ticket"
          content="IMPRIMIR"
          color="blue"
          onClick={onChangeView}
        /> */}
      </>
    ),
  };

  if (!storeCart) {
    return <Loading />;
  }

  const data = {
    saleNumber: saleNumber,
  };
  return (
    <div className="grid grid-cols-2 py-6">
      <div className="px-12 justify-center">
        <StepComponent
          storeCart={storeCart}
          changeCart={changeCart}
          data={data}
          onChangeView={onChangeView}
        />
      </div>
      <div className="justify-center">
        <img
          src={logoCompany}
          alt="logo-company"
          width={180}
          className="mx-auto"
        />
        <div className="mt-12">
          <Cart
            storeCart={storeCart}
            changeCart={changeCart}
            readOnly={cartReadOnly}
          />
        </div>
        <div className="py-12">{BUTTON[step]}</div>
      </div>
      <div className="pt-12 flex bottom-px">
        <p className="text-md text-slate-700 text-center pl-12">
          Powered by &nbsp;
        </p>
        <p className="text-md font-bold text-slate-700 text-center">
          www.presik.com
        </p>
      </div>
    </div>
  );
}

export default SelfServiceSale;
