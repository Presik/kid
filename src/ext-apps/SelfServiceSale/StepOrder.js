import React, { useState, useEffect } from "react";

import CategoryCard from "./CategoryCard";
import HeaderStep from "./HeaderStep";
import ModalProducts from "./ModalProducts";
import proxy from "api/proxy";
// import cats from "./test_data";

function StepOrder(props) {
  let [productsCategory, setProductsCat] = useState(null);
  const [categories, setCategories] = useState([]);

  useEffect(() => {}, [props]);

  function closeModal() {
    setProductsCat(null);
  }

  function onClickedCategory(products) {
    if (props.storeCart.lines.size >= 3) {
      return;
    }
    setProductsCat(products);
  }

  function onClickedProduct(value) {
    let { storeCart } = props;
    console.log(storeCart.lines, "lines");
    if (storeCart.lines.size >= 3) {
      return;
    }
    const product = {
      id: value.id,
      product: { id: value.id, name: value.name },
      unit_price: value.list_price,
      unit_price_w_tax: value.sale_price_w_tax,
    };
    storeCart.lines.set(product.id, product);
    props.changeCart("lines", storeCart.lines);
  }

  async function getCategories() {
    const fields = [
      "product_categories.name",
      "product_categories.products.sale_price_w_tax",
      "product_categories.products.salable",
      "product_categories.products.name",
      "product_categories.products.images.image_url",
      "product_categories.products.id",
      "product_categories.products.list_price",
      "product_categories.images.image",
    ];
    const { data: cats } = await proxy.search(
      "sale.shop",
      [["id", "=", props.storeCart?.shop]],
      fields,
    );
    const categories = Object.values(cats[0]["product_categories."]).map(
      (value) => (
        <CategoryCard onClicked={onClickedCategory} key={value.id} {...value} />
      ),
    );
    setCategories(categories);
  }

  useEffect(() => {
    getCategories();
  }, [props.storeCart]);

  return (
    <div>
      <HeaderStep label="PASO 1. ESCOJE TUS PRODUCTOS" />
      <div className="block">{categories}</div>
      {productsCategory && (
        <ModalProducts
          open={true}
          onClicked={onClickedProduct}
          products={productsCategory}
          buttons={["close"]}
          onClose={closeModal}
        />
      )}
    </div>
  );
}

export default StepOrder;
