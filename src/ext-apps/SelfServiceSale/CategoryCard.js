import React from "react";
import { PhotoIcon } from "@heroicons/react/24/outline";
import { classNames } from "tools/ui";

import data from "./test_data";
// import hamburgerImg from "./assets/hamburger.png";
// import pizzaImg from "./assets/pizza.png";
// import sodaImg from "./assets/soda.png";
// import calzoneImg from "./assets/calzone.png";

// const cardImg = {
//   hamburger: hamburgerImg,
//   pizza: pizzaImg,
//   soda: sodaImg,
//   calzone: calzoneImg,
// };

// const colors = {
//   yellow: "bg-amber-200 border-amber-400",
//   green: "bg-lime-200 border-lime-500",
//   skin: "bg-rose-100 border-rose-300",
//   blue: "bg-blue-200 border-cyan-500",
// };

const CategoryCard = (props) => {
  function onClicked() {
    props.onClicked(props["products."]);
  }

  const style = "text-center my-4 active:scale-95";

  return (
    <div onClick={onClicked} className={classNames(style)} key={props.id}>
      {props["images."].length > 0 ? (
        <img
          className="mx-auto mt-2 rounded-lg shadow-lg"
          width={250}
          src={props["images."][0].image}
        />
      ) : (
        <PhotoIcon className="mx-auto h-25 w-72" />
      )}
      <h2 className="text-2xl font-bold text-slate-800 p-3">{props.name}</h2>
    </div>
  );
};

export default CategoryCard;
