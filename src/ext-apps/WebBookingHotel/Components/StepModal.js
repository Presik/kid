import React from "react";

const STEP = ["Paso 1", "Paso 2"];

const StepModal = (step) => {
  return (
    <div className="mb-5 space-x-3 ">
      {STEP.map((item) => {
        return (
          <span
            className="bg-blue-presik py-2 px-5 text-white rounded-md "
            key={item}
          >
            {item}
          </span>
        );
        // ^ Agrega 'return' aquí y usa 'key' para cada elemento en el mapeo.
      })}
    </div>
  );
};

export default StepModal;
