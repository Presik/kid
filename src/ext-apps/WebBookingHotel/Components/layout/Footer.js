import React from "react";

const Footer = () => {
  return (
    <div className="px-10 py-5 bg-blue-presik text-white w-screen relative -left-7 z-20">
      Todos los derechos reservados <b>Presik Copyright 2023</b>
    </div>
  );
};

export default Footer;
