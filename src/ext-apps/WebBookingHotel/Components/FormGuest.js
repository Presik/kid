import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";

import TextField from "components/TextField";
import SelectionField from "components/SelectionField";
import Checkbox from "components/CheckboxField";
import StdButton from "components/StdButton";
import TwComboBox from "components/TwComboBox";
import DateField from "components/DateField";
import ModalMsg from "components/Modals/ModalMsg";
import proxy from "api/proxy";
import func from "tools/functions";
import { checkRequiredForm } from "tools/form";
import { TYPE_DOC, TYPE_GUEST, SEX } from "../constants";

function FormGuest(props) {
  // window.scrollTo(0, 0); FIXME
  const location = useLocation();
  const folio = location.state;
  let [btnDisabled, setBtnDisabled] = useState(true);
  let [store, setStore] = useState({
    main_guest: false,
    folio: 73, //aca va el folio creado
  });
  const [isLoading, setIsLoading] = useState(false);
  const [openModal, setOpenModal] = useState(null);
  const navigate = useNavigate();
  const storeRequired = [
    "name",
    "type_guest",
    "sex",
    "id_number",
    "email",
    "mobile",
    "address",
    // "nationality",
    // "country",
  ];

  async function acceptCreate() {
    setIsLoading(true);
    if (Object.keys(store).length > 1) {
      const _store = func.recToTryton(store);
      const fieldsNames = ["id"];
      _store.country = _store.nationality;
      await proxy.create("hotel.folio.guest", _store, fieldsNames);
      setIsLoading(false);
      setOpenModal({
        msg: "Huesped agregado exitosamente!",
      });
      props.onAdd();
    }
    setIsLoading(false);
  }

  function onCloseModal() {
    setOpenModal(null);
    navigate(-1);
  }

  function onChange(field, value) {
    store[field] = value;
    const newStore = { ...store };
    const res = checkRequiredForm(newStore, storeRequired);
    setBtnDisabled(!res);
    setStore(newStore);
  }

  async function handelCancel() {
    setIsLoading(false);
    navigate(-1);
  }

  return (
    <form>
      <TextField
        name="name"
        label="app.webcheckin.name"
        value={store.name || ""}
        required={true}
        onChange={onChange}
        translate={true}
      />
      <Checkbox
        value={store.main_guest}
        name="main_guest"
        label="app.webcheckin.main_guest"
        onChange={onChange}
        translate={true}
      />
      <SelectionField
        name="type_document"
        label="app.webcheckin.type_document"
        value={store.type_document}
        onChange={onChange}
        required={true}
        options={TYPE_DOC}
        translate={true}
      />
      <TextField
        name="id_number"
        label="app.webcheckin.id_number"
        value={store.id_number || ""}
        required={true}
        onChange={onChange}
        translate={true}
      />
      <SelectionField
        name="type_guest"
        label="app.webcheckin.type_guest"
        value={store.type_guest}
        onChange={onChange}
        required={true}
        options={TYPE_GUEST}
        translate={true}
      />
      <SelectionField
        name="sex"
        label="app.webcheckin.sex"
        value={store.sex}
        onChange={onChange}
        required={true}
        options={SEX}
        translate={true}
      />
      <TextField
        name="mobile"
        label="app.webcheckin.mobile"
        value={store.mobile || ""}
        required={true}
        onChange={onChange}
        translate={true}
      />
      <TextField
        name="email"
        label="app.webcheckin.email"
        value={store.email || ""}
        required={true}
        onChange={onChange}
        translate={true}
      />
      <TextField
        name="address"
        label="app.webcheckin.address"
        value={store.address || ""}
        required={true}
        onChange={onChange}
        translate={true}
      />
      {/* <TwComboBox
        name="nationality"
        label="app.webcheckin.nationality"
        model="country.country"
        value={store.nationality}
        onChange={onChange}
        required={true}
        translate={true}
      /> */}
      <TwComboBox
        name="country"
        label="app.webcheckin.country_residence"
        model="party.country_code"
        value={{ id: 2, name: "Colombia" }}
        onChange={onChange}
        required={true}
        translate={true}
      />
      <DateField
        id="birthday"
        name="birthday"
        value={store.birthday}
        label="app.webcheckin.birthday"
        onChange={onChange}
        translate={true}
      />
      <TextField
        name="profession"
        label="app.webcheckin.profession"
        value={store.profession || ""}
        onChange={onChange}
        translate={true}
      />
      <TextField
        name="visa_number"
        label="app.webcheckin.visa_number"
        value={store.visa_number || ""}
        onChange={onChange}
      />
      <DateField
        name="visa_date"
        value={store.visa_date}
        label="app.webcheckin.visa_date"
        onChange={onChange}
      />
      <div className="grid py-5 gap-3">
        <StdButton
          key="add"
          color="blue"
          disabled={btnDisabled}
          loading={isLoading}
          onClick={acceptCreate}
          size="w-full"
          content="app.webcheckin.add_guest"
        />
        <StdButton
          key="cancel"
          color="red"
          onClick={handelCancel}
          size="w-full"
          noFill={true}
          content="app.webcheckin.cancel"
        />
      </div>
      {openModal && (
        <ModalMsg
          open={true}
          onClose={onCloseModal}
          buttons={["close"]}
          type="success"
          msg={openModal.msg}
        />
      )}
    </form>
  );
}

export default FormGuest;
