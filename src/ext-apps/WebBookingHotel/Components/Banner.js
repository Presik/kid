import React from "react";

const Banner = ({ children }) => {
  return (
    <div className="bg-blue-presik w-screen relative top-0 -left-10 ">
      <div className="container mx-auto">{children}</div>
    </div>
  );
};

export default Banner;
