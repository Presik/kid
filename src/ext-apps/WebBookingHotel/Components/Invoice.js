import React from "react";
import { CheckCircleIcon } from "@heroicons/react/20/solid";

const Invoice = ({ data }) => {
  console.log("esto viene desde el invoice ", data);

  return (
    <div className="bg-blue-presik w-full flex  flex-col justify-center items-center h-[500px] rounded-md text-white space-y-3">
      <CheckCircleIcon className="w-20" />
      <h2 className="text-yellow-500 font-semibold text-xl">
        {data[0].rec_name}
      </h2>
      <span className="uppercase">
        <span className="text-yellow-500">¡</span>Reserva creada{" "}
        <span className="text-yellow-500">exitosamente!</span>
      </span>
      <span className="text-xs text-gray-400">
        Valida tu correo electronico
      </span>
    </div>
  );
};

export default Invoice;
