import React from "react";
import {
  ArrowDownIcon,
  CheckCircleIcon,
  ChevronDownIcon,
  PlusCircleIcon,
  UserIcon,
} from "@heroicons/react/20/solid";
import { FormattedMessage as FM } from "react-intl";
import SelectionField from "components/SelectionField";
import { IconAir } from "ext-apps/WebMelhous/components/ui/Icons";
import { Popover } from "@headlessui/react";
const ItemClassification = ({
  dataRooms,
  handleSelectClassification,
  imgDefault,
  handlePaymentView,
}) => {
  console.log(dataRooms, " somos ");

  const AmenitiesItem = ({ value, key }) => {
    console.log(value, "desde ameneties");
    return (
      <li key={key} className="flex space-x-2 items-center ">
        {/* <IconAir size="1rem" /> */}
        <CheckCircleIcon className="w-5 relative -top-[1px]" />
        <span>{value}</span>
      </li>
    );
  };

  const ButtomBooking = ({ category, onClick }) => {
    return (
      <div className="mt-4 flex justify-end absolute bottom-0 right-0">
        <button
          className="group bg-yellow-500 px-5 py-2  uppercase from-amber-300 to-amber-400 text-amber-900 flex space-x-2 hover:bg-blue-presik hover:text-yellow-500 transition-all duration-300 "
          onClick={() => onClick(category)} // Aquí agregamos el evento onClick
        >
          <span className="text-xl">Reservar</span>

          <PlusCircleIcon className="w-7 text-yellow-400 group-hover:text-white  transition-all duration-300" />
        </button>
      </div>
    );
  };

  const onChange = (name, value, id) => {
    let _dataRooms = { ...dataRooms };
    console.log(_dataRooms);
    console.log(value, name, id);
  };

  const SelectorFieldAdults = ({ value = 1, nameField }) => {
    let _value = value == 0 ? 1 : value;
    let _nameField = nameField == "adults" ? " Adultos" : " Niños";
    console.log(nameField, _value);
    // Generar las opciones automáticamente
    const options = Array.from({ length: _value }, (_, index) => ({
      id: String(index + 1),
      name: String(index + 1) + _nameField,
    }));

    return (
      <SelectionField
        name={nameField}
        value={options[0]}
        onChange={onChange}
        required={true}
        options={options}
        translate={true}
        id={22}
      />
    );
  };

  return (
    <div className=" grid grid-cols-2 gap-10 mb-10 md:mb-0">
      {dataRooms &&
        dataRooms.map((category) => (
          <div
            key={category.id}
            className="md:flex border border-gray-100 shadow-sm rounded-md bg-white relative cursor-pointer col-span-2"
          >
            <span className="bg-blue-presik absolute text-white px-3 py-1 right-0 font-thin">
              <FM id="web.booking.avalaible" /> {category.rooms.length}
            </span>
            <div className="md:w-[36%] w-full">
              <img
                src={category?.product.images[0] || imgDefault}
                className="h-60 object-cover w-full"
              />
            </div>

            {/* <Amenities /> */}
            <div className="md:pb-5 flex flex-col px-5 py-5 items-stretch  md:w-[65%] w-full relative pb-20">
              <h2 className="font-medium mb-4">{category.product.name}</h2>
              <div className="flex md:flex-col md:block justify-around">
                <div
                  className="md:flex md:divide-x md:divide-gray-200 md:space-x-10 text-gray-600  md:w-full   w-[
                  89%]"
                >
                  <span className="flex flex-col ">
                    <b>
                      <FM id="web.booking.occupation" />
                    </b>
                    <span className="flex">
                      <UserIcon className="w-5" /> {category.rooms.length}
                    </span>
                  </span>
                  <span className="md:pl-3 flex flex-col">
                    <b>
                      <FM id="web.booking.minimum_stay" />
                    </b>
                    <span>1 Noche</span>
                  </span>
                  <span className="md:pl-3 flex flex-col">
                    <b>Incluye</b>
                    <span>
                      <FM id="web.booking.minimum_stay" />
                    </span>
                  </span>
                </div>

                <ul className="text-[9px] w-full md:pl-0 pl-3 text-left list-none md:inline-flex md:space-x-3  list-inside border-t border-gray-50 pt-3 mt-2 ">
                  {category.rooms[0].amenities.map((item, key) => (
                    <AmenitiesItem
                      value={item.name}
                      key={`amenities-${key}`}
                      item={item}
                    />
                  ))}
                </ul>
              </div>

              {/* prices */}
              <div className="md:flex md:space-x-7 items-center w-full border-t border-gray-200 mt-5 pt-5">
                <span className="font-semibold text-lg uppercase">
                  Precio: ${category.sale_price_taxed.toLocaleString("es-ES")}
                </span>
                <div className="space-x-4 flex">
                  <SelectorFieldAdults
                    value={category.adults}
                    nameField="adults"
                    id={category.children}
                  />
                  {/* <SelectionField /> */}

                  <SelectorFieldAdults
                    item={category.children}
                    nameField="children"
                    id={category.children}
                  />
                </div>
              </div>
            </div>

            <ButtomBooking
              category={category}
              onClick={handleSelectClassification}
            />
          </div>
        ))}
    </div>
  );
};

export default ItemClassification;
