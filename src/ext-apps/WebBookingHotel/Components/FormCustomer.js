import React, { Fragment, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import date from "date-and-time";
import proxy from "api/proxy";
import tools from "tools/dates";

import TextField from "components/TextField";
import SelectionField from "components/SelectionField";
import StdButton from "components/StdButton";
import TwComboBox from "components/TwComboBox";
import ModalMsg from "components/Modals/ModalMsg";
import { checkRequiredForm } from "tools/form";
import { TYPE_DOC, SEX } from "../constants";
import funcs from "tools/functions";

const TODAY = tools.fmtDatetime2Tryton(new Date());

function FormGuest({
  room,
  classification,
  wizStore,
  changeStep,
  addInvoice,
  reset,
  dataApi,
}) {
  console.log(dataApi);
  let [btnDisabled, setBtnDisabled] = useState(true);
  let [store, setStore] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [openModal, setOpenModal] = useState(null);
  const [countries, setCountries] = useState([]);
  const [departments, setDepartments] = useState([]);
  const [response, setResponse] = useState(null);
  let [city, setCity] = useState([]);
  const navigate = useNavigate();
  const storeRequired = [
    "name",
    // "sex",
    "id_number",
    "email",
    "mobile",
    "address",
    // "nationality",
    "country",
    "city",
  ];

  useEffect(() => {
    dataCountry();
  }, []);

  const dataCountry = async () => {
    const dom = [];
    const { data } = await proxy.search("party.country_code", dom, [
      "name",
      "id",
    ]);
    if (data) {
      console.log(data);
      const countries = funcs.recs2Combobox(data);
      setCountries(countries);
    }
  };

  async function acceptCreate() {
    console.log(store);
    setIsLoading(true);
    if (store) {
      let address = [
        [
          "create",
          [
            {
              street: store.address,
              city_code: store.city_code,
              department_code: store.department_code,
              country_code: store.country_code,
            },
          ],
        ],
      ];

      let contact_mechanisms = [
        [
          "create",
          [
            {
              type: "mobile",
              value: store.mobile,
            },
            {
              type: "email",
              value: store.email,
            },
          ],
        ],
      ];

      let toStore = {
        name: store.name,
        id_number: store.id_number,
        type_document: 13,
        addresses: address,
        sex: store.sex.id,
        contact_mechanisms,
      };

      const fieldsNames = ["id"];
      const { data, error } = await proxy.create(
        "party.party",
        toStore,
        fieldsNames,
      );

      if (data) {
        await createBooking(data[0]);
      } else {
        alert(error);
      }

      setIsLoading(false);
    }
    setIsLoading(false);
  }

  const createBooking = async (id_customer) => {
    const lines = classification.map((item) => ({
      registration_state: "pending",
      product: item.product.id,
      unit_price: item.sale_price_taxed,
      room: item.rooms[0].id,
      arrival_date: wizStore.arrival_date,
      departure_date: wizStore.departure_date,
      nights_quantity: 2,
    }));

    const toStore = {
      responsible_payment: "holder",
      lines: lines.map((line) => ["create", [line]]),
      booking_date: TODAY,
      plan: "bed_breakfast",
      state: "offer",
      media: "web",
      lead_origin: 3,
      party: id_customer,
      contact: "",
      payment_term: 3,
    };

    const fieldsNames = ["id"];
    const { data, error } = await proxy.create(
      "hotel.booking",
      toStore,
      fieldsNames,
    );

    if (data) {
      const { data: dataBooking, error: errorBooking } = await proxy.browse(
        "hotel.booking",
        data,
        ["id", "rec_name"],
      );
      addInvoice(dataBooking);
      changeStep("step-2");
      reset();
    } else {
      alert(error);
    }
  };

  const makeRequest = async () => {
    console.log(dataApi);
    const merchantId = dataApi.openpay_merchant_id;
    const url = `https://sandbox-api.openpay.co/v1/${merchantId}/charges`;
    const apiKey = dataApi.openpay_public_key;
    console.log("api mer", merchantId, " api key", apiKey);

    const requestBody = {
      source_id: "kdx205scoizh93upqbte",
      method: "card",
      amount: 716,
      currency: "COP",
      iva: "10",
      description: "Cargo inicial a mi cuenta",
      order_id: "oid-12324",
      device_session_id: "kR1MiQhz2otdIuUlQkbEyitIqVMiI16f",
      customer: {
        name: "Cliente Colombia",
        last_name: "Vazquez Juarez",
        phone_number: "4448936475",
        email: "juan.vazquez@empresa.co",
      },
    };

    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Basic ${btoa(`${apiKey}:`)}`,
        },
        body: JSON.stringify(requestBody),
      });

      const data = await response.json();
      setResponse(data);
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const onCloseModal = () => {
    setOpenModal(null);
    // navigate(-1);
  };

  const onChange = (field, value) => {
    store[field] = value;
    const newStore = { ...store };
    const res = checkRequiredForm(newStore, storeRequired);
    setBtnDisabled(!res);
    setStore(newStore);
  };

  const handelCancel = async () => {
    setIsLoading(false);
    navigate(-1);
  };

  const addPay = async () => {
    setIsLoading(true);
    const OpenPay = window.OpenPay;
    if (!OpenPay.getId()) {
      openPay.initOpenPay(dataApi);
    }
    const customer = {
      name: "Mario",
      last_name: "Benedetti Farrugia",
      phone_number: "1111111111",
      email: "mario_benedetti@miempresa.mx",
    };

    console.log(openpay);
    // Crea un objeto con la información del pago (ajusta según tus necesidades)
    const paymentData = {
      method: "card",
      source_id: "ID_DE_LA_TARJETA", // Reemplaza con el ID de la tarjeta del cliente
      amount: 100, // Reemplaza con el monto del pago
      description: "Descripción del pago",
      device_session_id: "ID_DE_SESION_DEL_DISPOSITIVO", // Reemplaza con el ID de sesión del dispositivo
    };
    const chargeRequest = {
      method: "card",
      amount: 11122,
      description: "Cargo desde terminal virtual de 111",
      customer: customer,
      send_email: false,
      confirm: false,
      redirect_url: "http://localhost:3000/app/HOTELDUOLOCAL/web_booking",
    };

    try {
      // Realiza el pago utilizando la función create de OpenPay
      const charge = await OpenPay.charges.create(paymentData);
      console.log(charge, "ASDAS");
    } catch (error) {
      // Maneja los errores del pago
      console.error("Error en el pago:", error.message);
      // Puedes mostrar un mensaje de error al usuario, actualizar el estado, etc.
    }

    setIsLoading(false);
  };

  const onChangeCountry = async (event, value) => {
    store[event] = value.value;
    const newStore = { ...store };
    const res = checkRequiredForm(newStore, storeRequired);
    setBtnDisabled(!res);
    setStore(newStore);

    if (value.value === 48 && event === "country_code") {
      const { data } = await proxy.search(
        "party.department_code",
        [],
        ["name", "id"],
      );
      if (data) {
        const _departments = funcs.recs2Combobox(data);
        console.log("departamentos", data);
        setDepartments(_departments);
      }
    } else if (event === "department_code") {
      const { data } = await proxy.search(
        "party.city_code",
        [["department", "=", value.value]],
        ["name", "id", "department"],
      );
      if (data) {
        const _city = funcs.recs2Combobox(data);
        console.log("ciudades", data);
        setCity(_city);
      }
    }
  };

  return (
    <Fragment>
      <h2 className="text-blue-presik font-semibold text-center text-lg">
        Agrega los datos de el titular
      </h2>
      <form className="grid grid-cols-3 gap-x-3">
        <div className="md:col-span-2 col-span-3">
          <TextField
            name="name"
            label="app.booking.name"
            value={store.name || ""}
            required={true}
            onChange={onChange}
            translate={true}
            uppercase={true}
          />
        </div>
        <div className="md:col-span-1 col-span-3">
          <SelectionField
            name="sex"
            label="app.booking.sex"
            value={store.sex}
            onChange={onChange}
            options={SEX}
            translate={true}
          />
        </div>
        <div className="col-span-3 md:flex md:space-x-3">
          <SelectionField
            name="type_document"
            label="app.booking.type_document"
            value={store.type_document}
            onChange={onChange}
            required={true}
            options={TYPE_DOC}
            translate={true}
          />
          <TextField
            name="id_number"
            label="app.booking.id_number"
            value={store.id_number || ""}
            required={true}
            onChange={onChange}
            translate={true}
          />
        </div>
        <div className="col-span-3 md:flex md:space-x-3">
          <TextField
            name="mobile"
            label="app.booking.mobile"
            value={store.mobile || ""}
            required={true}
            onChange={onChange}
            translate={true}
          />
          <TextField
            name="email"
            label="app.booking.email"
            value={store.email || ""}
            required={true}
            onChange={onChange}
            translate={true}
          />
        </div>
        <div className="col-span-3 md:flex md:space-x-3">
          <TwComboBox
            name="country_code"
            label="app.booking.country"
            placeholder="País"
            onChange={onChangeCountry}
            options={countries}
            value={store.country_code || ""}
            required={true}
          />
          <TwComboBox
            name="department_code"
            label="app.booking.department"
            placeholder="Departamento"
            onChange={onChangeCountry}
            options={departments}
            readonly={true}
            required={true}
          />
          <TwComboBox
            name="city_code"
            placeholder="Ciudad"
            label="app.booking.city"
            onChange={onChangeCountry}
            options={city}
            required={true}

            // onChange={onChange}
          />
        </div>
        <div className="col-span-3">
          <TextField
            name="address"
            label="app.booking.address"
            value={store.address || ""}
            required={true}
            onChange={onChange}
            translate={true}
          />
        </div>
        <div className="py-5 gap-3 col-span-3">
          <StdButton
            key="add"
            color="bluePresik"
            disabled={false}
            loading={isLoading}
            onClick={acceptCreate}
            size="w-[200px]"
            content="app.booking.btn-form"
            style="mx-auto px-10 "
          />

          {/* <StdButton
            key="add"
            color="bluePresik"
            disabled={false}
            loading={isLoading}
            onClick={makeRequest}
            size="w-[200px]"
            content="Agregar pago"
            style="mx-auto px-10 "
          /> */}
        </div>
        {openModal && (
          <ModalMsg
            open={true}
            onClose={onCloseModal}
            buttons={["close"]}
            type="success"
            msg={openModal.msg}
          />
        )}
      </form>
    </Fragment>
  );
}

export default FormGuest;
