import React, { useState, useEffect, Fragment } from "react";
import StdButton from "components/StdButton";
import { FormattedMessage as FM } from "react-intl";
import { TrashIcon } from "@heroicons/react/20/solid";
// import {icon}

const CartBooking = ({
  selectClassification,
  onRemoveItem,
  onRemoveAllItem,
  handleForm,
  nights = 0,
  handlePaymentView,
}) => {
  const [totalPrice, setTotalPrice] = useState(0);
  const handleRemoveItem = (item) => {
    onRemoveItem(item);
  };

  const calculateTotalPrice = () => {
    const total = selectClassification.reduce((accumulator, item) => {
      return accumulator + item.sale_price_taxed * nights;
    }, 0);

    setTotalPrice(total);
  };

  useEffect(() => {
    calculateTotalPrice();
  }, [selectClassification]);

  return (
    <Fragment>
      <div className=" bg-gray-100   rounded-md shadow-md   overflow-hidden">
        <h2 className="px-5 mt-5 mb-4 font-semibold text-blue-presik uppercase text-lg">
          Tu Reserva
        </h2>
        <ul className="divide-y divide-gray-300 ">
          {selectClassification.map((item, index) => (
            <li key={index} className="bg-gray-200 p-3  relative">
              <div
                className="flex justify-between pr-12  items-start"
                title={item.product.name}
              >
                <h3 className="text-blue-presik font-semibold  w-[60%] line-clamp-2 text-[13px]">
                  {item.product.name}
                </h3>
                <div className="flex flex-col  text-left items-end text-[12px] w-[40%] ">
                  <span className="text-gray-600">
                    {item.sale_price_taxed.toLocaleString("es-ES") || "200.000"}{" "}
                    <span className="text-green-600 font-semibold">x</span>{" "}
                    <span className="font-bold text-black">noches</span>{" "}
                    {nights}
                  </span>
                  <span className="font-semibold text-blue-presik">
                    {(item.sale_price_taxed * nights).toLocaleString("es-ES")}{" "}
                  </span>
                </div>
                {/* Agrega aquí otros campos del objeto */}
              </div>
              <TrashIcon
                className="w-6 p-1.5 bg-red-300 rounded-full absolute right-3 top-5 text-red-800 cursor-pointer hover:bg-blue-presik hover:text-white transition duration-300  transform"
                onClick={() => handleRemoveItem(item)}
              />
            </li>
          ))}
        </ul>

        <div className=" bg-blue-presik ">
          <div className="text-white  space-x-2  px-5 py-6 text-right">
            <span className="font-medium text-lg">Total:</span>{" "}
            <span className="text-yellow-500 font-bold">
              ${totalPrice.toLocaleString("es-ES")}
            </span>
          </div>
          <div className="">
            <StdButton
              style={"relative !rounded-none"}
              content="CONFIRMAR"
              size="w-full"
              color="amber"
              onClick={() => {
                handleForm(true);
              }}
            />
            {/* <StdButton
              style={"relative !rounded-none"}
              content="REALIZAR PAGO"
              size="w-full"
              color="amber"
              onClick={() => {
                handlePaymentView();
              }}
            /> */}
            {/* <TrashIcon
              className="w-14 h-12 p-3 bg-red-300 rounded-full  right-1.5 top-2 text-red-800 cursor-pointer hover:bg-yellow-500 transition-all duration-300 transform hover:text-blue-presik"
              onClick={() => onRemoveAllItem()}
            /> */}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default CartBooking;
