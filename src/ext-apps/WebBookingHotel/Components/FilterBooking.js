import React from "react";
import DateField from "components/DateField";
import IntegerField from "components/IntegerField";
import { FormattedMessage as FM } from "react-intl";
import { classNames } from "tools/ui";

const FilterBooking = ({ onChange, nights, mode = "aside", dateUrl }) => {
  let BASE_STYLE = "space-y-5 rounded-md shadow-md p-10";

  if (mode == "fullWidth") {
    BASE_STYLE =
      "flex flex-col md:flex-row item-center align-center md:space-x-4 max-w-[900px] mx-auto -bottom-4 md:-bottom-14 relative pt-3 pb-5 px-10 md:rounded-md ";
  }

  return (
    <div
      className={classNames(
        BASE_STYLE,
        "bg-gray-100  align-middle items-center shadow-sm",
      )}
    >
      {
        mode !== "fullWidth" ? (
          <h1 className="text-gray-500 text-sm flex">
            {/* Contenido si la condición es verdadera */}
            {/* Selecciona la fecha de tu estancia */}
            <FM id="web.booking.title_filter" />
          </h1>
        ) : null // Otra opción es usar un fragmento vacío: <></>
      }
      <DateField
        onChange={onChange}
        name="arrival_date"
        label="hotel.booking.add_room.arrival_date"
        value={dateUrl?.arrival_date}
      />
      <DateField
        onChange={onChange}
        name="departure_date"
        label="hotel.booking.add_room.departure_date"
        value={dateUrl?.departure_date}
      />
      <div className="md:w-[50%] flex">
        <IntegerField
          onChange={onChange}
          name="nights_quantity"
          label="hotel.booking.add_rooms.nights_quantity"
          readOnly={true}
          value={nights}
        />
      </div>

      {/* <span>dasdsa</span> */}
    </div>
  );
};

export default FilterBooking;
