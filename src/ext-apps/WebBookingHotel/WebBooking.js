import React, { Fragment, useEffect, useState } from "react";
import date from "date-and-time";
import { useParams } from "react-router-dom";

import proxy from "api/proxy";
import dates from "tools/dates";
import funcs from "tools/functions";
import { useFormStore } from "store/formStore";
import imgDefault from "../../assets/img/default-image.jpg";
import PureModal from "components/Modals/PureModal";
import ModalRooms from "./ModalRooms";
import SectionNoData from "components/Tools/SectionNoData";
import CartBooking from "./Components/CartBooking";
import ItemClassification from "./Components/ItemClassification";
import Header from "./Components/layout/Header";
import Footer from "./Components/layout/Footer";
import FilterBooking from "./Components/FilterBooking";
import Banner from "./Components/Banner";
import Loading from "components/Loading";
import FormCustomer from "./Components/FormCustomer";

const WebBooking = () => {
  const { db } = useParams();

  const querystring = window.location.search;
  const params = new URLSearchParams(querystring);
  const dateStart = params.get("dateStart");
  const dateEnd = params.get("dateEnd");
  const dateUrl = { arrival_date: dateStart, departure_date: dateEnd };
  let [nights, setNights] = useState(0);
  let [wizStore, setWizStore] = useState();
  let [dataCompany, setDataCompany] = useState(null);
  let [records, setRecords] = useState(null);
  let [ratePlan, setRatePlan] = useState(null);
  let [dataRooms, setDataRooms] = useState([]);
  let [open, setOpen] = useState(false);
  let [view, setView] = useState("selectRoom");
  const [selectClassification, setSelectClassification] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const onChange = (name, value) => {
    let _wizStore = { ...wizStore };
    _wizStore[name] = value;
    setWizStore({ ..._wizStore });
    let _nights;
    if (_wizStore.arrival_date && _wizStore.departure_date) {
      _nights = getNights(_wizStore);

      fillCategories(_wizStore);
    } else {
      _nights = 0;
    }
    setNights(_nights);
  };

  const fillCategories = async (store) => {
    const args = {};
    const dom = [["kind", "=", "web"]];
    const fields = ["id"];
    const { data: dataRate, error } = await proxy.search(
      "hotel.rate_plan",
      dom,
      fields,
      1,
    );

    const { data } = await proxy.methodCall({
      model: "hotel.room",
      method: "available_by_classification",
      args: [store.arrival_date, store.departure_date, dataRate[0].id],
      kwargs: {},
    });

    setDataRooms(data);
    const mapRecs = new Map();
    data.forEach((cat) => {
      cat.id = cat.product.id;
      cat.adults = 2;
      cat.children = 0;
      cat.images = cat.product.images.length == 0 ? cat.product.images : false;
      cat.sale_price_taxed_default = cat.sale_price_taxed;
      mapRecs.set(cat.product.id, cat);
    });

    setRecords(mapRecs);
    if (data.length > 0) {
      const cat = data[0];
      console.log("cat", mapRecs);
      setRatePlan(cat.rate_plan);
    }
  };

  const onClose = () => {
    console.log("cerrando");
  };

  const getNights = (wizStore) => {
    const arrival_date = dates.getTrytonDate2Js(wizStore.arrival_date);
    const departure_date = dates.getTrytonDate2Js(wizStore.departure_date);
    const res = date.subtract(departure_date, arrival_date).toDays();
    return res;
  };

  const handleSelectClassification = (item) => {
    let _item = { ...item };
    setSelectClassification([...selectClassification, _item]);
  };

  const handleRemoveItem = (itemToRemove) => {
    const updatedClassification = selectClassification.filter(
      (item) => item !== itemToRemove,
    );

    setSelectClassification(updatedClassification);
  };

  const handleRemoveAllItem = () => {
    setSelectClassification([]);
    setDataRooms([]);
  };

  const handleForm = (value) => {
    setOpen(value);
  };
  const handlePaymentView = () => {
    setView("formCustomer");
  };

  const dataInfoCompany = async () => {
    const { data } = await proxy.search(
      "company.company",
      [["id", "=", "1"]],
      [
        "id",
        "party.name",
        "logo",
        "logo_link",
        "openpay_merchant_id",
        "openpay_public_key",
        "openpay_url",
      ],
    );
    setDataCompany(data[0]);
  };

  useEffect(() => {
    if (dateStart && dateEnd) {
      fillCategories(dateUrl);
      const nights = getNights(dateUrl);
      setNights(nights);
    }

    dataInfoCompany();
    setIsLoading(false);
  }, [dateStart, dateEnd]);

  return (
    <Fragment>
      <Header data={dataCompany} />
      <Banner>
        <FilterBooking
          onChange={onChange}
          nights={nights}
          mode="fullWidth"
          dateUrl={dateUrl}
        />
      </Banner>
      <div className="flex md:flex-row flex-col-reverse py-10  items-start md:min-h-screen relative overflow-hidden overflow-x-hidden container mx-auto md:mt-20">
        <div className="md:w-[25%] w-full sticky top-0 space-y-5">
          {/* end filter room */}
          <CartBooking
            selectClassification={selectClassification}
            onRemoveItem={handleRemoveItem}
            onRemoveAllItem={handleRemoveAllItem}
            handleForm={handleForm}
            nights={nights}
            handlePaymentView={handlePaymentView}
          />
        </div>

        <div className="md:px-10 md:w-[75%] w-full">
          {dataRooms.length == 0 && (
            <SectionNoData text="Selecciona Check-In y Check-Out" />
          )}

          {/* {view == "selectRoom" && ( */}
          <ItemClassification
            dataRooms={dataRooms}
            handleSelectClassification={handleSelectClassification}
            handlePaymentView={handlePaymentView}
            imgDefault={imgDefault}
          />
          {/* )} */}

          {view == "formCustomer" && (
            <FormCustomer
              classification={selectClassification}
              wizStore={wizStore}
              // changeStep={changeStep}
              // addInvoice={addInvoice}
              reset={handleRemoveAllItem}
              dataApi={dataCompany}
            />
          )}

          {isLoading && <Loading />}
        </div>
      </div>
      <Footer />
      <PureModal open={open} onClose={() => setOpen(false)} backdrop={true}>
        <ModalRooms
          data={selectClassification}
          reset={handleRemoveAllItem}
          wizStore={wizStore}
          onClose={() => setOpen(false)}
        />
      </PureModal>
    </Fragment>
  );
};

// <Footer />
export default WebBooking;
