import React, { useState } from "react";
import { PlusCircleIcon } from "@heroicons/react/20/solid";
import StepModal from "./Components/StepModal";
import FormCustomer from "./Components/FormCustomer";
import IntegerField from "components/IntegerField";
import Invoice from "./Components/Invoice";
import { XMarkIcon } from "@heroicons/react/24/solid";

const ModalRooms = ({
  data,
  imgDefault,
  wizStore,
  Amenities,
  onClose,
  reset,
}) => {
  let [step, setStep] = useState("step-1");
  let [room, setRoom] = useState(null);
  let [invoice, setInvoice] = useState(null);
  const selectRoom = (room) => {
    setRoom(room);
    changeStep("step-2");
  };

  const changeStep = (value) => {
    if (value) setStep(value);
  };

  const addInvoice = (value) => {
    setInvoice(value);
  };
  return (
    <div className={step == "step-2" ? "" : "md:p-20 p-5"}>
      <XMarkIcon
        className="w-10 absolute md:right-10 md:top-10 -right-3 -top-3 bg-blue-presik  text-white rounded-full p-2 cursor-pointer"
        onClick={onClose}
      />
      <div className="space-y-8">
        {/* step 1 */}

        {/* step dos  */}
        {step == "step-1" && step && (
          <FormCustomer
            room={room}
            classification={data}
            wizStore={wizStore}
            changeStep={changeStep}
            addInvoice={addInvoice}
            reset={reset}
          />
        )}

        {/* step tres  */}
        {step == "step-2" && step && <Invoice data={invoice} />}
      </div>
    </div>
  );
};

export default ModalRooms;
