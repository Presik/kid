const TYPES_OEXT = {
  odt: "application/vnd.oasis.opendocument.text",
  ods: "application/vnd.oasis.opendocument.spreadsheet",
  pdf: "application/pdf",
};

export { TYPES_OEXT };
